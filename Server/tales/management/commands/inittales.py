from django.core.management.base import BaseCommand
from tales.models import TaleText
from django.conf import settings
import json
import os

# Корневая папка
base = str(settings.BASE_DIR)


class Command(BaseCommand):

    def handle(self, *args, **options):
        TaleText.objects.all().delete()
        path1 = base + '/tales/text_list/tale1.json'
        with open(path1, 'r', encoding="utf-8") as file1:
            tale_text1 = json.load(file1)
        for text in tale_text1:
            tale_text = TaleText(**text)
            tale_text.save()
