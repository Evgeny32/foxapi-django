from rest_framework import serializers
from account.models import Account
from child.models import ChildProfile
from child.serializers import ChildrenSerializer
from account.helpers import get_random_number, get_random_string
import requests
from . import models


class TaleTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TaleText
        exclude = ['id', 'tale']


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Answer
        exclude = ['task', ]


class TaskSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True)

    class Meta:
        model = models.Task
        exclude = ['tale', ]


class TaskSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Task
        exclude = ['tale', ]


class TaleSerializer(serializers.ModelSerializer):
    tasks = TaskSerializer(many=True)
    text = TaleTextSerializer(many=True)

    class Meta:
        model = models.Tale
        fields = "__all__"

    def save(self):
        tale = {
            "name": self.validated_data['name'],
            "number": self.validated_data['number'],
            "pages": self.validated_data['pages'],
            "emoji": self.validated_data['emoji'],
            "child": self.validated_data['child'],
        }
        tale = models.Tale.objects.create(**tale)
        tasks = self.validated_data['tasks']
        for task in tasks:
            create_task = task.copy()
            answers = create_task.pop('answers')
            # del create_task['answers']
            create_task['tale'] = tale
            created_task = models.Task.objects.create(**create_task)
            for answer in answers:
                answer['task'] = created_task
                models.Answer.objects.create(**answer)
        return 'success'


class TaleSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tale
        fields = ['id', 'name', 'number', 'child', 'pages',
                  'emoji', 'success', 'rank', 'end_date']
