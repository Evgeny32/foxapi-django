# from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from . import models
from child.models import ChildProfile
from child.serializers import ChildrenSerializer
from rest_framework.response import Response
from rest_framework import status
from . import serializers
from django.conf import settings
import json
import os
from .tales_list import tales_list


class ListTales(APIView):
    """
    Сказки ребенка
    """
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        """
        Возвращает список сказок ребенка
        """
        child = ChildProfile.objects.get(child=request.user.id)
        tales = models.Tale.objects.filter(child=child.id)
        serializer = serializers.TaleSimpleSerializer(tales, many=True)
        return Response(serializer.data)


class CreateTale(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, tale_number, child_id):
        """
        create clear tale
        """
        tale = tales_list['tale'+str(tale_number)]
        tale['child'] = child_id
        serializer = serializers.TaleSerializer(data=tale)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)


class Tale(APIView):
    """
    One tale
    """
    permission_classes = [IsAuthenticated]

    def get_tale_by_id(self, id):
        try:
            tale = models.Tale.objects.get(id=id)
        except models.Tale.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return tale

    def get(self, request, id, format=None):
        """
        get one tale
        """

        tale = self.get_tale_by_id(id)
        tale.text = models.TaleText.objects.filter(
            tale=tale.number).order_by('order')
        tasks = models.Task.objects.filter(tale=tale.id)
        for task in tasks:
            answers = models.Answer.objects.filter(task=task.id)
            task.answers = answers
        tale.tasks = tasks
        serializer = serializers.TaleSerializer(tale)
        return Response(serializer.data)

    def post(self, request, id, format=None):
        """
        create clear tale
        """
        path = '/tales/tale_list/tale'+str(id)+'.json'
        base = str(settings.BASE_DIR)
        try:
            with open(base+path) as f:
                tale = json.load(f)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
        tale['child'] = 1  # !TODO: Make reactive child id
        serializer = serializers.TaleSerializer(data=tale)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)

    def delete(self, request, id, format=None):
        tale = self.get_tale_by_id(id)
        tale.delete()
        return Response({'msg': "Deleted"}, status=200)

    def patch(self, request, id, format=None):
        """
        update tale
        """
        tale = self.get_tale_by_id(id)
        serializer = serializers.TaleSimpleSerializer(
            tale, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': "Updated"}, status=200)
        return Response(serializer.errors, status=400)


class Task(APIView):
    """
    task view
    """
    permission_classes = [IsAuthenticated]

    def get_task_by_id(self, id):
        try:
            task = models.Task.objects.get(id=id)
        except models.Task.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return task

    def get(self, request, id):
        task = self.get_task_by_id(id)
        task.answers = models.Answer.objects.filter(task=task.id)
        serializer = serializers.TaskSerializer(
            task
        )
        return Response(serializer.data)

    def patch(self, request, id, format=None):
        """
        update task
        """
        task = self.get_task_by_id(id)
        serializer = serializers.TaskSimpleSerializer(
            task, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'Updated'}, status=200)
        return Response(serializer.errors, status=400)


class Answer(APIView):
    """
    answer view
    """
    permission_classes = [IsAuthenticated]

    def get_answer_by_id(self, id):
        try:
            answer = models.Answer.objects.get(id=id)
        except models.Answer.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return answer

    def patch(self, request, id, format=None):
        """
        update answer
        """
        answer = self.get_answer_by_id(id)
        serializer = serializers.AnswerSerializer(
            answer, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'Updated'}, status=200)
        return Response(serializer.errors, status=400)

    def get(self, request, id):
        answer = self.get_answer_by_id(id)
        serializer = serialziers.AnswerSerializer(
            answer
        )
        return Response(serializer.data)
