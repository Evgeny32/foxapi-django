from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils import timezone
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
# Create your models here.


class Tale(models.Model):
    name = models.CharField(max_length=200, verbose_name="Название",
                            default="Как лисенок узнал, что такое домашние дела")
    child = models.ForeignKey("child.ChildProfile",
                              verbose_name="Ребенок", on_delete=models.SET_NULL, null=True, blank=True)
    pages = models.PositiveIntegerField(
        default=0, verbose_name='Страница сказки')
    number = models.PositiveIntegerField(
        verbose_name="Номер сказки", default=1)
    EMOJIS_CHOICES = [
        ('calm', 'спокойствие'),
        ('happy', 'радость'),
        ('confuse', 'смущение'),
        ('grudge', 'обида'),
        ('anger', 'злость'),
        ('fury', 'ярость'),
        ('fear', 'испуг'),
        ('sad', 'грусть'),
        ('surprise', 'удивление'),
    ]
    emoji = models.CharField(
        max_length=20, verbose_name="Эмоция", choices=EMOJIS_CHOICES, null=True,  blank=True)
    success = models.BooleanField(
        verbose_name="Сказка завершена", default=False)
    RANK = (
        ('Silver', 'Silver'),
        ('Gold', 'Gold'),
        ('Platinum', 'Platinum'),
    )
    rank = models.CharField(max_length=10, choices=RANK,
                            default='Silver', verbose_name="Ранг", null=True, blank=True)

    end_date = models.DateField(null=True, blank=True)

    def unicode(self):
        return self.name


class Task(models.Model):
    tale = models.ForeignKey("tales.Tale",
                             verbose_name="Сказка", on_delete=models.CASCADE)
    name = models.CharField(
        max_length=200, verbose_name="Название", default="Задание")
    reject = models.BooleanField(_("Отказано"), default=False)
    success = models.BooleanField(_("Успешно"), default=False)
    target = models.PositiveIntegerField(verbose_name=("Цель задания"))
    coins = models.PositiveIntegerField(verbose_name=("Монетки"))
    page = models.PositiveIntegerField(verbose_name=("Страница"))
    img = models.CharField(verbose_name="Изображение", max_length=50)

    def unicode(self):
        return self.name + ' | ' + self.tale.name


class Answer(models.Model):
    task = models.ForeignKey("tales.Task", verbose_name=(
        "Задание"), on_delete=models.CASCADE)
    name = models.CharField(verbose_name="Название",
                            max_length=200, default="Название")
    reject = models.BooleanField(_("Отказано"), default=False)
    success = models.BooleanField(_("Успешно"), default=False)
    answer = models.FileField(verbose_name=(
        "Ответ"), upload_to="answers", null=True, blank=True)
    text_answer = models.CharField(
        verbose_name=("Текстовый ответ"), max_length=200, null=True, blank=True)


class TaleText(models.Model):
    tale = models.IntegerField(_("Номер Сказки"))
    sound = models.CharField(_("Озвучка"), max_length=254, blank=True)
    order = models.IntegerField(_("Порядок"), default=0)
    text = models.TextField(_('Текст'))


class Tale1(models.Model):
    name = models.CharField(max_length=200, verbose_name="Название",
                            default="Как лисенок узнал, что такое домашние дела")
    child = models.ForeignKey(
        "child.ChildProfile", verbose_name="Ребенок", on_delete=models.SET_NULL, null=True)
    pages = models.CharField(max_length=20,
                             default="1", verbose_name='Страница сказки')
    number = models.PositiveIntegerField(
        verbose_name="Номер сказки")
    EMOJIS_CHOICES = [
        ('calm', 'спокойствие'),
        ('happy', 'радость'),
        ('confuse', 'смущение'),
        ('grudge', 'обида'),
        ('anger', 'злость'),
        ('fury', 'ярость'),
        ('fear', 'испуг'),
        ('sad', 'грусть'),
        ('surprise', 'удивление'),
    ]
    emoji = models.CharField(
        max_length=20, verbose_name="Эмоция", choices=EMOJIS_CHOICES, null=True,  blank=True)
    tale_pages = models.PositiveIntegerField(
        default=0, verbose_name='Страница сказки внутри')
    date = models.DateField(
        verbose_name="Дата прохождения", auto_now=True)

    RANK = (
        ('Silver', 'Silver'),
        ('Gold', 'Gold'),
        ('Platinum', 'Platinum'),
    )

    rank = models.CharField(max_length=10, choices=RANK,
                            default='Silver', verbose_name="Ранг", null=True, blank=True)

# !FOX STORY

    def fox_story_answer1_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/fox-story/answer1{2}'.format(instance.id, instance.child.id, ext)
    fox_story_answer1 = models.FileField(
        upload_to=fox_story_answer1_upload_path, verbose_name="Ответ ребенка на вопрос 1", null=True, blank=True)

    def fox_story_answer2_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/fox-story/answer2{2}'.format(instance.id, instance.child.id, ext)
    fox_story_answer2 = models.FileField(
        upload_to=fox_story_answer2_upload_path, verbose_name="Ответ ребенка на вопрос 2", null=True, blank=True)

    def fox_story_answer3_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/fox-story/answer3{2}'.format(instance.id, instance.child.id, ext)
    fox_story_answer3 = models.FileField(
        upload_to=fox_story_answer3_upload_path, verbose_name="Ответ ребенка на вопрос 3", null=True, blank=True)

    def fox_story_answer4_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/fox-story/answer4{2}'.format(instance.id, instance.child.id, ext)
    fox_story_answer4 = models.FileField(
        upload_to=fox_story_answer4_upload_path, verbose_name="Ответ ребенка на вопрос 4", null=True, blank=True)

    def fox_story_answer5_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/fox-story/answer5{2}'.format(instance.id, instance.child.id, ext)
    fox_story_answer5 = models.FileField(
        upload_to=fox_story_answer5_upload_path, verbose_name="Ответ ребенка на вопрос 5", null=True, blank=True)
    fox_story_accept = models.BooleanField(
        default=False, verbose_name="Fox Story is completed")
    fox_story_coins = models.PositiveIntegerField(
        default=0, verbose_name="coins for Fox Story")


# !FAMILY INTERVIEW


    def family_interview_answer1_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/family_interview/answer1{2}'.format(instance.id, instance.child.id, ext)
    family_interview_answer1 = models.FileField(
        upload_to=family_interview_answer1_upload_path, verbose_name="Ответ ребенка на вопрос 1", null=True, blank=True)

    def family_interview_answer2_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/family_interview/answer2{2}'.format(instance.id, instance.child.id, ext)
    family_interview_answer2 = models.FileField(
        upload_to=family_interview_answer2_upload_path, verbose_name="Ответ ребенка на вопрос 2", null=True, blank=True)

    def family_interview_answer3_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/family_interview/answer3{2}'.format(instance.id, instance.child.id, ext)
    family_interview_answer3 = models.FileField(
        upload_to=family_interview_answer3_upload_path, verbose_name="Ответ ребенка на вопрос 1", null=True, blank=True)

    def family_interview_answer4_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/family_interview/answer4{2}'.format(instance.id, instance.child.id, ext)
    family_interview_answer4 = models.FileField(
        upload_to=family_interview_answer4_upload_path, verbose_name="Ответ ребенка на вопрос 2", null=True, blank=True)

    def family_interview_answer5_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/family_interview/answer5{2}'.format(instance.id, instance.child.id, ext)
    family_interview_answer5 = models.FileField(
        upload_to=family_interview_answer5_upload_path, verbose_name="Ответ ребенка на вопрос 1", null=True, blank=True)

    def family_interview_answer6_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/family_interview/answer6{2}'.format(instance.id, instance.child.id, ext)
    family_interview_answer6 = models.FileField(
        upload_to=family_interview_answer6_upload_path, verbose_name="Ответ ребенка на вопрос 2", null=True, blank=True)

    family_interview_accept = models.BooleanField(
        default=False, verbose_name="Family Interview is completed")
    family_interview_coins = models.PositiveIntegerField(
        default=0, verbose_name="Coins for Family Interview")


# ! CREATIVE PROJECT


    def creative_project_answer1_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/creative_project/answer1{2}'.format(instance.id, instance.child.id, ext)
    creative_project_answer1 = models.FileField(
        upload_to=creative_project_answer1_upload_path, verbose_name="Ответ ребенка на вопрос 1", null=True, blank=True)

    def creative_project_answer2_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/creative_project/answer2{2}'.format(instance.id, instance.child.id, ext)
    creative_project_answer2 = models.FileField(
        upload_to=creative_project_answer2_upload_path, verbose_name="Ответ ребенка на вопрос 2", null=True, blank=True)

    def creative_project_answer3_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/creative_project/answer3{2}'.format(instance.id, instance.child.id, ext)
    creative_project_answer3 = models.FileField(
        upload_to=creative_project_answer3_upload_path, verbose_name="Ответ ребенка на вопрос 3", null=True, blank=True)

    def creative_project_answer4_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/creative_project/answer4{2}'.format(instance.id, instance.child.id, ext)
    creative_project_answer4 = models.FileField(
        upload_to=creative_project_answer4_upload_path, verbose_name="Ответ ребенка на вопрос 4", null=True, blank=True)

    creative_project_accept = models.BooleanField(
        default=False, verbose_name="Creative_Project is completed")
    creative_project_coins = models.PositiveIntegerField(
        default=0, verbose_name="Coins for Creative Project")


# ! COMMUNICATION CLUB

    communication_club_quest1_day = models.CharField(
        null=True, blank=True, max_length=20)
    communication_club_quest1_hour = models.CharField(
        null=True, blank=True, max_length=20)

    def communication_club_report1_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/communication_club/report1{2}'.format(instance.id, instance.child.id, ext)
    communication_club_quest1_report = models.FileField(
        upload_to=communication_club_report1_upload_path, verbose_name="Ответ ребенка на вопрос 1", null=True, blank=True)

    communication_club_quest2_day = models.CharField(
        null=True, blank=True, max_length=20)
    communication_club_quest2_hour = models.CharField(
        null=True, blank=True, max_length=20)

    def communication_club_report2_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/communication_club/report2{2}'.format(instance.id, instance.child.id, ext)
    communication_club_quest2_report = models.FileField(
        upload_to=communication_club_report2_upload_path, verbose_name="Ответ ребенка на вопрос 2", null=True, blank=True)

    communication_club_quest3_day = models.CharField(
        null=True, blank=True, max_length=20)
    communication_club_quest3_hour = models.CharField(
        null=True, blank=True, max_length=20)

    def communication_club_report3_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/communication_club/report3{2}'.format(instance.id, instance.child.id, ext)
    communication_club_quest3_report = models.FileField(
        upload_to=communication_club_report3_upload_path, verbose_name="Ответ ребенка на вопрос 3", null=True, blank=True)

    communication_club_quest4_day = models.CharField(
        null=True, blank=True, max_length=20)
    communication_club_quest4_hour = models.CharField(
        null=True, blank=True, max_length=20)

    def communication_club_report4_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/communication_club/report4{2}'.format(instance.id, instance.child.id, ext)
    communication_club_quest4_report = models.FileField(
        upload_to=communication_club_report4_upload_path, verbose_name="Ответ ребенка на вопрос 4", null=True, blank=True)

    communication_club_quest5_day = models.CharField(
        null=True, blank=True, max_length=20)
    communication_club_quest5_hour = models.CharField(
        null=True, blank=True, max_length=20)

    def communication_club_report5_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/communication_club/report5{2}'.format(instance.id, instance.child.id, ext)
    communication_club_quest5_report = models.FileField(
        upload_to=communication_club_report5_upload_path, verbose_name="Ответ ребенка на вопрос 5", null=True, blank=True)

    communication_club_accept = models.BooleanField(
        default=False, verbose_name="Communication Club is completed")
    communication_club_coins = models.PositiveIntegerField(
        default=0, verbose_name="Coins for Communication Club")

# ! LOGIC CLUB

    logic_club_accept = models.BooleanField(
        default=False, verbose_name="Logic Club is completed")
    logic_club_coins = models.PositiveIntegerField(
        default=0, verbose_name="coins for Logic Club")


# ! FOX BURSE

    fox_burse_answers = models.CharField(
        null=True, blank=True, max_length=5, verbose_name="Fox Биржа ответы")

    fox_burse_accept = models.BooleanField(
        default=False, verbose_name="Fox Биржа is completed")
    fox_burse_coins = models.PositiveIntegerField(
        default=0, verbose_name="Coins for Fox Биржа")


# ! BUSINESS VOCABULARY TASK


    def business_vocabulary_answer1_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/business_vocabulary/answer1{2}'.format(instance.id, instance.child.id, ext)
    business_vocabulary_answer1 = models.FileField(
        upload_to=business_vocabulary_answer1_upload_path, verbose_name="Ответ ребенка на вопрос 1", null=True, blank=True)

    def business_vocabulary_answer2_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/business_vocabulary/answer2{2}'.format(instance.id, instance.child.id, ext)
    business_vocabulary_answer2 = models.FileField(
        upload_to=business_vocabulary_answer2_upload_path, verbose_name="Ответ ребенка на вопрос 2", null=True, blank=True)

    def business_vocabulary_answer3_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/business_vocabulary/answer3{2}'.format(instance.id, instance.child.id, ext)
    business_vocabulary_answer3 = models.FileField(
        upload_to=business_vocabulary_answer3_upload_path, verbose_name="Ответ ребенка на вопрос 3", null=True, blank=True)

    def business_vocabulary_answer4_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/business_vocabulary/answer4{2}'.format(instance.id, instance.child.id, ext)
    business_vocabulary_answer4 = models.FileField(
        upload_to=business_vocabulary_answer4_upload_path, verbose_name="Ответ ребенка на вопрос 4", null=True, blank=True)

    def business_vocabulary_answer5_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/business_vocabulary/answer5{2}'.format(instance.id, instance.child.id, ext)
    business_vocabulary_answer5 = models.FileField(
        upload_to=business_vocabulary_answer5_upload_path, verbose_name="Ответ ребенка на вопрос 5", null=True, blank=True)

    business_vocabulary_accept = models.BooleanField(
        default=False, verbose_name="Business Vocabulary is completed")
    business_vocabulary_coins = models.PositiveIntegerField(
        default=0, verbose_name="Coins for Business Vocabulary")


# ! PRESENTATION GUIDE

    presentation_guide_profession_id = models.IntegerField(
        default=0, verbose_name="ID профессии")

    def presentation_guide_answer_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/presentation_guide/answer{2}'.format(instance.id, instance.child.id, ext)
    presentation_guide_answer = models.FileField(
        upload_to=presentation_guide_answer_upload_path, verbose_name="Ответ ребенка на вопрос", null=True, blank=True)

    presentation_guide_text = models.CharField(
        null=True, blank=True, max_length=40, verbose_name="Presentation guide ответы")

    presentation_guide_accept = models.BooleanField(
        default=False, verbose_name="Presentation Guide is completed")
    presentation_guide_coins = models.PositiveIntegerField(
        default=0, verbose_name="Coins for Presentation Guide")


# ! TEAMWORK

    teamwork_accept = models.BooleanField(
        default=False, verbose_name="Teamwork is completed")
    teamwork_coins = models.PositiveIntegerField(
        default=0, verbose_name="Coins for Teamwork")


# ! RELAX


    def relax_answer_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/relax/answer{2}'.format(instance.id, instance.child.id, ext)
    relax_answer = models.FileField(
        upload_to=relax_answer_upload_path, verbose_name="Ответ ребенка на вопрос", null=True, blank=True)

    relax_text = models.CharField(
        null=True, blank=True, max_length=40, verbose_name="Relax ответы")

    relax_accept = models.BooleanField(
        default=False, verbose_name="Relax is completed")
    relax_coins = models.PositiveIntegerField(
        default=0, verbose_name="Coins for Relax")


# ! CARRER GUIDANCE

    career_guidance_answers = models.CharField(
        null=True, blank=True, max_length=25)
    career_guidance_answerId = models.CharField(
        null=True, blank=True, max_length=55)

    career_guidance_accept = models.BooleanField(
        default=False, verbose_name="Career Guidance is completed")
    career_guidance_coins = models.PositiveIntegerField(
        default=0, verbose_name="Coins for Career Guidance")


# ! CREATIVE CLUB

    creative_club_accept = models.BooleanField(
        default=False, verbose_name="Creative Club is completed")
    creative_club_coins = models.PositiveIntegerField(
        default=0, verbose_name="Coins for Creative Club")


# ! SOFT SKILLS

    soft_skills_role_id = models.IntegerField(
        default=0, verbose_name="ID роли")
    soft_skills_member_id = models.IntegerField(
        default=0, verbose_name="ID члена семьи")

    def soft_skills_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/soft_skills/answer{2}'.format(instance.id, instance.child.id, ext)
    soft_skills_answer = models.FileField(
        upload_to=soft_skills_upload_path, verbose_name="Ответ ребенка на вопрос", null=True, blank=True)

    soft_skills_text = models.CharField(
        null=True, blank=True, max_length=40, verbose_name="Soft skills ответы")

    soft_skills_accept = models.BooleanField(
        default=False, verbose_name="Soft Skills is completed")
    soft_skills_coins = models.PositiveIntegerField(
        default=0, verbose_name="Coins for Soft Skills")


# ! LETS TALK


    def lets_talk_answer1_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/lets_talk/answer1{2}'.format(instance.id, instance.child.id, ext)
    lets_talk_answer1 = models.FileField(
        upload_to=lets_talk_answer1_upload_path, verbose_name="Ответ ребенка на вопрос", null=True, blank=True)

    def lets_talk_answer2_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/lets_talk/answer2{2}'.format(instance.id, instance.child.id, ext)
    lets_talk_answer2 = models.FileField(
        upload_to=lets_talk_answer2_upload_path, verbose_name="Ответ ребенка на вопрос", null=True, blank=True)

    def lets_talk_answer3_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/lets_talk/answer3{2}'.format(instance.id, instance.child.id, ext)
    lets_talk_answer3 = models.FileField(
        upload_to=lets_talk_answer3_upload_path, verbose_name="Ответ ребенка на вопрос", null=True, blank=True)

    def lets_talk_answer4_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return 'tale{0}/child{1}/lets_talk/answer4{2}'.format(instance.id, instance.child.id, ext)
    lets_talk_answer4 = models.FileField(
        upload_to=lets_talk_answer4_upload_path, verbose_name="Ответ ребенка на вопрос", null=True, blank=True)

    lets_talk_accept = models.BooleanField(
        default=False, verbose_name="Lets Talk is completed")
    lets_talk_coins = models.PositiveIntegerField(
        default=0, verbose_name="Coins for Lets Talk")

    coins_get = models.BooleanField(
        default=False, verbose_name="coins_get")

    def unicode(self):
        return self.name

    def tales_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        field1 = User._meta.get_field().split('_')[-1]
        field2 = User._meta.get_field().split(
            '_')[0] + User._meta.get_field().split('_')[1]
        return 'tale{0}/child{1}/field2/field1{2}'.format(instance.id, instance.child.id, ext)
    fox_story_answer1 = models.FileField(
        upload_to=tales_upload_path, verbose_name="Ответ ребенка на вопрос 1", null=True, blank=True)


@receiver(pre_save, sender=Tale1)
def delete_old_image(sender, instance=None, **kwargs):

    if instance.obj._meta.get_field:
        try:
            old_tale = Tale1.objects.get(pk=instance.pk).obj._meta.get_field
        except Tale1.DoesNotExist:
            return
        else:
            new_tale = instance.obj._meta.get_field

            if old_tale != new_tale:
                old_tale.delete(save=False)


@receiver(pre_save, sender=Tale1)
def delete_old_image(sender, instance=None, **kwargs):

    # !FOX STORY

    if instance.fox_story_answer1:
        try:
            old_tale = Tale1.objects.get(pk=instance.pk).fox_story_answer1
        except Tale1.DoesNotExist:
            return
        else:
            new_tale = instance.fox_story_answer1

            if old_tale != new_tale:
                old_tale.delete(save=False)

    if instance.fox_story_answer2:
        try:
            old_tale = Tale1.objects.get(pk=instance.pk).fox_story_answer2
        except Tale1.DoesNotExist:
            return
        else:
            new_tale = instance.fox_story_answer2

            if old_tale != new_tale:
                old_tale.delete(save=False)

    if instance.fox_story_answer3:
        try:
            old_tale = Tale1.objects.get(pk=instance.pk).fox_story_answer3
        except Tale1.DoesNotExist:
            return
        else:
            new_tale = instance.fox_story_answer3

    if instance.fox_story_answer4:
        try:
            old_tale = Tale1.objects.get(pk=instance.pk).fox_story_answer4
        except Tale1.DoesNotExist:
            return
        else:
            new_tale = instance.fox_story_answer4

            if old_tale != new_tale:
                old_tale.delete(save=False)

            if old_tale != new_tale:
                old_tale.delete(save=False)

    if instance.fox_story_answer5:
        try:
            old_tale = Tale1.objects.get(pk=instance.pk).fox_story_answer5
        except Tale1.DoesNotExist:
            return
        else:
            new_tale = instance.fox_story_answer5

            if old_tale != new_tale:
                old_tale.delete(save=False)
