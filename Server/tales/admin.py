from django.contrib import admin
from . import models
# Register your models here.


@admin.register(models.Answer)
class AnswerAdmin(admin.ModelAdmin):
    model = models.Answer


@admin.register(models.TaleText)
class TaleTextAdmin(admin.ModelAdmin):
    model = models.TaleText


class AnswerAdminInline(admin.TabularInline):
    model = models.Answer


@admin.register(models.Task)
class TaskAdmin(admin.ModelAdmin):
    model = models.Task
    inlines = [AnswerAdminInline]


class TaskAdminInline(admin.StackedInline):
    model = models.Task
    inlines = [AnswerAdminInline]


@admin.register(models.Tale)
class TaleAdmin(admin.ModelAdmin):
    model = models.Tale
    inlines = [TaskAdminInline]
