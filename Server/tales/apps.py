from django.apps import AppConfig


class TalesConfig(AppConfig):
    name = 'tales'
    verbose_name ="6) Сказки"
