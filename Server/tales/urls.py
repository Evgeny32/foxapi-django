from django.urls import path
from .views import (
    ListTales,
    Tale,
    Task,
    Answer,
    CreateTale
)


app_name = 'tales'

# ! TALES
urlpatterns = [
    path('', ListTales.as_view(), name="ListTales"),
    path('<int:id>/', Tale.as_view(), name="tale"),
    path('task/<int:id>/', Task.as_view(), name="task"),
    path('answer/<int:id>/', Answer.as_view(), name="answer"),
    path('create/<int:tale_number>/<int:child_id>/',
         CreateTale.as_view(), name="CreateTale")

]
