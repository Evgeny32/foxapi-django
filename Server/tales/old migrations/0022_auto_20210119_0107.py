# Generated by Django 3.1.4 on 2021-01-19 01:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tales', '0021_auto_20210119_0033'),
    ]

    operations = [
        migrations.AddField(
            model_name='tale1',
            name='task7_accept',
            field=models.BooleanField(default=False, verbose_name='task7 is completed'),
        ),
        migrations.AddField(
            model_name='tale1',
            name='task7_coins',
            field=models.PositiveIntegerField(default=0, verbose_name='coins for task 7'),
        ),
    ]
