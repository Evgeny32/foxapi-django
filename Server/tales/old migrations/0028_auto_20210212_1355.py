# Generated by Django 3.1.4 on 2021-02-12 13:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tales', '0027_auto_20210212_1354'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tale1',
            name='number',
            field=models.PositiveIntegerField(verbose_name='Номер сказки'),
        ),
    ]
