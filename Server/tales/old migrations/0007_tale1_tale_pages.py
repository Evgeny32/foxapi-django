# Generated by Django 3.1.4 on 2021-01-14 09:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tales', '0006_auto_20210114_0929'),
    ]

    operations = [
        migrations.AddField(
            model_name='tale1',
            name='tale_pages',
            field=models.PositiveIntegerField(default=1, verbose_name='Страница сказки внутри'),
        ),
    ]
