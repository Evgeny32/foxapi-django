# Generated by Django 3.1.4 on 2021-01-17 19:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tales', '0013_auto_20210117_1950'),
    ]

    operations = [
        migrations.AddField(
            model_name='tale1',
            name='task2_accept',
            field=models.PositiveIntegerField(default=0, verbose_name='task2 is completed'),
        ),
        migrations.AddField(
            model_name='tale1',
            name='task2_coins',
            field=models.PositiveIntegerField(default=0, verbose_name='coins for task 2'),
        ),
    ]
