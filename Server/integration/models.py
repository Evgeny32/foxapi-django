from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.


PARTNER_TYPE_CHOICES = (
    ('Юрлицо', 'Юрлицо'),
    ('Физлицо', 'Физлицо')
)

OWNER_TYPE_CHOICES = (
    ('Организация', 'Организация'),
    ('Контрагент', 'Контрагент')
)

CONTRACT_TYPE_CHOICES = (
    ('С покупателем', 'С покупателем'),
    ('С поставщиком', 'С поставщиком')
)

VAT_RATE_CHOICES = (
    ('20%', '20%'),
    ('10%', '10%'),
    ('0%', '0%'),
    ('Без НДС', 'Без НДС')
)


class Partner(models.Model):
    """ Партнер / Организация """
    Id = models.IntegerField(_("Уникальный Id"), primary_key=True)
    INN = models.CharField(_("ИНН"), max_length=12)
    KPP = models.CharField(_("КПП"), max_length=9, blank=True)
    Name = models.CharField(_("Наименование"), max_length=256)
    Type = models.CharField(_("Вид контрагента"), max_length=50,
                            choices=PARTNER_TYPE_CHOICES, default="Юрлицо")
    IsDeleted = models.BooleanField(_("Отметка об удалении"), default=False)
    IsOrganization = models.BooleanField(
        _("Является организацией"), default=False)

    class Meta:
        verbose_name = 'Партнер / Организация'
        verbose_name_plural = 'Партнеры / Организация'

    def __str__(self):
        return str(self.Id)


class BankAccount(models.Model):
    """ Банковский счет """
    Id = models.BigAutoField(_("Уникальный код"), primary_key=True)
    OwnerType = models.CharField(
        _("Тип владельца"), max_length=50, choices=OWNER_TYPE_CHOICES)
    OwnerId = models.ForeignKey("integration.Partner", verbose_name=_(
        "Id владельца"), on_delete=models.CASCADE)
    Account = models.CharField(_("Расчетный счет"), max_length=255)
    BIC = models.CharField(_("БИК"), max_length=255)
    Main = models.BooleanField(_("Признак основного счета"), default=False)
    IsDeleted = models.BooleanField(_("Отметка об удалении"), default=False)

    class Meta:
        verbose_name = 'Банковский счет'
        verbose_name_plural = 'Банковские счета'

    def __str__(self):
        return str(self.Id)


class Service(models.Model):
    """ Услуга / Номенклатура """
    Id = models.BigAutoField(_("Уникальный код"), primary_key=True)
    Name = models.CharField(_("Наименование"), max_length=256)
    IsDeleted = models.BooleanField(_("Отметка об удалении"), default=False)

    class Meta:
        verbose_name = 'Услуга (Номенклатура)'
        verbose_name_plural = 'Услуги (Номенклатуры)'

    def __str__(self):
        return str(self.Id)


class Contract(models.Model):
    """ Договор """
    Cont_Id = models.BigAutoField(_("Уникальный код"), primary_key=True)
    Cont_Org = models.ForeignKey("integration.Partner", related_name="Contract_Cont_Org", verbose_name=_(
        "ID организации"), on_delete=models.CASCADE)
    Cont_Owner = models.ForeignKey("integration.Partner", related_name="Contract_Cont_Owner", verbose_name=_(
        "ID Контрагента - Владельца"), on_delete=models.CASCADE)
    Cont_Type = models.CharField(
        _("Вид договора"), max_length=50, choices=CONTRACT_TYPE_CHOICES)
    Cont_Number = models.CharField(_("Номер договора"), max_length=255)
    Cont_Data = models.DateField(_("Дата договора"))
    Cont_Name = models.CharField(_("Наименование договора"), max_length=255)
    IsDeleted = models.BooleanField(_("Отметка об удалении"), default=False)

    class Meta:
        verbose_name = 'Договор'
        verbose_name_plural = 'Договоры'

    def __str__(self):
        return str(self.Cont_Id)


class InvoiceHeader(models.Model):
    """ Шапка счета на оплату """
    Doc_Id = models.BigAutoField(_("Уникальный код"), primary_key=True)
    doc_number = models.CharField(_("Номер документа"), max_length=11)
    doc_data = models.DateField(_("Дата документа"))
    Id_Org = models.ForeignKey("integration.Partner", related_name="InvoiceHeader_Id_Org", verbose_name=_(
        "Id Организации"), on_delete=models.CASCADE)
    Id_Partner = models.ForeignKey("integration.Partner", related_name="InvoiceHeader_Id_Partner", verbose_name=_(
        "Id Партнера"), on_delete=models.CASCADE)
    Account = models.CharField(_("Банковский счет организации"), max_length=50)
    Id_dog = models.ForeignKey("integration.Contract", verbose_name=_(
        "Id договора"), on_delete=models.CASCADE)
    IsDeleted = models.BooleanField(_("Отметка об удалении"), default=False)

    class Meta:
        verbose_name = 'Шапка счета на оплату'
        verbose_name_plural = 'Шапки счетов на оплату'

    def __str__(self):
        return str(self.Doc_Id)


class InvoiceLine(models.Model):
    """ Строка счета на оплату """
    Service_Id = models.ForeignKey("integration.Service", verbose_name=_(
        "Id номенклатуры"), on_delete=models.CASCADE)
    Quantity = models.FloatField(_("Количество"))
    Price = models.FloatField(_("Цена"))
    Cost = models.FloatField(_("Сумма без НДС"))
    VatRate = models.CharField(
        _("Ставка НДС"), max_length=50, choices=VAT_RATE_CHOICES)
    Vat = models.FloatField(_("Сумма НДС"))
    CostWithVat = models.FloatField(_("Сумма с НДС"))

    class Meta:
        verbose_name = "Строка счета на оплату"
        verbose_name_plural = "Строки счетов на оплату"

    def __str__(self):
        return str(self.pk)


class Invoice(models.Model):
    """ Счет на оплату """
    Header = models.ForeignKey("integration.InvoiceHeader", verbose_name=_(
        "Шапка документа"), on_delete=models.CASCADE)
    Lines = models.ManyToManyField(
        "integration.InvoiceLine", verbose_name=_("Строки документа"))

    class Meta:
        verbose_name = "Счет на оплату"
        verbose_name_plural = "Счета на оплату"

    def __str__(self):
        return str(self.pk)


class RealizationHeader(models.Model):
    """ Шапка реализации услуг """
    Doc_Id = models.BigAutoField(_("Уникальный код"), primary_key=True)
    doc_number = models.CharField(_("Номер документа"), max_length=11)
    doc_data = models.DateField(_("Дата документа"))
    Id_Org = models.ForeignKey("integration.Partner", related_name="RealizationHeader_Id_Org", verbose_name=_(
        "Id Организации"), on_delete=models.CASCADE)
    Id_Partner = models.ForeignKey("integration.Partner", related_name="RealizationHeader_Id_Partner", verbose_name=_(
        "Id организации"), on_delete=models.CASCADE)
    Account = models.CharField(_("Банковский счет организации"), max_length=50)
    Id_dog = models.ForeignKey("integration.Contract", verbose_name=_(
        "Id договора"), on_delete=models.CASCADE)
    Id_Invoice = models.ForeignKey("integration.Invoice", verbose_name=_(
        "Id счета на оплату"), on_delete=models.CASCADE)
    IsDeleted = models.BooleanField(_("Отметка об удалении"), default=False)

    class Meta:
        verbose_name = 'Шапка реализации услуг'
        verbose_name_plural = 'Шапки реализаций услуг'


class RealizationLine(models.Model):
    """ Строка реализации услуг  """
    Service_Id = models.ForeignKey("integration.Service", verbose_name=_(
        "Id номенклатуры"), on_delete=models.CASCADE)
    Quantity = models.FloatField(_("Количество"))
    Price = models.FloatField(_("Сумма без НДС"))
    Cost = models.FloatField(_("Сумма без НДС"))
    VatRate = models.CharField(
        _("Ставка НДС"), max_length=50, choices=VAT_RATE_CHOICES)
    Vat = models.FloatField(_("Сумма НДС"))
    CostWithVat = models.FloatField(_("Сумма с НДС"))

    class Meta:
        verbose_name = "Строка реализации услуг"
        verbose_name_plural = "Строки реализаций услуг"


class Realization(models.Model):
    """ Реализация услуг """
    Header = models.ForeignKey("integration.RealizationHeader", verbose_name=_(
        "Шапка документа"), on_delete=models.CASCADE)
    Lines = models.ManyToManyField(
        "integration.RealizationLine", verbose_name=_("Строки документа"))

    class Meta:
        verbose_name = "Реализация услуг"
        verbose_name_plural = "Реализации услуг"


class ReceiptHeader(models.Model):
    """ Шапка поступления услуг """
    Doc_Id = models.BigAutoField(_("Уникальный код"), primary_key=True)
    doc_number = models.CharField(_("Номер документа"), max_length=11)
    doc_data = models.DateField(_("Дата документа"))
    Id_Org = models.ForeignKey("integration.Partner", related_name="ReceiptHeader_Id_Org", verbose_name=_(
        "Id Организации"), on_delete=models.CASCADE)
    Id_Partner = models.ForeignKey("integration.Partner", related_name="ReceiptHeader_Id_Partner", verbose_name=_(
        "Id партнера"), on_delete=models.CASCADE)
    Id_dog = models.ForeignKey("integration.Contract", verbose_name=_(
        "Id договора"), on_delete=models.CASCADE)
    IsDeleted = models.BooleanField(_("Отметка об удалении"), default=False)

    class Meta:
        verbose_name = 'Шапка поступления услуг'
        verbose_name_plural = 'Шапки поступлений услуг'


class ReceiptLine(models.Model):
    """ Строка поступления услуг """
    Service_Id = models.ForeignKey("integration.Service", verbose_name=_(
        "Id номенклатуры"), on_delete=models.CASCADE)
    Quantity = models.FloatField(_("Количество"))
    Price = models.FloatField(_("Сумма без НДС"))
    Cost = models.FloatField(_("Сумма без НДС"))
    VatRate = models.CharField(
        _("Ставка НДС"), max_length=50, choices=VAT_RATE_CHOICES)
    Vat = models.FloatField(_("Сумма НДС"))
    CostWithVat = models.FloatField(_("Сумма с НДС"))

    class Meta:
        verbose_name = "Строка поступления услуг"
        verbose_name_plural = "Строки поступлений услуг"


class Receipt(models.Model):
    """ Поступление услуг """
    Header = models.ForeignKey("integration.ReceiptHeader", verbose_name=_(
        "Шапка документа"), on_delete=models.CASCADE)
    Lines = models.ManyToManyField(
        "integration.ReceiptLine", verbose_name=_("Строки документа"))

    class Meta:
        verbose_name = "Поступление услуг"
        verbose_name_plural = "Поступления услуг"

    def __str__(self):
        return str(self.pk)


class PaymentOrder(models.Model):
    """ Платежное поручение """
    Doc_Id = models.BigAutoField(_("Уникальный код"), primary_key=True)
    doc_number = models.CharField(_("Номер документа"), max_length=11)
    doc_data = models.DateField(_("Дата документа"))
    Id_Org = models.ForeignKey("integration.Partner", related_name="PaymentOrder_Id_Org", verbose_name=_(
        "Id Организации"), on_delete=models.CASCADE)
    Account = models.CharField(_("Банковский счет организации"), max_length=50)
    Id_Partner = models.ForeignKey("integration.Partner", related_name="PaymentOrder_Id_Partner", verbose_name=_(
        "Id организации"), on_delete=models.CASCADE)
    Id_dog = models.ForeignKey("integration.Contract", verbose_name=_(
        "Id договора"), on_delete=models.CASCADE)
    Cost = models.FloatField(_("Сумма без НДС"))
    VatRate = models.CharField(
        _("Ставка НДС"), max_length=50, choices=VAT_RATE_CHOICES)
    Vat = models.FloatField(_("Сумма НДС"))
    Purpose = models.CharField(_("Назначение платежа"), max_length=255)
    IsDeleted = models.BooleanField(_("Отметка об удалении"), default=False)

    class Meta:
        verbose_name = "Платежное поручение"
        verbose_name_plural = "Платежные поручения"

    def __str__(self):
        return str(self.Doc_Id)


class SalesReportHeader(models.Model):
    """ Шапка отчета о розничной продаже """
    Doc_Id = models.BigAutoField(_("Уникальный код"), primary_key=True)
    doc_number = models.CharField(_("Номер документа"), max_length=11)
    doc_data = models.DateField(_("Дата документа"))
    Id_Org = models.ForeignKey("integration.Partner", related_name="Id_Org", verbose_name=_(
        "Id Организации"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Шапка отчета о розничной продаже"
        verbose_name_plural = "Шапки отчетов о розничных продажах"


class SalesReportLine(models.Model):
    """ Строка отчета о розничной продаже """
    Service_Id = models.ForeignKey("integration.Service", verbose_name=_(
        "Id номенклатуры"), on_delete=models.CASCADE)
    Quantity = models.FloatField(_("Количество"))
    Price = models.FloatField(_("Сумма без НДС"))
    Cost = models.FloatField(_("Сумма без НДС"))
    VatRate = models.CharField(
        _("Ставка НДС"), max_length=50, choices=VAT_RATE_CHOICES)
    Vat = models.FloatField(_("Сумма НДС"))
    CostWithVat = models.FloatField(_("Сумма с НДС"))
    IsDeleted = models.BooleanField(_("Отметка об удалении"), default=False)

    class Meta:
        verbose_name = "Строка отчета о розничной продаже"
        verbose_name_plural = "Строки отчетов о розничных продажах"


class SalesReport(models.Model):
    """ Отчет о розничной продаже """
    Header = models.ForeignKey("integration.SalesReportHeader", verbose_name=_(
        "Шапка документа"), on_delete=models.CASCADE)
    Lines = models.ManyToManyField(
        "integration.SalesReportLine", verbose_name=_("Строки документа"))

    class Meta:
        verbose_name = "Отчет о розничной продаже"
        verbose_name_plural = "Отчеты о розничных продажах"


class Income(models.Model):
    """ Поступление денежных средств """
    Doc_Id = models.BigAutoField(_("Уникальный код"), primary_key=True)
    doc_number = models.CharField(_("Номер документа"), max_length=11)
    doc_data = models.DateField(_("Дата документа"))
    Id_Org = models.ForeignKey("integration.Partner", related_name="Income_Id_Org", verbose_name=_(
        "Id Организации"), on_delete=models.CASCADE)
    Account = models.CharField(_("Банковский счет организации"), max_length=50)
    Id_Partner = models.ForeignKey("integration.Partner", related_name="Income_Id_Partner", verbose_name=_(
        "Id организации"), on_delete=models.CASCADE)
    Id_dog = models.ForeignKey("integration.Contract", verbose_name=_(
        "Id договора"), on_delete=models.CASCADE)
    Cost = models.FloatField(_("Сумма без НДС"))
    VatRate = models.CharField(
        _("Ставка НДС"), max_length=50, choices=VAT_RATE_CHOICES)
    Vat = models.FloatField(_("Сумма НДС"))
    Purpose = models.CharField(_("Назначение платежа"), max_length=255)
    Id_Invoice = models.ForeignKey("integration.Invoice", verbose_name=_(
        "Id документа 'Счет на оплату'"), on_delete=models.CASCADE)
    IsDeleted = models.BooleanField(_("Отметка об удалении"), default=False)

    class Meta:
        verbose_name = "Поступление денежных средств"
        verbose_name_plural = "Поступления денежных средств"


class Payment(models.Model):
    """ Списание денежных средств """
    Doc_Id = models.BigAutoField(_("Уникальный код"), primary_key=True)
    doc_number = models.CharField(_("Номер документа"), max_length=11)
    doc_data = models.DateField(_("Дата документа"))
    Id_Org = models.ForeignKey("integration.Partner", related_name="Payment_Id_Org", verbose_name=_(
        "Id Организации"), on_delete=models.CASCADE)
    Account = models.CharField(_("Банковский счет организации"), max_length=50)
    Id_Partner = models.ForeignKey("integration.Partner", related_name="Payment_Id_Partner", verbose_name=_(
        "Id организации"), on_delete=models.CASCADE)
    Id_dog = models.ForeignKey("integration.Contract", verbose_name=_(
        "Id договора"), on_delete=models.CASCADE)
    Cost = models.FloatField(_("Сумма без НДС"))
    VatRate = models.CharField(
        _("Ставка НДС"), max_length=50, choices=VAT_RATE_CHOICES)
    Vat = models.FloatField(_("Сумма НДС"))
    Purpose = models.CharField(_("Назначение платежа"), max_length=255)
    Id_Receipt = models.ForeignKey("integration.Receipt", verbose_name=_(
        "Id документа 'Поступление услуг'"), on_delete=models.CASCADE)
    Id_Order = models.ForeignKey("integration.PaymentOrder", verbose_name=_(
        "Id документа 'Платежное поручение'"), on_delete=models.CASCADE)
    IsDeleted = models.BooleanField(_("Отметка об удалении"), default=False)

    class Meta:
        verbose_name = "Списание денежных средств"
        verbose_name_plural = "Списания денежных средств"
