from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from django.shortcuts import get_object_or_404
from .models import *


class OrganizationSerializer(ModelSerializer):
    Id = serializers.CharField()

    class Meta:
        model = Partner
        exclude = ('IsOrganization', 'IsDeleted', 'Type')


class PartnerSerializer(ModelSerializer):
    Id = serializers.CharField()

    class Meta:
        model = Partner
        exclude = ('IsOrganization',)


class BankAccountSerializer(ModelSerializer):
    # Id = serializers.CharField()
    OwnerId = serializers.CharField()

    class Meta:
        model = BankAccount
        exclude = ('Id',)


class ServiceSerializer(ModelSerializer):
    Id = serializers.CharField()

    class Meta:
        model = Service
        fields = '__all__'


class ContractSerializer(ModelSerializer):
    Cont_Id = serializers.CharField()
    Cont_Org = serializers.CharField()
    Cont_Owner = serializers.CharField()

    class Meta:
        model = Contract
        fields = '__all__'


class InvoiceHeaderSerializer(ModelSerializer):
    Doc_Id = serializers.CharField()
    Id_Org = serializers.CharField()
    Id_Partner = serializers.CharField()
    Id_dog = serializers.CharField()

    class Meta:
        model = InvoiceHeader
        fields = '__all__'


class InvoiceLineSerialzier(ModelSerializer):
    Service_Id = serializers.CharField()

    class Meta:
        model = InvoiceLine
        exclude = ('id',)


class InvoiceSerializer(ModelSerializer):
    Header = InvoiceHeaderSerializer()
    Lines = InvoiceLineSerialzier(many=True)

    class Meta:
        model = Invoice
        exclude = ('id',)


class InvoiceResponseSerializer(serializers.Serializer):
    Invoices = InvoiceSerializer(many=True)
    Organizations = OrganizationSerializer(many=True)
    Partners = PartnerSerializer(many=True)
    Contracts = ContractSerializer(many=True)
    Services = ServiceSerializer(many=True)


class RealizationHeaderSerializer(ModelSerializer):
    Doc_Id = serializers.CharField()
    Id_Org = serializers.CharField()
    Id_Partner = serializers.CharField()
    Id_dog = serializers.CharField()
    Id_Invoice = serializers.CharField()

    class Meta:
        model = RealizationHeader
        fields = '__all__'


class RealizationLineSerialzier(ModelSerializer):
    Service_Id = serializers.CharField()

    class Meta:
        model = RealizationLine
        exclude = ('id',)


class RealizationSerializer(ModelSerializer):
    Header = RealizationHeaderSerializer()
    Lines = RealizationLineSerialzier(many=True)

    class Meta:
        model = Realization
        exclude = ('id',)


class RealizationResponseSerializer(serializers.Serializer):
    Realizations = RealizationSerializer(many=True)
    Invoices = InvoiceSerializer(many=True)
    Organizations = OrganizationSerializer(many=True)
    Partners = PartnerSerializer(many=True)
    Contracts = ContractSerializer(many=True)
    Services = ServiceSerializer(many=True)


class ReceiptHeaderSerializer(ModelSerializer):
    Doc_Id = serializers.CharField()
    Id_Org = serializers.CharField()
    Id_Partner = serializers.CharField()
    Id_dog = serializers.CharField()

    class Meta:
        model = ReceiptHeader
        fields = '__all__'


class ReceiptLineSerialzier(ModelSerializer):
    Service_Id = serializers.CharField()

    class Meta:
        model = ReceiptLine
        exclude = ('id',)


class ReceiptSerializer(ModelSerializer):
    Header = ReceiptHeaderSerializer()
    Lines = ReceiptLineSerialzier(many=True)

    class Meta:
        model = Receipt
        exclude = ('id',)


class ReceiptResponseSerializer(serializers.Serializer):
    Receipts = ReceiptSerializer(many=True)
    Organizations = OrganizationSerializer(many=True)
    Partners = PartnerSerializer(many=True)
    Contracts = ContractSerializer(many=True)
    Services = ServiceSerializer(many=True)


class PaymentOrderSerializer(ModelSerializer):
    Doc_Id = serializers.CharField()
    Id_Org = serializers.CharField()
    Id_Partner = serializers.CharField()
    Id_dog = serializers.CharField()

    class Meta:
        model = PaymentOrder
        fields = '__all__'


class PaymentOrderResponseSerializer(serializers.Serializer):
    PaymentOrders = PaymentOrderSerializer(many=True)
    Organizations = OrganizationSerializer(many=True)
    Partners = PartnerSerializer(many=True)
    Contracts = ContractSerializer(many=True)


class SalesReportHeaderSerializer(ModelSerializer):
    Doc_Id = serializers.CharField()
    Id_Org = serializers.CharField()

    class Meta:
        model = SalesReportHeader
        fields = '__all__'


class SalesReportLineSerialzier(ModelSerializer):
    Service_Id = serializers.CharField()

    class Meta:
        model = SalesReportLine
        exclude = ('id',)


class SalesReportSerializer(ModelSerializer):
    Header = SalesReportHeaderSerializer()
    Lines = SalesReportLineSerialzier(many=True)

    class Meta:
        model = SalesReport
        exclude = ('id',)


class SalesReportResponseSerializer(serializers.Serializer):
    SalesReports = SalesReportSerializer(many=True)
    Organizations = OrganizationSerializer(many=True)
    Services = ServiceSerializer(many=True)


class IncomeSerializer(ModelSerializer):
    Doc_Id = serializers.CharField(read_only=True)
    Id_Org = serializers.CharField()
    Id_Partner = serializers.CharField()
    Id_dog = serializers.CharField()
    Id_Invoice = serializers.CharField()

    class Meta:
        model = Income
        fields = '__all__'

    def create(self, validated_data):
        data = {**validated_data}
        data['Id_Org'] = get_object_or_404(Partner, Id=int(data['Id_Org']))
        data['Id_Partner'] = get_object_or_404(
            Partner, Id=int(data['Id_Partner']))
        data['Id_dog'] = get_object_or_404(
            Contract, Cont_Id=int(data['Id_dog']))
        data['Id_Invoice'] = get_object_or_404(
            Invoice, id=int(data['Id_Invoice']))

        income = Income.objects.create(**data)
        income.save()
        return income


class IncomeResponseSerializer(serializers.Serializer):
    Incomes = IncomeSerializer(many=True)
    Organizations = OrganizationSerializer(many=True)
    Services = ServiceSerializer(many=True)
    Partners = PartnerSerializer(many=True)
    Contracts = ContractSerializer(many=True)
    Invoices = InvoiceSerializer(many=True)


class PaymentSerializer(ModelSerializer):
    Doc_Id = serializers.CharField(read_only=True)
    Id_Org = serializers.CharField()
    Id_Partner = serializers.CharField()
    Id_dog = serializers.CharField()
    Id_Receipt = serializers.CharField()
    Id_Order = serializers.CharField()

    class Meta:
        model = Payment
        fields = '__all__'

    def create(self, validated_data):
        data = {**validated_data}
        data['Id_Org'] = get_object_or_404(Partner, Id=int(data['Id_Org']))
        data['Id_Partner'] = get_object_or_404(
            Partner, Id=int(data['Id_Partner']))
        data['Id_dog'] = get_object_or_404(
            Contract, Cont_Id=int(data['Id_dog']))
        data['Id_Receipt'] = get_object_or_404(
            Receipt, id=int(data['Id_Receipt']))
        data['Id_Order'] = get_object_or_404(
            PaymentOrder, Doc_Id=int(data['Id_Order']))

        payment = Payment.objects.create(**data)
        payment.save()
        return payment


class PaymentResponseSerializer(serializers.Serializer):
    Payments = PaymentSerializer(many=True)
    Organizations = OrganizationSerializer(many=True)
    Services = ServiceSerializer(many=True)
    Partners = PartnerSerializer(many=True)
    Contracts = ContractSerializer(many=True)
    Receipts = ReceiptSerializer(many=True)
    PaymentOrders = PaymentOrderSerializer(many=True)


class CatalogSerializer(serializers.Serializer):
    Organizations = OrganizationSerializer(many=True)
    Partners = PartnerSerializer(many=True)
    BankAccounts = BankAccountSerializer(many=True)
    Services = ServiceSerializer(many=True)
    Contracts = ContractSerializer(many=True)


class DocumentsSerializer(serializers.Serializer):
    Invoices = InvoiceSerializer(many=True)
    Realizations = RealizationSerializer(many=True)
    Receipts = ReceiptSerializer(many=True)
    PaymentOrders = PaymentOrderSerializer(many=True)
    SalesReports = SalesReportSerializer(many=True)


class DocumentsOutcomeSerializer(serializers.Serializer):
    Incomes = IncomeSerializer(many=True)
    Payments = PaymentSerializer(many=True)

    def create(self, validated_data):
        data = {**validated_data}
        incomes = []
        for income in data['Incomes']:
            income['Id_Org'] = get_object_or_404(
                Partner, Id=int(income['Id_Org']))
            income['Id_Partner'] = get_object_or_404(
                Partner, Id=int(income['Id_Partner']))
            income['Id_dog'] = get_object_or_404(
                Contract, Cont_Id=int(income['Id_dog']))
            income['Id_Invoice'] = get_object_or_404(
                Invoice, id=int(income['Id_Invoice']))
            new_income = Income.objects.create(**income)
            new_income.save()
            incomes.append(new_income)

        payments = []
        for payment in data['Payments']:
            payment['Id_Org'] = get_object_or_404(
                Partner, Id=int(payment['Id_Org']))
            payment['Id_Partner'] = get_object_or_404(
                Partner, Id=int(payment['Id_Partner']))
            payment['Id_dog'] = get_object_or_404(
                Contract, Cont_Id=int(payment['Id_dog']))
            payment['Id_Receipt'] = get_object_or_404(
                Receipt, id=int(payment['Id_Receipt']))
            payment['Id_Order'] = get_object_or_404(
                PaymentOrder, Doc_Id=int(payment['Id_Order']))
            new_payment = Payment.objects.create(**payment)
            new_payment.save()
            payments.append(new_payment)

        return {'Incomes': incomes, 'Payments': payments}
