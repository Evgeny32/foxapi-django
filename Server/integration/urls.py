from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name="IntegrationDocs"),
    path('organization/', OrganizationView.as_view(), name="OrganizationView"),
    path('partners/', PartnersView.as_view(), name="PartnersView"),
    path('bank-accounts/', BankAccountsView.as_view(), name="BankAccountsView"),
    path('services/', ServicesView.as_view(), name="ServicesView"),
    path('contracts/', ContractsView.as_view(), name="ContractsView"),
    path('invoices/', InvoicesView.as_view(), name="InvoicesView"),
    path('realizations/', RealizationsView.as_view(), name="RealizationsView"),
    path('receipts/', ReceiptsView.as_view(), name="ReceiptsView"),
    path('payment-orders/', PaymentOrdersView.as_view(), name="PaymentOrdersView"),
    path('sales-reports/', SalesReportsView.as_view(), name="SalesReportsView"),
    path('incomes/', IncomesView.as_view(), name="IncomesView"),
    path('payments/', PaymentsView.as_view(), name="PaymentsView"),
    path('catalog/', CatalogView.as_view(), name="CatalogView"),
    path('documents/', DocumentsView.as_view(), name="DocumentsView"),
    path('documents-from-1c/', DocumentsOutcomeView.as_view(),
         name="DocumentsOutcomeView"),
]
