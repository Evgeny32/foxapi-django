from django.contrib import admin
from .models import *
# Register your models here.
class BankAccountInline(admin.StackedInline):
    model = BankAccount

@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    list_display = ('OwnerType', 'OwnerId', 'Account', 'BIC', 'Main', 'IsDeleted')
    list_editable = ( 'OwnerId', 'Account', 'BIC', 'Main', 'IsDeleted')

@admin.register(Partner)
class PartnerAdmin(admin.ModelAdmin):
    list_display = ('Name', 'Type', 'INN', 'KPP','IsDeleted', 'IsOrganization')
    list_editable = ( 'Type', 'INN', 'KPP','IsDeleted', 'IsOrganization')
    inlines = [
        BankAccountInline
    ]


admin.site.register(Service)
admin.site.register(Contract)
admin.site.register(InvoiceHeader)
admin.site.register(InvoiceLine)
admin.site.register(Invoice)
admin.site.register(RealizationHeader)
admin.site.register(RealizationLine)
admin.site.register(Realization)
admin.site.register(ReceiptHeader)
admin.site.register(ReceiptLine)
admin.site.register(Receipt)
admin.site.register(PaymentOrder)
admin.site.register(SalesReportHeader)
admin.site.register(SalesReportLine)
admin.site.register(SalesReport)
admin.site.register(Income)
admin.site.register(Payment)
