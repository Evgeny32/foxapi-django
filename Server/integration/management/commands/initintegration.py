from django.core.management.base import BaseCommand
from integration import models


class Command(BaseCommand):

    def get_nds(self, value, nds):
        return value * (100 + nds) / nds

    def handle(self, *args, **options):
        self.stdout.write(self.style.WARNING('Очищаем все'))
        self.stdout.write(self.style.WARNING('Удаляем партнеров'))
        partners = models.Partner.objects.all()
        for partner in partners:
            partner.delete()
        self.stdout.write(self.style.WARNING('Удаляем счета'))
        bank_accounts = models.BankAccount.objects.all()
        for account in bank_accounts:
            account.delete()
        self.stdout.write(self.style.WARNING('Удаляем услуги'))
        items = models.Service.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING('Удаляем договора'))
        items = models.Contract.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING('Удаляем шапки счета на оплату'))
        items = models.InvoiceHeader.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING('Удаляем строки счета на оплату'))
        items = models.InvoiceLine.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING('Удаляем счета на оплату'))
        items = models.Invoice.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING('Удаляем шапки реализации услуг'))
        items = models.RealizationHeader.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING(
            'Удаляем строки реализации услуг'))
        items = models.RealizationLine.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING('Удаляем реализации услуг'))
        items = models.Realization.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING(
            'Удаляем шапки поступления услуг'))
        items = models.ReceiptHeader.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING(
            'Удаляем строки поступления услуг'))
        items = models.ReceiptLine.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING('Удаляем поступления услуг'))
        items = models.Receipt.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING('Удаляем платежные поручения'))
        items = models.PaymentOrder.objects.all()
        for item in items:
            item.delete()

        self.stdout.write(self.style.WARNING(
            'Удаляем шапки отчетов о розничной продаже'))
        items = models.SalesReportHeader.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING(
            'Удаляем строки отчетов о розничной продаже'))
        items = models.SalesReportLine.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING(
            'Удаляем отчеты о розничной продаже'))
        items = models.SalesReport.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING(
            'Удаляем поступления денежных средств'))
        items = models.Income.objects.all()
        for item in items:
            item.delete()
        self.stdout.write(self.style.WARNING(
            'Удаляем списания денежных средств'))
        items = models.Payment.objects.all()
        for item in items:
            item.delete()

        self.stdout.write(self.style.SUCCESS('Все данные удалены'))
        self.stdout.write(self.style.WARNING('Создаем организацию'))
        organization = models.Partner(
            Id=1,
            INN='123456789012',
            KPP='123456789',
            Name='ПинКод',
            Type='Юрлицо',
            IsOrganization=True
        )
        organization.save()
        self.stdout.write(self.style.SUCCESS('Организация создана'))
        self.stdout.write(self.style.WARNING('Создаем партнеров'))
        partner1 = models.Partner(
            Id=2,
            INN="234567890123",
            KPP="234567890",
            Name='ООО "Маленькая надежда"',
            Type="Юрлицо",
        )
        partner1.save()
        partner2 = models.Partner(
            Id=3,
            INN="3455678901234",
            KPP="345678901",
            Name="Михайлов Юрий Николаевич",
            Type="Физлицо"
        )
        partner2.save()
        partner3 = models.Partner(
            Id=4,
            INN="456789012345",
            KPP="456789012",
            Name='ООО "Мечта"',
            Type="Юрлицо"
        )
        partner3.save()
        self.stdout.write(self.style.SUCCESS('Партнеры созданы'))
        self.stdout.write(self.style.WARNING('Создаем банковские счета'))
        account = models.BankAccount(
            OwnerType="Организация",
            OwnerId=organization,
            Account="40206810045370101201",
            BIC="200000271",
            Main=True
        )
        account.save()
        account1 = models.BankAccount(
            OwnerType="Контрагент",
            OwnerId=partner1,
            Account="30101810300000000765",
            BIC="041012765",
            Main=True
        )
        account1.save()

        account2 = models.BankAccount(
            OwnerType="Контрагент",
            OwnerId=partner2,
            Account="40102810245370000015",
            BIC="011012100",
            Main=True
        )
        account2.save()

        account3 = models.BankAccount(
            OwnerType="Контрагент",
            OwnerId=partner3,
            Account="40106810200000000002",
            BIC="229000102",
            Main=True
        )
        account3.save()

        self.stdout.write(self.style.SUCCESS('Банковские счета созданы'))
        self.stdout.write(self.style.WARNING('Создаем услуги'))
        services = ['Услуга №1', 'Услуга №2', 'Услуга №3', 'Услуга №4',
                    'Услуга №4', 'Услуга №5', 'Услуга №6', 'Услуга №7', 'Услуга №8']
        for name in services:
            service = models.Service(
                Name=name
            )
            service.save()
        services = models.Service.objects.all()
        self.stdout.write(self.style.SUCCESS('Услуги созданы'))
        self.stdout.write(self.style.WARNING('Создаем договора'))
        contracts = [
            {
                'Cont_Org': organization,
                'Cont_Owner': partner1,
                'Cont_Type': 'С покупателем',
                'Cont_Number': '123',
                'Cont_Data': '2021-08-23',
                'Cont_Name': 'Тестовое имя 1'
            },
            {
                'Cont_Org': organization,
                'Cont_Owner': partner2,
                'Cont_Type': 'С покупателем',
                'Cont_Number': '1234',
                'Cont_Data': '2021-08-20',
                'Cont_Name': 'Тестовое имя 2'
            },
            {
                'Cont_Org': organization,
                'Cont_Owner': partner3,
                'Cont_Type': 'С поставщиком',
                'Cont_Number': '12345',
                'Cont_Data': '2021-08-15',
                'Cont_Name': 'Тестовое имя 3'
            },
        ]
        for contract in contracts:
            con = models.Contract(**contract)
            con.save()
        contracts = models.Contract.objects.all()
        self.stdout.write(self.style.SUCCESS('Договора созданы'))
        self.stdout.write(self.style.WARNING('Создаем Шапки счета на оплату'))
        invoice_headers = [
            {
                "doc_number": '123',
                'doc_data': '2021-07-10',
                "Id_Org": organization,
                "Id_Partner": partner1,
                "Account": account,
                "Id_dog": contracts[0]
            },
            {
                "doc_number": '1234',
                'doc_data': '2021-07-01',
                "Id_Org": organization,
                "Id_Partner": partner2,
                "Account": account,
                "Id_dog": contracts[1]
            },
            {
                "doc_number": '12345',
                'doc_data': '2021-08-10',
                "Id_Org": organization,
                "Id_Partner": partner3,
                "Account": account,
                "Id_dog": contracts[2]
            },
        ]
        for header in invoice_headers:
            invoice_header = models.InvoiceHeader(**header)
            invoice_header.save()
        invoice_headers = models.InvoiceHeader.objects.all()
        self.stdout.write(self.style.SUCCESS('Шапки счета на оплату созданы'))
        self.stdout.write(self.style.WARNING(
            'Создаем Строки счета на оплату'))
        invoice_lines = [
            {
                "Service_Id": services[0],
                "Quantity": 100.00,
                "Price": 200.00,
                "Cost": 20000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "CostWithVat": 20000.00
            },
            {
                "Service_Id": services[1],
                "Quantity": 200.00,
                "Price": 300.00,
                "Cost": 60000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "CostWithVat": 60000.00
            },
            {
                "Service_Id": services[3],
                "Quantity": 50.00,
                "Price": 200.00,
                "Cost": 10000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "CostWithVat": 10000.00
            },
        ]
        for line in invoice_lines:
            invoice_line = models.InvoiceLine(**line)
            invoice_line.save()
        invoice_lines = models.InvoiceLine.objects.all()
        self.stdout.write(self.style.SUCCESS(
            'Строки счета на оплату созданы'))
        self.stdout.write(self.style.WARNING('Создаем счета на оплату'))
        invoices = [
            {
                'Header': invoice_headers[0],
                'Lines': invoice_lines
            },
            {
                'Header': invoice_headers[1],
                'Lines': invoice_lines
            },
            {
                'Header': invoice_headers[2],
                'Lines': invoice_lines
            },
        ]
        for invoice in invoices:
            inv = models.Invoice(
                Header=invoice['Header']
            )
            inv.save()
            inv.Lines.set(invoice['Lines'])
        invoices = models.Invoice.objects.all()
        self.stdout.write(self.style.SUCCESS('Счета на оплату созданы'))
        self.stdout.write(self.style.WARNING('Создаем шапки реализации услуг'))
        realization_headers = [
            {
                "doc_number": '123',
                'doc_data': '2021-07-10',
                "Id_Org": organization,
                "Id_Partner": partner1,
                "Account": account,
                "Id_dog": contracts[0],
                "Id_Invoice": invoices[0]
            },
            {
                "doc_number": '1234',
                'doc_data': '2021-07-01',
                "Id_Org": organization,
                "Id_Partner": partner2,
                "Account": account,
                "Id_dog": contracts[1],
                "Id_Invoice": invoices[1]
            },
            {
                "doc_number": '12345',
                'doc_data': '2021-08-10',
                "Id_Org": organization,
                "Id_Partner": partner3,
                "Account": account,
                "Id_dog": contracts[2],
                "Id_Invoice": invoices[2]
            },
        ]
        for header in realization_headers:
            realization_header = models.RealizationHeader(**header)
            realization_header.save()
        realization_headers = models.RealizationHeader.objects.all()
        self.stdout.write(self.style.SUCCESS('Шапки реализации услуг созданы'))
        self.stdout.write(self.style.WARNING(
            'Создаем строки реализации услуг'))
        realization_lines = [
            {
                "Service_Id": services[0],
                "Quantity": 100.00,
                "Price": 200.00,
                "Cost": 20000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "CostWithVat": 20000.00
            },
            {
                "Service_Id": services[1],
                "Quantity": 200.00,
                "Price": 300.00,
                "Cost": 60000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "CostWithVat": 60000.00
            },
            {
                "Service_Id": services[3],
                "Quantity": 50.00,
                "Price": 200.00,
                "Cost": 10000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "CostWithVat": 10000.00
            },
        ]
        for line in realization_lines:
            realization_line = models.RealizationLine(**line)
            realization_line.save()
        realization_lines = models.RealizationLine.objects.all()
        self.stdout.write(self.style.SUCCESS(
            'Строки реализации услуг созданы'))
        self.stdout.write(self.style.WARNING('Создаем реализации услуг'))
        realizations = [
            {
                'Header': realization_headers[0],
                'Lines': realization_lines
            },
            {
                'Header': realization_headers[1],
                'Lines': realization_lines
            },
            {
                'Header': realization_headers[2],
                'Lines': realization_lines
            },
        ]
        for realization in realizations:
            rea = models.Realization(
                Header=realization['Header']
            )
            rea.save()
            rea.Lines.set(realization['Lines'])
        realizations = models.Realization.objects.all()
        self.stdout.write(self.style.SUCCESS('Реализации услуг созданы'))
        self.stdout.write(self.style.WARNING(
            'Создаем шапки поступления услуг'))
        receipt_headers = [
            {
                "doc_number": '123',
                'doc_data': '2021-07-10',
                "Id_Org": organization,
                "Id_Partner": partner1,
                "Id_dog": contracts[0],
            },
            {
                "doc_number": '1234',
                'doc_data': '2021-07-01',
                "Id_Org": organization,
                "Id_Partner": partner2,
                "Id_dog": contracts[1],
            },
            {
                "doc_number": '12345',
                'doc_data': '2021-08-10',
                "Id_Org": organization,
                "Id_Partner": partner3,
                "Id_dog": contracts[2],
            },
        ]
        for header in receipt_headers:
            receipt_header = models.ReceiptHeader(**header)
            receipt_header.save()
        receipt_headers = models.ReceiptHeader.objects.all()
        self.stdout.write(self.style.SUCCESS(
            'Шапки поступления услуг созданы'))
        self.stdout.write(self.style.WARNING(
            'Создаем строки поступления услуг'))
        receipt_lines = [
            {
                "Service_Id": services[0],
                "Quantity": 100.00,
                "Price": 200.00,
                "Cost": 20000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "CostWithVat": 20000.00
            },
            {
                "Service_Id": services[1],
                "Quantity": 200.00,
                "Price": 300.00,
                "Cost": 60000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "CostWithVat": 60000.00
            },
            {
                "Service_Id": services[3],
                "Quantity": 50.00,
                "Price": 200.00,
                "Cost": 10000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "CostWithVat": 10000.00
            },
        ]
        for line in receipt_lines:
            receipt_line = models.ReceiptLine(**line)
            receipt_line.save()
        receipt_lines = models.ReceiptLine.objects.all()
        self.stdout.write(self.style.SUCCESS(
            'Строки поступления услуг созданы'))
        self.stdout.write(self.style.WARNING('Создаем поступления услуг'))
        receipts = [
            {
                'Header': receipt_headers[0],
                'Lines': receipt_lines
            },
            {
                'Header': receipt_headers[1],
                'Lines': receipt_lines
            },
            {
                'Header': receipt_headers[2],
                'Lines': receipt_lines
            },
        ]
        for receipt in receipts:
            rec = models.Receipt(
                Header=receipt['Header']
            )
            rec.save()
            rec.Lines.set(receipt['Lines'])
        receipts = models.Receipt.objects.all()
        self.stdout.write(self.style.SUCCESS(
            'Поступления услуг созданы'))
        self.stdout.write(self.style.WARNING(
            'Создаем платежные поручения'))
        payment_orders = [
            {
                "doc_number": '123',
                'doc_data': '2021-07-10',
                "Id_Org": organization,
                "Id_Partner": partner1,
                "Account": "23452345345",
                "Id_dog": contracts[0],
                "Cost": 10000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "Purpose": "Назначение 1"
            },
            {
                "doc_number": '1234',
                'doc_data': '2021-07-01',
                "Id_Org": organization,
                "Id_Partner": partner2,
                "Account": "23452345345",
                "Id_dog": contracts[1],
                "Cost": 60000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "Purpose": "Назначение 2"
            },
            {
                "doc_number": '12345',
                'doc_data': '2021-08-10',
                "Id_Org": organization,
                "Id_Partner": partner3,
                "Account": "23452345345",
                "Id_dog": contracts[2],
                "Cost": 20000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "Purpose": "Назначение 3"
            },
        ]
        for order in payment_orders:
            payment_order = models.PaymentOrder(**order)
            payment_order.save()
        payment_orders = models.PaymentOrder.objects.all()
        self.stdout.write(self.style.SUCCESS(
            'Платежные поручения созданы'))
        self.stdout.write(self.style.WARNING(
            'Создаем шапки отчета о розничной продаже'))
        sales_report_headers = [
            {
                "doc_number": '123',
                'doc_data': '2021-07-10',
                "Id_Org": organization,
            },
            {
                "doc_number": '1234',
                'doc_data': '2021-07-01',
                "Id_Org": organization,
            },
            {
                "doc_number": '12345',
                'doc_data': '2021-08-10',
                "Id_Org": organization,
            },
        ]
        for header in sales_report_headers:
            sales_report_header = models.SalesReportHeader(**header)
            sales_report_header.save()
        sales_report_headers = models.SalesReportHeader.objects.all()
        self.stdout.write(self.style.SUCCESS(
            'Шапки отчета о розничной продаже созданы'))
        self.stdout.write(self.style.WARNING(
            'Создаем строки отчета о розничной продаже'))
        sales_report_lines = [
            {
                "Service_Id": services[0],
                "Quantity": 100.00,
                "Price": 200.00,
                "Cost": 20000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "CostWithVat": 20000.00
            },
            {
                "Service_Id": services[1],
                "Quantity": 200.00,
                "Price": 300.00,
                "Cost": 60000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "CostWithVat": 60000.00
            },
            {
                "Service_Id": services[3],
                "Quantity": 50.00,
                "Price": 200.00,
                "Cost": 10000.00,
                "VatRate": 'Без НДС',
                "Vat": 0.00,
                "CostWithVat": 10000.00
            },
        ]
        for line in sales_report_lines:
            sales_report_line = models.SalesReportLine(**line)
            sales_report_line.save()
        sales_report_lines = models.SalesReportLine.objects.all()
        self.stdout.write(self.style.SUCCESS(
            'Строки отчета о розничной продаже созданы'))
        self.stdout.write(self.style.WARNING(
            'Создаем отчеты о розничной продаже'))
        sales_reports = [
            {
                'Header': sales_report_headers[0],
                'Lines': sales_report_lines
            },
            {
                'Header': sales_report_headers[1],
                'Lines': sales_report_lines
            },
            {
                'Header': sales_report_headers[2],
                'Lines': sales_report_lines
            },
        ]
        for sales_report in sales_reports:
            rea = models.SalesReport(
                Header=sales_report['Header']
            )
            rea.save()
            rea.Lines.set(sales_report['Lines'])
        sales_reports = models.SalesReport.objects.all()
        self.stdout.write(self.style.SUCCESS(
            'Отчеты о розничной продаже созданы'))
        self.stdout.write(self.style.SUCCESS(
            'ИНИЦИАЛИЗАЦИЯ ПРОВЕДЕНА УСПЕШНО'))
