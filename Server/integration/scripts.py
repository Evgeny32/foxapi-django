from .models import *
# from account.models import Account
# from franchisee.models import FranchiseeProfile
# from agent.models import AgentProfile
from django.shortcuts import get_object_or_404
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save, pre_delete, pre_delete


def make_partner(account_id):
    """Создание партнера при создании профиля франчайзи или агента"""
    from account.models import Account
    partner = get_object_or_404(Account, id=account_id)
    partner_data = {
        'Id': partner.id
    }
    if partner.role == 'Franchisee':
        from franchisee.models import FranchiseeProfile
        
        # franchisee_profile = get_object_or_404(
        #     FranchiseeProfile, user_id=partner.id)
        try: 
            franchisee_profile = FranchiseeProfile.objects.get(user_id=partner.id)
            partner_data = {
                **partner_data,
                'Type': 'Юрлицо',
                'Name': franchisee_profile.name,
                'INN': franchisee_profile.inn,
                'KPP': franchisee_profile. KPP
            }
        except FranchiseeProfile.DoesNotExist:
            pass
    elif partner.role == 'Agent':
        from agent.models import AgentProfile
        # agent_profile = get_object_or_404(
        #     AgentProfile, user_id=partner.id)
        try: 
            agent_profile = AgentProfile.objects.get(user_id=partner.id)
            partner_data = {
                **partner_data,
                'Type': 'Физлицо',
                'Name': '{0} {1} {2}'.format(partner.second_name, partner.name, partner.last_name),
                'INN': agent_profile.inn,
                'KPP': ''
            }
        except AgentProfile.DoesNotExist:
            pass
    new_partner = Partner(
        **partner_data
    )
    new_partner.save()
    make_bank_account(account_id)


def make_bank_account(account_id):
    """Создание банковского аккаунта при создании профиля франчайзи или агента"""
    from account.models import Account
    partner = get_object_or_404(Account, id=account_id)
    partner_model = get_object_or_404(Partner, Id=account_id)
    account_data = {
        'OwnerType': 'Контрагент',
        'OwnerId': partner_model,
        'Main': True
    }
    if partner.role == 'Franchisee':
        from franchisee.models import FranchiseeProfile
        try: 
            franchisee_profile = FranchiseeProfile.objects.get(user_id=partner.id)
        # franchisee_profile = get_object_or_404(
        #     FranchiseeProfile, user_id=account_id)
            account_data['Account'] = franchisee_profile.RS
            account_data['BIC'] = franchisee_profile.BIK
            bank_account = BankAccount(
                **account_data
            )
            bank_account.save()
        
        except FranchiseeProfile.DoesNotExist:
            pass


def delete_partner(account_id):
    """Удаление партнера при удалении аккаунта"""
    partner = get_object_or_404(Partner, Id=account_id)
    partner.IsDeleted = True
    partner.save()
    delete_bank_accounts(account_id)


def delete_bank_accounts(account_id):
    """Удаление банковского аккаунта при удалении аккаунта"""
    accounts = BankAccount.objects.filter(OwnerId=account_id)
    for account in accounts:
        account.IsDeleted = True
        account.save()
