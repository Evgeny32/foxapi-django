from rest_framework.views import APIView
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND
from rest_framework.permissions import IsAuthenticated, AllowAny, BasePermission
from rest_framework.decorators import permission_classes
from rest_framework.response import Response
from django.http import FileResponse
from django.shortcuts import get_object_or_404
from .models import *
from .serializers import *
from django.conf import settings
import os


class KeyOnly(BasePermission):
    def has_permission(self, request, view):
        try:
            result = request.headers['ApiKey'] == settings.SECRET_KEY
            return result
        except KeyError:
            return False


def index(request):
    base = str(settings.BASE_DIR)
    html = open(base + '/account/templates/account/1c.html', 'rb')
    response = FileResponse(html)
    return response


class OrganizationView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Организация """

    def get(self, request):
        orgs = Partner.objects.filter(IsOrganization=True)
        Id = request.query_params.get('Id')
        if Id is not None:
            orgs = orgs.filter(Id=Id)
        serializer = OrganizationSerializer(orgs, many=True)
        return Response(serializer.data, status=HTTP_200_OK)


class PartnersView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Партнеры """

    def get(self, request):
        partners = Partner.objects.filter(IsOrganization=False)
        Id = request.query_params.get('Id')
        if Id is not None:
            partners = partners.filter(Id=Id)
        serializer = PartnerSerializer(partners, many=True)
        return Response(serializer.data, status=HTTP_200_OK)


# class PartnerView(APIView):
# permission_class=[AllowAny]
#     """ Партнер по id """

#     def get(self, request, Id):
#         partner = get_object_or_404(Partner, Id=Id)
#         serializer = PartnerSerializer(partner)
#         return Response(serializer.data, status=HTTP_200_OK)

class BankAccountsView(APIView):
    permission_classes = [KeyOnly]
    # permission_classes = [AllowAny]  # ! Поменять потом!
    """ Банковские счета """

    def get(self, request):
        bank_accounts = BankAccount.objects.all()
        OwnerId = request.query_params.get('OwnerId')
        if OwnerId is not None:
            bank_accounts = bank_accounts.filter(OwnerId=OwnerId)
        serializer = BankAccountSerializer(bank_accounts, many=True)
        return Response(serializer.data, status=HTTP_200_OK)


class ServicesView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Номенклатура (услуги) """

    def get(self, request):
        services = Service.objects.all()
        Id = request.query_params.get('Id')
        if Id is not None:
            services = services.filter(Id=Id)
        serializer = ServiceSerializer(services, many=True)
        return Response(serializer.data, status=HTTP_200_OK)


class ContractsView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Договоры """

    def get(self, request):
        contracts = Contract.objects.all()
        Cont_Id = request.query_params.get('Cont_Id')
        Cont_Org = request.query_params.get('Cont_Org')
        Cont_Owner = request.query_params.get('Cont_Owner')
        if Cont_Id is not None:
            contracts = contracts.filter(Cont_Id=Cont_Id)
        if Cont_Org is not None:
            contracts = contracts.filter(Cont_Org=Cont_Org)
        if Cont_Owner is not None:
            contracts = contracts.filter(Cont_Owner=Cont_Owner)
        serializer = ContractSerializer(contracts, many=True)
        return Response(serializer.data, status=HTTP_200_OK)


class InvoicesView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Счета на оплату """

    def get(self, request):
        invoices = Invoice.objects.all()
        Doc_Id = request.query_params.get('Doc_Id')
        doc_data1 = request.query_params.get('doc_data1')
        doc_data2 = request.query_params.get('doc_data2')
        Id_Org = request.query_params.get('Id_Org')
        Id_Partner = request.query_params.get('Id_Partner')
        if Doc_Id is not None:
            invoices = invoices.filter(Header__Doc_Id=Doc_Id)
        if Id_Org is not None:
            invoices = invoices.filter(Header__Id_Org=Id_Org)
        if Id_Partner is not None:
            invoices = invoices.filter(Header__Id_Partner=Id_Partner)
        if doc_data1 is not None:
            invoices = invoices.filter(Header__doc_data__gte=doc_data1)
        if doc_data2 is not None:
            invoices = invoices.filter(Header__doc_data__lte=doc_data2)

        res_data = {
            'Invoices': set(),
            'Services': set(),
            'Organizations': set(),
            'Partners': set(),
            'Contracts': set()
        }
        res_data['Invoices'] = set(invoices)
        for invoice in res_data['Invoices']:
            res_data['Organizations'].add(invoice.Header.Id_Org)
            res_data['Partners'].add(invoice.Header.Id_Partner)
            res_data['Contracts'].add(invoice.Header.Id_dog)
            for line in invoice.Lines.all():
                res_data['Services'].add(line.Service_Id)
        serializer = InvoiceResponseSerializer(res_data)
        return Response(serializer.data, status=HTTP_200_OK)


class RealizationsView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Реализации услуг """

    def get(self, request):
        realizations = Realization.objects.all()
        Doc_Id = request.query_params.get('Doc_Id')
        doc_data1 = request.query_params.get('doc_data1')
        doc_data2 = request.query_params.get('doc_data2')
        Id_Org = request.query_params.get('Id_Org')
        Id_Partner = request.query_params.get('Id_Partner')
        if Doc_Id is not None:
            realizations = realizations.filter(Header__Doc_Id=Doc_Id)
        if Id_Org is not None:
            realizations = realizations.filter(Header__Id_Org=Id_Org)
        if Id_Partner is not None:
            realizations = realizations.filter(Header__Id_Partner=Id_Partner)
        if doc_data1 is not None:
            realizations = realizations.filter(Header__doc_data__gte=doc_data1)
        if doc_data2 is not None:
            realizations = realizations.filter(Header__doc_data__lte=doc_data2)
        res_data = {
            'Realizations': set(),
            'Invoices': set(),
            'Services': set(),
            'Organizations': set(),
            'Partners': set(),
            'Contracts': set()
        }
        res_data['Realizations'] = list(realizations)
        for realization in res_data['Realizations']:
            res_data['Organizations'].add(realization.Header.Id_Org)
            res_data['Partners'].add(realization.Header.Id_Partner)
            res_data['Contracts'].add(realization.Header.Id_dog)
            res_data['Invoices'].add(realization.Header.Id_Invoice)
            res_data['Organizations'].add(
                realization.Header.Id_Invoice.Header.Id_Org)
            res_data['Partners'].add(
                realization.Header.Id_Invoice.Header.Id_Partner)
            res_data['Contracts'].add(
                realization.Header.Id_Invoice.Header.Id_dog)
            for line in realization.Header.Id_Invoice.Lines.all():
                res_data['Services'].add(line.Service_Id)
            for line in realization.Lines.all():
                res_data['Services'].add(line.Service_Id)

        serializer = RealizationResponseSerializer(res_data)
        return Response(serializer.data, status=HTTP_200_OK)


class ReceiptsView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Поступления услуг """

    def get(self, request):
        receipts = Receipt.objects.all()
        Doc_Id = request.query_params.get('Doc_Id')
        doc_data1 = request.query_params.get('doc_data1')
        doc_data2 = request.query_params.get('doc_data2')
        Id_Org = request.query_params.get('Id_Org')
        Id_Partner = request.query_params.get('Id_Partner')
        if Doc_Id is not None:
            receipts = receipts.filter(Header__Doc_Id=Doc_Id)
        if Id_Org is not None:
            receipts = receipts.filter(Header__Id_Org=Id_Org)
        if Id_Partner is not None:
            receipts = receipts.filter(Header__Id_Partner=Id_Partner)
        if doc_data1 is not None:
            receipts = receipts.filter(Header__doc_data__gte=doc_data1)
        if doc_data2 is not None:
            receipts = receipts.filter(Header__doc_data__lte=doc_data2)

        res_data = {
            'Receipts': set(),
            'Services': set(),
            'Organizations': set(),
            'Partners': set(),
            'Contracts': set()
        }
        res_data['Receipts'] = list(receipts)
        for receipt in res_data['Receipts']:
            res_data['Organizations'].add(receipt.Header.Id_Org)
            res_data['Partners'].add(receipt.Header.Id_Partner)
            res_data['Contracts'].add(receipt.Header.Id_dog)
            for line in receipt.Lines.all():
                res_data['Services'].add(line.Service_Id)
        serializer = ReceiptResponseSerializer(res_data)
        return Response(serializer.data, status=HTTP_200_OK)


class PaymentOrdersView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Платежные поручения """

    def get(self, request):
        payment_orders = PaymentOrder.objects.all()
        Doc_Id = request.query_params.get('Doc_Id')
        doc_data1 = request.query_params.get('doc_data1')
        doc_data2 = request.query_params.get('doc_data2')
        Id_Org = request.query_params.get('Id_Org')
        Id_Partner = request.query_params.get('Id_Partner')
        Account = request.query_params.get('Account')
        if Account is not None:
            payment_orders = payment_orders.filter(Account=Account)
        if Doc_Id is not None:
            payment_orders = payment_orders.filter(Doc_Id=Doc_Id)
        if Id_Org is not None:
            payment_orders = payment_orders.filter(Id_Org=Id_Org)
        if Id_Partner is not None:
            payment_orders = payment_orders.filter(Id_Partner=Id_Partner)
        if doc_data1 is not None:
            payment_orders = payment_orders.filter(doc_data__gte=doc_data1)
        if doc_data2 is not None:
            payment_orders = payment_orders.filter(doc_data__lte=doc_data2)
        res_data = {
            'PaymentOrders': set(),
            'Organizations': set(),
            'Partners': set(),
            'Contracts': set()
        }
        res_data['PaymentOrders'] = list(payment_orders)
        for payment_order in res_data['PaymentOrders']:
            res_data['Organizations'].add(payment_order.Id_Org)
            res_data['Partners'].add(payment_order.Id_Partner)
            res_data['Contracts'].add(payment_order.Id_dog)

        serializer = PaymentOrderResponseSerializer(res_data)
        return Response(serializer.data, status=HTTP_200_OK)


class SalesReportsView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Отчеты о розничных продажах """

    def get(self, request):
        sales_reports = SalesReport.objects.all()
        Doc_Id = request.query_params.get('Doc_Id')
        doc_data1 = request.query_params.get('doc_data1')
        doc_data2 = request.query_params.get('doc_data2')
        Id_Org = request.query_params.get('Id_Org')
        if Doc_Id is not None:
            sales_reports = sales_reports.filter(Header__Doc_Id=Doc_Id)
        if Id_Org is not None:
            sales_reports = sales_reports.filter(Header__Id_Org=Id_Org)
        if doc_data1 is not None:
            sales_reports = sales_reports.filter(
                Header__doc_data__gte=doc_data1)
        if doc_data2 is not None:
            sales_reports = sales_reports.filter(
                Header__doc_data__lte=doc_data2)
        res_data = {
            'SalesReports': set(),
            'Services': set(),
            'Organizations': set(),
        }
        res_data['SalesReports'] = list(sales_reports)
        for sales_report in res_data['SalesReports']:
            res_data['Organizations'].add(sales_report.Header.Id_Org)
            for line in sales_report.Lines.all():
                res_data['Services'].add(line.Service_Id)
        serializer = SalesReportResponseSerializer(res_data)
        return Response(serializer.data, status=HTTP_200_OK)


class IncomesView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Поступления денежных средств """

    def get(self, request):
        incomes = Income.objects.all()
        res_data = {
            'Incomes': set(),
            'Services': set(),
            'Organizations': set(),
            'Partners': set(),
            'Invoices': set(),
            'Services': set(),
            'Contracts': set()
        }
        res_data['Incomes'] = list(incomes)
        for income in res_data['Incomes']:
            res_data['Organizations'].add(income.Id_Org)
            res_data['Partners'].add(income.Id_Partner)
            res_data['Contracts'].add(income.Id_dog)
            res_data['Invoices'].add(income.Id_Invoice)
            for line in income.Id_Invoice.Lines.all():
                res_data['Services'].add(line.Service_Id)
        serializer = IncomeResponseSerializer(res_data)
        return Response(serializer.data, status=HTTP_200_OK)

    def post(self, request):
        incomes = request.data
        res_incomes = []
        for income in incomes:
            try:
                income_in_db = Income.objects.get(Doc_Id=income.Doc_Id)
                serializer = IncomeSerializer(income_in_db, data=income)
            except Income.DoesNotExist:
                serializer = IncomeSerializer(data=payment)
            if serializer.is_valid():
                serializer.save()
                res_incomes.append(serializer.data)
            else:
                return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
        serializer = IncomeSerializer(res_incomes)
       
        return Response(serializer.data, status=HTTP_201_CREATED)


class PaymentsView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Списания денежных средств """

    def get(self, request):
        payments = Payment.objects.all()
        res_data = {
            'Payments': set(),
            'Services': set(),
            'Organizations': set(),
            'Partners': set(),
            'Receipts': set(),
            'PaymentOrders': set(),
            'Services': set(),
            'Contracts': set()
        }
        res_data['Payments'] = list(payments)
        for payment in res_data['Payments']:
            res_data['Organizations'].add(payment.Id_Org)
            res_data['Partners'].add(payment.Id_Partner)
            res_data['Contracts'].add(payment.Id_dog)
            res_data['Receipts'].add(payment.Id_Receipt)
            res_data['Organizations'].add(payment.Id_Receipt.Header.Id_Org)
            res_data['Partners'].add(payment.Id_Receipt.Header.Id_Partner)
            res_data['Contracts'].add(payment.Id_Receipt.Header.Id_dog)
            res_data['PaymentOrders'].add(payment.Id_Order)
            res_data['Organizations'].add(payment.Id_Order.Id_Org)
            res_data['Partners'].add(payment.Id_Order.Id_Partner)
            res_data['Contracts'].add(payment.Id_Order.Id_dog)
            for line in payment.Id_Receipt.Lines.all():
                res_data['Services'].add(line.Service_Id)
        serializer = PaymentResponseSerializer(res_data)
        return Response(serializer.data, status=HTTP_200_OK)

    def post(self, request):
        payments = request.data
        res_payments = []
        for payment in payments:
            try:
                payment_in_db = Payment.objects.get(Doc_Id=payment.Doc_Id)
                serializer = PaymentSerializer(payment_in_db, data=payment)
            except Payment.DoesNotExist:
                serializer = PaymentSerializer(data=payment)
            if serializer.is_valid():
                serializer.save()
                res_payments.append(serializer.data)
            else:
                return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
        serializer = PaymentSerializer(res_payments)
       
        return Response(serializer.data, status=HTTP_201_CREATED)


class CatalogView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Справочник """

    def get(self, request):
        data = {}
        data['Organizations'] = Partner.objects.filter(IsOrganization=True)
        data['Partners'] = Partner.objects.filter(IsOrganization=False)
        data['BankAccounts'] = BankAccount.objects.all()
        data['Services'] = Service.objects.all()
        data['Contracts'] = Contract.objects.all()

        serializer = CatalogSerializer(data)
        return Response(serializer.data, status=HTTP_200_OK)


class DocumentsView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Документы """

    def get(self, request):
        data = {}
        data['Invoices'] = Invoice.objects.all()
        data['Realizations'] = Realization.objects.all()
        data['Receipts'] = Receipt.objects.all()
        data['PaymentOrders'] = PaymentOrder.objects.all()
        data['SalesReports'] = SalesReport.objects.all()

        serializer = DocumentsSerializer(data)
        return Response(serializer.data, status=HTTP_200_OK)


class DocumentsOutcomeView(APIView):
    # permission_classes = [KeyOnly]
    permission_classes = [AllowAny]  # ! Поменять потом!
    """ Документы от 1С """

    def get(self, request):
        data = {}
        data['Incomes'] = Income.objects.all()
        data['Payments'] = Payment.objects.all()

        serializer = DocumentsOutcomeSerializer(data)
        return Response(serializer.data, status=HTTP_200_OK)

    def post(self, request):
        serializer = DocumentsOutcomeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
