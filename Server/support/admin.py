from django.contrib import admin
from .models import SupportMessage, SupportTheme



@admin.register(SupportMessage)
class SupportMessageAdmin(admin.ModelAdmin):
    list_display = ('sended_at', 'support_text', )
admin.site.register(SupportTheme)
   
