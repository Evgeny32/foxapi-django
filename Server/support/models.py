from django.db import models
from account.models import Account

#! ПРОФИЛЬ ПОДДЕРЖКИ
#!!!!!!!!!!!!!!!!!!!!!!!


class SupportTheme(models.Model):
    creator = models.ForeignKey("account.Account", verbose_name="Создатель темы", on_delete=models.CASCADE)
    name = models.CharField('Название темы', max_length=50, default='Название темы')
    creator_name = models.CharField('Имя создателя темы', max_length=100, default="name")
    STATUS = (
        ('Новое', 'Новое'),
        ('Прочитано', 'Прочитано'),        
    )
    status = models.CharField(verbose_name="Статус",
                              choices=STATUS, max_length=20, default='Новое')
    
    class Meta:
        verbose_name = "Тема сообщений поддержки"
        verbose_name_plural = "Темы сообщений поддержки"

    def unicode(self):
        return self.name
class SupportMessage(models.Model):
    theme = models.ForeignKey("support.SupportTheme", verbose_name='Тема сообщения', on_delete=models.CASCADE, null=True)
    support_text = models.TextField(verbose_name="Текст сообшении", blank=True)
    sended_at = models.DateField(verbose_name="Дата создания", auto_now_add=True)
    sender = models.ForeignKey("account.Account", verbose_name='Отправитель', on_delete=models.SET_NULL, null=True)
    class Meta:
        verbose_name = "Сообщения для поддержки"
        verbose_name_plural = "Сообщении для поддержки"

    def unicode(self):
        return self.support_text
