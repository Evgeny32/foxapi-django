from account.models import Account
from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from support.models import SupportMessage, SupportTheme
from support.serializers import SupportSerializer, SupportThemeSerializer, SupportCreateSerializer
from rest_framework.response import Response
from rest_framework import serializers, status
from account.serializers import AccountProfileSerializer


#! Создание данных Поддержки филиала
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# @api_view(['POST', 'GET'])
# @permission_classes([IsAuthenticated, ])
# def branch_support_profile_post(request):

#     if request.method == 'POST':

#         request.data['_mutable'] = True
#         request.data['user_id'] = request.user.id
#         request.data['_mutable'] = False
#         serializer = BranchSupportSerializer(data=request.data)
#         data = {}
#         if serializer.is_valid():
#             serializer.save()
#             data['response'] = "Данные успешно добавлены."
#             return Response(data, status=status.HTTP_201_CREATED)
#         else:
#             data = serializer.errors
#             return Response(data, status=status.HTTP_400_BAD_REQUEST)


class SupportView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        msgs = []
        themes = SupportTheme.objects.filter(creator=request.user.id)
        for theme in themes:
            theme_msgs = SupportMessage.objects.filter(theme=theme.id)
            msgs += theme_msgs
        serializer = SupportSerializer(msgs, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        request.data['_mutable'] = True
        request.data['theme']['creator'] = request.user.id
        try:
            theme = SupportTheme.objects.get(name=request.data['theme']['name'], creator=request.user.id)
        except SupportTheme.DoesNotExist:
            serializer = SupportThemeSerializer(data=request.data['theme'])
            if serializer.is_valid():
                serializer.save()
                theme = SupportTheme.objects.get(name=request.data['theme']['name'], creator=request.user.id)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        request.data['msg']['theme'] = theme.id
        request.data['_mutable'] = False

        msg_serializer = SupportCreateSerializer(data=request.data['msg'])
        if msg_serializer.is_valid():
            msg_serializer.save()
            return Response({"response": "Данные успешно добавлены."}, status=status.HTTP_201_CREATED)
        return Response(msg_serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class AdminSupportView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        msgs = SupportMessage.objects.all()
        # themes = SupportTheme.objects.all()   
        # for theme in themes:
        #     msgsThemes = SupportMessage.objects.filter(theme=theme.id)
        #     msgs.theme = msgsThemes
        
        serializer = SupportSerializer(msgs, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        request.data['_mutable'] = True
        request.data['user_id'] = request.user.id
        request.data['_mutable'] = False
        serializer = SupportSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"response": "Данные успешно добавлены."}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AdminSupportThemeView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        theme = SupportTheme.objects.get(id=id)
        msgs = SupportMessage.objects.filter(theme=id)
        theme.msgs = msgs
        serializer = SupportThemeSerializer(theme)
        return Response(serializer.data)

    def post(self, request, id, format=None):
        request.data['_mutable'] = True
        request.data['user_id'] = request.user.id
        request.data['_mutable'] = False
        theme = SupportTheme.objects.get(id=id)
        theme.status = 'Прочитано';
        theme.save()
        request.data['theme'] = theme.id
        serializer = SupportCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"response": "Данные успешно добавлены."}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

 