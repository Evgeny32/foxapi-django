from rest_framework import serializers
from support.models import SupportMessage, SupportTheme
from account.serializers import AccountProfileSerializer


class SupportMsgsThemeSerializer(serializers.ModelSerializer):
    account = AccountProfileSerializer(source="creator")
    
    class Meta:
        model = SupportTheme
        fields = '__all__'


class SupportSerializer(serializers.ModelSerializer):
    # account = AccountProfileSerializer(source="creator")
    # theme = serializers.SlugRelatedField(slug_field="name", read_only=True)
    theme = SupportMsgsThemeSerializer()
    class Meta:
        model = SupportMessage
        fields = '__all__'

class SupportCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = SupportMessage
        fields = '__all__'





class SupportThemeSerializer(serializers.ModelSerializer):
    account = AccountProfileSerializer(source="creator", read_only=True)
    msgs = SupportSerializer(many=True, read_only=True)
    class Meta:
        model = SupportTheme
        fields = '__all__'