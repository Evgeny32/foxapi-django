from django.urls import path
from support.views import SupportView, AdminSupportView, AdminSupportThemeView


app_name = "support"

urlpatterns = [    
    path('', SupportView.as_view(), name="SupportView"),
    path('admin/', AdminSupportView.as_view(), name="AdminSupprtView"),
    path('admin/<int:id>', AdminSupportThemeView.as_view(), name="AdminSupportThemeView")
]
