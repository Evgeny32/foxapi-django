from rest_framework.response import Response
from child.models import ChildProfile
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import status
from django.db.models import Q
from tales.models import Tale
from mentor.models import MentorProfile, ChildGroup
from branch.models import BranchProfile
from account.models import Account
from account.serializers import AccountSerializer
from agent.models import AgentProfile
from agent.serializers import AgentSerializer
from franchisee.models import FranchiseeProfile, FranchiseePackage
from franchisee.serializers import BranchSerializer
from administration.models import CalcData
from administration.serializers import (
    AgentProfileSerializer,
    FranchiseeProfileSerializer,
    FranchiseePackageSerializer,
    ChildrenListSerializer,
    ChildDetailsSerializer,
    BranchListSerializer,
    FranchiseePackagePatchSerializer,
    FranchiseePackageSimpleSerializer,
    AllCountSerializer,
    AllChildPackageSerializer,
    CalcDataSerializer
)

from django.core.mail import send_mail, EmailMessage


# class ChildrenListView(APIView):
#     permission_classes = [IsAuthenticated]

#     def get(self, request):
#         try:
#             children = ChildProfile.objects.all()
#         except:
#             if ChildProfile.DoesNotExist:
#                 return Response(status=status.HTTP_404_NOT_FOUND)
#         serializer = ChildrenListSerializer(children, many=True)
#         return Response(serializer.data)


# ! CRM
class FranchiseePackageListView(APIView):
    def get(self, request, format=None):

        packages = FranchiseePackage.objects.all()
        for package in packages:
            try:
                package.franchisee = FranchiseeProfile.objects.get(
                    user_id=package.purchaser)
                package.agent = None
            except FranchiseeProfile.DoesNotExist:
                try:
                    package.agent = AgentProfile.objects.get(
                        user_id=package.purchaser)
                    package.franchisee = None
                except AgentProfile.DoesNotExist:
                    package.agent = None
                    package.franchisee = None

        serializer = FranchiseePackageSerializer(packages, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseeOtherPackageListView(APIView):
    def get(self, request, id, format=None):

        packages = FranchiseePackage.objects.filter(purchaser=id)
        for package in packages:
            try:
                package.franchisee = FranchiseeProfile.objects.get(
                    user_id=package.purchaser)
                package.agent = None
            except FranchiseeProfile.DoesNotExist:
                try:
                    package.agent = AgentProfile.objects.get(
                        user_id=package.purchaser)
                    package.franchisee = None
                except AgentProfile.DoesNotExist:
                    package.agent = None
                    package.franchisee = None

        serializer = FranchiseePackageSerializer(packages, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseePackagePatchView(APIView):

    def patch(self, request, id, format=None):

        packages = FranchiseePackage.objects.get(id=id)
        serializer = FranchiseePackagePatchSerializer(
            packages, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FranchiseePackageSendView(APIView):
    permission_classes = [AllowAny]

    def patch(self, request, id, format=None):

        package = FranchiseePackage.objects.get(id=id)
        serializer = FranchiseePackagePatchSerializer(
            package, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()

        return Response(status=status.HTTP_201_CREATED)


class AllCountView(APIView):

    def get(self, request):
        count = {}
        count['agents'] = len(Account.objects.filter(role="Agent"))
        count['franchisee'] = len(Account.objects.filter(role="Franchisee"))
        serializer = AllCountSerializer(count)
        return Response(serializer.data)


class AllChildPackageView(APIView):

    def get(self, request):
        count = {}
        count['children'] = len(Account.objects.filter(role="Child"))
        count['packages'] = len(FranchiseePackage.objects.filter(
            status=2)) + len(FranchiseePackage.objects.filter(status=5))
        serializer = AllChildPackageSerializer(count)
        return Response(serializer.data)

# ! CRM


# ! Администрирование
class AdminUserListView(APIView):
    def get(self, request):
        users = []
        users += Account.objects.filter(role="BuhAdmin")
        users += Account.objects.filter(role='HeadSalesManager')
        serializer = AccountSerializer(users, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = AccountSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors)


class CalcDataView(APIView):
    permission_classes = [AllowAny]

    def get(self, request):
        data = CalcData.objects.filter().first()
        serializer = CalcDataSerializer(data)
        return Response(seializer.data)

    def patch(self, request):
        data = CalcData.objects.filter().first()
        if(data):
            serializer = CalcDataSerializer(
                data, data=request.data, partial=True)
        else:
            serializer = CalcDataSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)


# ! Администрирование


class ChildrenListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):

        children = ChildProfile.objects.all()

        for child in children:
            tale = Tale.objects.filter(child=child.id).order_by(
                'number').first()  # * Достаем название последней сказки ребенка
            if tale:
                child.tale = tale.name
            else:
                child.tale = 'Сказка не найдена'
            try:
                mentor_profile = MentorProfile.objects.get(
                    mentor=child.mentor.id)
                try:
                    branch_profile = BranchProfile.objects.get(
                        user_id=mentor_profile.branch.id)
                    if branch_profile.franchisee.id == request.user.id:
                        child.is_my = True
                    else:
                        child.is_my = False
                except BranchProfile.DoesNotExist:
                    child.is_my = False
                    branch_profile = None
            except MentorProfile.DoesNotExist:
                mentor_profile = None
                child.is_my = False

        serializer = ChildrenListSerializer(children, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ChildDetailView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        try:
            child = ChildProfile.objects.get(id=id)
        except ChildProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        try:
            mentor_profile = MentorProfile.objects.get(mentor=child.mentor.id)
            try:
                branch = BranchProfile.objects.get(
                    user_id=mentor_profile.branch.id).name
            except BranchProfile.DoesNotExist:
                branch = ''
        except MentorProfile.DoesNotExist:
            branch = ''

        tale = Tale.objects.filter(child=child.id).order_by(
            'number').first()  # * Достаем название последней сказки ребенка
        if tale:
            tale = tale.name
        else:
            tale = 'Сказка не найдена'

        group = ChildGroup.objects.get(
            children__in=[child.id])  # * Достаем группу ребенка
        if group:
            group = group.name
        else:
            group = 'Группа не найдена'

        account = Account.objects.get(id=child.child.id)
        child.ban = account.ban

        # * Присваиваем все
        child.branch = branch
        child.tale = tale
        child.group = group

        serializer = ChildDetailsSerializer(child)
        return Response(serializer.data, status=status.HTTP_200_OK)


class BranchesOtherListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):  # * Вывод списка филиалов
        branches = BranchProfile.objects.filter(franchisee=id)
        for branch in branches:
            mentor_all = MentorProfile.objects.filter(branch=branch.user_id.id)
            children_all = 0
            group_all = 0
            for mentor in mentor_all:
                children = len(ChildProfile.objects.filter(
                    mentor=mentor.mentor.id))
                children_all += children
                groups = len(ChildGroup.objects.filter(
                    mentor=mentor.mentor.id))
                group_all += groups
            # print(mentor)
            branch.mentors = len(mentor_all)
            branch.children = children_all
            branch.groups = group_all
            branch.all = 0

        serializer = BranchListSerializer(
            branches, many=True)
        return Response(serializer.data)


class BranchesListView(APIView):  # * Вывод и создание филиалов
    permission_classes = [IsAuthenticated]

    # def post(self, request, format=None):  # * Создание филиала
    #     data = request.data.copy()
    #     data['franchisee'] = request.user.id
    #     serializer = BranchRegistrationSerializer(data=data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response({'response': 'Филиал успешно зарегистрирован.', 'data': serializer.data}, status=status.HTTP_201_CREATED)
    #     else:
    #         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, format=None):  # * Вывод списка филиалов
        branches = BranchProfile.objects.all()
        for branch in branches:
            mentor_all = MentorProfile.objects.filter(branch=branch.user_id.id)
            children_all = 0
            group_all = 0
            for mentor in mentor_all:
                children = len(ChildProfile.objects.filter(
                    mentor=mentor.mentor.id))
                children_all += children
                groups = len(ChildGroup.objects.filter(
                    mentor=mentor.mentor.id))
                group_all += groups
            # print(mentor)
            branch.mentors = len(mentor_all)
            branch.children = children_all
            branch.groups = group_all
            branch.all = 0

        serializer = BranchListSerializer(
            branches, many=True)
        return Response(serializer.data)


class MakeMainBranchView(APIView):

    permission_classes = [IsAuthenticated]

    def get_branch(self, id):
        try:
            branch = BranchProfile.objects.get(id=id)
        except BranchProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return branch

    def get_main_branch(self):
        try:
            branch = BranchProfile.objects.get(main=True)
        except BranchProfile.DoesNotExist:
            return False
        else:
            return branch

    def remove_main_tag_from_branch(self):
        main_branch = self.get_main_branch()
        if main_branch:
            main_branch_serializer = BranchSerializer(
                main_branch, data={'main': False}, partial=True)
            if main_branch_serializer.is_valid():
                main_branch_serializer.save()

    def patch(self, request, id, format=None):
        self.remove_main_tag_from_branch()
        branch = self.get_branch(id)
        branch_serializer = BranchSerializer(
            branch, data={'main': True}, partial=True)
        if branch_serializer.is_valid():
            branch_serializer.update(branch, request.data)
            return Response(branch_serializer.data)
        return Response(branch_serializer.errors)


class AgentsListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):

        try:
            agent = AgentProfile.objects.all()
        except:
            if AgentProfile.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = AgentProfileSerializer(agent, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class AgentView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        try:
            profile = AgentProfile.objects.get(user_id=id)
        except AgentProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = AgentSerializer(profile)
        return Response(serializer.data)

    def patch(self, request, id, format=None):
        try:
            agent = AgentProfile.objects.get(user_id=id)
        except AgentProfile.DoesNotExist:
            serializer = AgentSerializer(data=request.data)
        else:
            serializer = AgentSerializer(
                agent, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FranchiseeListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):

        try:
            franchisee = FranchiseeProfile.objects.all()
        except:
            if FranchiseeProfile.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = FranchiseeProfileSerializer(franchisee, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseeDetailsListView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, id, format=None):

        try:
            franchisee = FranchiseeProfile.objects.get(user_id=id)
        except FranchiseeProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = FranchiseeProfileSerializer(franchisee)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, id, format=None):
        try:
            profile = FranchiseeProfile.objects.get(user_id=id)
        except FranchiseeProfile.DoesNotExist:
            serializer = FranchiseeProfileSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            serializer = FranchiseeProfileSerializer(
                profile, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FranchiseePackageBlock(APIView):
    permission_classes = [IsAuthenticated]

    def get_packages(self, id):
        try:
            package = FranchiseePackage.objects.get(purchaser=id)
            return package
        except FranchiseePackage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def patch(self, request, id, format=None):
        # data = request.data.copy()
        package = self.get_packages(id)
        serializer = FranchiseePackageSimpleSerializer(
            package, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FranchiseePackageView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):

        packages = FranchiseePackage.objects.filter(purchaser=id)
        for package in packages:
            franchisee = FranchiseeProfile.objects.get(
                user_id=package.purchaser)
            package.franchisee = franchisee
            package.agent = None

        serializer = FranchiseePackageSerializer(packages, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
