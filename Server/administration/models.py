from django.db import models
from account.models import Account


class AdministrationProfile(models.Model):
    user_id = models.ForeignKey(
        Account, related_name='administration_account', verbose_name="Администратор", on_delete=models.CASCADE)

# Create your models here.


class CalcData(models.Model):
    kagent_LK_cost = models.PositiveIntegerField(
        'Стоимость ЛК для контрагента', default=0)
    kagent_franch_online_procent = models.PositiveIntegerField(
        "Процент франчайзи онлайн для контрагента", default=0)
    kagent_franch_offline_procent = models.PositiveIntegerField(
        "Процент франчайзи офлайн для контрагента", default=0)
    kagent_procent = models.PositiveIntegerField(
        "Процент контрагента", default=0)

    mentor_LK_online = models.PositiveIntegerField(
        'Оплата ментору за ЛК онлайн', default=0)
    mentor_LK_offline = models.PositiveIntegerField(
        'Оплата ментору за ЛК офлайн', default=0)
    mentor_coach_procent = models.PositiveIntegerField(
        "Процент ментора за тренинг", default=0)
