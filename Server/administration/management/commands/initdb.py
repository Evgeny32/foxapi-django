from django.core.management.base import BaseCommand
import os
from account.models import Account
from branch.models import BranchProfile
from mentor.models import MentorProfile, ChildGroup


class Command(BaseCommand):
    finish = ""

    def handle(self, *args, **options):
        try:
            os.remove('db.sqlite3')
        except FileNotFoundError:
            pass
        os.system(
            'find . -path "*/migrations/*.py" -not -name "__init__.py" -delete')
        os.system('find . -path "*/migrations/*.pyc"  -delete')
        os.system('pip uninstall Django')
        os.system('pip install Django')
        os.system('python manage.py makemigrations')
        os.system('python manage.py migrate')
        # Аккаунт администратора
        self.stdout.write(self.style.WARNING('Создаем администратора'))
        admin_account = Account(
            email="admin@mail.ru",
            is_active=True,
            is_admin=True,
            is_staff=True,
            role='Admin',
            is_superuser=True,
        )
        admin_account.set_password('7777')
        admin_account.save()
        self.finish += f'Админ: {admin_account.email}\n'
        # Бухгалтер администратора
        # Филиал администратора
        self.stdout.write(self.style.WARNING('Создаем филиал администратора'))
        admin_branch_account = Account(
            email="admin-branch@mail.ru",
            is_active=True,
            role="Branch",
            phone="admin-branch-phone-1"
        )
        admin_branch_account.set_password('7777')
        admin_branch_account.save()
        admin_branch_profile = BranchProfile(
            user_id=admin_branch_account,
            name="Филиал администратора",
            franchisee=admin_account,
            region="Регион филиала",
            director="Директор филиала",
            main=True
        )
        admin_branch_profile.save()
        self.finish += f'Филиал: {admin_branch_account.email}\n'
        # Аккаунт ментора
        self.stdout.write(self.style.WARNING('Создаем ментора администратора'))
        admin_mentor_account = Account(
            second_name="Менторов",
            name="Очень",
            last_name="Люблю",
            email="admin-mentor@mail.ru",
            is_active=True,
            role="Mentor",
            phone="admin-mentor-phone-1"
        )
        admin_mentor_account.set_password('7777')
        admin_mentor_account.save()
        admin_mentor_profile = MentorProfile(
            mentor=admin_mentor_account,
            branch=admin_branch_account,
            job_status='Трудовой договор',
            passport="12-34 567890",
            passport_who="МВД РОССИИ",
            passport_when="2021-08-08",
            adress="Адрес"
        )
        admin_mentor_profile.save()
        self.finish += f'Ментор: {admin_mentor_account.email}\n'
        # Группы ментора
        self.stdout.write(self.style.WARNING('Создаем группы ментора'))
        mentor_first_step_group = ChildGroup(
            name="Онлайн группа № 1 (5-7 лет)",
            age_group='5-7',
            mentor=admin_mentor_account,
            online=True
        )
        mentor_first_step_group.save()
        self.finish += f'Группа 1: {mentor_first_step_group.name}\n'
        mentor_first_idea_group = ChildGroup(
            name="Онлайн группа № 1 (8-12 лет)",
            age_group='8-12',
            mentor=admin_mentor_account,
            online=True
        )
        mentor_first_idea_group.save()
        self.finish += f'Группа 2: {mentor_first_idea_group.name}\n'
        mentor_first_business_group = ChildGroup(
            name="Онлайн группа № 1 (13-16 лет)",
            age_group='13-16',
            mentor=admin_mentor_account,
            online=True
        )
        mentor_first_business_group.save()
        self.finish += f'Группа 3: {mentor_first_business_group.name}\n'
        self.stdout.write(self.style.SUCCESS(
            'Инициализация завершена успешно'))
        self.stdout.write(self.finish)
