from django.core.management.base import BaseCommand
import os
from account.models import Account
from agent.models import AgentProfile
from franchisee.models import FranchiseeProfile
from branch.models import BranchProfile
class Command(BaseCommand):
    users_list = []

    def handle(self, *args, **options):
        agent_account = self.make_agent()
        org_franchisee_account = self.make_org_franchisee()
        self.make_franchisee_branch(org_franchisee_account)
        ip_franchisee_account = self.make_ip_franchisee()
        self.make_franchisee_branch(ip_franchisee_account)
        
        

        # Аккаунт родителя
        self.stdout.write(self.style.WARNING('Создаем родителя'))
        parent_account = Account(
            email="parent@mail.ru",
            is_active=True,
            role='Parent',
            phone="parent-phone-1"
        )
        parent_account.set_password('7777')
        parent_account.save()
        self.users_list.append(parent_account.email)

        # self.stdout.write(self.style.WARNING('Создаем ребенка онлайн ребенка'))


    def make_agent():
        self.stdout.write(self.style.WARNING('Создаем агента'))
        agent_account = Account(
            email="agent@mail.ru",
            is_active=True,
            role='Agent',
            phone="agent-phone-1"
        )
        agent_account.set_password('7777')
        agent_account.save()
        agent_profile = AgentProfile(
            user_id=agent_account,
            birthday="2021-08-08",
            citizen="Российская Федерация",
            education="Высшее",
            emailBuch="agent-buh@mail.ru",
            passport="12-34 567890",
            passportWho="МВД РОССИИ",
            passportWhen="2021-08-08",
            country="Российская Федерация",
            adress="Москва",
            factAdress="Москва",
            inn="7674822873647",
            snils="8329492384"
        )
        agent_profile.save()
        return agent_account
    def make_org_franchisee():
        self.stdout.write(self.style.WARNING('Создаем франчайзи Организацию без агента'))
        franchisee_org_account = Account(
            email="franchisee-org@mail.ru",
            is_active=True,
            role='Franchisee',
            phone="franch-phone-1"
        )
        franchisee_org_account.set_password('7777')
        franchisee_org_account.save()
        self.stdout.write(self.style.WARNING('Создаем бухгалтера франчайзи Организации без агента'))
        franchisee_org_buh = Account(
            email="franchisee-org-buh@mail.ru",
            is_active=True,
            role='BuhFranch',
            phone="franch-buh-phone-1"
        )
        franchisee_org_buh.set_password('7777')
        franchisee_org_buh.save()
        franchisee_org_profile = FranchiseeProfile(
            user_id=franchisee_org_account,
            name='ООО "Маленькая Надежда"',
            inn="324252345345",
            OGRN="234512512525",
            KPP="123412341242434",
            passport="12-34 567890",
            passportWho="МВД РОССИИ",
            passportWhen="2021-08-08",
            emailBuch=franchisee_org_buh,
            director="ФИО Директора",
            director_post="Директор",
            organization=True,
            urAdress="Юр Адрес",
            adress="Адрес",
            RS="27439827384723894",
            CS="32414123412342342",
            bank="Банк",
            BIK="12342341234"
        )
        franchisee_org_profile.save()
        return franchisee_org_account
        
    def make_ip_franchisee():
        self.stdout.write(self.style.WARNING('Создаем франчайзи ИП без агента'))
        franchisee_ip_account = Account(
            email="franchisee-ip@mail.ru",
            is_active=True,
            role='Franchisee',
            phone="franch-phone-2"
        )
        franchisee_ip_account.set_password('7777')
        franchisee_ip_account.save()
        self.stdout.write(self.style.WARNING('Создаем бухгалтера франчайзи ИП без агента'))
        franchisee_ip_buh = Account(
            email="franchisee-ip-buh@mail.ru",
            is_active=True,
            role='BuhFranch',
            phone="franch-buh-phone-2"
        )
        franchisee_ip_buh.set_password('7777')
        franchisee_ip_buh.save()
        franchisee_ip_profile = FranchiseeProfile(
            user_id=franchisee_ip_account,
            name='ИП "Гаврилов Сергей Николаевич"',
            inn="324252345345",
            OGRN="234512512525",
            KPP="123412341242434",
            passport="12-34 567890",
            passportWho="МВД РОССИИ",
            passportWhen="2021-08-08",
            emailBuch=franchisee_ip_buh,
            director="ФИО Директора",
            director_post="Директор",
            organization=True,
            urAdress="Юр Адрес",
            adress="Адрес",
            RS="27439827384723894",
            CS="32414123412342342",
            bank="Банк",
            BIK="12342341234"
        )
        franchisee_ip_profile.save()
        return franchisee_ip_account

    def make_org_franchisee_branch(org_franchisee_account):
        self.stdout.write(self.style.WARNING('Создаем филиал франчайзи'))
        branch_account = Account(
            email="org-branch@mail.ru",
            is_active=True,
            role='BuhFranch',
            phone="franch-buh-phone-2"
        )

