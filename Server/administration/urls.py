from django.urls import path
from administration.views import (
    ChildrenListView,
    ChildDetailView,
    BranchesListView,
    AgentsListView,
    FranchiseeListView,
    FranchiseePackageListView,
    FranchiseeDetailsListView,
    FranchiseePackageView,
    FranchiseePackageBlock,
    AgentView,
    AllCountView,
    AllChildPackageView,
    AdminUserListView,
    CalcDataView,
    MakeMainBranchView,
    BranchesOtherListView,
    FranchiseePackagePatchView,
    FranchiseeOtherPackageListView,
    FranchiseePackageSendView
)

app_name = "administration"

# urlpatterns = [
#     path("", name="AdminProfileView")
# ]

urlpatterns = [
    path("children/", ChildrenListView.as_view(), name="ChildrenListView"),
    path("child/<int:id>/", ChildDetailView.as_view(), name="ChildDetailView"),
    path("branches/", BranchesListView.as_view(), name="BranchesListView"),
    path("branches/<int:id>", BranchesOtherListView.as_view(), name="BranchesOtherListView"),
]


urlpatterns += [
    path("agents/", AgentsListView.as_view(), name="AgentsListView"),
    path("all/", AllCountView.as_view(), name="all_count_view"),
    path("child-package/", AllChildPackageView.as_view(),
         name="all_child_package_view"),
    path('agent/<int:id>/', AgentView.as_view(), name="agent_view"),
    path("franchisee/", FranchiseeListView.as_view(), name="FranchiseeListView"),
    path("franchisee/<int:id>/", FranchiseeDetailsListView.as_view(),
         name="FranchiseeDetailsListView"),
    path("packages/", FranchiseePackageListView.as_view(),
         name="FranchiseePackageListView"),
     path("packages/franchisee/<int:id>", FranchiseeOtherPackageListView.as_view(),
         name="FranchiseePackageListView"),
    path("packages/<int:id>/", FranchiseePackagePatchView.as_view(),
         name="FranchiseePackageListView"),
    path("package/<int:id>/", FranchiseePackageView.as_view(),
         name="FranchiseePackageView"),
    path("package/<int:id>/block/", FranchiseePackageBlock.as_view(),
         name="FranchiseePackageBlock")
]

urlpatterns += [
    path('user/', AdminUserListView.as_view(), name="admin_user_list_view"),
    path('calc/', CalcDataView.as_view(), name="calc_data_view"),

]

urlpatterns += [
     path('branch/main/<int:id>/', MakeMainBranchView.as_view(), name="MakeMainBranchView")
]


urlpatterns += [
     path('childpackages/<int:id>/', FranchiseePackageSendView.as_view(), name="FranchiseePackageSendView")
]