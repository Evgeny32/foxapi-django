from django.apps import AppConfig


class AdminConfig(AppConfig):
    name = 'administration'
    verbose_name = '0) Расчетные данные'
