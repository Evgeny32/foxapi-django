from administration.models import AdministrationProfile
from re import S
from django.db.models import fields
from agent.models import AgentProfile
from rest_framework import serializers
from child.models import ChildProfile
from account.models import Account
from branch.models import BranchProfile
from franchisee.models import FranchiseeProfile, FranchiseePackage
from .models import CalcData
from account.serializers import AccountSerializer


class CalcDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = CalcData
        fields = '__all__'


class AllChildPackageSerializer(serializers.Serializer):
    children = serializers.IntegerField()
    packages = serializers.IntegerField()


class AllCountSerializer(serializers.Serializer):
    agents = serializers.IntegerField()
    franchisee = serializers.IntegerField()


class FIOSerializer(serializers.ModelSerializer):  # *
    class Meta:
        model = Account
        fields = ['id', 'name', 'second_name', 'last_name', 'email', 'phone']


class ChildrenListSerializer(serializers.ModelSerializer):

    tale = serializers.CharField()
    child = FIOSerializer()
    is_my = serializers.BooleanField()

    class Meta:
        model = ChildProfile
        fields = ['id', 'child', 'age', 'tale',
                  'rank', 'creation_date', 'is_my', 'learning']


class ChildDetailsSerializer(serializers.ModelSerializer):

    branch = serializers.CharField()
    mentor = FIOSerializer()
    parent = FIOSerializer()
    child = FIOSerializer()
    group = serializers.CharField()
    tale = serializers.CharField()
    ban = serializers.BooleanField()

    class Meta:
        model = ChildProfile
        fields = ['id', 'child', 'age', 'parent',
                  'branch', 'mentor', 'group', 'tale', 'rank', 'ban']


class AccountBanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'ban', 'ban_cause']


class FranchiseePackageSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseePackage
        fields = '__all__'


class BranchListSerializer(serializers.ModelSerializer):  # * Список филиалов

    mentors = serializers.IntegerField()
    children = serializers.IntegerField()
    groups = serializers.IntegerField()
    all = serializers.IntegerField()
    account = AccountBanSerializer(source="user_id")
    package = FranchiseePackageSimpleSerializer()

    class Meta:
        model = BranchProfile
        fields = ['id', 'account', 'mentors', 'children',
                  'groups', 'all', 'name', 'region', 'main', 'package']


class AgentProfileSerializer(serializers.ModelSerializer):

    user_id = FIOSerializer()

    class Meta:
        model = AgentProfile
        fields = "__all__"


class FranchiseeProfileSerializer(serializers.ModelSerializer):

    user_id = FIOSerializer(read_only=True)
    emailBuch = AccountSerializer(read_only=True)

    class Meta:
        model = FranchiseeProfile
        fields = "__all__"


class FranchiseePackageSerializer(serializers.ModelSerializer):

    # purchaser = FIOSerializer()
    franchisee = FranchiseeProfileSerializer()

    # franchisee = serializers.CharField()

    class Meta:
        model = FranchiseePackage
        fields = '__all__'


class FranchiseePackagePatchSerializer(serializers.ModelSerializer):

    class Meta:
        model = FranchiseePackage
        fields = '__all__'
