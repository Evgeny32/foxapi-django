"""foxapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from .views import index

urlpatterns = [
    path('api/v1/', index, name="docs"),
    path('admin/', admin.site.urls),
    path('api/v1/account/', include('account.urls')),
    path('api/v1/coachings/', include('coach.urls')),
    path('api/v1/child/', include('child.urls')),
    path('api/v1/mentor/', include('mentor.urls')),
    path('api/v1/tales/', include('tales.urls')),
    path('api/v1/agent/', include('agent.urls')),
    path('api/v1/branch/', include('branch.urls')),
    path('api/v1/support/', include('support.urls')),
    path('api/v1/franchisee/', include('franchisee.urls')),
    path('api/v1/administration/', include('administration.urls')),
    path('api/v1/admin-buch/', include('admin_buch.urls')),
    path('api/v1/franch-buch/', include('franch_buch.urls')),
    path('api/v1/clientmanager/', include('clientManager.urls')),
    path('api/v1/salesmanager/', include('salesManager.urls')),
    path('api/v1/headsalesmanager/', include('headSalesManager.urls')),

    path('api/v1/notifications/', include('notifications.urls')),
    path('api/v1/terms/', include('child.terms_urls')),
    path('api/v1/cloud/', include('cloud.urls')),
    path('api/v1/achievements/', include('achievements.urls')),
    path('integration/', include('integration.urls')),
    path('api/v1/stickers/', include('stickers.urls')),
    path('api/v1/certificates/', include('certificates.urls')),
    path('api/v1/chat/', include('chat.urls')),
    path('api/v1/transactions/', include('transactions.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
