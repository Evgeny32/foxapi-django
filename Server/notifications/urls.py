from django.urls import path
from .views import NotificationsView, NotificationView, LastNotificationsView

urlpatterns = [
    path('', NotificationsView.as_view(), name='NotificationsView'),
    path('last/', LastNotificationsView.as_view(), name="LastNotificationsView"),
    path('<int:id>', NotificationView.as_view(), name='NotificationView'),
]
