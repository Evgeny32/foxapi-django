from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.


class Notification(models.Model):
    """Уведомление"""
    user = models.ForeignKey("account.Account", verbose_name=_(
        "Пользователь"), on_delete=models.CASCADE)
    title = models.CharField(_("Титул"), max_length=100)
    description = models.TextField(_("Описание"))
    type = models.CharField(_("Тип уведомления"), max_length=50)
    link = models.URLField(_("Ссылка для кнопки"), max_length=200)
    date = models.DateTimeField(_("Время события"), auto_now_add=True)
    new = models.BooleanField(_("Новое"), default=True)

    def unicode(self):
        return self.title

    class Meta:
        verbose_name = 'Уведомление'
        verbose_name_plural = 'Уведомления'
