from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from .models import Notification
from .serializers import NotificationSerializer
# Create your views here.


class NotificationsView(APIView):
    """Отображение списка и создание уведомлений"""
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """Получение пользователем собственных уведомлений"""
        notifications = Notification.objects.filter(user=request.user.id)
        serializer = NotificationSerializer(notifications, many=True)
        return Response({'msg': 'Notifications received', 'data': serializer.data}, status=status.HTTP_200_OK)

    def post(self, request):
        """Создание уведомления"""
        serializer = NotificationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': "Notification created", 'data': serializer.data}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LastNotificationsView(APIView):
    """ Получение пользователем пяти последних новых уведомлений """
    permission_classes = [IsAuthenticated]

    def get(self, request):
        notifications = Notification.objects.filter(
            user=request.user.id, new=True).order_by('date').reverse()[:5]
        serializer = NotificationSerializer(notifications, many=True)
        return Response({'msg': 'Notifications received', 'data': serializer.data}, status=status.HTTP_200_OK)


class NotificationView(APIView):
    """Отображение, изменение и удаление уведомлений"""
    permission_classes = [IsAuthenticated]

    def get_notification(self, id):
        """Получение уведомления"""
        try:
            notification = Notification.objects.get(id=id)
        except Notification.DoesNotExist:
            return Response({'msg': "Not found"}, status=status.HTTP_404_NOT_FOUND)
        return notification

    def get(self, request, id):
        """Отображение уведомления"""
        notification = self.get_notification(id)
        serializer = NotificationSerializer(notification)
        return Response({'msg': 'Notification received', 'data': serializer.data}, status=status.HTTP_200_OK)

    def patch(self, request, id):
        """Изменение уведомления"""
        notification = self.get_notification(id)
        serializer = NotificationSerializer(
            notification, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(
                notification, validated_data=serializer.validated_data)
            return Response({'msg': 'Notification updated', 'data': serializer.data}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        """Удаление уведомления"""
        notification = self.get_notification(id)
        notification.delete()
        return Response({'msg': 'Notification deleted'}, status=status.HTTP_200_OK)
