from django.contrib import admin
from .models import Coaching
# Register your models here.




@admin.register(Coaching)
class CoachAdmin(admin.ModelAdmin):
    list_display = ('coaching_name', 'cost', 'date_start', 'date_end')
    fieldsets = [        
        ('Тренинг', {'fields': ['coaching_name', 'mentor', 'description', 'cost', 'date_start', 'date_end']}),
        ('Покупатели', {'fields':['customers',]}),
    ]





