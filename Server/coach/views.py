from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from rest_framework.views import APIView
from .models import Coaching
from account.models import Account
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, AllowAny
from .serializers import CoachingListSerializer, CustomerRegisterSerializer, CoachingSerializer


class CoachingsView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        coachings = Coaching.objects.filter(mentor=request.user.id)
        serializer = CoachingSerializer(coachings, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CoachingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)


class CoachingView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        coaching = get_object_or_404(Coaching, id=id)
        serializer = CoachingSerializer(coaching)
        return Response(serializer.data)

    def patch(self, request, id):
        coaching = get_object_or_404(Coaching, id=id)
        serializer = CoachingSerializer(
            coaching, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(coaching, validated_data=request.data)
            return rResponse(serializer.data)
        return Response(serializer.errors, status=400)

    def delete(self, request, id):
        coaching = get_object_or_404(Coaching, id=id)
        coaching.delete()
        return Response({'msg': "Coaching deleted"})


class CoachingUnsubscribeView(APIView):
    permission_classes = [IsAuthenticated]

    def patch(self, request, id):
        coaching = get_object_or_404(Coaching, id=id)
        coaching.customers.remove(request.user.id)
        coaching.save()
        serializer = CoachingSerializer(coaching)
        return Response(serializer.data)


class CoachingSubscribeView(APIView):
    permission_classes = [IsAuthenticated]

    def patch(self, request, id):
        coaching = get_object_or_404(Coaching, id=id)
        coaching.customers.add(request.user.id)
        coaching.save()
        serializer = CoachingSerializer(coaching)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
def free_coaching_view_list(request):
    if request.method == "GET":
        coachings = Coaching.objects.all().exclude(customers__id=request.user.id)
        serializer = CoachingListSerializer(coachings, many=True)
        return Response(serializer.data)


@api_view(['PUT', ])
@permission_classes([IsAuthenticated, ])
def customer_register_view(request, id):
    try:
        coaching = Coaching.objects.get(id=id)
    except Coaching.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'PUT':
        customers = coaching.customers.all()
        data_request = {
            'customers': {}
        }
        for customer in customers:
            data_request['customers'][customer.id] = customer.id

        data_request['customers'][request.user.id] = request.user.id
        serializer = CustomerRegisterSerializer(coaching, data=data_request)
        data = {}
        if serializer.is_valid():
            serializer.save()
            data['response'] = "Успешно обновлено."
            return Response(data=serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
def taken_coaching_view_list(request):
    if request.method == "GET":
        coachings = Coaching.objects.filter(customers__id=request.user.id)
        serializer = CoachingListSerializer(coachings, many=True)
        return Response(serializer.data)


@api_view(['PUT', ])
@permission_classes([IsAuthenticated, ])
def customer_unregister_view(request, id):
    try:
        coaching = Coaching.objects.get(id=id)
    except Coaching.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'PUT':
        customers = coaching.customers.all()
        data_request = {
            'customers': {}
        }
        for customer in customers:
            data_request['customers'][customer.id] = customer.id
        del data_request['customers'][request.user.id]
        serializer = CustomerRegisterSerializer(coaching, data=data_request)
        data = {}
        if serializer.is_valid():
            serializer.save()
            data['response'] = "Успешно обновлено."
            return Response(data=serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# @api_view(['GET', 'PUT'])
# @permission_classes([IsAuthenticated,])
# def coaching_view_detail(request):
#     account = request.user

#     try:
#         avatar = Account.objects.get(id=account.id)
#     except Account.DoesNotExist:
#         return Response(status=status.HTTP_404_NOT_FOUND)

#     if request.method == 'GET':
#         serializer = AccountAvatarSerializer(avatar)
#         return Response(serializer.data)
#     if request.method == 'PUT':
#         serializer = AccountAvatarSerializer(avatar, data=request.data)
#         data = {}
#         if serializer.is_valid():
#             serializer.save()
#             data['response'] = "Успешно обновлено."
#             return Response(data=data)
#         return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)
