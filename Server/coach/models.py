from django.db import models
from account.models import Account


# Create your models here.


class Coaching(models.Model):
    """Тренинг"""
    id = models.AutoField(primary_key=True)
    mentor = models.ForeignKey(Account, related_name='mentors_coaching',
                               verbose_name="Ментор", null=True, blank=True, on_delete=models.SET_NULL)
    coaching_name = models.CharField(
        max_length=50, verbose_name="Название", blank=True, unique=True)
    description = models.TextField(verbose_name="Описание")
    cost = models.IntegerField(verbose_name="Стоимость")
    date_created = models.DateTimeField(
        verbose_name="Дата создания", auto_now_add=True)
    date_start = models.DateTimeField(verbose_name="Время начала")
    date_end = models.DateTimeField(verbose_name="Время конца")
    customers = models.ManyToManyField(Account, verbose_name="Заказчик")

    class Meta:
        verbose_name = "Тренинг"
        verbose_name_plural = "Тренинги"
