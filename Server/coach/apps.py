from django.apps import AppConfig


class CoachConfig(AppConfig):
    name = 'coach'
    verbose_name = '9) Тренинги'
