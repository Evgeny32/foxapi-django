from django.urls import path
from .views import (
    free_coaching_view_list,
    customer_register_view,
    taken_coaching_view_list,
    # customer_unregister_view
    CoachingsView, CoachingView, CoachingSubscribeView, CoachingUnsubscribeView
)


app_name = 'coach'


urlpatterns = [
    path('', CoachingsView.as_view(), name="CoachingsView"),
    path('free/', free_coaching_view_list, name="free_coachings"),
    path('taken/', taken_coaching_view_list, name="taken_coachings"),
    path('<int:id>', CoachingView.as_view(), name="CoachingView"),
    path('<int:id>/subscribe', CoachingSubscribeView.as_view(), name="CoachingView"),
    path('<int:id>/unsubscribe',
         CoachingUnsubscribeView.as_view(), name="CoachingView"),
]
