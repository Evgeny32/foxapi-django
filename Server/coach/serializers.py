from rest_framework import serializers
from .models import Coaching
from account.models import Account
from account.serializers import AccountProfileSerializer


class CoachingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coaching
        fields = '__all__'


class CoachingListSerializer(serializers.ModelSerializer):
    mentor = AccountProfileSerializer()

    class Meta:
        model = Coaching
        exclude = ['date_created', ]


class CustomerRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coaching
        fields = ['customers']
