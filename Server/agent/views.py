from rest_framework.decorators import api_view, permission_classes
from .models import AgentProfile
from franchisee.models import FranchiseeProfile, FranchiseePackage
from franchisee.serializers import PackageDetailsSerializer
from account.models import Account
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .serializers import AgentSerializer, AgentPatchSerializer
from rest_framework import status

from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny


class AgentView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        try:
            profile = AgentProfile.objects.get(user_id=request.user.id)
        except AgentProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = AgentSerializer(profile)
        return Response(serializer.data)

    def patch(self, request, format=None):
        try:
            agent = AgentProfile.objects.get(user_id=request.user)
        except AgentProfile.DoesNotExist:
            serializer = AgentPatchSerializer(data=request.data)

        else:
            serializer = AgentPatchSerializer(
                agent, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()

            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AgentDetailsView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, id):
        try:
            profile = AgentProfile.objects.get(user_id=id)
        except AgentProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = AgentSerializer(profile)
        return Response(serializer.data)


class PackageListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        package_list = []
        franchisee_list = FranchiseeProfile.objects.filter(
            agent=request.user.id)
        for franchisee in franchisee_list:
            packages = FranchiseePackage.objects.filter(
                purchaser=franchisee.user_id.id, status=2)
            for package in packages:
                package.franchisee = franchisee
            package_list = package_list + list(packages)

        serializer = PackageDetailsSerializer(package_list, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
