from django.apps import AppConfig


class AgentConfig(AppConfig):
    name = 'agent'
    verbose_name="7) Агент"
