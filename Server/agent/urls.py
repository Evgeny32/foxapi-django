from django.urls import path

from .views import (AgentView, AgentDetailsView, PackageListView)


app_name = "agent"

urlpatterns = [
    path('', AgentView.as_view(), name="AgentView"),
    path('<int:id>', AgentDetailsView.as_view(), name="AgentDetailsView"),
    path('packages', PackageListView.as_view(), name="PackageListView")
]
