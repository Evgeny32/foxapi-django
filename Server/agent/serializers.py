from rest_framework import serializers
from .models import AgentProfile
from account.serializers import AccountSerializer


class AgentSerializer(serializers.ModelSerializer):
    user_id = AccountSerializer(read_only=True)    

    class Meta:
        model = AgentProfile
        fields = '__all__'


class AgentPatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgentProfile
        fields = '__all__'
