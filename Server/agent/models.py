from django.db import models
from account.models import Account
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save, pre_delete
from django.utils.translation import ugettext_lazy as _


class AgentProfile(models.Model):
    def agent_upload_path(instance, filename):
        return '{0}/{1}'.format(instance.user_id.email, filename)

    user_id = models.ForeignKey(
        Account, related_name='agent_account', verbose_name="Агент", on_delete=models.CASCADE)
    birthday = models.DateField(verbose_name="Дата рождения")
    citizen = models.CharField(
        verbose_name="Гражданство", max_length=20, blank=True)
    education = models.CharField(
        verbose_name="Образование", max_length=20, blank=True)
    emailBuch = models.EmailField(
        verbose_name="email бухгалтерии", max_length=60, unique=True)
    passport = models.CharField(
        verbose_name="Серия/номер пасспорта", max_length=20)
    passportWho = models.CharField(
        verbose_name="Кем выдан пасспорт", max_length=20, blank=True)
    passportWhen = models.DateField(verbose_name="Дата выдачи пасспорта")
    country = models.CharField(
        verbose_name="Страна", max_length=20, blank=True)
    adress = models.CharField(
        max_length=500,  verbose_name="Адрес прописки", blank=True, null=True)
    factAdress = models.CharField(
        max_length=500, verbose_name="Фактический адрес", blank=True, null=True)
    inn = models.CharField(verbose_name="ИНН", max_length=20)
    snils = models.CharField(verbose_name="Снилс", max_length=20)

    passportPDF = models.FileField(
        upload_to=agent_upload_path, verbose_name="Пасспорт PDF")
    snilsPDF = models.FileField(
        upload_to=agent_upload_path, verbose_name="СНИЛС PDF")
    innPDF = models.FileField(
        upload_to=agent_upload_path, verbose_name="ИНН PDF")
    resumePDF = models.FileField(
        upload_to=agent_upload_path, verbose_name="Анкета агента PDF")
    clients = models.ManyToManyField(
        "account.Account", related_name='agentClients', verbose_name=_("Клиенты"), blank=True)
    # contractPDF = models.FileField(
    #     upload_to=agent_upload_path, verbose_name="Договор агента PDF")

    class Meta:
        verbose_name = "Профиль агента"
        verbose_name_plural = "Профили агентов"

    def unicode(self):
        return self.user_id.second_name + ' ' + self.user_id.name


@receiver(pre_save, sender=AgentProfile)
def delete_old_image(sender, instance=None, **kwargs):
    if instance.passportPDF:
        try:
            old_file = AgentProfile.objects.get(pk=instance.pk).passportPDF
        except AgentProfile.DoesNotExist:
            return
        else:
            new_file = instance.passportPDF

            if old_file != new_file:
                old_file.delete(save=False)

    if instance.snilsPDF:
        try:
            old_file = AgentProfile.objects.get(pk=instance.pk).snilsPDF
        except AgentProfile.DoesNotExist:
            return
        else:
            new_file = instance.snilsPDF

            if old_file != new_file:
                old_file.delete(save=False)

    if instance.innPDF:
        try:
            old_file = AgentProfile.objects.get(pk=instance.pk).innPDF
        except AgentProfile.DoesNotExist:
            return
        else:
            new_file = instance.innPDF

            if old_file != new_file:
                old_file.delete(save=False)

    if instance.resumePDF:
        try:
            old_file = AgentProfile.objects.get(pk=instance.pk).resumePDF
        except AgentProfile.DoesNotExist:
            return
        else:
            new_file = instance.resumePDF

            if old_file != new_file:
                old_file.delete(save=False)

    # if instance.contractPDF:
    #     try:
    #         old_file = AgentProfile.objects.get(pk=instance.pk).contractPDF
    #     except AgentProfile.DoesNotExist:
    #         return
    #     else:
    #         new_file = instance.contractPDF

    #         if old_file != new_file:
    #             old_file.delete(save=False)
