from django.db import models

class SalesManagerProfile(models.Model):
    
    user_id = models.ForeignKey("account.Account", related_name='sales_manager_account', verbose_name="Менеджер продаж", on_delete=models.CASCADE)
    agents = models.ManyToManyField("account.Account", verbose_name=("Агенты"), related_name="salesManagerAgents", blank=True)
    franchisees = models.ManyToManyField("account.Account", verbose_name=("Франчайзи"), related_name="salesManagerFranchisees",  blank=True)

    class Meta:
        verbose_name = "Профиль менеджера продаж"
        verbose_name_plural = "Профили менеджера продаж"

    def unicode(self):
        return self.user_id.second_name + ' ' + self.user_id.name