from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from salesManager.models import SalesManagerProfile
from account.models import Account
from rest_framework.response import Response
from salesManager.serializers import SalesManagerSerializer
from rest_framework import status
from django.shortcuts import get_object_or_404, render

from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny

from franchisee.serializers import FranchiseeProfileSerializer, ClientsFranchiseeSerializer
from agent.serializers import AgentSerializer
from account.serializers import AccountProfileSerializer, AccountsSerializer
from agent.models import AgentProfile
from franchisee.models import FranchiseeProfile, FranchiseePackage
from franchisee.serializers import PatchFranchiseeProfileSerializer

from administration.serializers import (
    AgentProfileSerializer,
    FranchiseeProfileSerializer,
    FranchiseePackageSerializer,
    ChildrenListSerializer,
    ChildDetailsSerializer,
    BranchListSerializer,
    FranchiseePackagePatchSerializer,
    FranchiseePackageSimpleSerializer,
    AllCountSerializer,
    AllChildPackageSerializer,
    CalcDataSerializer
)


class SalesManagerDetailsView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, id):
        try:
            profile = SalesManagerProfile.objects.get(user_id=id)
        except SalesManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = SalesManagerSerializer(profile)
        return Response(serializer.data)


# !ОТДЕЛ ПРОФИЛЯ МЕНЕДЖЕРА ПО ПРОДАЖАМ


class SalesManagerProfileView(APIView):
    """Просмотр, создание и изменение профиля менеджера по продажам"""
    permission_classes = [IsAuthenticated]

    def get_profile(self, id):
        try:
            profile = SalesManagerProfile.objects.get(user_id=id)
            return profile
        except SalesManagerProfile.DoesNotExist:
            return None

    def get(self, request, format=None):
        profile = self.get_profile(request.user.id)
        if(profile):
            serializer = SalesManagerSerializer(profile)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def post(self, request, format=None):
        request.data['_mutable'] = True
        request.data['user_id'] = request.user.id
        request.data['_mutable'] = False
        serializer = SalesManagerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'response': 'Данные успешно добавлены.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, format=None):
        profile = self.get_profile(request.user.id)
        if(profile):
            serializer = SalesManagerSerializer(
                profile, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.update(profile, validated_data=request.data)
                return Response({'response': 'Успешно обновлено.'})
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

# !ОТДЕЛ ПРОФИЛЯ МЕНЕДЖЕРА ПО ПРОДАЖАМ

# !ОТДЕЛ ФРАНЧАЙЗИ


class FranchiseeView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = SalesManagerProfile.objects.get(user_id=id)
        except SalesManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        franchisees = []
        for franch in profile.franchisees.all():
            try:
                franchprofile = FranchiseeProfile.objects.get(
                    user_id=franch.id)
                franchisees.append(franchprofile)
            except FranchiseeProfile.DoesNotExist:
                pass
        serializer = ClientsFranchiseeSerializer(franchisees, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseeByIdView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = SalesManagerProfile.objects.get(user_id=id)
        except SalesManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        franchisees = []
        for franch in profile.franchisees.all():
            try:
                franchprofile = FranchiseeProfile.objects.get(
                    user_id=franch.id)
                franchisees.append(franchprofile)
            except FranchiseeProfile.DoesNotExist:
                pass
        serializer = ClientsFranchiseeSerializer(franchisees, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseeDetailView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):

        franchisee = get_object_or_404(FranchiseeProfile, id=id)

        serializer = ClientsFranchiseeSerializer(franchisee)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, format=None):
        try:
            profile = FranchiseeProfile.objects.get(user_id=request.user.id)
        except FranchiseeProfile.DoesNotExist:
            serializer = PatchFranchiseeProfileSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            try:
                buh = Account.objects.get(email=request.data['emailBuch'])
            except:
                None
            else:
                request.data['_mutable'] = True
                request.data['emailBuch'] = buh.id
                request.data['_mutable'] = False
            serializer = PatchFranchiseeProfileSerializer(
                profile, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# !ОТДЕЛ ФРАНЧАЙЗИ

# !ОТДЕЛ АГЕНТА

class AgentsView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = SalesManagerProfile.objects.get(user_id=id)
        except SalesManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        agents = []
        for agent in profile.agents.all():
            try:
                agentprofile = AgentProfile.objects.get(user_id=agent.id)
                agents.append(agentprofile)
            except AgentProfile.DoesNotExist:
                pass
        serializer = AgentSerializer(agents, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class AgentsByIdView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = SalesManagerProfile.objects.get(user_id=id)
        except SalesManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        agents = []
        for agent in profile.agents.all():
            try:
                agentprofile = AgentProfile.objects.get(user_id=agent.id)
                agents.append(agentprofile)
            except AgentProfile.DoesNotExist:
                pass
        serializer = AgentSerializer(agents, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class AgentDetailView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            agent = AgentProfile.objects.get(id=id)
        except AgentProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = AgentSerializer(agent)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, id, format=None):
        try:
            agent = AgentProfile.objects.get(id=id)
        except AgentProfile.DoesNotExist:
            serializer = AgentSerializer(data=request.data)
        else:
            serializer = AgentSerializer(
                agent, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# !ОТДЕЛ АГЕНТА

# !CRM

class AllCountView(APIView):

    def get(self, request):
        count = {}
        count['agents'] = len(Account.objects.filter(role="Agent"))
        count['franchisee'] = len(Account.objects.filter(role="Franchisee"))
        serializer = AllCountSerializer(count)
        return Response(serializer.data)


class AllCountByIdView(APIView):

    def get(self, request, id):
        count = {}
        count['agents'] = len(Account.objects.filter(role="Agent"))
        count['franchisee'] = len(Account.objects.filter(role="Franchisee"))
        serializer = AllCountSerializer(count)
        return Response(serializer.data)


class FranchiseePackageListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):

        try:
            profile = SalesManagerProfile.objects.get(user_id=id)
        except SalesManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        packages = []
        for franch in profile.franchisees.all():

            packages += FranchiseePackage.objects.filter(purchaser=franch.id)

        for package in packages:
            try:
                package.franchisee = FranchiseeProfile.objects.get(
                    user_id=package.purchaser)
                package.agent = None
            except FranchiseeProfile.DoesNotExist:
                try:
                    package.agent = AgentProfile.objects.get(
                        user_id=package.purchaser)
                    package.franchisee = None
                except AgentProfile.DoesNotExist:
                    package.agent = None
                    package.franchisee = None

        serializer = FranchiseePackageSerializer(packages, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseePackageListByIdView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):

        try:
            profile = SalesManagerProfile.objects.get(user_id=id)
        except SalesManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        packages = []
        for franch in profile.franchisees.all():

            packages += FranchiseePackage.objects.filter(purchaser=franch.id)

        for package in packages:
            try:
                package.franchisee = FranchiseeProfile.objects.get(
                    user_id=package.purchaser)
                package.agent = None
            except FranchiseeProfile.DoesNotExist:
                try:
                    package.agent = AgentProfile.objects.get(
                        user_id=package.purchaser)
                    package.franchisee = None
                except AgentProfile.DoesNotExist:
                    package.agent = None
                    package.franchisee = None

        serializer = FranchiseePackageSerializer(packages, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseePackagePatchView(APIView):

    def patch(self, request, id, format=None):

        packages = FranchiseePackage.objects.get(id=id)
        serializer = FranchiseePackagePatchSerializer(
            packages, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# !CRM
