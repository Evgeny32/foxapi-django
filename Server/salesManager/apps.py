from django.apps import AppConfig


class SalesmanagerConfig(AppConfig):
    name = 'salesManager'
    verbose_name="11) Менеджер продаж"
