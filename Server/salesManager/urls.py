from django.urls import path
from salesManager.views import (

    SalesManagerProfileView,
    FranchiseeView,
    FranchiseeDetailView,
    AgentsView,
    AgentDetailView,
    AllCountView,
    FranchiseePackageListView,
    FranchiseePackagePatchView,
    SalesManagerDetailsView,
    FranchiseePackageListByIdView,
    AllCountByIdView,
    FranchiseeByIdView,
    AgentsByIdView
)


app_name = "salesmanager"


# * Профиль
urlpatterns = [
    path('', SalesManagerProfileView.as_view(), name="SalesManagerProfileView"),
    path('<int:id>', SalesManagerDetailsView.as_view(),
         name="SalesManagerDetailsView"),

]

# * Франчайзи
urlpatterns += [
    path('franchisees/<int:id>/', FranchiseeView.as_view(), name="FranchiseeView"),
    path('<int:id>/franchisees/', FranchiseeByIdView.as_view(),
         name="FranchiseeByIdView"),
    path('franchisee/<int:id>/', FranchiseeDetailView.as_view(),
         name="FranchiseeDetailView"),
]


# * Агент
urlpatterns += [
    path('agents/<int:id>/', AgentsView.as_view(), name="AgentsView"),
    path('<int:id>/agents/', AgentsByIdView.as_view(), name="AgentsByIdView"),
    path('agent/<int:id>/', AgentDetailView.as_view(), name="AgentDetailView"),
]


# * CRM

urlpatterns += [
    path('packages/<int:id>/', FranchiseePackageListView.as_view(),
         name="FranchiseePackageListView"),
    path('<int:id>/packages/', FranchiseePackageListByIdView.as_view(),
         name="FranchiseePackageListByIdView"),
    path('package/<int:id>/', FranchiseePackagePatchView.as_view(),
         name="FranchiseePackagePatchView"),
    path('all/', AllCountView.as_view(), name="AllCountView"),
    path('<int:id>/all/', AllCountByIdView.as_view(), name="AllCountByIdView"),

]
