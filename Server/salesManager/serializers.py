from rest_framework import serializers
from .models import SalesManagerProfile
from account.serializers import AccountSerializer


class SalesManagerSerializer(serializers.ModelSerializer):
    user_id = AccountSerializer(read_only=True)

    class Meta:
        model = SalesManagerProfile
        fields = '__all__'

class FranchiseeSerializer(serializers.ModelSerializer):    

    class Meta:
        model = SalesManagerProfile
        fields = ["franchisees"]