from django.db import models
from django.db.models.fields import IntegerField
from account.models import Account
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save, pre_delete, pre_delete
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from integration.scripts import make_partner, delete_partner
#! ПРОФИЛЬ ФРАНЧАЙЗИ
#!!!!!!!!!!!!!!!!!!!!!!!


class FranchiseeProfile(models.Model):

    def franchisee_upload_path(instance, filename):
        return '{0}/{1}'.format(instance.user_id.email, filename)

    user_id = models.ForeignKey(
        Account, related_name='franchisee_account', verbose_name="Франщиза", on_delete=models.CASCADE)
    agent = models.ForeignKey("account.Account", verbose_name='Агент',
                              related_name="franchisee_agent", on_delete=models.CASCADE, null=True, blank=True)
    manager = models.ForeignKey("account.Account", verbose_name='Менеджер',
                                related_name="franchisee_manager", on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(verbose_name="Наименование франчайзера",
                            max_length=50, blank=True)
    inn = models.CharField(verbose_name="ИНН франчайзера",
                           max_length=15, blank=True)
    OGRN = models.CharField(verbose_name="ОГРН франчайзера",
                            max_length=25, blank=True)
    KPP = models.CharField(verbose_name="КПП франчайзера",
                           max_length=25, blank=True)
    passport = models.CharField(verbose_name="Пасспорт франчайзера",
                                max_length=25, blank=True)
    passportWho = models.CharField(verbose_name="Кем выдан пасспорт",
                                   max_length=25, blank=True)
    passportWhen = models.DateField(
        verbose_name="Дата выдачи пасспорта", blank=True, null=True)
    emailBuch = models.ForeignKey(
        Account, related_name='franchisee_buch_account', verbose_name="Бухгалтер франщизы", on_delete=models.CASCADE, blank=True, null=True)
    director = models.CharField(verbose_name="Директор",
                                max_length=50, blank=True)
    director_post = models.CharField(verbose_name="Должность директора",
                                     max_length=50, blank=True)
    ip = models.BooleanField(verbose_name="ИП", default=False)
    organization = models.BooleanField(
        verbose_name="Организация", default=False)

    urAdress = models.CharField(verbose_name="Юридический адрес франчайзера",
                                max_length=50, blank=True)
    adress = models.CharField(verbose_name="Адрес франчайзера",
                              max_length=50, blank=True)
    RS = models.CharField(verbose_name="Р/С франчайзера",
                          max_length=50, blank=True)
    CS = models.CharField(verbose_name="К/С франчайзера",
                          max_length=50, blank=True)
    bank = models.CharField(verbose_name="Банк франчайзера",
                            max_length=50, blank=True)
    BIK = models.CharField(verbose_name="БИК франчайзера",
                           max_length=50, blank=True)

    passportPDF = models.FileField(
        upload_to=franchisee_upload_path, verbose_name="Пасспорт PDF", null=True, blank=True)
    decisionPDF = models.FileField(
        upload_to=franchisee_upload_path, verbose_name="Решение PDF", null=True, blank=True)
    protocolPDF = models.FileField(
        upload_to=franchisee_upload_path, verbose_name="Протокол PDF", null=True, blank=True)
    organizationPDF = models.FileField(
        upload_to=franchisee_upload_path, verbose_name="Устав организации", null=True, blank=True)
    IP_PDF = models.FileField(
        upload_to=franchisee_upload_path, verbose_name="СНИЛС PDF", null=True, blank=True)
    innPDF = models.FileField(
        upload_to=franchisee_upload_path, verbose_name="ИНН PDF", null=True, blank=True)
    OQRNIP_PDF = models.FileField(
        upload_to=franchisee_upload_path, verbose_name="ОГРНИП PDF", null=True, blank=True)
    resumePDF = models.FileField(
        upload_to=franchisee_upload_path, verbose_name="Анкета франчайзи PDF", null=True, blank=True)
    contractPDF = models.FileField(
        upload_to=franchisee_upload_path, verbose_name="Договор франчайзи PDF", null=True, blank=True)

    def unicode(self):
        return self.name

    class Meta:
        verbose_name = "Профиль франчайза"
        verbose_name_plural = "Профили франчайзов"


class FranchiseePackage(models.Model):

    def package_upload_path(instance, filename):
        return '{0}'.format(filename)

    purchaser = models.ForeignKey(
        "account.Account", verbose_name="Покупатель", on_delete=models.SET_NULL, null=True)
    NAMES = {
        (0, 'Детский сад'),
        (1, 'Начальная школа'),
        (2, 'Школа')
    }
    name = models.PositiveIntegerField('Название', choices=NAMES)
    child_count = models.PositiveIntegerField('Количество детей')
    FORMATS = {
        (0, 'Offline'),
        (1, 'Online')
    }
    format = models.PositiveIntegerField('Формат франшизы', choices=FORMATS)
    STATUSES = {
        (0, 'В обработке'),
        (1, 'Повторная'),
        (2, 'В работе'),
        (3, 'Документы отправлены'),
        (4, 'Отклонена'),
        (5, 'Счет оплачен'),
        (6, 'Заблокирована'),
        (7, 'Ожидается оплата')
    }
    status = models.PositiveIntegerField(
        'Статус заявки', choices=STATUSES, default=0)

    date = models.DateField('Дата подачи заявки',
                            auto_now=False, auto_now_add=True)

    block_reason = models.CharField(verbose_name="Причина блокировки",
                                    max_length=50, blank=True)
    price = models.FloatField(_("Цена пакета"), default=0.0)
    count = models.IntegerField(_("Количество пакетов"), default=1)

    contract_file = models.FileField(
        upload_to=package_upload_path, verbose_name="Договор PDF", null=True, blank=True)
    bill_file = models.FileField(
        upload_to=package_upload_path, verbose_name="Счет PDF", null=True, blank=True)
    act_file = models.FileField(
        upload_to=package_upload_path, verbose_name="Акт PDF", null=True, blank=True)

    class Meta:
        verbose_name = "Пакет франчайза"
        verbose_name_plural = "Пакеты франчайзов"


@receiver(post_save, sender=FranchiseeProfile)
def franchisee_post_save(sender, instance=None, created=False, **kwargs):
    if not instance.user_id.ban:
        make_partner(instance.user_id.id)
    else:
        delete_partner(instance.user_id.id)


@receiver(pre_delete, sender=FranchiseeProfile)
def franchisee_pre_delete(sender, instance=None, created=False, **kwargs):
    delete_partner(instance.user_id.id)
