from django.contrib import admin
from .models import FranchiseeProfile, FranchiseePackage

admin.site.register(FranchiseePackage)


@admin.register(FranchiseeProfile)
class FranchiseeFields(admin.ModelAdmin):
    list_display = ('user_id', 'name', 'adress', 'agent')
