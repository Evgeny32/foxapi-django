from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import serializers, status
from account.serializers import AccountSerializer
from .models import FranchiseeProfile, FranchiseePackage
from administration.serializers import FranchiseePackagePatchSerializer


from .serializers import (
    BranchListSerializer,
    BranchSerializer,
    BranchRegistrationSerializer,
    FranchiseeProfileSerializer,
    MentorListSerializer,
    MentorProfileSerializer,
    MentorGroupListSerializer,
    BranchChildListSerializer,
    BranchChildSerializer,
    MentorProfileSimpleSerializer,
    AccountBanSerializer,
    FranchiseePackageSerializer,
    FranchiseeAllInfoSerializer,
    PatchFranchiseeProfileSerializer,
    FranchiseeBuhSerializer,
    PackageDetailsSerializer, BranchAdminRegistrationSerializer
)

from branch.models import BranchProfile
from mentor.models import MentorProfile, ChildGroup
from coach.models import Coaching
from child.models import ChildProfile
from tales.models import Tale

from account.models import Account
from account.serializers import AccountSerializer
from django.core.mail import send_mail, EmailMessage


# * Филиалы

# ! ОТДЕЛ ФИЛИАЛОВ

class BranchListView(APIView):  # * Вывод и создание филиалов
    permission_classes = [IsAuthenticated]

    def post(self, request, format=None):  # * Создание филиала
        serializer = BranchRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'response': 'Филиал успешно зарегистрирован.', 'data': serializer.data}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, format=None):  # * Вывод списка филиалов
        branches = BranchProfile.objects.filter(franchisee=request.user.id)
        for branch in branches:
            mentor_all = MentorProfile.objects.filter(branch=branch.user_id.id)
            children_all = 0
            group_all = 0
            for mentor in mentor_all:
                children = len(ChildProfile.objects.filter(
                    mentor=mentor.mentor.id))
                children_all += children
                groups = len(ChildGroup.objects.filter(
                    mentor=mentor.mentor.id))
                group_all += groups
            branch.mentors = len(mentor_all)
            branch.children = children_all
            branch.groups = group_all
            branch.all = 0

        serializer = BranchListSerializer(
            branches, many=True)
        return Response(serializer.data)


class BranchAdminListView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, format=None):  # * Создание филиала
        serializer = BranchAdminRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'response': 'Филиал успешно зарегистрирован.', 'data': serializer.data}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BranchView(APIView):  # * Просмотр данных по одному филиалу
    permission_classes = [IsAuthenticated]

    def get_branch(self, id):
        try:
            branch = BranchProfile.objects.get(user_id=id)
            return branch
        except BranchProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id, format=None):
        branch = self.get_branch(id)

        serializer = BranchSerializer(
            branch)
        return Response(serializer.data)

    def patch(self, request, id, format=None):
        branch = self.get_branch(id)

        serializer = BranchSerializer(
            branch, data=request.data, partitial=True)
        if serializer.is_valid:
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors)


# * Филиалы


class FranchBuchAccount(APIView):

    def post(self, request):
        serializer = FranchiseeBuhSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors)


class FranchBuchAccountPatchView(APIView):

    def patch(self, request, id):
        buch = Account.objects.get(id=id)
        serializer = AccountSerializer(buch, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(buch, validated_data=request.data)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors)


class FranchiseeProfileView(APIView):
    permission_classes = [IsAuthenticated]

    def get_profile(self, id):
        try:
            profile = FranchiseeProfile.objects.get(user_id=id)
            return profile
        except FranchiseeProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, format=None):
        try:
            profile = FranchiseeProfile.objects.get(user_id=request.user.id)
        except FranchiseeProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        profile.email = profile.user_id.email
        serializer = FranchiseeProfileSerializer(profile)
        return Response(serializer.data)

    def put(self, request, format=None):
        try:
            profile = FranchiseeProfile.objects.get(user_id=request.user.id)
        except FranchiseeProfile.DoesNotExist:
            serializer = PatchFranchiseeProfileSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            try:
                buh = Account.objects.get(email=request.data['emailBuch'])
            except:
                None
            else:
                request.data['_mutable'] = True
                request.data['emailBuch'] = buh.id
                request.data['_mutable'] = False
            serializer = PatchFranchiseeProfileSerializer(
                profile, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, format=None):
        request.data['_mutable'] = True
        request.data['user_id'] = request.user.id
        request.data['_mutable'] = False
        serializer = FranchiseeProfileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'response': 'Данные успешно добавлены.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, format=None):
        profile = self.get_profile(request.user.id)
        if(profile):
            serializer = FranchiseeProfileSerializer(
                profile, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.update(profile, validated_data=request.data)
                return Response({'response': 'Успешно обновлено.'})
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


# * Бан/разбан филиала, его менторов и детей
class BanBranchView(APIView):
    permission_classes = [IsAuthenticated]

    def get_account(self, id):
        try:
            account = Account.objects.get(id=id)
        except Account.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return account

    def patch(self, request, id, format=None):
        ban_accounts = []
        account = self.get_account(id)
        serializer = AccountBanSerializer(
            account, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            ban_accounts.append(serializer.data)
            mentors = MentorProfile.objects.filter(branch=id)
            if not len(mentors):
                return Response(ban_accounts)
            else:
                for mentor in mentors:

                    mentor_account = self.get_account(mentor.mentor.id)

                    serializer = AccountBanSerializer(
                        mentor_account, data=request.data, partial=True)
                    if serializer.is_valid():
                        serializer.update(
                            mentor_account, validated_data=request.data)
                        ban_accounts.append(serializer.data)
                        children = ChildProfile.objects.filter(
                            mentor=mentor_account.id)
                        if not len(children):
                            return Response(ban_accounts)
                        else:
                            for child in children:
                                child_account = self.get_account(
                                    child.child.id)

                                serializer = AccountBanSerializer(
                                    child_account, data=request.data, partial=True)
                                if serializer.is_valid():
                                    serializer.update(
                                        child_account, validated_data=request.data)
                                    ban_accounts.append(serializer.data)
                                    return Response(ban_accounts)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

   # ! ОТДЕЛ ФИЛИАЛОВ

   # ! ОТДЕЛ МЕНТОРОВ

# * Отображение всех менторов филиала по id филиала


class MentorListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):  # * Список
        mentors = MentorProfile.objects.filter(branch=id)

        for mentor in mentors:
            mentor.children = len(ChildProfile.objects.filter(
                mentor=mentor.mentor.id))  # * Колличество детей ментора
            mentor.trainings = len(
                Coaching.objects.filter(mentor=mentor.mentor.id))  # * Колличество тренингов ментора
            mentor.groups = len(ChildGroup.objects.filter(
                mentor=mentor.mentor.id))  # * Колличество групп ментора
            mentor.all = 0  # * Оборот ментора
        serializer = MentorListSerializer(
            mentors, many=True)
        return Response(serializer.data)


# * Отображение одного ментора по его id
class MentorView(APIView):
    permission_classes = [IsAuthenticated]

    def get_mentor(self, id):
        try:
            mentor = MentorProfile.objects.get(branch=id)
            return mentor
        except MentorProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id, format=None):
        mentor = self.get_mentor(id)
        serializer = MentorProfileSerializer(
            mentor)
        return Response(serializer.data)


# * Отображение групп ментора по его id
class MentorGroupListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        mentors = MentorProfile.objects.filter(branch=id)

        for mentor in mentors:
            groups = ChildGroup.objects.filter(mentor=mentor.mentor.id)

        serializer = MentorGroupListSerializer(groups, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


# * Увольнение менторов по id
class MentorBan(APIView):
    permission_classes = [IsAuthenticated]

    def get_mentor(self, id):
        try:
            mentor = MentorProfile.objects.get(id=id)
            return mentor
        except MentorProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def patch(self, request, id, format=None):

        mentor = self.get_mentor(id)
        serializer = MentorProfileSimpleSerializer(
            mentor, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# ! ОТДЕЛ МЕНТОРОВ


# ! ОТДЕЛ РЕБЕНКА


# * Получение всех детей филиала
class BranchChildListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        mentors = MentorProfile.objects.filter(branch=id)

        children = []
        for mentor in mentors:
            child_list = ChildProfile.objects.filter(mentor=mentor.mentor.id)
            for child in child_list:
                tale = Tale.objects.filter(child=child.id).order_by(
                    'number').first()  # * Достаем название последней сказки ребенка
                if tale:
                    child.tale = tale.name
                else:
                    child.tale = 'Сказка не найдена'
            children += child_list
        serializer = BranchChildListSerializer(children, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


# * Отображение одного ребенка
class BranchChildView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        child = ChildProfile.objects.get(mentor=id)
        branch = BranchProfile.objects.get(user_id=request.user.id).name
        tale = Tale.objects.filter(child=child.id).order_by(
            'number').first()  # * Достаем название последней сказки ребенка
        if tale:
            tale = tale.name
        else:
            tale = 'Сказка не найдена'

        group = ChildGroup.objects.get(
            children__in=[child.id])  # * Достаем группу ребенка
        if group:
            group = group.name
        else:
            group = 'Группа не найдена'

        # * Присваиваем все
        child.branch = branch
        child.tale = tale
        child.group = group

        serializer = BranchChildSerializer(child)
        return Response(serializer.data, status=status.HTTP_200_OK)


# * Пакеты
class FranchiseePackageListView(APIView):
    def get(self, request, format=None):
        packages = FranchiseePackage.objects.filter(purchaser=request.user.id)
        serializer = FranchiseePackageSerializer(packages, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        request.data['_mutable'] = True
        request.data['purchaser'] = request.user.id
        request.data['_mutable'] = False

        serializer = FranchiseePackageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FranchiseeAllInfoView(APIView):
    def get(self, request, format=None):

        packages = FranchiseePackage.objects.all()
        for package in packages:
            try:
                package.franchisee = FranchiseeProfile.objects.get(
                    user_id=package.purchaser)
            except FranchiseeProfile.DoesNotExist:
                package.franchisee = None

        serializer = FranchiseeAllInfoSerializer(packages, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseeAllInfoDetailView(APIView):
    def get(self, request, id, format=None):

        package = FranchiseePackage.objects.get(id=id)

        try:
            package.franchisee = FranchiseeProfile.objects.get(
                user_id=package.purchaser)
        except FranchiseeProfile.DoesNotExist:
            package.franchisee = None

        serializer = FranchiseeAllInfoSerializer(package)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseeOtherPackageListView(APIView):
    def get(self, request, id, format=None):
        packages = FranchiseePackage.objects.filter(purchaser=id)
        serializer = FranchiseePackageSerializer(packages, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseePackageView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            package = FranchiseePackage.objects.get(id=id)
        except FranchiseePackage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        print(package)
        try:
            franchisee = FranchiseeProfile.objects.get(
                user_id=package.purchaser.id)
        except FranchiseeProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        print(franchisee)
        package.franchisee = franchisee
        serializer = PackageDetailsSerializer(package)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseePackagePayView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, id):
        package = get_object_or_404(FranchiseePackage, id=id)
        # Magicaly payment is hapening
        serializer = FranchiseePackagePatchSerializer(
            package, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
        admin = get_object_or_404(Account, role="Admin")
        email = EmailMessage(
            'Здравствуйте',
            'Отправляем документы для по полаченному счету. Документы необходимо заполнить и отправить почтой в ПинКод.',
            'businessfoxmail@gmail.com',
            [request.data["email"], 'mrismayil2012@gmail.com',
                'netherlander01@gmail.com', admin.email]
            # [request.data["email"]],

        )

        email.attach_file(package.contract_file.path)
        email.attach_file(package.bill_file.path)
        email.attach_file(package.act_file.path)
        email.send()
        return Response({'msg': 'Payment accepted'})
# * Пакеты
