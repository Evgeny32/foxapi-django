# Generated by Django 3.2.6 on 2021-08-27 13:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('franchisee', '0006_auto_20210827_1259'),
    ]

    operations = [
        migrations.AlterField(
            model_name='franchiseepackage',
            name='format',
            field=models.PositiveIntegerField(choices=[(0, 'Offline'), (1, 'Online')], verbose_name='Формат франшизы'),
        ),
        migrations.AlterField(
            model_name='franchiseepackage',
            name='name',
            field=models.PositiveIntegerField(choices=[(1, 'Начальная школа'), (0, 'Детский сад'), (2, 'Школа')], verbose_name='Название'),
        ),
        migrations.AlterField(
            model_name='franchiseepackage',
            name='status',
            field=models.PositiveIntegerField(choices=[(0, 'В обработке'), (5, 'Счет оплачен'), (1, 'Повторная'), (4, 'Отклонена'), (2, 'В работе'), (7, 'Ожидается оплата'), (6, 'Заблокирована'), (3, 'Документы отправлены')], default=0, verbose_name='Статус заявки'),
        ),
    ]
