from django.apps import AppConfig


class FranchiseeConfig(AppConfig):
    name = 'franchisee'
    verbose_name="5) Франчайз"
