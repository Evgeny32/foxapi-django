from django.db.models import fields
from branch.models import BranchProfile
from rest_framework import serializers
from .models import FranchiseeProfile, FranchiseePackage
from mentor.models import MentorProfile
from account.models import Account
from account.serializers import AccountProfileSerializer
from account.serializers import AccountSerializer, AccountBuchSerializer
from mentor.models import ChildGroup
from child.models import ChildProfile
# from franch_buch.models import FranchBuchProfile


# * Филиалы


class AccountBanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'ban', 'ban_cause']


# class BuchRegistrationSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = Account
#         fields = ['email', 'name', 'franchisee']

#     def save(self):
#         account = Account(
#             email=self.validated_data['email'],
#             role='BuhFranch',
#         )
#         account_password = "7777"
#         # account_password = get_random_string(12)
#         # send_mail(
#         #     'Данные об аккаунте',
#         #     'Ваша почта - '+ account.email + '\nВаш пароль - ' + account_password,
#         #     'businessfoxmail@gmail.com',
#         #     [account.email],
#         #     fail_silently=False,
#         # )
#         account.set_password(account_password)
#         account.save()
#         franchiseeAccount = Account.objects.get(
#             id=self.validated_data['franchisee'])
#         franchBuchAccount = Account.objects.get(email=self.validated_data['email'])
#         profile = FranchBuchProfile(

#             franchisee=franchiseeAccount,
#             user_id=franchBuchAccount,
#             name=self.validated_data['name'],
#         )
#         profile.save()
#         data = {
#             'profile': profile,
#             'account': account
#         }
#         return data
class FranchiseePackageSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseePackage
        fields = '__all__'

class BranchListSerializer(serializers.ModelSerializer):  # * Список филиалов

    mentors = serializers.IntegerField()
    children = serializers.IntegerField()
    groups = serializers.IntegerField()
    all = serializers.IntegerField()
    account = AccountBanSerializer(source="user_id")
    package = FranchiseePackageSerializer(read_only=True)

    class Meta:
        model = BranchProfile
        fields = ['id', 'account', 'mentors', 'children',
                  'groups', 'all', 'name', 'region', 'main','package']

# ! ОТДЕЛ ФРАНЧАЙЗА


# ! ОТДЕЛ ФИЛИАЛОВ

# * Регистрация нового филиала
# * Создание филиала
class BranchRegistrationSerializer(serializers.ModelSerializer):

    franchisee = serializers.IntegerField()
    region = serializers.CharField()
    director = serializers.CharField()

    registration_sertificate = serializers.FileField()
    accounting_sertificate = serializers.FileField()
    partner_form = serializers.FileField()
    package = serializers.IntegerField()

    class Meta:
        model = Account
        fields = ['email', 'name', 'franchisee', 'region', 'director',
                  'registration_sertificate', 'accounting_sertificate', 'partner_form', 'package']

    def save(self):
        account = Account(
            email=self.validated_data['email'],
            role='Branch',
        )
        account_password = "7777"
        # account_password = get_random_string(12)
        # send_mail(
        #     'Данные об аккаунте',
        #     'Ваша почта - '+ account.email + '\nВаш пароль - ' + account_password,
        #     'businessfoxmail@gmail.com',
        #     [account.email],
        #     fail_silently=False,
        # )
        account.set_password(account_password)
        account.save()
        franchiseeAccount = Account.objects.get(
            id=self.validated_data['franchisee'])
        branchAccount = Account.objects.get(email=self.validated_data['email'])
        package = FranchiseePackage.objects.get(id=self.validated_data['package'])
        profile = BranchProfile(
            region=self.validated_data['region'],
            director=self.validated_data['director'],

            registration_sertificate=self.validated_data['registration_sertificate'],
            accounting_sertificate=self.validated_data['accounting_sertificate'],
            partner_form=self.validated_data['partner_form'],
            franchisee=franchiseeAccount,
            user_id=branchAccount,
            name=self.validated_data['name'],
            package=package
        )
        profile.save()
        data = {
            'profile': profile,
            'account': account
        }
        return data

class BranchAdminRegistrationSerializer(serializers.ModelSerializer):

    franchisee = serializers.IntegerField()
    region = serializers.CharField()
    director = serializers.CharField()

    registration_sertificate = serializers.FileField()
    accounting_sertificate = serializers.FileField()
    partner_form = serializers.FileField()

    class Meta:
        model = Account
        fields = ['email', 'name', 'franchisee', 'region', 'director',
                  'registration_sertificate', 'accounting_sertificate', 'partner_form']

    def save(self):
        account = Account(
            email=self.validated_data['email'],
            role='Branch',
        )
        account_password = "7777"
        # account_password = get_random_string(12)
        # send_mail(
        #     'Данные об аккаунте',
        #     'Ваша почта - '+ account.email + '\nВаш пароль - ' + account_password,
        #     'businessfoxmail@gmail.com',
        #     [account.email],
        #     fail_silently=False,
        # )
        account.set_password(account_password)
        account.save()
        franchiseeAccount = Account.objects.get(
            id=self.validated_data['franchisee'])
        branchAccount = Account.objects.get(email=self.validated_data['email'])
        profile = BranchProfile(
            region=self.validated_data['region'],
            director=self.validated_data['director'],

            registration_sertificate=self.validated_data['registration_sertificate'],
            accounting_sertificate=self.validated_data['accounting_sertificate'],
            partner_form=self.validated_data['partner_form'],
            franchisee=franchiseeAccount,
            user_id=branchAccount,
            name=self.validated_data['name'],
        )
        profile.save()
        data = {
            'profile': profile,
            'account': account
        }
        return data

# * Сериализатор списка филиалов


class BranchesProfileSerializer(serializers.ModelSerializer):

    mentors = serializers.IntegerField()
    children = serializers.IntegerField()
    groups = serializers.IntegerField()
    all = serializers.IntegerField()
    ban = serializers.BooleanField()

    class Meta:
        model = BranchProfile
        fields = "__all__"


class BranchSerializer(serializers.ModelSerializer):

    class Meta:
        model = BranchProfile
        fields = "__all__"

# * Филиалы


class MentorProfileSerializer(serializers.ModelSerializer):
    mentor = AccountProfileSerializer()

    class Meta:
        model = MentorProfile
        fields = '__all__'


class FranchiseeProfileAccountSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['email']


class FranchiseeBuhSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'email', 'role']

    def create(self, validated_data):
        user = Account.objects.create(**validated_data)
        user.set_password('7777')
        user.save()
        return user


class FranchiseeProfileSerializer(serializers.ModelSerializer):
    email = serializers.CharField(read_only=True)
    emailBuch = AccountSerializer()
    user_id = AccountSerializer()
    # emailBuch = serializer_choice_field.CharField
    # emailBuch = serializers.RelatedField(read_only=True)

    class Meta:
        model = FranchiseeProfile
        fields = "__all__"



class ClientsFranchiseeSerializer(serializers.ModelSerializer):    
    user_id = AccountSerializer()  
    emailBuch = AccountSerializer()
    # emailBuch = serializers.CharField()

    class Meta:
        model = FranchiseeProfile
        fields = "__all__"


class FranchiseeAllInfoSerializer(serializers.ModelSerializer):    
    user_id = AccountSerializer()  
    emailBuch = AccountSerializer()
    branches = AccountSerializer()
    packages = AccountSerializer()
    mentors = AccountSerializer()
    children = AccountSerializer()
    childGroup = AccountSerializer()
    groupChild = AccountSerializer()
    # emailBuch = serializers.CharField()

    class Meta:
        model = FranchiseeProfile
        fields = "__all__"



class PatchFranchiseeProfileSerializer(serializers.ModelSerializer):
    # emailBuch = serializer_choice_field.CharField
    # emailBuch = serializers.RelatedField(read_only=True)

    class Meta:
        model = FranchiseeProfile
        fields = "__all__"

# ! ОТДЕЛ ФИЛИАЛОВ


# ! ОТДЕЛ МЕНТОРОВ


# * Сериализатор списка менторов
class FIOSerializer(serializers.ModelSerializer):  # *
    class Meta:
        model = Account
        fields = ['id', 'name', 'second_name', 'last_name', 'email', 'phone']


class ChildGroupListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChildGroup
        fields = ['id', 'name', 'age_group', 'mentor', 'children']


class MentorListSerializer(serializers.ModelSerializer):
    children = serializers.IntegerField()
    trainings = serializers.IntegerField()
    groups = serializers.IntegerField()
    all = serializers.IntegerField()
    mentor = FIOSerializer()

    class Meta:
        model = MentorProfile
        fields = ['id', 'mentor', 'children', 'all', 'trainings', 'job_status',
                  'groups', 'all', 'date_work', 'date_layoff']


# * Сериализатор на странице ментора
# class MentorProfileSerializer(serializers.ModelSerializer):  # * Для отображения
#     mentor = AccountSerializer()

#     class Meta:
#         model = MentorProfile
#         fields = '__all__'


class MentorProfileSerializer(serializers.ModelSerializer):
    mentor = AccountProfileSerializer()

    class Meta:
        model = MentorProfile
        fields = '__all__'


# * Для изменений
class MentorProfileSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = MentorProfile
        fields = '__all__'


# * Отображение групп ментора
class MentorGroupListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChildGroup
        fields = ['id', 'name', 'children', 'creation_date']


# ! ОТДЕЛ МЕНТОРОВ


# ! ОТДЕЛ РЕБЕНКА

# * Получение всех детей филиала
class BranchChildListSerializer(serializers.ModelSerializer):
    tale = serializers.CharField()
    child = FIOSerializer()

    class Meta:
        model = ChildProfile
        fields = ['id', 'child', 'age', 'tale', 'rank', 'creation_date']


# * Отображение данных по одному ребенку
class BranchChildSerializer(serializers.ModelSerializer):

    branch = serializers.CharField()
    mentor = FIOSerializer()
    parent = FIOSerializer()
    child = FIOSerializer()
    group = serializers.CharField()
    tale = serializers.CharField()

    class Meta:
        model = ChildProfile
        fields = ['id', 'child', 'age', 'parent',
                  'branch', 'mentor', 'group', 'tale', 'rank']


# * Пакеты

class FranchiseeProfileDetailsSerializer(serializers.ModelSerializer):
    user_id = AccountProfileSerializer()
    emailBuch = AccountProfileSerializer()
    class Meta:
        model = FranchiseeProfile
        fields = '__all__'

class PackageDetailsSerializer(serializers.ModelSerializer):
    franchisee = FranchiseeProfileDetailsSerializer() 
    class Meta:
        model = FranchiseePackage
        fields = '__all__'

class FranchiseeAllProfileSerializer(serializers.ModelSerializer):

    user_id = FIOSerializer(read_only=True)
    emailBuch = AccountSerializer(read_only=True)

    class Meta:
        model = FranchiseeProfile
        fields = "__all__"


class FranchiseeAllInfoSerializer(serializers.ModelSerializer):
    
    franchisee = FranchiseeAllProfileSerializer()    
    

    class Meta:
        model = FranchiseePackage
        fields = '__all__'

# * Пакеты
