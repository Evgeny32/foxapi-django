from django.urls import path
from .views import (
    MentorGroupListView,
    MentorView,
    MentorListView,
    BranchView,
    BranchListView,
    FranchiseeProfileView,
    BranchChildListView,
    BranchChildView,
    BanBranchView,
    MentorBan,
    FranchiseePackageListView,
    FranchBuchAccount,
    FranchBuchAccountPatchView,
    FranchiseePackageView,
    FranchiseeOtherPackageListView,
    BranchAdminListView,
    FranchiseeAllInfoView,
    FranchiseeAllInfoDetailView,
    FranchiseePackagePayView
)


app_name = "franchisee"

# * Профиль
urlpatterns = [
    path('', FranchiseeProfileView.as_view(), name="franchisee_profile_post"),
]

# * Филиалы
urlpatterns += [
    path('branch/', BranchListView.as_view(),
         name="BranchListView"),  # * Вывод списка филиалов
    path('branch/admin/', BranchAdminListView.as_view(),
         name="BranchListView"),
    # * Вывод и изменение одного филиала
    path('branch/<int:id>/', BranchView.as_view(), name="BranchView"),
    path('branch/<int:id>/ban/', BanBranchView.as_view(),
         name="BanBranchView"),  # * Бан филиала и всех людей в нем
]

# * Менторы
urlpatterns += [
    path('branch/<int:id>/mentor/',
         MentorListView.as_view(), name="MentorListView"),  # * Отображение списка менторов филиала
    path('branch/mentor/<int:id>/ban/',
         MentorBan.as_view(), name="MentorBan"),  # * Увольнение одного ментора
    path('branch/mentor<int:id>/', MentorView.as_view(),
         name="MentorView"),  # * Информация об одном менторе
    path('branch/mentor/<int:id>/group/',
         MentorGroupListView.as_view(), name="MentorGroupListView"),
]
# * Дети
urlpatterns += [
    path('branch/<int:id>/child/', BranchChildListView.as_view(),
         name="BranchChildListView"),
    path('branch/child/<int:id>/',
         BranchChildView.as_view(), name="BranchChildView"),
]


# * Франшизы
urlpatterns += [
    path('package/', FranchiseePackageListView.as_view(),
         name="FranchiseePacketListView"),
    path('package/<int:id>', FranchiseePackageView.as_view(),
         name="FranchiseePacketListView"),
    path('package/<int:id>/pay/', FranchiseePackagePayView.as_view(),
         name="FranchiseePackagePayView"),
    path('package/other/<int:id>', FranchiseeOtherPackageListView.as_view(),
         name="FranchiseePacketListView"),
    path('buch/', FranchBuchAccount.as_view(),
         name="FranchBuchAccount"),
    path('buch/<int:id>', FranchBuchAccountPatchView.as_view(),
         name="FranchBuchAccountPatchView"),
    path('allinfo/', FranchiseeAllInfoView.as_view(),
         name="FranchiseeAllInfoView"),
    path('packageallinfo/<int:id>', FranchiseeAllInfoDetailView.as_view(),
         name="FranchiseeAllInfoDetailView"),

]
