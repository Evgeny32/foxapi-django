from django.db.models import fields
from rest_framework import serializers

from .models import MentorProfile, ChildGroup, MetodicMaterial
from account.models import Account
from child.models import ChildProfile, Term
from tales.models import Tale

from child.serializers import ChildrenSerializer, ChildAccountSerializer
from tales.serializers import TaleSerializer
from account.serializers import AccountProfileSerializer, AccountSerializer


from branch.serializers import FIOSerializer


class MentorProfileSerializer(serializers.ModelSerializer):
    mentor = AccountSerializer(read_only=True)

    class Meta:
        model = MentorProfile
        fields = '__all__'


# ! ОТДЕЛ ГРУПП ДЕТЕЙ


# * Список групп детей
class ChildGroupListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChildGroup
        fields = ['id', 'name', 'age_group', 'mentor']


class AccountWithAvatarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        exclude = ['password', ]


class ChildFromGroupSerializer(serializers.ModelSerializer):
    child = AccountWithAvatarSerializer()

    class Meta:
        model = ChildProfile
        fields = ['id', 'child']


class ChildGroupSerializer(serializers.ModelSerializer):
    children = ChildFromGroupSerializer(many=True)
    requests = ChildFromGroupSerializer(many=True)

    class Meta:
        model = ChildGroup
        fields = ['id', 'name', 'children', 'requests', 'age_group']


class ChildGroupPatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChildGroup
        fields = ['id', 'name', 'children', 'requests', 'deny']


# ! ОТДЕЛ ГРУПП ДЕТЕЙ

# ! ОТДЕЛ ТЕРМИНОВ

class TermListSerializer(serializers.ModelSerializer):
    request_user = FIOSerializer(required=False)

    class Meta:
        model = Term
        fields = ['id', 'name', 'descriptionGeneral', 'description1', 'description2', 'description3', 'is_active', 'mentor',
                  'request_user', 'deny_reason']

# ! ОТДЕЛ ТЕРМИНОВ

# ! ОТДЕЛ ДЕТЕЙ
#! СЕРИАЛАЙЗЕР ДЛЯ ПРОСМОТРА ПРОФИЛЯ РЕБЕНКА\ ИМПОРТ ИЗ ЧАЙЛДА


class ChildSerializer(serializers.ModelSerializer):
    child = AccountWithAvatarSerializer()

    class Meta:
        model = ChildProfile
        fields = ['id', 'child', 'rank', 'foxies']


class TaleListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tale
        fields = ['id', 'name', 'number', 'success']
# ! ОТДЕЛ ДЕТЕЙ

# Методички


class MetodicMaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = MetodicMaterial
        fields = '__all__'
