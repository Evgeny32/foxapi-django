from django.urls import path
from .views import (
    GroupListView,
    GroupView,
    TermListView,
    TermView,
    ChildView,
    TaleListView,
    TaleView,
    GroupDenyView,
    GroupAcceptView,
    MetodicMaterialsView,
    MetodicMaterialView
)

from tales.views import (
    Task as TaskView,
    Answer as AnswerView
)


app_name = 'mentor'

#!ГРУППЫ
urlpatterns = [
    path('group/', GroupListView.as_view(), name='GroupListView'),
    path('group/<int:id>', GroupView.as_view(), name="GroupView"),
    path('group/<int:id>/deny/', GroupDenyView.as_view(), name="GroupDenyView"),
    path('group/<int:id>/accept/', GroupAcceptView.as_view(), name="GroupAcceptView")
]
#!ТЕРМИНЫ
urlpatterns += [
    path('term/', TermListView.as_view(), name='TermListView'),
    path('term/<int:id>/', TermView.as_view(), name="TermView"),

]
#!ДЕТИ
urlpatterns += [
    path('child/<int:id>', ChildView.as_view(), name="ChildView"),
    path('child/<int:id>/tales/', TaleListView.as_view(), name="TaleListView")
]

#!СКАЗКА
urlpatterns += [
    path('tale/<int:id>/', TaleView.as_view(), name="TaleView"),
    path('child/<int:id>/tales/', TaleListView.as_view(), name="TaleListView"),
    path('task/<int:id>/', TaskView.as_view(), name="task"),
    path('answer/<int:id>/', AnswerView.as_view(), name="answer")
]


# Методичка
urlpatterns += [
    path('metodics', MetodicMaterialsView.as_view()),
    path('metodics/<int:id>', MetodicMaterialView.as_view())
]
