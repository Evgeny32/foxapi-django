from django.apps import AppConfig


class MentorConfig(AppConfig):
    name = 'mentor'
    verbose_name="3) Ментор"
