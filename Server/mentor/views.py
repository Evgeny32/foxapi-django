from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from .models import ChildGroup, MetodicMaterial
from child.models import ChildProfile, Term
from tales.models import Tale, Task, Answer
from account.models import Account
from .serializers import (
    ChildGroupListSerializer,
    ChildGroupSerializer,
    ChildGroupPatchSerializer,
    TermListSerializer,
    ChildSerializer,
    TaleListSerializer,
    MetodicMaterialSerializer
)
from tales.serializers import TaleSerializer, TaleSimpleSerializer
from django.shortcuts import get_object_or_404
from chat.models import Room
# ! ОТДЕЛ ГРУПП ДЕТЕЙ


class GroupListView(APIView):  # * Показ и создание групп детей

    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        groups = ChildGroup.objects.filter(mentor=request.user)
        serializer = ChildGroupListSerializer(groups, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        request.data['_mutable'] = True
        request.data['mentor'] = request.user.id
        request.data['_mutable'] = False

        serializer = ChildGroupListSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GroupView(APIView):  # * Показ и изменение одной группы детей

    permission_classes = [IsAuthenticated]

    def get_group(self, id):
        try:
            group = ChildGroup.objects.get(id=id)
        except ChildGroup.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return group

    def get(self, request, id, format=None):
        group = self.get_group(id)
        serializer = ChildGroupSerializer(group)
        return Response(serializer.data)

    def patch(self, request, id, format=None):
        group = self.get_group(id)
        serializer = ChildGroupPatchSerializer(
            group, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(group, validated_data=request.data)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GroupDenyView(APIView):
    permission_classes = [IsAuthenticated]

    def patch(self, request, id):
        group = get_object_or_404(ChildGroup, id=id)
        group.requests.remove(request.data['child'])
        group.deny.add(request.data['child'])
        group.save()
        serializer = ChildGroupSerializer(group)
        return Response(serializer.data)


class GroupAcceptView(APIView):
    permission_classes = [IsAuthenticated]

    def patch(self, request, id):
        group = get_object_or_404(ChildGroup, id=id)
        group.requests.remove(request.data['child'])
        group.children.add(request.data['child'])
        group.save()
        child = get_object_or_404(ChildProfile, id=request.data['child'])
        # child = get_object_or_404(Account, id=child.child.id)
        # parent = get_object_or_404(Account, id=child.parent)
        mentor = get_object_or_404(Account, id=request.user.id)
        room1 = Room(
            creator=mentor,
            name='{0} {1} {2} - {3} {4} {5}'.format(
                mentor.second_name, mentor.name, mentor.last_name, child.child.second_name, child.child.name, child.child.last_name)
        )
        room1.save()
        room1.users.set([mentor, child.child])
        room1.save()

        room2 = Room(
            creator=mentor,
            name='{0} {1} {2} - {3} {4} {5}'.format(
                mentor.second_name, mentor.name, mentor.last_name, child.parent.second_name, child.parent.name, child.parent.last_name)
        )
        room2.save()
        room2.users.set([mentor, child.parent])
        room2.save()
        serializer = ChildGroupSerializer(group)
        return Response(serializer.data)

# ! ОТДЕЛ ГРУПП ДЕТЕЙ


# ! ОТДЕЛ ТЕРМИНОВ

class TermListView(APIView):  # * Показ и создание терминов

    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        terms = Term.objects.filter(mentor=request.user.id)
        serializer = TermListSerializer(terms, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):

        serializer = TermListSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TermView(APIView):  # * Изменение термина

    permission_classes = [IsAuthenticated]

    def patch(self, request, id, format=None):
        term = get_object_or_404(Term, id=id)
        serializer = TermListSerializer(
            term, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(term, validated_data=request.data)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        term = get_object_or_404(Term, id=id)
        term.delete()
        return Response({'msg': 'Term deleted'})


# ! ОТДЕЛ ТЕРМИНОВ

# ! ОТДЕЛ ДЕТЕЙ
class ChildView(APIView):  # * Показ ребенка

    permission_classes = [IsAuthenticated]

    def get_child(self, id):
        try:
            child = ChildProfile.objects.get(id=id)
        except ChildProfile.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return child

    def get(self, request, id, format=None):
        child = self.get_child(id)
        serializer = ChildSerializer(child)
        return Response(serializer.data)


class TaleListView(APIView):  # * Показ списка сказок ребенка

    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        tales = Tale.objects.filter(child=id).order_by('-number')
        serializer = TaleListSerializer(tales, many=True)
        return Response(serializer.data)


# ! ОТДЕЛ ДЕТЕЙ

# ! ОТДЕЛ СКАЗКИ
class TaleView(APIView):  # * Показ сказки ребенка

    permission_classes = [IsAuthenticated]

    def get_tale_by_id(self, id):
        try:
            tale = Tale.objects.get(id=id)
        except Tale.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return tale

    def get(self, request, id, format=None):
        tale = Tale.objects.get(id=id)
        tasks = Task.objects.filter(tale=tale.id)
        for task in tasks:
            task.answers = Answer.objects.filter(task=task.id)
        tale.tasks = tasks
        serializer = TaleSerializer(tale)
        return Response(serializer.data)

    def patch(self, request, id, format=None):
        """
        update tale
        """
        tale = self.get_tale_by_id(id)
        serializer = TaleSimpleSerializer(
            tale, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)
# ! ОТДЕЛ СКАЗКИ


class MetodicMaterialsView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        metodics = MetodicMaterial.objects.filter(user=request.user.id)
        serializer = MetodicMaterialSerializer(metodics, many=True)
        return Response(serializer.data)

    def post(self, request):
        request.data['_mutable'] = True
        request.data['user'] = request.user.id
        serializer = MetodicMaterialSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MetodicMaterialView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        metodic = get_object_or_404(MetodicMaterial, id=id)
        serializer = MetodicMaterialSerializer(metodic)
        return Response(serializer.data)

    def patch(self, request, id):
        metodic = get_object_or_404(MetodicMaterial, id=id)
        serializer = MetodicMaterialSerializer(
            metodic, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(metodic, validated_data=request.data)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        metodic = get_object_or_404(MetodicMaterial, id=id)
        metodic.delete()
        return Response({'msg': "Metodic Material deleted"})
