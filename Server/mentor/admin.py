from django.contrib import admin
from .models import MentorProfile, ChildGroup
# Register your models here.

@admin.register(MentorProfile)

class MentorFields(admin.ModelAdmin):
    list_display = ('mentor', 'branch', 'date_work', 'turnover', 'job_status', )


admin.site.register(ChildGroup)

# @admin.register(ChildGroup)
# class ChildGroupFields(admin.ModelAdmin):
#     list_display = ('name', 'mentor', 'children')