from django.db import models
from account.models import Account
from child.models import ChildProfile
from branch.models import BranchProfile
from django.utils import timezone
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save, pre_delete
from django.conf import settings
import shutil
from django.utils.translation import ugettext_lazy as _
# Create your models here.

#! ПРОФИЛ МЕНТОРА
#!!!!!!!!!!!!!!!!!!!!


def mentor_upload_path(instance, filename):
    return '{0}/{1}'.format(instance.mentor.email, filename)


class MentorProfile(models.Model):
    mentor = models.ForeignKey(Account, related_name='mentors',
                               verbose_name="Ментор", null=True, blank=True, on_delete=models.SET_NULL)
    branch = models.ForeignKey(Account, related_name='branch',
                               verbose_name="Филиал", null=True, blank=True, on_delete=models.SET_NULL)
    date_work = models.DateField(
        verbose_name="Дата взятия на работу", auto_now_add=True)
    date_layoff = models.DateField(
        verbose_name="Дата увольнения", null=True, blank=True)
    turnover = models.IntegerField(
        verbose_name="Оборот за все время", null=True, blank=True)
    job_status = models.CharField(verbose_name="Статус работы", max_length=50)
    passport = models.CharField(
        max_length=15, verbose_name="Серия/Номер паспорта")
    passport_who = models.CharField(
        verbose_name="Пасспорт кем выдан", max_length=50)
    passport_when = models.DateField(verbose_name="Дата выдачи пасспорта")
    adress = models.CharField(verbose_name="Адрес", max_length=50)

    passport_file = models.FileField(
        upload_to=mentor_upload_path, verbose_name="Пасспорт ментора PDF")
    inn_file = models.FileField(
        upload_to=mentor_upload_path, verbose_name="Пасспорт ментора PDF")
    snils_file = models.FileField(
        upload_to=mentor_upload_path, verbose_name="Пасспорт ментора PDF")

    # def unicode(self):
    #     return self.mentor.name

    class Meta:
        verbose_name = "Ментор"
        verbose_name_plural = "Менторы"


#! ГРУППЫ ДЕТЕЙ
#!!!!!!!!!!!!!!!!!!!!!
class ChildGroup(models.Model):
    name = models.CharField(max_length=40, verbose_name="Название группы")
    # create_date = models.DateTimeField(verbose_name="Дата создании группы")
    AGE_GROUPS = (
        ('5-7', ('Дошкольная(5-7)')),
        ('8-12', ('Начальная школа(8-12)')),
        ('13-16', ('Средняя школа(13-16)')),
    )
    age_group = models.CharField(
        max_length=30, choices=AGE_GROUPS, verbose_name='Возрастная категория')
    mentor = models.ForeignKey(
        Account, related_name='group_mentor', verbose_name='Ментор', on_delete=models.CASCADE)
    children = models.ManyToManyField(
        ChildProfile, related_name='childrens_in_group', verbose_name="Дети", blank=True)
    requests = models.ManyToManyField(
        ChildProfile, related_name='requested_children', verbose_name='Заявки', blank=True)
    deny = models.ManyToManyField(
        ChildProfile, related_name='denyed_requests', verbose_name='Отклоненные', blank=True)
    creation_date = models.DateField(
        verbose_name="Дата создания", auto_now=True)
    online = models.BooleanField('Онлайн', default=False)

    def unicode(self):
        return self.name

    class Meta:
        verbose_name = "Группа детей"
        verbose_name_plural = "Группы детей"


@receiver(pre_save, sender=MentorProfile)
def delete_old_image(sender, instance=None, **kwargs):
    if instance.passport_file:
        try:
            old_file = MentorProfile.objects.get(pk=instance.pk).passport_file
        except MentorProfile.DoesNotExist:
            return
        else:
            new_file = instance.passport_file

            if old_file != new_file:
                old_file.delete(save=False)

    if instance.inn_file:
        try:
            old_file = MentorProfile.objects.get(pk=instance.pk).inn_file
        except MentorProfile.DoesNotExist:
            return
        else:
            new_file = instance.inn_file

            if old_file != new_file:
                old_file.delete(save=False)

    if instance.snils_file:
        try:
            old_file = MentorProfile.objects.get(pk=instance.pk).snils_file
        except MentorProfile.DoesNotExist:
            return
        else:
            new_file = instance.snils_file

            if old_file != new_file:
                old_file.delete(save=False)


@receiver(pre_delete, sender=MentorProfile)
def delete_media_files(sender, instance=None, **kwargs):
    if instance.mentor:
        mentor = MentorProfile.objects.get(pk=instance.pk)
        mentor.snils_file.delete(save=False)
        mentor.passport_file.delete(save=False)
        mentor.inn_file.delete(save=False)


class MetodicMaterial(models.Model):
    """Методички"""
    def metodic_material_upload_path(instance, filename):
        return '{0}/{1}'.format(instance.user.email, filename)
    name = models.CharField(_("Название"), max_length=256)
    document = models.FileField(_("Документ"), upload_to=None, max_length=100)
    user = models.ForeignKey("account.Account", verbose_name=_(
        "Ментор"), on_delete=models.CASCADE)
    AGE_CHOICES = (
        ('5-7', 'First Step'),
        ('8-12', 'First Idea'),
        ('13-16', "First Business")
    )
    age = models.CharField(_("Возрастной рейтинг"),
                           choices=AGE_CHOICES, max_length=50)
