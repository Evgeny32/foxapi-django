from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.conf import settings
import shutil
# Create your models here.


class Cloud(models.Model):
    """Базовые настройки облака"""
    initial_capacity = models.BigIntegerField(
        _("Начальное место"), default=314572800)

    class Meta:
        verbose_name = "Настройка облака"
        verbose_name_plural = "Настройки облака"


class UserCloud(models.Model):
    """Облако пользователя"""
    user = models.ForeignKey("account.Account", verbose_name=_(
        "Пользователь облака"), on_delete=models.CASCADE)
    capacity = models.BigIntegerField(
        _("Размер облака в байтах"), default=314572800)
    created_at = models.DateTimeField(_("Время создания"), auto_now_add=True)
    used_capacity = models.BigIntegerField(
        _("Использованное пространство"), default=0)

    def __str__(self):
        user_fio = '{0} {1} {2}'.format(
            self.user.second_name, self.user.name, self.user.last_name)  # ФИО пользователя облака

        cloud_capacity = (self.capacity - self.used_capacity) \
            / 1024 / 1024  # Оставшийся объем в мегабайтах
        return '{0} | {1} Mb'.format(user_fio, round(cloud_capacity, 3))


class CloudFile(models.Model):
    """Файл в облаке"""
    cloud = models.ForeignKey("cloud.UserCloud", verbose_name=_(
        "Облако"), on_delete=models.CASCADE)

    def cloud_file_upload_path(instance, filename):
        return 'cloud/{0}/{1}/{2}'.format(instance.cloud.id, instance.type, filename)
    file = models.FileField(_("Файл"), upload_to=cloud_file_upload_path)
    name = models.CharField(_("Название файла"),
                            max_length=100, default='file')
    size = models.BigIntegerField(_("Размер файла"))
    type_choices = [
        ('image', 'Изображение'),
        ('document', 'Документ'),
        ('sound', 'Звук'),
        ('video', 'Видео'),
        ('other', 'Другое')
    ]
    type = models.CharField(
        _("Тип файла"), max_length=50, choices=type_choices, default='other')

    def __str__(self):
        user_fio = '{0} {1} {2}'.format(
            self.cloud.user.second_name, self.cloud.user.name, self.cloud.user.last_name)  # ФИО пользователя облака
        return '{0} | {1}'.format(user_fio, self.file.name)


@receiver(pre_delete, sender=CloudFile)
def cloud_file_pre_delete(sender, instance=None, **kwargs):
    """Удаление файла из хранилища"""
    if instance.file:
        instance.file.delete(save=False)


@receiver(pre_delete, sender=UserCloud)
def user_cloud_pre_delete(sender, instance=None, **kwargs):
    """Удаление облака. Удаляет все файлы облака"""
    dirpath = settings.BASE_DIR / ('media/cloud/{0}'.format(instance.id))
    shutil.rmtree(dirpath, ignore_errors=True)
