# Generated by Django 3.2.6 on 2021-08-18 13:54

import cloud.models
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Cloud',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('initial_capacity', models.BigIntegerField(default=314572800, verbose_name='Начальное место')),
            ],
            options={
                'verbose_name': 'Настройка облака',
                'verbose_name_plural': 'Настройки облака',
            },
        ),
        migrations.CreateModel(
            name='UserCloud',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('capacity', models.BigIntegerField(default=314572800, verbose_name='Размер облака в байтах')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Время создания')),
                ('used_capacity', models.BigIntegerField(default=0, verbose_name='Использованное пространство')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь облака')),
            ],
        ),
        migrations.CreateModel(
            name='CloudFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(upload_to=cloud.models.CloudFile.cloud_file_upload_path, verbose_name='Файл')),
                ('name', models.CharField(default='file', max_length=100, verbose_name='Название файла')),
                ('size', models.BigIntegerField(verbose_name='Размер файла')),
                ('type', models.CharField(choices=[('image', 'Изображение'), ('document', 'Документ'), ('sound', 'Звук'), ('video', 'Видео'), ('other', 'Другое')], default='other', max_length=50, verbose_name='Тип файла')),
                ('cloud', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cloud.usercloud', verbose_name='Облако')),
            ],
        ),
    ]
