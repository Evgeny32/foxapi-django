from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import Cloud, UserCloud, CloudFile
from account.serializers import AccountSerializer


class CloudSerializer(ModelSerializer):
    class Meta:
        model = Cloud
        fields = ('initial_capacity',)


class UserCloudSerializer(ModelSerializer):
    class Meta:
        model = UserCloud
        fields = ('id', 'user', 'capacity', 'used_capacity', 'created_at')
        read_only_fields = ('created_at',)


class CloudFileSerializer(ModelSerializer):
    class Meta:
        model = CloudFile
        fields = ('id', 'file', 'name', 'type', 'cloud', 'size')


class CloudInfoSerializer(ModelSerializer):
    files = CloudFileSerializer(many=True)
    user = AccountSerializer()

    class Meta:
        model = UserCloud
        fields = ('id', 'user', 'capacity',
                  'used_capacity', 'files', 'created_at')
