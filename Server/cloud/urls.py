from django.urls import path
from .views import (
    CloudView,
    UserCloudTokenView, UserCloudView,
    CloudInfoTokenView, CloudInfoView,
    CloudFilesTokenView, CloudFilesView,
    CloudFileView
)

urlpatterns = [
    path('settings/', CloudView.as_view(), name='CloudView'),

    path('', UserCloudTokenView.as_view(), name="UserCloudTokenView"),
    path('info/', CloudInfoTokenView.as_view(), name="CloudInfoTokenView"),
    path('files/', CloudFilesTokenView.as_view(), name="CloudFilesTokenView"),

    path('<int:id>/', UserCloudView.as_view(), name="UserCloudView"),
    path('<int:id>/info/', CloudInfoView.as_view(), name="CloudInfoView"),
    path('<int:id>/files/', CloudFilesView.as_view(), name="CloudFilesView"),

    path('file/<int:id>/', CloudFileView.as_view(), name="CloudFileView")
]
