from django.contrib import admin
from .models import Cloud, UserCloud, CloudFile
# Register your models here.

admin.site.register(Cloud)
admin.site.register(UserCloud)
admin.site.register(CloudFile)
