from django.shortcuts import get_object_or_404
from django.conf import settings
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import permissions
from .serializers import CloudSerializer, UserCloudSerializer, CloudFileSerializer, CloudInfoSerializer
from .models import Cloud, UserCloud, CloudFile
from account.models import Account
import mimetypes
import os
# Create your views here.

# :TODO: Названия файлов менять


class IsAdminPermission(permissions.BasePermission):
    """ Проверяет на роль админа """

    def has_permission(self, request, view):
        try:
            role = Account.objects.get(id=request.user.id).role
        except Account.DoesNotExist:
            return False
        return role == 'Admin'


class CloudView(APIView):
    """ Запросы, связанные с изменением настроек облака """
    permission_classes = [IsAuthenticated, IsAdminPermission]

    def get_or_create_cloud_settings(self):
        cloud_settings = Cloud.objects.all().first()
        if not cloud_settings:
            cloud_settings = Cloud()
            cloud_settings.save()
        return cloud_settings

    def get(self, request):
        cloud_settings = self.get_or_create_cloud_settings()
        serializer = CloudSerializer(cloud_settings)
        return Response({
            'msg': 'Cloud settings received',
            'data': serializer.data
        }, status=status.HTTP_200_OK)

    def patch(self, request):
        cloud_settings = self.get_or_create_cloud_settings()
        serializer = CloudSerializer(
            cloud_settings, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(cloud_settings, validated_data=request.data)
            return Response({
                'msg': 'Cloud settings updated',
                'data': serializer.data
            }, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserCloudView(APIView):
    """ Отображение, изменение и удаление данных о облаке пользователя """
    permission_classes = [IsAuthenticated]

    def get_or_create_cloud_settings(self):
        cloud_settings = Cloud.objects.all().first()
        if not cloud_settings:
            cloud_settings = Cloud()
            cloud_settings.save()
        return cloud_settings

    def get(self, request, id):
        cloud = get_object_or_404(UserCloud, user=id)
        serializer = UserCloudSerializer(cloud)
        return Response({
            'msg': 'Cloud received',
            'data': serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request, id):
        """ Создание нового облака """

        # Проверка на наличие старого облака. Если имеется - выдает 406 ошибку
        try:
            old_cloud = UserCloud.objects.get(user=id)
            if old_cloud:
                return Response({
                    'detail': 'Cloud already exist'
                }, status=status.HTTP_406_NOT_ACCEPTABLE)
        except UserCloud.DoesNotExist:
            pass

        # Смотрим общие настройки для взщятия начального объема облака. Если настроек нет - создаем с дефолтными параметрами
        cloud_settings = self.get_or_create_cloud_settings()
        data = {
            **request.data, 'capacity': cloud_settings.initial_capacity, 'user': id
        }
        serializer = UserCloudSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response({
                'msg': 'Cloud created',
                'data': serializer.data
            }, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, id):
        cloud = get_object_or_404(UserCloud, user=id)
        serializer = UserCloudSerializer(
            cloud, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(cloud, validated_data=request.data)
            return Response({
                'msg': 'Cloud updated',
                'data': serializer.data
            }, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        cloud = get_object_or_404(UserCloud, user=id)
        cloud.delete()
        return Response({'msg': 'Cloud deleted'}, status=status.HTTP_200_OK)


class UserCloudTokenView(APIView):
    """ CRU по токену на облако"""
    permission_classes = [IsAuthenticated]

    def get_cloud_settings(self):
        cloud_settings = Cloud.objects.all().first()
        if not cloud_settings:
            cloud_settings = Cloud()
            cloud_settings.save()
        return cloud_settings

    def get(self, request):
        cloud = get_object_or_404(UserCloud, user=request.user)
        serializer = UserCloudSerializer(cloud)
        return Response({
            'msg': 'Cloud received',
            'data': serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request):

        # Проверка на наличие старого облака. Если имеется - выдает 406 ошибку
        try:
            old_cloud = UserCloud.objects.get(user=request.user.id)
            if old_cloud:
                return Response({
                    'detail': 'Cloud already exist'
                }, status=status.HTTP_406_NOT_ACCEPTABLE)
        except UserCloud.DoesNotExist:
            pass
        cloud_settings = self.get_cloud_settings()
        data = {
            **request.data, 'capacity': cloud_settings.initial_capacity, 'user': request.user.id
        }
        serializer = UserCloudSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response({
                'msg': 'Cloud created',
                'data': serializer.data
            }, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    # Пока скрою, ибо на стороне фронта тут менять нечего, все меняется на сервере
    # def patch(self, request):
    #     cloud = get_object_or_404(UserCloud, user=request.user.id)
    #     serializer = UserCloudSerializer(
    #         cloud, data=request.data, partial=True)
    #     if serializer.is_valid():
    #         serializer.update(cloud, validated_data=request.data)
    #         return Response({
    #             'msg': 'Cloud updated',
    #             'data': serializer.data
    #         }, status=status.HTTP_200_OK)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        cloud = get_object_or_404(UserCloud, user=request.user.id)
        cloud.delete()
        return Response({'msg': 'Cloud deleted'}, status=status.HTTP_200_OK)


class CloudInfoTokenView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        cloud = get_object_or_404(UserCloud, user=request.user.id)

        cloud_files = CloudFile.objects.filter(cloud=cloud.id)
        cloud.files = cloud_files
        serializer = CloudInfoSerializer(cloud)
        return Response({
            'msg': 'Cloud info received',
            'data': serializer.data
        })


class CloudInfoView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        cloud = get_object_or_404(UserCloud, user=id)

        cloud_files = CloudFile.objects.filter(cloud=cloud.id)
        cloud.files = cloud_files
        serializer = CloudInfoSerializer(cloud)
        return Response({
            'msg': 'Cloud info received',
            'data': serializer.data
        })


class CloudFilesTokenView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        cloud = get_object_or_404(UserCloud, user=request.user.id)
        files = CloudFile.objects.filter(cloud=cloud.id)
        serializer = CloudFileSerializer(files, many=True)
        return Response({
            'msg': 'Files received',
            'data': serializer.data
        })

    def post(self, request):
        cloud = get_object_or_404(UserCloud, user=request.user.id)
        # Проверка на вместимость хранилища
        if cloud.used_capacity + request.data['file'].size < cloud.capacity:
            file_type = request.data['file'].content_type.split(
                '/')[0]  # Получаем формат файла
            if file_type == 'application':
                file_type = 'other'
            request.data['_mutable'] = True
            request.data['type'] = file_type
            request.data['cloud'] = cloud.id
            request.data['name'] = request.data['file'].name
            request.data['size'] = request.data['file'].size
            serializer = CloudFileSerializer(data=request.data)
            if serializer.is_valid():
                cloud.used_capacity += request.data['size']
                cloud.save()
                serializer.save()
                return Response({
                    'msg': "File saved",
                    'data': serializer.data
                }, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({
            'detail': 'File is too large',
        }, status=status.HTTP_406_NOT_ACCEPTABLE)


class CloudFilesView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        cloud = get_object_or_404(UserCloud, user=id)
        files = CloudFile.objects.filter(cloud=cloud.id)
        serializer = CloudFileSerializer(files, many=True)
        return Response({
            'msg': 'Files received',
            'data': serializer.data
        })

    def post(self, request, id):
        cloud = get_object_or_404(UserCloud, user=id)
        # Проверка на вместимость хранилища
        if cloud.used_capacity + request.data['file'].size < cloud.capacity:
            file_type = request.data['file'].content_type.split(
                '/')[0]  # Получаем формат файла
            if file_type == 'application':
                file_type = 'other'
            request.data['_mutable'] = True
            request.data['type'] = file_type
            request.data['cloud'] = cloud.id
            request.data['name'] = request.data['file'].name
            request.data['size'] = request.data['file'].size
            serializer = CloudFileSerializer(data=request.data)
            if serializer.is_valid():
                cloud.used_capacity += request.data['size']
                cloud.save()
                serializer.save()
                return Response({
                    'msg': "File saved",
                    'data': serializer.data
                })
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({
            'error': 'File is too large',
        }, status=411)


class CloudFileView(APIView):
    """ Отображение, переименование и удаление файла """
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        file = get_object_or_404(CloudFile, id=id)
        serializer = CloudFileSerializer(file)
        return Response({
            'msg': 'File received',
            'data': serializer.data
        })

    def patch(self, request, id):
        file = get_object_or_404(CloudFile, id=id)
        initial_path = file.file.path
        new_path = file.file.path.replace(file.file.name.split(
            '/')[-1], request.data['name'])
        new_name = file.file.name.replace(file.file.name.split(
            '/')[-1], request.data['name'])
        serializer = CloudFileSerializer(
            file,
            data={
                "name": request.data['name']
            },
            partial=True)
        if serializer.is_valid():
            file.file = new_name
            file.save()
            os.rename(initial_path, new_path)
            serializer.save()
            return Response({
                'msg': 'File renamed',
                'data': serializer.data
            })
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        file = get_object_or_404(CloudFile, id=id)
        cloud = get_object_or_404(UserCloud, id=file.cloud.id)

        cloud.used_capacity -= file.size
        if cloud.used_capacity >= 0:
            cloud.save()
        file.delete()
        return Response({
            'msg': 'File deleted'
        }, status=status.HTTP_200_OK)
