from django.db import models
from account.models import Account

class HeadSalesManagerProfile(models.Model):
    
    user_id = models.ForeignKey(Account, related_name='head_sales_manager_account', verbose_name="Руководитель отдела продаж", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Профиль руководителя отдела продаж"
        verbose_name_plural = "Профили руководителей отдела продаж"

    def unicode(self):
        return self.user_id.second_name + ' ' + self.user_id.name