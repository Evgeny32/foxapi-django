from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from headSalesManager.models import HeadSalesManagerProfile 
from headSalesManager.serializers import CreateManagerSerializer, ParentAccountSerializer
from account.models import Account
from rest_framework.response import Response
from rest_framework import status
from account.serializers import AccountSerializer
from franchisee.models import FranchiseeProfile, FranchiseePackage
from agent.models import AgentProfile
from clientManager.models import ClientManagerProfile
from salesManager.models import SalesManagerProfile
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from mentor.models import MentorProfile
from mentor.serializers import MentorProfileSerializer
from administration.serializers import FranchiseePackageSerializer



from headSalesManager.serializers import (
    HeadSalesManagerSerializer, 
    FranchiseeSerializer, 
    AgentSerializer)


# !МЕНЕДЖЕРЫ


class ManagersProfileList(APIView):
    permission_classes = [IsAuthenticated]
    """Просмотр и создание менеджеров"""

    def get(self, request):
        users = []
        users += Account.objects.filter(role="SalesManager")
        users += Account.objects.filter(role='ClientManager')
        serializer = AccountSerializer(users, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CreateManagerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors)


class ManagerProfileView(APIView):
    permission_classes = [IsAuthenticated]
    """Детальная информация о менеджеров"""

    def get(self, request, id):
        manager = Account.objects.get(id=id)       
        serializer = AccountSerializer(manager)
        return Response(serializer.data)


class SelectManagerView(APIView):
    permission_classes = [IsAuthenticated]
    """Выбор менеджеров для клиентов"""

    def patch(self, request):

        if request.data['managerrole'] == 'ClientManager':
            profile_manager = ClientManagerProfile.objects.get(user_id=request.data["managerId"])
        else:
            profile_manager = SalesManagerProfile.objects.get(user_id=request.data["managerId"])


        if request.data['role'] == 'Franchisee':

            if request.data['managerrole'] == 'ClientManager':
                managers = ClientManagerProfile.objects.filter(franchisees__id=request.data["clientAccountId"])
            else:
                managers = SalesManagerProfile.objects.filter(franchisees__id=request.data["clientAccountId"])
            for manager in managers:
                manager.franchisees.remove(request.data['clientAccountId'])            
            franchisee = get_object_or_404(Account, id=request.data['clientAccountId'])
            profile_manager.franchisees.add(franchisee)
        elif request.data['role'] == 'Agent':

            if request.data['managerrole'] == 'ClientManager':
                managers = ClientManagerProfile.objects.filter(agents__id=request.data["clientAccountId"])
            else:
                managers = SalesManagerProfile.objects.filter(agents__id=request.data["clientAccountId"])
            for manager in managers:
                manager.agents.remove(request.data['clientAccountId'])       
            agent = get_object_or_404(Account, id=request.data['clientAccountId'])
            profile_manager.agents.add(agent)

        elif request.data['role'] == 'Parent':

            managers = ClientManagerProfile.objects.filter(clients__id=request.data["clientAccountId"])
            for manager in managers:
                manager.clients.remove(request.data['clientAccountId'])       
            parent = get_object_or_404(Account, id=request.data['clientAccountId'])
            profile_manager.clients.add(parent)

        elif request.data['role'] == 'Mentor':

            managers = ClientManagerProfile.objects.filter(mentors__id=request.data["clientAccountId"])
            for manager in managers:
                manager.mentors.remove(request.data['clientAccountId'])       
            mentor = get_object_or_404(Account, id=request.data['clientAccountId'])
            profile_manager.mentors.add(mentor)
        for manager in managers:
            manager.save
        profile_manager.save()
      
        return Response(status=status.HTTP_200_OK)
   
            
# !МЕНЕДЖЕРЫ

# !ФРАНЧАЙЗЕРЫ

class FranchiseeProfileList(APIView):
    permission_classes = [IsAuthenticated]
    """Список франчайзеров"""

    def get (self, request):

        franchisee = FranchiseeProfile.objects.all()
        serializer = FranchiseeSerializer(franchisee, many=True)
        return Response(serializer.data)


class FranchiseeProfileDetail(APIView):
    permission_classes = [IsAuthenticated]
    """Детальный профиль франчайза"""

    def get (self, request, id):

        franchisee = FranchiseeProfile.objects.get(id=id)
        serializer = FranchiseeSerializer(franchisee)
        return Response(serializer.data)

class FranchiseePackageListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        packages = FranchiseePackage.objects.all()
        for package in packages:
            package.franchisee = get_object_or_404(FranchiseeProfile, user_id=package.purchaser)
        serializer = FranchiseePackageSerializer(packages, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)



# !ФРАНЧАЙЗЕРЫ

# !АГЕНТЫ

class AgentProfileList(APIView):
    permission_classes = [IsAuthenticated]
    """Список агентов"""

    def get (self, request):

        agents = AgentProfile.objects.all()
        serializer = AgentSerializer(agents, many=True)
        return Response(serializer.data)


class AgentProfileDetail(APIView):
    permission_classes = [IsAuthenticated]
    """Детальный профиль агента"""

    def get (self, request, id):

        agent = AgentProfile.objects.get(id=id)
        serializer = AgentSerializer(agent)
        return Response(serializer.data)


# !АГЕНТЫ

# !РОДИТЕЛИ

class ParentsProfileList(APIView):
    permission_classes = [IsAuthenticated]
    """Список родителей"""

    def get (self, request):

        parents = Account.objects.filter(role="Parent")
        serializer = ParentAccountSerializer(parents, many=True)
        return Response(serializer.data)


class ParentProfileDetail(APIView):
    permission_classes = [IsAuthenticated]
    """Детальный профиль родителя"""

    def get (self, request, id):

        parent = Account.objects.get(role="Parent", id=id)        
        serializer = ParentAccountSerializer(parent)
        return Response(serializer.data)


# !РОДИТЕЛИ


# !МЕНТОРЫ

class MentorProfileList(APIView):
    permission_classes = [IsAuthenticated]
    """Список менторов"""

    def get (self, request):

        mentors = MentorProfile.objects.all()
        serializer = MentorProfileSerializer(mentors, many=True)
        return Response(serializer.data)


class MentorProfileDetail(APIView):
    permission_classes = [IsAuthenticated]
    """Детальный профиль ментора"""

    def get (self, request, id):

        mentor = MentorProfile.objects.get(id=id)
        serializer = MentorProfileSerializer(mentor)
        return Response(serializer.data)


# !МЕНТОРЫ






