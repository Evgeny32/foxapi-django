from django.urls import path
from headSalesManager.views import (
    ManagersProfileList,
    ManagerProfileView, 
    FranchiseeProfileList,
    FranchiseeProfileDetail,
    FranchiseePackageListView,
    AgentProfileList,
    AgentProfileDetail, 
    SelectManagerView,
    ParentsProfileList,    
    ParentProfileDetail,
    MentorProfileList,
    MentorProfileDetail )


app_name = "headsalesmanager"


# * Профиль
urlpatterns = [
    path('', ManagersProfileList.as_view(), name="ManagersProfileList"),
    path('manager/<int:id>/', ManagerProfileView.as_view(), name="ManagerProfileView"),
    path('selectmanager/', SelectManagerView.as_view(), name="SelectManagerView"),
]


# * Франчайзер
urlpatterns += [
    path('franchisees/', FranchiseeProfileList.as_view(), name="FranchiseeProfileList"),
    path('franchisee/<int:id>/', FranchiseeProfileDetail.as_view(), name="FranchiseeProfileDetail"),
    path('packages/', FranchiseePackageListView.as_view(), name="FranchiseePackageListView"),
]


# * Агент
urlpatterns += [
    path('agents/', AgentProfileList.as_view(), name="AgentProfileList"),
    path('agent/<int:id>/', AgentProfileDetail.as_view(), name="AgentProfileDetail"),
]


# * Родитель
urlpatterns += [
    path('parents/', ParentsProfileList.as_view(), name="AgentProfileList"),
    path('parent/<int:id>/', ParentProfileDetail.as_view(), name="ParentProfileDetail"),
]


# * Ментор
urlpatterns += [
    path('mentors/', MentorProfileList.as_view(), name="MentorProfileList"),
    path('mentor/<int:id>/', MentorProfileDetail.as_view(), name="MentorProfileDetail"),
]
