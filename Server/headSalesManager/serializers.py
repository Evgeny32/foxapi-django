from rest_framework import serializers
from headSalesManager.models import HeadSalesManagerProfile
from account.serializers import AccountSerializer
from franchisee.models import FranchiseeProfile
from agent.models import AgentProfile
from account.models import Account
from clientManager.models import ClientManagerProfile
from salesManager.models import SalesManagerProfile


class HeadSalesManagerSerializer(serializers.ModelSerializer):
    user_id = AccountSerializer(read_only=True)

    class Meta:
        model = HeadSalesManagerProfile
        fields = '__all__'


class CreateManagerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Account
        exclude = ['password', 'is_staff', 'is_admin', 'is_active',
                   'date_joined', 'last_login', 'is_superuser']

    def save(self):

        account = Account(
            email=self.validated_data['email'],
            role=self.validated_data['role'],
            phone=self.validated_data['phone'],
            name=self.validated_data['name'],
            second_name=self.validated_data['second_name'],
            last_name=self.validated_data['last_name'],
        )
        account_password = "7777"
        account.set_password(account_password)
        account.save()

        profile = {}
        manager_account = Account.objects.get(
            email=self.validated_data['email'])
        if self.validated_data["role"] == "ClientManager":
            profile = ClientManagerProfile(user_id=manager_account)
        elif self.validated_data["role"] == "SalesManager":
            profile = SalesManagerProfile(user_id=manager_account)

        profile.save()
        data = {
            'profile': profile,
            'account': account
        }
        return data


class FranchiseeSerializer(serializers.ModelSerializer):
    user_id = AccountSerializer(read_only=True)

    class Meta:
        model = FranchiseeProfile
        fields = '__all__'


class AgentSerializer(serializers.ModelSerializer):
    user_id = AccountSerializer(read_only=True)

    class Meta:
        model = AgentProfile
        fields = '__all__'


class ParentAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        exclude = ['password', 'is_staff', 'is_admin', 'is_active',
                   'date_joined', 'last_login', 'is_superuser']
