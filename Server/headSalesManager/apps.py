from django.apps import AppConfig


class HeadsalesmanagerConfig(AppConfig):
    name = 'headSalesManager'
    verbose_name="10) Руководитель отдела продаж"
