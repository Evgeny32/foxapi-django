from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.


class StickerPack(models.Model):
    """Альбом со стикерами"""
    name = models.CharField(_("Название пака"), max_length=256)
    img = models.ImageField(_("Лого пака"), upload_to='stiker_packs')

    class Meta:
        verbose_name = _("Набор стикеров")
        verbose_name_plural = _("Наборы стикеров")

    def __str__(self):
        return self.name


class Sticker(models.Model):
    """Стикер"""
    name = models.CharField(_("Название стикера"), max_length=256)
    description = models.TextField(_("Описание стикера"))
    img = models.ImageField(_("Изображение стикера"), upload_to='stikers')
    pack = models.ForeignKey(StickerPack, verbose_name=_(
        "Альбом"), on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = _("Стикер")
        verbose_name_plural = _("Стикеры")

    def __str__(self):
        return self.name


class ReceivedSticker(models.Model):
    """Связка стикер-пользователь. Показывает, какой стикер есть у пользователя"""
    user = models.ForeignKey("account.Account", verbose_name=_(
        "Пользователь"), on_delete=models.CASCADE)
    sticker = models.ForeignKey(Sticker, verbose_name=_(
        "Стикер"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Полученный стикер")
        verbose_name_plural = _("Полученные стикеры")
