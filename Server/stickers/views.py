from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import *
from rest_framework.permissions import IsAuthenticated, AllowAny
from .serializers import *
from .models import *
from account.models import Account


class StickerPacksView(APIView):
    permission_classes = [IsAuthenticated]
    """Все стикер паки. Создание нового стикер пака"""

    def get(self, request):
        packs = StickerPack.objects.all()
        serializer = StickerPackSerializer(packs, many=True)
        return Response({'msg': 'Stiker packs received', 'data': serializer.data}, status=HTTP_200_OK)

    def post(self, request):
        serializer = StickerPackSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'Sticker pack created', 'data': serializer.data}, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StickerPackView(APIView):
    permission_classes = [IsAuthenticated]
    """Стикер пак по id"""

    def get(self, request, id):
        pack = get_object_or_404(StickerPack, id=id)
        stickers = Sticker.objects.filter(pack=id)
        for sticker in stickers:
            try:
                ReceivedSticker.objects.get(
                    user=request.user.id, sticker=sticker.id)
            except ReceivedSticker.DoesNotExist:
                sticker.received = False
            else:
                sticker.received = True
        pack.stickers = stickers
        serializer = StickerPackFullSerializer(pack)
        return Response({'msg': 'Sticker pack received', 'data': serializer.data}, status=HTTP_200_OK)


class StickerView(APIView):
    permission_classes = [IsAuthenticated]
    """Стикер по id"""

    def get(self, request, id):
        sticker = get_object_or_404(Sticker, id=id)
        try:
            ReceivedSticker.objects.get(
                user=request.user.id, sticker=sticker.id)
        except ReceivedSticker.DoesNotExist:
            sticker.received = False
        else:
            sticker.received = True
        serializer = StickerSerializer(sticker)
        return Response({'msg': 'Sticker received', 'data': serializer.data}, status=HTTP_200_OK)


class StickersView(APIView):
    permission_classes = [IsAuthenticated]
    """Стикеры. Создание стикера"""

    def get(self, request):
        stickers = Sticker.objects.all()
        for sticker in stickers:
            try:
                ReceivedSticker.objects.get(
                    user=request.user.id, sticker=sticker.id)
            except ReceivedSticker.DoesNotExist:
                sticker.received = False
            else:
                sticker.received = True
        serializer = StickerSerializer(stickers, many=True)
        return Response({'msg': 'Stickers received', 'data': serializer.data})

    def post(self, request):
        serializer = StickerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'Sticker created', 'data': serializer.data}, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class ReceivedStickerView(APIView):
    permission_classes = [IsAuthenticated]
    """Получение стикера. Удаление стикера"""

    def post(self, request, id):
        data = {
            'sticker': get_object_or_404(Sticker, id=id),
            'user': get_object_or_404(Account, id=request.user.id)
        }
        try:
            ReceivedSticker.objects.get(user=request.user.id, sticker=id)
        except ReceivedSticker.DoesNotExist:
            pass
        else:
            return Response({'detail': 'Sticker already received'}, status=HTTP_409_CONFLICT)
        received_sticker = ReceivedSticker(**data)
        received_sticker.save()
        return Response({'msg': 'Sticker received'}, status=HTTP_200_OK)

    def delete(self, request, id):
        data = {
            'sticker': id,
            'user': request.user.id
        }
        try:
            sticker = ReceivedSticker.objects.get(
                sticker=id, user=data['user'])
        except ReceivedSticker.DoesNotExist:
            return Response({'detail': ' Not Found '}, status=HTTP_404_NOT_FOUND)
        sticker.delete()
        return Response({'msg': 'Sticker unreceived'}, status=HTTP_200_OK)
