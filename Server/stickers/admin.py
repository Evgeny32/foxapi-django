from django.contrib import admin
from .models import *


class StickerInLine(admin.TabularInline):
    model = Sticker


@admin.register(StickerPack)
class StickerPackAdmin(admin.ModelAdmin):
    inlines = [StickerInLine]


admin.site.register(Sticker)
admin.site.register(ReceivedSticker)
