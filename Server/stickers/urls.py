from django.urls import path
from .views import *

urlpatterns = [
    path('', StickersView.as_view(), name="StickersView"),
    path('<int:id>/', StickerView.as_view(), name="StickerView"),
    path('<int:id>/receive/', ReceivedStickerView.as_view(),
         name="ReceivedStickerView"),
    path('packs/', StickerPacksView.as_view(), name="StickerPacksView"),
    path('packs/<int:id>/', StickerPackView.as_view(), name="StickerPackView"),
]
