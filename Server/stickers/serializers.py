from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import Sticker, StickerPack


class StickerPackSerializer(ModelSerializer):

    class Meta:
        model = StickerPack
        fields = '__all__'


class StickerSerializer(ModelSerializer):
    received = serializers.BooleanField(default=False)

    class Meta:
        model = Sticker
        fields = '__all__'


class StickerPackFullSerializer(ModelSerializer):
    stickers = StickerSerializer(many=True)

    class Meta:
        model = StickerPack
        fields = '__all__'
