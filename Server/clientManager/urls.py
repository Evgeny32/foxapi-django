from django.urls import path
from clientManager.views import (
    ClientManagerProfileView,
    FranchiseeView,
    FranchiseeDetailView,
    ClientManagerReportView,
    FranchiseePackageListView,
    AgentsView,
    AgentDetailView,
    ParentsView,
    ParentDetailView,
    MentorsView,
    MentorDetailView,
    FranchiseeByIdView,
    AgentsByIdView,
    ParentsByIdView,
    MentorsByIdView

)


app_name = "clientmanager"


# * Профиль
urlpatterns = [
    path('', ClientManagerProfileView.as_view(),
         name="ClientManagerProfileView"),
]


# * Франчайзи
urlpatterns += [
    path('franchisees/<int:id>', FranchiseeView.as_view(), name="FranchiseeView"),
    path('<int:id>/franchisees/', FranchiseeByIdView.as_view(),
         name="FranchiseeByIdView"),
    path('franchisee/<int:id>/', FranchiseeDetailView.as_view(),
         name="FranchiseeDetailView"),
    path('franchisees/all/<int:id>', ClientManagerReportView.as_view(), name="ClientManagerReportView"),
    path('franchisees/packages/<int:id>', FranchiseePackageListView.as_view(), name="FranchiseePackageListView"),
]


# * Агент
urlpatterns += [
    path('agents/<int:id>/', AgentsView.as_view(), name="AgentsView"),
    path('<int:id>/agents/', AgentsByIdView.as_view(), name="AgentsByIdView"),
    path('agent/<int:id>/', AgentDetailView.as_view(), name="AgentDetailView"),
]

# * Родитель
urlpatterns += [
    path('parents/<int:id>/', ParentsView.as_view(), name="ParentsView"),
    path('<int:id>/parents/', ParentsByIdView.as_view(), name="ParentsByIdView"),
    path('parent/<int:id>/', ParentDetailView.as_view(), name="ParentDetailView"),
]

# * Ментор
urlpatterns += [
    path('mentors/<int:id>/', MentorsView.as_view(), name="MentorsView"),
    path('<int:id>/mentors/', MentorsByIdView.as_view(), name="MentorsByIdView"),
    path('mentor/<int:id>/', MentorDetailView.as_view(), name="MentorDetailView"),
]
