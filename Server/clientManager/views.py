from django.shortcuts import get_object_or_404, render
from rest_framework.decorators import api_view, permission_classes
from clientManager.models import ClientManagerProfile
from account.models import Account
from rest_framework.response import Response
from clientManager.serializers import ClientManagerSerializer, FranchiseeSerializer, ClientManagerReportSerializer
from rest_framework import status
from franchisee.serializers import FranchiseeProfileSerializer, ClientsFranchiseeSerializer, FranchiseeAllInfoSerializer
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from agent.serializers import AgentSerializer
from account.serializers import AccountProfileSerializer, AccountsSerializer
from mentor.serializers import MentorProfileSerializer
from agent.models import AgentProfile
from franchisee.models import FranchiseeProfile, FranchiseePackage
from franchisee.serializers import PatchFranchiseeProfileSerializer
from mentor.models import MentorProfile, ChildGroup
from mentor.serializers import MentorProfileSerializer
from branch.models import BranchProfile
from child.models import ChildProfile
from administration.serializers import FranchiseePackageSerializer


# !ОТДЕЛ КЛИЕНТ МЕНЕДЖЕРА


class ClientManagerProfileView(APIView):
    """Просмотр, создание и изменение профиля Менеджера по клиентам"""
    permission_classes = [IsAuthenticated]

    def get_profile(self, id):
        try:
            profile = ClientManagerProfile.objects.get(user_id=id)
            return profile
        except ClientManagerProfile.DoesNotExist:
            return None

    def get(self, request, format=None):
        profile = self.get_profile(request.user.id)
        if(profile):
            serializer = ClientManagerSerializer(profile)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def post(self, request, format=None):
        request.data['_mutable'] = True
        request.data['user_id'] = request.user.id
        request.data['_mutable'] = False
        serializer = ClientManagerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'response': 'Данные успешно добавлены.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, format=None):
        profile = self.get_profile(request.user.id)
        if(profile):
            serializer = ClientManagerSerializer(
                profile, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.update(profile, validated_data=request.data)
                return Response({'response': 'Успешно обновлено.'})
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

# !ОТДЕЛ КЛИЕНТ МЕНЕДЖЕРА


# !ОТДЕЛ ФРАНЧАЙЗИ

class FranchiseeView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = ClientManagerProfile.objects.get(user_id=id)
        except ClientManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        franchisees = []
        for franch in profile.franchisees.all():
            try:
                franchprofile = FranchiseeProfile.objects.get(
                    user_id=franch.id)
                franchisees.append(franchprofile)
            except FranchiseeProfile.DoesNotExist:
                pass
        serializer = ClientsFranchiseeSerializer(franchisees, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseeByIdView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = ClientManagerProfile.objects.get(user_id=id)
        except ClientManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        franchisees = []
        for franch in profile.franchisees.all():
            try:
                franchprofile = FranchiseeProfile.objects.get(
                    user_id=franch.id)
                franchisees.append(franchprofile)
            except FranchiseeProfile.DoesNotExist:
                pass
        serializer = ClientsFranchiseeSerializer(franchisees, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseeDetailView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):

        franchisee = get_object_or_404(FranchiseeProfile, id=id)

        serializer = ClientsFranchiseeSerializer(franchisee)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id):
        try:
            profile = FranchiseeProfile.objects.get(user_id=id)
        except FranchiseeProfile.DoesNotExist:
            serializer = PatchFranchiseeProfileSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            try:
                buh = Account.objects.get(email=request.data['emailBuch'])
            except:
                None
            else:
                request.data['_mutable'] = True
                request.data['emailBuch'] = buh.id
                request.data['_mutable'] = False
            serializer = PatchFranchiseeProfileSerializer(
                profile, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ClientManagerReportView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = ClientManagerProfile.objects.get(user_id=id)
        except ClientManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        items = []
        for franch in profile.franchisees.all(): 
            packages = FranchiseePackage.objects.filter(purchaser=franch.id)
            franchisee = get_object_or_404(FranchiseeProfile, user_id=franch.id)
            for package in packages:
                branches = BranchProfile.objects.filter(package=package.id)
                for branch in branches:
                    mentors = MentorProfile.objects.filter(branch=branch.user_id.id)
                    for mentor in mentors:
                        groups = ChildGroup.objects.filter(mentor=mentor.mentor.id)
                        for group in groups:
                            for child in group.children.all():
                                item = {
                                    'date': package.date,
                                    'doc_number': package.id,
                                    'franchisee_name': franchisee.name,
                                    'mentor_name': '{0} {1} {2}'.format(mentor.mentor.second_name, mentor.mentor.name, mentor.mentor.last_name),
                                    'group_number': group.id,
                                    'child_name': '{0} {1} {2}'.format(child.child.second_name, child.child.name, child.child.last_name),
                                    'phone': franchisee.user_id.phone,
                                    'duty': '{0} {1} {2}'.format(franchisee.agent.second_name, franchisee.agent.name, franchisee.agent.last_name) if franchisee.agent else 'Без ответственного',
                                    'format': package.format,
                                    'age': package.name

                                }
                                items.append(item)
        
                                

        serializer = ClientManagerReportSerializer(items, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FranchiseePackageListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):

        try:
            profile = ClientManagerProfile.objects.get(user_id=id)
        except ClientManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        packages = []
        for franch in profile.franchisees.all():

            packages += FranchiseePackage.objects.filter(purchaser=franch.id)

        for package in packages:
            try:
                package.franchisee = FranchiseeProfile.objects.get(
                    user_id=package.purchaser)
                package.agent = None
            except FranchiseeProfile.DoesNotExist:
                try:
                    package.agent = AgentProfile.objects.get(
                        user_id=package.purchaser)
                    package.franchisee = None
                except AgentProfile.DoesNotExist:
                    package.agent = None
                    package.franchisee = None

        serializer = FranchiseePackageSerializer(packages, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


# !ОТДЕЛ ФРАНЧАЙЗИ

# !ОТДЕЛ АГЕНТА

class AgentsView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = ClientManagerProfile.objects.get(user_id=id)
        except ClientManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        agents = []
        for agent in profile.agents.all():
            try:
                agentprofile = AgentProfile.objects.get(user_id=agent.id)
                agents.append(agentprofile)
            except AgentProfile.DoesNotExist:
                pass
        serializer = AgentSerializer(agents, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class AgentsByIdView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = ClientManagerProfile.objects.get(user_id=id)
        except ClientManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        agents = []
        for agent in profile.agents.all():
            try:
                agentprofile = AgentProfile.objects.get(user_id=agent.id)
                agents.append(agentprofile)
            except AgentProfile.DoesNotExist:
                pass
        serializer = AgentSerializer(agents, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class AgentDetailView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            agent = AgentProfile.objects.get(id=id)
        except AgentProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = AgentSerializer(agent)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, id, format=None):
        try:
            agent = AgentProfile.objects.get(id=id)
        except AgentProfile.DoesNotExist:
            serializer = AgentSerializer(data=request.data)
        else:
            serializer = AgentSerializer(
                agent, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# !ОТДЕЛ АГЕНТА


# !ОТДЕЛ РОДИТЕЛЯ

class ParentsView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = ClientManagerProfile.objects.get(user_id=id)
        except ClientManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = AccountsSerializer(profile.clients.all(), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ParentsByIdView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = ClientManagerProfile.objects.get(user_id=id)
        except ClientManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = AccountsSerializer(profile.clients.all(), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ParentDetailView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = Account.objects.get(id=id)
        except Account.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = AccountsSerializer(profile)
        return Response(serializer.data, status=status.HTTP_200_OK)


# !ОТДЕЛ РОДИТЕЛЯ


# !ОТДЕЛ МЕНТОРА

class MentorsView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = ClientManagerProfile.objects.get(user_id=id)
        except ClientManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        mentors = []
        for mentor in profile.mentors.all():
            mentors.append(MentorProfile.objects.get(mentor=mentor.id))

        serializer = MentorProfileSerializer(mentors, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class MentorsByIdView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        try:
            profile = ClientManagerProfile.objects.get(user_id=id)
        except ClientManagerProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        mentors = []
        for mentor in profile.mentors.all():
            mentors.append(MentorProfile.objects.get(mentor=mentor.id))

        serializer = MentorProfileSerializer(mentors, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class MentorDetailView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        # try:
        #     profile = ClientManagerProfile.objects.get(user_id=request.user.id)
        # except ClientManagerProfile.DoesNotExist:
        #     return Response(status=status.HTTP_404_NOT_FOUND)

        mentor = get_object_or_404(MentorProfile, id=id)

        serializer = MentorProfileSerializer(mentor)

        # serializer = MentorProfileSerializer(profile.mentors.get(id=id))
        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, id, format=None):
        try:
            mentor = MentorProfile.objects.get(id=id)
        except MentorProfile.DoesNotExist:
            serializer = MentorProfileSerializer(data=request.data)
        else:
            serializer = MentorProfileSerializer(
                mentor, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# !ОТДЕЛ МЕНТОРА
