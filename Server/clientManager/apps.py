from django.apps import AppConfig


class ClientmanagerConfig(AppConfig):
    name = 'clientManager'
    verbose_name="12) Клиент-менеджер"
