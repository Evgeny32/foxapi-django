from django.db import models
from django.utils.translation import ugettext_lazy as _

class ClientManagerProfile(models.Model):
    user_id = models.ForeignKey("account.Account", related_name='client_manager_account', verbose_name="Клиент менеджер", on_delete=models.CASCADE)    
    clients = models.ManyToManyField("account.Account", verbose_name=_("Клиенты"), related_name="clientManagerClients", blank=True)
    franchisees = models.ManyToManyField("account.Account", verbose_name=_("Франчайзи"), related_name="clientManagerFranchisees",  blank=True)
    agents = models.ManyToManyField("account.Account", verbose_name=_("Агенты"), related_name="clientManagerAgents", blank=True)
    mentors = models.ManyToManyField("account.Account", verbose_name=_("Менторы"),related_name="clientManagerMentors", blank=True)

    class Meta:
        verbose_name = "Профиль клиент менеджера"
        verbose_name_plural = "Профили клиент менеджеров"

    def unicode(self):
        return self.user_id.second_name + ' ' + self.user_id.name