from rest_framework import serializers
from clientManager.models import ClientManagerProfile
from account.serializers import AccountSerializer


class ClientManagerSerializer(serializers.ModelSerializer):
    user_id = AccountSerializer(read_only=True)

    class Meta:
        model = ClientManagerProfile
        fields = '__all__'


class FranchiseeSerializer(serializers.ModelSerializer):    

    class Meta:
        model = ClientManagerProfile
        fields = ["franchisees"]

class ClientManagerReportSerializer(serializers.Serializer):
    date = serializers.CharField()
    doc_number = serializers.CharField()
    franchisee_name = serializers.CharField()
    mentor_name = serializers.CharField()
    group_number = serializers.CharField()
    child_name = serializers.CharField()
    phone = serializers.CharField()
    duty = serializers.CharField()
    format = serializers.IntegerField()
    age = serializers.IntegerField()