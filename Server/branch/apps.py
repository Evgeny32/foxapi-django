from django.apps import AppConfig


class BranchConfig(AppConfig):
    name = 'branch'
    verbose_name = '4) Филиал'
