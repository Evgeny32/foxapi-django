from django.contrib import admin
from .models import BranchProfile



@admin.register(BranchProfile)
class BranchFields(admin.ModelAdmin):
    list_display = ('user_id', 'name', 'region', 'director', 'package')


