from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from .models import BranchProfile
from mentor.models import MentorProfile, ChildGroup
from child.models import ChildProfile, LevelLogs
from tales.models import Tale, Task, Answer
from coach.models import Coaching
from account.models import Account

from .serializers import (
    MentorRegistrationSerializer,
    MentorListSerializer,
    MentorProfileSerializer,
    MentorProfileSimpleSerializer,
    MentorGroupListSerializer,
    BranchChildListSerializer,
    BranchChildSerializer,
    TaleListSerializer,
    ChildLogListSerializer,
    BranchProfileSerializer
)
from tales.serializers import TaleSerializer
from account.serializers import AccountSerializer
from franchisee.models import FranchiseeProfile
# ! ОТДЕЛ МЕНТОРОВ

# * Регистрация нового ментора и отображение списка менторов


class MentorListView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, format=None):  # * Регистрация
        request.data['_mutable'] = True
        request.data['branch'] = request.user.id
        request.data['_mutable'] = False
        serializer = MentorRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, format=None):  # * Список
        mentors = MentorProfile.objects.filter(branch=request.user.id)

        for mentor in mentors:
            groups = ChildGroup.objects.filter(mentor=mentor.mentor.id)
            children = 0
            for group in groups:
                children += len(group.children.all())
            mentor.children = children  # * Колличество детей ментора
            mentor.trainings = len(
                Coaching.objects.filter(mentor=mentor.mentor.id))  # * Колличество тренингов ментора
            mentor.groups = len(groups)  # * Колличество групп ментора
            mentor.all = 0  # * Оборот ментора
        serializer = MentorListSerializer(
            mentors, many=True)
        return Response(serializer.data)


# * Отображение одного ментора и его изменение по id
class MentorView(APIView):
    permission_classes = [IsAuthenticated]

    def get_mentor(self, id):
        try:
            mentor = MentorProfile.objects.get(mentor=id)
            return mentor
        except MentorProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id, format=None):
        mentor = self.get_mentor(id)
        serializer = MentorProfileSerializer(
            mentor)
        return Response(serializer.data)

    def patch(self, request, id, format=None):
        mentor = self.get_mentor(id)
        serializer = MentorProfileSimpleSerializer(
            mentor, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# * Отображение групп ментора по его id
class MentorGroupListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        groups = ChildGroup.objects.filter(mentor=id)
        serializer = MentorGroupListSerializer(groups, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

# ! ОТДЕЛ МЕНТОРОВ

# ! ОТДЕЛ ДЕТЕЙ

# * Получение всех детей филиала


class BranchChildListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        mentors = MentorProfile.objects.filter(branch=request.user.id)

        children = []
        for mentor in mentors:
            child_list = ChildProfile.objects.filter(mentor=mentor.mentor.id)
            for child in child_list:
                tale = Tale.objects.filter(child=child.id).order_by(
                    'number').first()  # * Достаем название последней сказки ребенка
                if tale:
                    child.tale = tale.name
                else:
                    child.tale = 'Сказка не найдена'
            children += child_list
        serializer = BranchChildListSerializer(children, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


# * Отображение одного ребенка
class BranchChildView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        try:
            child = ChildProfile.objects.get(child=id)
        except:
            return Response({"title": "Не найден", "message": "Профиль ребенка не был найден"}, status=status.HTTP_404_NOT_FOUND)

        try:
            mentor_profile = MentorProfile.objects.get(mentor=child.mentor.id)
            try:
                branch = BranchProfile.objects.get(
                    user_id=mentor_profile.branch.id)
                franchisee_account_id = branch.franchisee.id
                branch = branch.name
                try:
                    franchisee = FranchiseeProfile.objects.get(
                        user_id=franchisee_account_id).name
                except FranchiseeProfile.DoesNotExist:
                    branch = ''
                    franchisee = 'Франчайз еще не добавил свои данные'
            except BranchProfile.DoesNotExist:
                branch = ''
                franchisee = 'Франчайз еще не добавил свои данные'
        except MentorProfile.DoesNotExist:
            branch = ''
            franchisee = 'Франчайз еще не добавил свои данные'

        tale = Tale.objects.filter(child=child.id).order_by(
            'number').first()  # * Достаем название последней сказки ребенка
        if tale:
            tale = tale.name
        else:
            tale = 'Сказка не найдена'
        try:
            group = ChildGroup.objects.get(
                children__in=[child.id])  # * Достаем группу ребенка
            group = group.name
        except:
            group = 'Группа не найдена'

        account = Account.objects.filter(id=child.child.id)
        for item in account:
            child.ban = item.ban

        # * Присваиваем все
        child.branch = branch
        child.franchisee = franchisee
        child.tale = tale
        child.group = group

        serializer = BranchChildSerializer(child)
        return Response(serializer.data, status=status.HTTP_200_OK)


# * Отображение ежедневных логов ребенка
class ChildLogsView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        logs = LevelLogs.objects.filter(child=id).order_by('-date')
        serializer = ChildLogListSerializer(logs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

# * Отображение сказок ребенка


class ChildTaleListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        child_profile = ChildProfile.objects.get(child=id)
        tales = Tale.objects.filter(child=child_profile.id)
        # for tale in tales:
        #     tasks = Task.objects.filter(tale=tale.id)
        #     for task in tasks:
        #         task.answers = Answer.objects.filter(task=task.id)
        #     tale.tasks = tasks
        serializer = TaleListSerializer(tales, many=True)
        return Response(serializer.data)


class ChildTaleView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        tale = Tale.objects.get(id=id)
        tasks = Task.objects.filter(tale=tale.id)
        for task in tasks:
            task.answers = Answer.objects.filter(task=task.id)
        tale.tasks = tasks
        serializer = TaleSerializer(tale)
        return Response(serializer.data)


# * Бан/разбан ребенка
class BanChildView(APIView):
    permission_classes = [IsAuthenticated]

    def patch(self, request, id, format=None):
        try:
            child = ChildProfile.objects.get(id=id)
        except ChildProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        try:
            account = Account.objects.get(id=child.child.id)
        except Account.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = AccountSerializer(
            account, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(account, validated_data=request.data)
            return Response({'response': "success"})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# !ОТДЕЛ РЕБЕНКА

# !ОТДЕЛ ПРОФИЛЯ ФИЛИАЛА

# * Отображение, создание и изменение профиля филиала
class BranchProfileView(APIView):
    permission_classes = [IsAuthenticated]

    def get_profile(self, id):
        try:
            profile = BranchProfile.objects.get(user_id=id)
            return profile
        except BranchProfile.DoesNotExist:
            return None

    def get(self, request, format=None):
        profile = self.get_profile(request.user.id)
        if(profile):
            serializer = BranchProfileSerializer(profile)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def post(self, request, format=None):
        request.data['_mutable'] = True
        request.data['user_id'] = request.user.id
        serializer = BranchProfileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'response': 'Данные успешно добавлены.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, format=None):
        profile = self.get_profile(request.user.id)
        if(profile):
            serializer = BranchProfileSerializer(
                profile, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.update(profile, validated_data=request.data)
                return Response({'response': 'Успешно обновлено.'})
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)
# !ОТДЕЛ ПРОФИЛЯ ФИЛИАЛА
