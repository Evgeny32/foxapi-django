from django.urls import path

from .views import (
    BranchProfileView,
    MentorListView,
    MentorView,
    MentorGroupListView,
    BranchChildListView,
    BranchChildView,
    ChildLogsView,
    ChildTaleListView,
    ChildTaleView,
    BanChildView
)


app_name = "branch"

# * Профиль
urlpatterns = [
    path('', BranchProfileView.as_view(), name="BranchProfileView"),
]
# * Менторы
urlpatterns += [
    path('mentor/', MentorListView.as_view(), name="MentorListView"),
    path('mentor/<int:id>/', MentorView.as_view(), name="MentorView"),
    path('mentor/<int:id>/group/', MentorGroupListView.as_view(),
         name="MentorGroupListView"),
]
# * Дети
urlpatterns += [
    path('child/', BranchChildListView.as_view(), name="BranchChildListView"),
    path('child/<int:id>/', BranchChildView.as_view(), name="BranchChildView"),
    path('child/<int:id>/tale_log/', ChildLogsView.as_view(), name="ChildLogsView"),
    path('child/<int:id>/tales/', ChildTaleListView.as_view(),
         name="ChildTaleListView"),
    path('child/tales/<int:id>/',
         ChildTaleView.as_view(), name="ChildTaleView"),
    path('child/ban/<int:id>/', BanChildView.as_view(), name="BanChild"),

]
