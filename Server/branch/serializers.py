from django.db.models import fields
from rest_framework import serializers

from .models import BranchProfile
from account.models import Account
from child.models import ChildProfile, LevelLogs
from mentor.models import MentorProfile, ChildGroup
from tales.models import Tale

from account.serializers import AccountSerializer


# ! ОТДЕЛ МЕНТОРОВ

# * Регистрация нового ментора


class MentorRegistrationSerializer(serializers.ModelSerializer):
    branch = serializers.IntegerField()
    job_status = serializers.CharField()
    passport = serializers.CharField()
    passport_who = serializers.CharField()
    passport_when = serializers.DateTimeField()
    adress = serializers.CharField()

    passport_file = serializers.FileField()
    inn_file = serializers.FileField()
    snils_file = serializers.FileField()

    class Meta:
        model = Account
        fields = ['email', 'name', 'second_name', 'last_name',
                  'phone', 'branch', 'job_status', 'passport', 'passport_who', 'passport_when', 'adress',
                  'passport_file', 'inn_file', 'snils_file']

    def save(self):
        account = Account(
            email=self.validated_data['email'],
            role='Mentor',
            phone=self.validated_data['phone'],
            name=self.validated_data['name'],
            second_name=self.validated_data['second_name'],
            last_name=self.validated_data['last_name'],
        )
        account_password = "7777"
        # account_password = get_random_string(12)
        # send_mail(
        #     'Данные об аккаунте',
        #     'Ваша почта - '+ account.email + '\nВаш пароль - ' + account_password,
        #     'businessfoxmail@gmail.com',
        #     [account.email],
        #     fail_silently=False,
        # )
        account.set_password(account_password)
        account.save()
        branchAccount = Account.objects.get(id=self.validated_data['branch'])
        mentorAccount = Account.objects.get(email=self.validated_data['email'])
        profile = MentorProfile(
            job_status=self.validated_data['job_status'],
            passport=self.validated_data['passport'],
            passport_who=self.validated_data['passport_who'],
            passport_when=self.validated_data['passport_when'],
            adress=self.validated_data['adress'],

            passport_file=self.validated_data['passport_file'],
            inn_file=self.validated_data['inn_file'],
            snils_file=self.validated_data['snils_file'],
            branch=branchAccount,
            mentor=mentorAccount
        )
        profile.save()
        data = {
            'profile': profile,
            'account': account
        }
        return data


# * Сериализатор списка менторов
class FIOSerializer(serializers.ModelSerializer):  # *
    class Meta:
        model = Account
        fields = ['id', 'name', 'second_name', 'last_name']


class MentorListSerializer(serializers.ModelSerializer):
    children = serializers.IntegerField()
    trainings = serializers.IntegerField()
    groups = serializers.IntegerField()
    all = serializers.IntegerField()
    mentor = FIOSerializer()

    class Meta:
        model = MentorProfile
        fields = ['id', 'mentor', 'children', 'all', 'trainings',
                  'groups', 'all', 'date_work', 'date_layoff']


# * Сериализатор на странице ментора
class MentorProfileSerializer(serializers.ModelSerializer):  # * Для отображения
    mentor = AccountSerializer()

    class Meta:
        model = MentorProfile
        fields = '__all__'


# * Для изменений
class MentorProfileSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = MentorProfile
        fields = '__all__'


# * Отображение групп ментора
class MentorGroupListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChildGroup
        fields = ['id', 'name', 'children', 'creation_date']

# ! ОТДЕЛ МЕНТОРОВ

# ! ОТДЕЛ РЕБЕНКА
# * Получение всех детей филиала


class BranchChildListSerializer(serializers.ModelSerializer):
    tale = serializers.CharField()
    child = AccountSerializer()

    class Meta:
        model = ChildProfile
        fields = ['id', 'child', 'age', 'tale',
                  'rank', 'creation_date', 'learning']

# * Отображение данных по одному ребенку


class BranchChildSerializer(serializers.ModelSerializer):

    branch = serializers.CharField()
    franchisee = serializers.CharField()
    mentor = FIOSerializer()
    parent = FIOSerializer()
    child = AccountSerializer()
    group = serializers.CharField()
    tale = serializers.CharField()
    ban = serializers.BooleanField()

    class Meta:
        model = ChildProfile
        fields = ['id', 'child', 'age', 'parent',
                  'branch', 'franchisee', 'mentor', 'group', 'tale', 'rank', 'ban']

# * Отображение ежедневных логов ребенка


class ChildLogListSerializer(serializers.ModelSerializer):

    class Meta:
        model = LevelLogs
        fields = ['id', 'date', 'rank', 'fox_story', 'program']

# * Отображение списка сказок ребенка


class TaleListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tale
        fields = ['id', 'name', 'rank', 'end_date']

# ! ОТДЕЛ РЕБЕНКА


# ! ОТДЕЛ ФИЛИАЛА

# * Профиль филиала
class BranchProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = BranchProfile
        fields = "__all__"
