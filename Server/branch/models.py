from django.db import models
from account.models import Account
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save, pre_delete
from django.conf import settings


#! ПРОФИЛЬ ФИЛИАЛА
#!!!!!!!!!!!!!!!!!!!!!!!

def branch_upload_path(instance, filename):
    return '{0}/{1}'.format(instance.user_id.email, filename)


class BranchProfile(models.Model):

    user_id = models.ForeignKey(
        Account, related_name='branch_account', verbose_name="Филиал", on_delete=models.CASCADE)
    package = models.ForeignKey("franchisee.FranchiseePackage", verbose_name="Родительский пакет", on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(verbose_name="Наименование",
                            max_length=30, blank=True)    
    franchisee = models.ForeignKey(
        Account, related_name='franchisee', verbose_name="Франщиза", null=True, blank=True, on_delete=models.CASCADE)
    region = models.CharField(verbose_name="Регион", max_length=30, blank=True)
    director = models.CharField(
        verbose_name="Директор", max_length=30, blank=True)

    registration_sertificate = models.FileField(
        upload_to=branch_upload_path, verbose_name="Свидетельство о регистрации")
    accounting_sertificate = models.FileField(
        upload_to=branch_upload_path, verbose_name="Свидетельство о постановке на учет")
    partner_form = models.FileField(
        upload_to=branch_upload_path, verbose_name="Анкета партнёра")
    main = models.BooleanField("Основной филиал", default=False)

    def unicode(self):
        return self.name

    class Meta:
        verbose_name = "Профиль филиала"
        verbose_name_plural = "Профили филиалов"




@receiver(pre_save, sender=BranchProfile)
def delete_old_image(sender, instance=None, **kwargs):
    if instance.registration_sertificate:
        try:
            old_file = BranchProfile.objects.get(pk=instance.pk).registration_sertificate
        except BranchProfile.DoesNotExist:
            return
        else:
            new_file = instance.registration_sertificate

            if old_file != new_file:
                old_file.delete(save=False)

    if instance.accounting_sertificate:
        try:
            old_file = BranchProfile.objects.get(pk=instance.pk).accounting_sertificate
        except BranchProfile.DoesNotExist:
            return
        else:
            new_file = instance.accounting_sertificate

            if old_file != new_file:
                old_file.delete(save=False)


    if instance.partner_form:
        try:
            old_file = BranchProfile.objects.get(pk=instance.pk).partner_form
        except BranchProfile.DoesNotExist:
            return
        else:
            new_file = instance.partner_form

            if old_file != new_file:
                old_file.delete(save=False)

    
