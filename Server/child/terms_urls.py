from django.urls import path
from .terms_views import GetChildTermsByIdView, TermsView, TermView
urlpatterns = [
    path('child/<int:id>/', GetChildTermsByIdView.as_view(),
         name="GetChildTermsByIdView"),
    path('', TermsView.as_view(), name="TermsView"),
    path('<int:id>', TermView.as_view(), name="TermView"),
]
