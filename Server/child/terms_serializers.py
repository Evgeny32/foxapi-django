from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import Term, ChildProfile


class TermSerializer(serializers.ModelSerializer):
    """ Сериализатор терминов """
    class Meta:
        model = Term
        fields = '__all__'
