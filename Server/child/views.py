from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from account.models import Account
from mentor.models import ChildGroup, MentorProfile
from mentor.serializers import ChildGroupSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from branch.models import BranchProfile
from .serializers import (
    ChildRegistrationSerializer,
    ChildrenSerializer,
    ChildNameSerializer,
    ChildSloganSerializer,
    ChildTermsSerializer,
    ChildTermSerializer,
    ChildNickAndSloganSerializer,
    ChildTermTakeSerializer,
    ChildAccountSerializer,
    ChildProfileChangeSerializer,
    ChildAccountChangeSerializer
)

# ################################################3
from account.models import Account
from .models import ChildProfile, Term
from .serializers import ChildrenSerializer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import status
from chat.models import Room


class ParentChildrenView(APIView):
    """Отображение детей родителя"""
    permissions_classes = [IsAuthenticated]

    def get(self, request):
        children = ChildProfile.objects.filter(parent=request.user.id)
        print(children)
        serializer = ChildrenSerializer(children, many=True)
        return Response(serializer.data, status=200)


class ChildRegistrationView(APIView):
    """Регистрация нового ребенка"""
    permission_classes = [IsAuthenticated]

    def check_online_group(self, age):
        """Проверяет наличие свободных онлайн групп заданной возрастной группы"""
        # online_admin_branch = BranchProfile.objects.get(main="True") # находим онлайн филиал
        # main_mentor = MentorProfile.objects.filter(branch=online_admin_branch.user_id.id).first() # находим первого ментора филиала
        mentor_groups = ChildGroup.objects.filter(
            online=True)  # находим все группы данного ментора

        chosen_group = None
        age_groups = {
            '5-7': [5, 6, 7],
            '8-12': [8, 9, 10, 11, 12],
            '13-16': [13, 14, 15, 16]
        }
        for mentor_group in mentor_groups:
            if len(mentor_group.children.all()) <= 50:
                if int(age) in age_groups[mentor_group.age_group]:
                    chosen_group = mentor_group
                    return chosen_group
        return None

    def post(self, request, format=None):

        group = request.data['group']
        if not group:
            chosen_online_group = self.check_online_group(request.data['age'])
            if chosen_online_group:
                request.data['_mutable'] = True
                request.data['group'] = chosen_online_group.id
                request.data['mentor'] = chosen_online_group.mentor.id
                request.data['_mutable'] = False
            else:
                online_admin_branch = BranchProfile.objects.get(
                    main="True")  # находим онлайн филиал
                main_mentor = MentorProfile.objects.filter(
                    branch=online_admin_branch.user_id.id).first()  # находим первого ментора филиала

                age_groups = {
                    '5-7': [5, 6, 7],
                    '8-12': [8, 9, 10, 11, 12],
                    '13-16': [13, 14, 15, 16]
                }
                online_groups = ChildGroup.objects.filter(online=True)
                age_online_groups_len = 1
                for online_group in online_groups:
                    if int(request.data['age']) in age_groups[online_group.age_group]:
                        age_online_groups_len = age_online_groups_len + 1
                child_age_category = '13-16'
                if int(request.data['age']) < 13:
                    child_age_category = '8-12'
                if int(request.data['age']) < 8:
                    child_age_category = '5-7'
                new_online_group = ChildGroup(
                    name="Онлайн группа № " +
                    str(age_online_groups_len) +
                    " (" + str(child_age_category) + " лет)",
                    age_group=child_age_category,
                    mentor=main_mentor.mentor,
                    online=True
                )
                new_online_group.save()

                request.data['_mutable'] = True
                request.data['mentor'] = main_mentor.mentor.id
                request.data['group'] = new_online_group.id
                request.data['_mutable'] = False
        request.data['_mutable'] = True
        request.data['parent'] = request.user.id
        serializer = ChildRegistrationSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            child = get_object_or_404(Account, email=request.data['email'])
            parent = get_object_or_404(Account, id=request.user.id)
            room = Room(
                creator=request.user,
                name='{0} {1} {2} - {3} {4} {5}'.format(
                    parent.second_name, parent.name, parent.last_name, child.second_name, child.name, child.last_name)
            )
            room.save()
            room.users.set([request.user, child])
            room.save()

            return Response({'msg': "Child registered successfully"}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChildNameView(APIView):
    """Изменение фио и слогана ребенка родителем через id в реквесте"""
    permissions_classes = [IsAuthenticated]

    def get_child(self, id):
        try:
            child = Account.objects.get(id=id)
        except Account.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        try:
            profile = ChildProfile.objects.get(child=id)
        except ChildProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return {child, profile}

    def put(self, request):
        child_data = self.get_child(request.data['id'])
        child = child_data['child']
        profile = child_data['profile']
        child_serializer = ChildNameSerializer(child, data=request.data)
        profile_serializer = ChildSloganSerializer(profile, data=request.data)
        if child_serializer.is_valid() and profile_serializer.is_valid():
            child_serializer.save()
            profile_serializer.save()
            return Response({'msg': "Child data updated successfully"}, status=200)
        if not child_serializer.is_valid():

            return Response(child_serializer.errors, status=400)
        if not profile_serializer.is_valid():

            return Response(profile_serializer.errors, status=400)


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
def get_children_mentor_view(request):
    """Отображение ГРУПП родителя"""
    if request.method == 'GET':
        mentor = Account.objects.filter(role="Mentor")
        serializer = ChildAccountSerializer(mentor, many=True)
        return Response(serializer.data)


class MentorView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        mentor = Account.objects.get(id=id)
        serializer = ChildAccountSerializer(mentor)
        return Response(serializer.data)


#! ОТОБРАЖЕНИЕ групп
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


@api_view(['POST'])
@permission_classes([IsAuthenticated, ])
def get_children_group_view(request):
    """Отображение ГРУПП родителя"""
    if request.method == 'POST':
        groups = ChildGroup.objects.filter(mentor=request.data['mentor'])
        serializer = ChildGroupSerializer(groups, many=True)
        return Response(serializer.data)


class GroupView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        mentor = ChildGroup.objects.get(id=id)
        serializer = ChildGroupSerializer(mentor)
        return Response(serializer.data)


class ChildrenTermsView(APIView):
    permission_classes = [IsAuthenticated]
    """Отображение терминов по id ребенка"""

    def get(self, request):
        terms = Term.object.filter(
            is_active=True, child__id=request.data['child'])
        serializer = ChildTermSerializer(terms, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


#! Отображение терминов ребенка у родителя
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@api_view(['POST'])
@permission_classes([IsAuthenticated, ])
def get_children_terms_view(request):
    """Отображение терминов ребенка у родитchild__idеля"""
    if request.method == 'POST':
        terms = Term.objects.filter(
            child__id=request.data['child'], is_active=True)
        serializer = ChildTermsSerializer(terms, many=True)
        return Response(serializer.data)


#!Добавление не подтвержденных терминов для детей со стороны родителя
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@api_view(['POST'])
@permission_classes([IsAuthenticated, ])
def add_child_term_view(request):
    """Добавление не подтвержденных терминов для детей со стороны родителя"""
    if request.method == 'POST':
        serializer = ChildTermSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            ser_data = serializer.save()
            data['response'] = "Term успешно зарегистрирован."
            return Response(data, status=status.HTTP_201_CREATED)
        else:
            data = serializer.errors
            return Response(data, status=status.HTTP_400_BAD_REQUEST)


#! РЕБЕНОК
#! РЕБЕНОК
#! РЕБЕНОК


#!Изменение ника и слогана ребенка со стороны ребенка
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@api_view(['PUT'])
@permission_classes([IsAuthenticated, ])
def child_nick_and_slogan_view(request):
    """Изменение ника и слогана ребенка со стороны ребенка"""
    child_id = request.user.id
    try:
        profile = ChildProfile.objects.get(child=child_id)
    except ChildProfile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'PUT':
        serializer = ChildNickAndSloganSerializer(profile, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': "Updated"}, status=200)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#! ОТОБРАЖЕНИЕ ПРОФИЛЯ РЕБЕНКА
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
def child_profile_view(request):
    """Отображение профиля ребенка"""
    if request.method == 'GET':
        children = ChildProfile.objects.get(child=request.user)
        serializer = ChildrenSerializer(children)
        return Response(serializer.data)


#! Отображение терминов ребенка у ребенка
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@api_view(['POST'])
@permission_classes([IsAuthenticated, ])
def get_child_terms_view(request):
    """Отображение терминов ребенка у ребенка"""
    if request.method == 'POST':
        terms = Term.objects.filter(
            child__id=request.data['child'], is_active=True)
        serializer = ChildTermsSerializer(terms, many=True)
        return Response(serializer.data)


#! Отображение терминов ментора, которые ребенок может взять
@api_view(['POST', ])
@permission_classes([IsAuthenticated, ])
def get_terms_view(request):
    """Отображение терминов ментора, которые ребенок может взять"""
    if request.method == 'POST':
        terms = Term.objects.filter(
            mentor=request.data['mentor'], is_active=True)
        serializer = ChildTermsSerializer(terms, many=True)
        return Response(serializer.data)


#! ВЗЯТИЕ РЕБЕНКОМ ТЕРМИНА, ПРЕДЛОЖЕННОГО МЕНТОРОМ
@api_view(['PUT', ])
@permission_classes([IsAuthenticated, ])
def child_term_take_view(request, id):
    try:
        term = Term.objects.get(id=id)
    except Term.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'PUT':

        serializer = ChildTermTakeSerializer(term, data=request.data)
        data = {}
        if serializer.is_valid():
            serializer.update(term, validated_data=request.data)
            data['response'] = "Успешно обновлено."
            return Response(data=serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChildProfileChangeView(APIView):
    """ Изменение данных ребенка ребенком """
    permission_classes = [IsAuthenticated]

    def get(self, request):
        try:
            child = ChildProfile.objects.get(child=request.user.id)
        except ChildProfile.DoesNotExist:
            return Response({'msg': 'Not Found'}, status=status.HTTP_404_NOT_FOUND)
        serializer = ChildrenSerializer(child)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request):
        try:
            profile = ChildProfile.objects.get(child=request.user.id)
        except ChildProfile.DoesNotExist:
            return Response({'msg': 'Not Found'}, status=status.HTTP_404_NOT_FOUND)

        try:
            account = Account.objects.get(id=request.user.id)
        except Account.DoesNotExist:
            return Response({'msg': 'Not Found'}, status=status.HTTP_404_NOT_FOUND)

        profile_serializer = ChildProfileChangeSerializer(
            profile, data=request.data, partial=True)
        account_serializer = ChildAccountSerializer(
            account, data=request.data, partial=True)

        if profile_serializer.is_valid() and account_serializer.is_valid():
            profile_serializer.update(
                profile, validated_data=profile_serializer.validated_data)
            account_serializer.update(
                account, validated_data=account_serializer.validated_data)
            return Response({'msg': 'Updated', 'data': {**account_serializer.data, **profile_serializer.data}}, status=status.HTTP_200_OK)
        return Response(profile_serializer.errors + account_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
