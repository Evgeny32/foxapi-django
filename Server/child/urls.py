from django.urls import path
from .views import (
    ChildRegistrationView,
    add_child_term_view,
    child_nick_and_slogan_view,
    ChildProfileChangeView,
    get_child_terms_view,
    get_terms_view,
    child_term_take_view,
    get_children_mentor_view,
    get_children_group_view,
    MentorView,
    GroupView,
    ChildrenTermsView
)

from .views import (
    ParentChildrenView,
    ChildRegistrationView,
    ChildNameView
)


app_name = 'child'


urlpatterns = [
    path('', ParentChildrenView.as_view(), name='ParentChildrenView'),
    path('register/', ChildRegistrationView.as_view(),
         name='ChildRegistrationView'),
    path('name/', ChildNameView.as_view(), name='ChildNameView'),

    # ! Отображение терминов ребенка у родителя
    path('terms/', ChildrenTermsView.as_view(), name='child_terms'),
    # !Добавление не подтвержденных терминов для детей со стороны родителя
    path('term/', add_child_term_view, name='child_term'),
    path('mentor/', get_children_mentor_view,
         name='get_children_mentor_view'),  # !МЕнторы
    path('mentor/<int:id>', MentorView.as_view(),
         name='MentorView'),  # !МЕнторы
    path('group/', get_children_group_view,
         name='get_children_group_view'),  # !Группы
    path('group/<int:id>', GroupView.as_view(),
         name='GroupView'),  # !Группы
    #! РЕБЕНОК
    # !Изменение ника и слогана ребенка со стороны ребенка
    path('nickname/', child_nick_and_slogan_view,
         name='child_nick_and_slogan_view'),
    # !Изменение ника и слогана ребенка со стороны ребенка
    path('profile/', ChildProfileChangeView.as_view(),
         name='ChildProfileChangeView'),
    # ! Отображение терминов ребенка у ребенка
    path('notebook/', get_child_terms_view, name='get_child_terms_view'),
    # ! Отображение доступных ребенку терминов
    path('dictionary/', get_terms_view, name='get_terms_view'),
    # ! ВЗЯТИЕ РЕБЕНКОМ ТЕРМИНА, ПРЕДЛОЖЕННОГО МЕНТОРОМ
    path('term/take/<int:id>/', child_term_take_view,
         name='child_term_take_view'),
]
