
from .models import LevelLogs, ChildProfile
from tales.models import Tale
from django.utils import timezone


def hi():
    children = ChildProfile.objects.all()
    for child in children:
        tales = Tale.objects.filter(child=child.id)
        log = LevelLogs(
            child=child,
            date=timezone.now(),
            rank=child.rank,
            fox_story=tales[-1].name if len(tales) > 0 else 'Сказки нет',
            program='Undefined program'
        )
        log.save()
