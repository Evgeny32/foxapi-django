from django.contrib import admin
from .models import ChildProfile, Term, LevelLogs
# Register your models here.

admin.site.register(ChildProfile)
admin.site.register(Term)
admin.site.register(LevelLogs)
