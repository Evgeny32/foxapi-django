from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from .terms_serializers import TermSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny
from .models import Term, ChildProfile


class TermsView(APIView):
    permission_classes = [IsAuthenticated]
    """ Отображение всех терминов и создание """

    def get(self, request):
        """ Отображение всех терминов """
        terms = Term.objects.all()
        serializer = TermSerializer(terms, many=True)
        return Response({
            'msg': 'Terms reveived',
            'data': serializer.data
        }, status=status.HTTP_200_OK)

    def post(self, request):
        """ Создание нового термина """
        serializer = TermSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({
                'msg': 'Term added',
                'data': serializer.data
            }, status=status.HTTP_201_CREATED)


class TermView(APIView):
    """ Отображение, изменение и удаление термина """
    permission_classes = [IsAuthenticated]

    def get_term(self, id):
        try:
            term = Term.objects.get(id=id)
        except Term.DoesNotExist:
            return Response({
                'msg': 'Not found'
            }, status=status.HTTP_404_NOT_FOUND)
        return term

    def get(self, request, id):
        """ Отображение термина """
        term = self.get_term(id)
        serializer = TermSerializer(term)
        return Response({
            'msg': 'Term received',
            'data': serializer.data
        }, status=status.HTTP_200_OK)

    def patch(self, request, id):
        """ Изменение термина """
        term = self.get_term(id)
        serializer = TermSerializer(term, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(term, validated_data=request.data)
            return Response({
                'msg': 'Term updated',
                'data': serializer.data
            }, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        """ Удаление термина """
        term = self.get_term(id)
        term.delete()
        return Response({'msg': 'Term deleted'}, status=status.HTTP_200_OK)


class GetChildTermsByIdView(APIView):
    permission_classes = [IsAuthenticated]
    """Отображение терминов по id ребенка"""

    def get(self, request, id):
        terms = Term.objects.filter(is_active=True, child__id=id)
        serializer = TermSerializer(terms, many=True)
        return Response({'msg': 'Terms received', 'data': serializer.data}, status=status.HTTP_200_OK)


class GetMentorTermsByIdView(APIView):
    permission_classes = [IsAuthenticated]
    """Отображение терминов по id ментора"""

    def get(self, request, id):
        terms = Term.objects.filter(mentor=id)
        serializer = TermSerializer(terms, many=True)
        return Response({'msg': 'Terms received', 'data': serializer.data}, status=status.HTTP_200_OK)


class GetParentTermsByIdView(APIView):
    permission_classes = [IsAuthenticated]
    """Отображение терминов по id родителя"""

    def get(self, request, id):
        terms = Term.objects.filter(request_user=id)
        serializer = TermSerializer(terms, many=True)
        return Response({'msg': 'Terms received', 'data': serializer.data}, status=status.HTTP_200_OK)
