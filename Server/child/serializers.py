from rest_framework import serializers
from account.models import Account
from mentor.models import ChildGroup
from .models import ChildProfile, Term, LevelLogs
from account.helpers import get_random_number, get_random_string
from django.core.mail import send_mail
import requests


#! РОДИТЕЛЬ
#! РОДИТЕЛЬ
#! РОДИТЕЛЬ


#! Регистрация нового ребенка
#!#############################
class ChildRegistrationSerializer(serializers.ModelSerializer):
    age = serializers.IntegerField()
    learning = serializers.CharField()
    parent = serializers.IntegerField()
    mentor = serializers.IntegerField()
    group = serializers.IntegerField()

    class Meta:
        model = Account
        fields = ['email', 'name', 'second_name', 'last_name',
                  'age', 'learning', 'parent', 'mentor', 'group']

    def save(self):
        account = Account(
            email=self.validated_data['email'],
            role='Child',
            name=self.validated_data['name'],
            second_name=self.validated_data['second_name'],
            last_name=self.validated_data['last_name'],
        )
        account_password = "7777"
        # account_password = get_random_string(12)
        # send_mail(
        #     'Данные об аккаунте',
        #     'Ваша почта - '+ account.email + '\nВаш пароль - ' + account_password,
        #     'businessfoxmail@gmail.com',
        #     [account.email],
        #     fail_silently=False,
        # )
        account.set_password(account_password)
        account.save()
        parentAccount = Account.objects.get(pk=self.validated_data['parent'])
        childAccount = Account.objects.get(email=self.validated_data['email'])
        mentorAccount = Account.objects.get(id=self.validated_data['mentor'])
        profile = ChildProfile(
            age=self.validated_data['age'],
            learning=self.validated_data['learning'],
            parent=parentAccount,
            child=childAccount,
            mentor=mentorAccount
        )
        profile.save()
        childProfile = ChildProfile.objects.get(child=childAccount)
        group = ChildGroup.objects.get(pk=self.validated_data['group'])
        group.requests.add(childProfile.id)
        data = {
            'profile': profile,
            'account': account
        }
        return data


class ChildAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        exclude = ['password', 'date_joined', 'last_login',
                   'is_admin', 'is_active', 'is_staff', 'is_superuser']


#! ОТОБРАЖЕНИЕ ОТЧЕТА ПРО СКАЗОК
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


#! ОТОБРАЖЕНИЕ ДЕТЕЙ РОДИТЕЛЯ
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
class ChildrenSerializer(serializers.ModelSerializer):
    child = ChildAccountSerializer()
    mentor = ChildAccountSerializer()
    parent = ChildAccountSerializer()

    class Meta:
        model = ChildProfile
        fields = '__all__'


#! Изменение слогана ребенка
#!!!!!!!!!!!!!!!!!!!!!!!!!
class ChildSloganSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChildProfile
        fields = ['slogan']
# *СВЯЗАНЫ
#! Изменение ФИО ребенка
#!!!!!!!!!!!!!!!!!!!!!!!!!


class ChildNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['second_name', 'name', 'last_name']


#! Отображение терминов ребенка у родителя
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
class ChildTermsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Term
        fields = '__all__'


#!Добавление не подтвержденных терминов для детей со стороны родителя
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
class ChildTermSerializer(serializers.ModelSerializer):
    class Meta:
        model = Term
        fields = '__all__'

    def save(self):
        term = Term(
            is_active=False,
            name=self.validated_data['name'],
            description=self.validated_data['description'],
            mentor=self.validated_data['mentor'],
        )
        term.save()
        term.child.set(self.validated_data['child'])
        term.save()
        return term


#! РЕБЕНОК
#! РЕБЕНОК
#! РЕБЕНОК

#!Изменение ника и слогана ребенка со стороны ребенка
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
class ChildNickAndSloganSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChildProfile
        fields = ['name', 'slogan']


#! ВЗЯТИЕ РЕБЕНКОМ ТЕРМИНА, ПРЕДЛОЖЕННОГО МЕНТОРОМ
class ChildTermTakeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Term
        exclude = ['description', ]

    def update(self, instance, validated_data):
        for child_id in validated_data['child']:
            instance.child.add(child_id)
        instance.save()
        return instance


class ChildProfileChangeSerializer(serializers.ModelSerializer):
    """ Сериализатор для изменения данных профиля ребенка ребенком в моб приложении """
    class Meta:
        model = ChildProfile
        fields = ('age', 'slogan')


class ChildAccountChangeSerializer(serializers.ModelSerializer):
    """ Сериализатор для изменения данных аккаунта ребенка ребенком в моб приложении """
    class Meta:
        model = Account
        fields = ('second_name', 'name', 'last_name', 'avatar')
