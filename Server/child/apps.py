from django.apps import AppConfig


class ChildConfig(AppConfig):
    name = 'child'
    verbose_name="2) Дети"
