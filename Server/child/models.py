from django.db import models
from account.models import Account
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
#! ПРОФИЛЬ РЕБЕНКА
#!###################


class ChildProfile(models.Model):
    parent = models.ForeignKey(Account, related_name='parent',
                               verbose_name="Родитель", null=True,  on_delete=models.SET_NULL)
    child = models.ForeignKey(
        Account, related_name='child', verbose_name="Ребенок", on_delete=models.CASCADE)

    mentor = models.ForeignKey(Account, related_name='mentor',
                               verbose_name="Ментор", null=True, blank=True, on_delete=models.SET_NULL)
    RANK = (
        ('Silver', 'Silver'),
        ('Gold', 'Gold'),
        ('Platinum', 'Platinum'),
        ('Basic Business', 'Basic Business'),
        ('Advance Business', 'Advance Business'),
        ('Profi Business', 'Profi Business'),
        ('Super Basic Business', 'Super Basic Business'),
        ('Super Advance Business', 'Super Advance Business'),
        ('Super Profi Business', 'Super Profi Business')
    )
    rank = models.CharField(max_length=50, choices=RANK,
                            default='Silver', verbose_name="Ранг")
    foxies = models.PositiveIntegerField(default=0, verbose_name='Фоксики')
    slogan = models.CharField(
        max_length=150, default="", blank=True, verbose_name='Слоган')
    LEARNING = (
        ('Online', 'Online'),
        ('Offline', 'Offline'),
    )
    learning = models.CharField(
        max_length=10, choices=LEARNING, default="Online", verbose_name='Способ обучения')
    gender = models.CharField(_("Пол"), max_length=50, default='')
    STEPS = (
        ('First Step', 'First Step'),
        ('First Idea', 'First Idea'),
        ('First Business', 'First Business'),
    )
    step = models.CharField(_("Ступень программы"),
                            choices=STEPS,  max_length=50, default='First step')
    year = models.PositiveIntegerField(_("Год обучения"), default=1)
    EMOJIS_CHOICES = [
        ('calm', 'спокойствие'),
        ('happy', 'радость'),
        ('confuse', 'смущение'),
        ('grudge', 'обида'),
        ('anger', 'злость'),
        ('fury', 'ярость'),
        ('fear', 'испуг'),
        ('sad', 'грусть'),
        ('surprise', 'удивление'),
    ]
    emotion = models.CharField(
        _("Звезда настроения"), max_length=50, choices=EMOJIS_CHOICES, null=True)
    age = models.PositiveIntegerField(default=0, verbose_name='Возраст')
    name = models.CharField(max_length=20, default="", verbose_name="Погоняло")
    creation_date = models.DateField(auto_now=True)

    def unicode(self):
        return self.child.second_name + " " + self.child.name + " " + self.child.last_name

    class Meta:
        verbose_name = "Профиль ребенка"
        verbose_name_plural = "Профили детей"


#! ТЕРМИНЫ РЕБЕНКА
#!#####################
class Term(models.Model):
    is_active = models.BooleanField(default=False, verbose_name="Одобрен")
    mentor = models.ForeignKey(Account, related_name='mentor_term',
                               verbose_name="Ментор", on_delete=models.SET_NULL, null=True)
    child = models.ManyToManyField(
        "child.ChildProfile", verbose_name=_("Дети"))
    request_user = models.ForeignKey(
        "account.Account", verbose_name="Заявитель", on_delete=models.SET_NULL, null=True, blank=True)
    # child = models.ForeignKey(ChildProfile, related_name='child_profile', verbose_name="Ребенок", on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=100, blank=True,
                            verbose_name="Название")
    descriptionGeneral = models.TextField(
        verbose_name="Общая трактовка, либо трактовка ребенка", blank=True)
    description1 = models.TextField(
        _("Трактовка для ступени First Step"), blank=True, null=True)
    description2 = models.TextField(
        _("Трактовка для ступени First Idea"), blank=True, null=True)
    description3 = models.TextField(
        _("Трактовка для ступени First Business"), blank=True, null=True)

    def description_file_upload_path(self, instance, filename):
        return '{0}/{1}'.format(instance.name, filename)
    description_file = models.FileField(
        _("Трактовка ребенка в виде изображения или записи"), upload_to=description_file_upload_path, null=True)

    # AGE_GROUPS = (
    #     ('5-7', ('Дошкольная(5-7)')),
    #     ('8-12', ('Начальная школа(8-12)')),
    #     ('13-16', ('Средняя школа(13-16)')),
    # )
    # age_category = models.CharField(
    #     max_length=20, choices=AGE_GROUPS, verbose_name='Возрастная категория')

    deny_reason = models.TextField(default="", blank=True)

    class Meta:
        verbose_name = "Термин"
        verbose_name_plural = "Термины"


class LevelLogs(models.Model):
    child = models.ForeignKey(ChildProfile, related_name='log_child',
                              verbose_name="Ребенко", on_delete=models.SET_NULL, null=True)
    date = models.DateField(verbose_name="Дата создания",
                            auto_now=True)
    RANK = (
        ('Silver', 'Silver'),
        ('Gold', 'Gold'),
        ('Platinum', 'Platinum'),
    )
    rank = models.CharField(max_length=10, choices=RANK,
                            default='Silver', verbose_name="Ранг")
    fox_story = models.CharField(max_length=100, verbose_name="Fox-story")
    program = models.CharField(max_length=50, verbose_name="Программа")

    def unicode(self):
        return str(self.date) + ' | ' + self.child.child.second_name + " " + self.child.child.name + " " + self.child.child.last_name

    class Meta:
        verbose_name = "Логи ребенка"
        verbose_name_plural = "Логи детей"
