from django.urls import path
from . import views
from rest_framework.authtoken.views import obtain_auth_token
from .views import RegistrationView, SendCodeView, SelfAccountView, AccountView, PasswordView
app_name = "account"

urlpatterns = [
    path('send_code/', SendCodeView.as_view(), name='SendCodeView'),
    path('register/', RegistrationView.as_view(), name='RegistrationView'),
    path('login/', obtain_auth_token, name="LoginView"),

    path('profile/', SelfAccountView.as_view(), name="SelfAccountView"),
    path('profile/<int:id>/', AccountView.as_view(), name="AccountView"),

    path('change_password/', PasswordView.as_view(), name="PasswordView")
]
