from .serializers import RegistrationSerializer, ActivationCodeSerializer, AccountSerializer, PasswordChangeSerializer
from .models import Account, SMS
from franchisee.models import FranchiseeProfile
from agent.models import AgentProfile
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from franchisee.models import FranchiseeProfile
from salesManager.models import SalesManagerProfile
from rest_framework import status

# Create your views here.


class SendCodeView(APIView):
    """Отправка кода на введенный номер"""

    def post(self, request):
        serializer = ActivationCodeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'Sended successfully'}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RegistrationView(APIView):
    """Регистрация нового пользователя"""

    def check_agent(self, request, account):
        try:
            agentId = request.data['agent']
        except KeyError:
            pass
        else:
            agent = Account.objects.get(id=agentId)
            franchisee_profile = FranchiseeProfile(
                user_id=account,
                agent=agent
            )
            franchisee_profile.save()

    def check_parent_agent(self, request, account):
        try:
            agentId = request.data['parentAgent']
        except KeyError:
            pass
        else:
            agent = AgentProfile.objects.get(user_id=agentId)
            agent.clients.add(account)
            agent.save()

    def check_manager(self, request, account):
        try:
            managerId = request.data['manager']
        except KeyError:
            pass
        else:
            manager = Account.objects.get(id=managerId)
            franchisee_profile = FranchiseeProfile(
                user_id=account,
                manager=manager
            )
            franchisee_profile.save()

            manager_profile = SalesManagerProfile.objects.get(
                user_id=managerId)

            manager_profile.franchisees.add(account)
            manager_profile.save()

    def check_email(self, request):
        """Проверка на наличие почты в системе"""
        try:
            Account.objects.get(email=request.data['email'])
        except Account.DoesNotExist:
            pass
        else:
            return Response({'msg': 'Email already exist'}, status=411)

    def check_phone(self, request):
        """Проверка на наличие телефона в системе"""
        try:
            Account.objects.get(phone=request.data['phone'])
        except Account.DoesNotExist:
            pass
        else:
            return Response({'msg': 'Phone already exist'}, status=412)

    def check_code(self, request):
        """Проверка на совпадение введенного кода с кодом в системе"""
        try:
            SMS.objects.get(
                phone=request.data['phone'],
                code=request.data['code']
            )
        except SMS.DoesNotExist:
            return Response({'msg': 'Not valid code'}, status=413)

    def check_all(self, request):
        """Запуск всех проверок"""
        self.check_email(request)
        self.check_phone(request)
        self.check_code(request)

    def post(self, request):
        """Регистрация пользователя"""
        self.check_all(request)
        serializer = RegistrationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        account = Account.objects.get(email=request.data['email'])
        print(account)
        self.check_agent(request, account)
        self.check_manager(request, account)
        self.check_parent_agent(request, account)
        return Response({'msg': 'Registered successfuly'}, status=200)


class SelfAccountView(APIView):
    """Просмотр, изменение пользователя по токену"""
    permissions_classes = [IsAuthenticated]

    def get_account(self, id):
        """Поиск пользователя в системе по id"""
        try:
            account = Account.objects.get(id=id)
        except Account.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return account

    def get(self, request):
        """Просмотр данных о пользователе данного токена"""
        account = self.get_account(request.user.id)
        serializer = AccountSerializer(account)
        return Response(serializer.data, status=200)

    def patch(self, request):
        """Изменение данных о пользователе данного токена"""
        account = self.get_account(request.user.id)
        serializer = AccountSerializer(
            account, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(account, validated_data=request.data)
            return Response(data=serializer.data, status=200)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AccountView(APIView):
    """Просмотр и изменение пользователя по id"""
    permissions_classes = [IsAuthenticated]

    def get_account(self, id):
        """Поиск пользователя в системе по id"""
        try:
            account = Account.objects.get(id=id)
        except Account.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return account

    def get(self, request, id):
        """Просмотр данных о пользователе"""
        account = self.get_account(id)
        serializer = AccountSerializer(account)
        return Response(serializer.data, status=200)

    def patch(self, request, id):
        """Изменение данных о пользователе данного токена"""
        account = self.get_account(id)
        serializer = AccountSerializer(
            account, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(account, validated_data=request.data)
            return Response(data=serializer.data, status=200)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PasswordView(APIView):
    """ Изменение пароля пользователя """
    permissions_classes = [IsAuthenticated]

    def get_account(self, id):
        """Поиск пользователя в системе по id"""
        try:
            account = Account.objects.get(id=id)
        except Account.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return account

    def patch(self, request):
        """ Изменение пароля пользователя """
        account = self.get_account(request.user.id)
        serializer = PasswordChangeSerializer(
            account, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(account, validated_data=request.data)
            return Response({'msg': "Password changed successfully"}, status=200)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
