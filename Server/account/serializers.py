from rest_framework import serializers
from .models import Account, SMS
from .helpers import get_random_number, get_random_string
from django.core.mail import send_mail
from .iqsms import Gate
import requests


class ActivationCodeSerializer(serializers.ModelSerializer):
    """Сериализатор с отправкой кода"""
    class Meta:
        model = SMS
        fields = ['phone', ]

    def save(self):
        # random_code = "7777"
        random_code = get_random_number(4)
        try:
            base_code = SMS.objects.get(phone=self.validated_data['phone'])
            base_code.code = random_code
        except SMS.DoesNotExist:
            base_code = SMS(
                phone=self.validated_data['phone'],
                code=random_code
            )
        # post_data = {
        #     'login': 'foxapitest',
        #     'psw': 'bryansk2021',
        #     'phones': self.validated_data['phone'],
        #     'mes': random_code
        # }
        # response = requests.post(
        #     'https://smsc.ru/sys/send.php', data=post_data)
        sender = Gate(api_login='z1632828360839', api_password='515760')
        print(sender.credits())  # узнаем текущий баланс
        print(sender.senders())  # получаем список доступных подписей
        # отправляем sms
        senders = str(sender.senders()).replace("b'", "").split("\\n")
        print(sender.send(
            phone=self.validated_data['phone'], text=random_code, sender=senders[0]))
        print(sender.status(12345))

        base_code.save()
        return base_code


class AccountSerializer(serializers.ModelSerializer):
    """Сериализатор аккаунта пользователя"""
    class Meta:
        model = Account
        exclude = ['password', 'is_staff', 'is_admin', 'is_active',
                   'last_login', 'is_superuser']
        # extra_kwargs = {
        #     'password': {'write_only': True}
        # }

    def create(self, validated_data):
        user = Account.objects.create(**validated_data)
        # user = Account(
        #     email=validated_data['email'],
        #     phone=validated_data['phone'],
        #     second_name=validated_data['second_name'],
        #     name=validated_data['name'],
        #     last_name=validated_data['last_name'],
        #     role=validated_data['role']
        # )
        user.set_password('7777')
        user.save()
        return user


class AccountBuchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['email']


class RegistrationSerializer(serializers.ModelSerializer):
    """Сериализатор регистрации с отправкой письма по почте"""
    code = serializers.CharField(
        max_length=4,
        min_length=4,
        style={'input_type': 'password'},
        write_only=True,
        label="Код",
        trim_whitespace=True
    )

    class Meta:
        model = Account
        fields = ['email', 'phone', 'code', 'role']

    def save(self):

        account = Account(
            email=self.validated_data['email'],
            phone=self.validated_data['phone'],
            role=self.validated_data['role']
        )
        account_password = "7777"
        # account_password = get_random_string(12)
        send_mail(
            'Данные об аккаунте',
            'Ваша почта - ' + account.email + '\nВаш пароль - ' + account_password,
            'businessfoxmail@gmail.com',
            [account.email],
            fail_silently=False,
        )
        print(account)

        account.set_password(account_password)
        account.save()
        return account


#! Просмотр, изменение профиля пользователя
#!##########################
# * name - Имя пользователя
# * second_name - Фамилия пользователя
# * last_name - Отчество пользователя
class AccountProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['second_name', 'name', 'last_name', 'role', 'id', 'ban']


class AccountsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        exclude = ['password', 'date_joined', 'last_login',
                   'is_admin', 'is_staff', 'is_superuser']

#! Просмотр, изменение аватара пользователя
#!##########################


class AccountAvatarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['avatar', ]

#! Изменение пароля пользователя
#!##########################
# * email - почта пользователя
# * phone - телефон пользователя
# * password - новый пароль пользователя


class PasswordChangeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['password', ]

    def update(self, instance, validated_data):
        print(instance)
        instance.save()
        instance.set_password(self.validated_data['password'])
        # instance.save()
        return instance
