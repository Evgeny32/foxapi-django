from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Account, SMS
from coach.models import Coaching
from django.contrib.auth.models import Group

admin.site.unregister(Group)
# Register your models here.


admin.site.register(Account)
# @admin.register(Account)
# class AccountAdmin(UserAdmin):
# # class AccountAdmin(UserAdmin):
#     list_display = ('email','phone','second_name','name','last_name', 'role', 'date_joined', 'last_login')
#     search_fields = ('email','phone','second_name','name','last_name', 'role')
#     read_only = ('date_joined', 'last_login')

#     exclude = ('username',)
#     filter_horizontal = ()
#     list_filter = ()
#     fieldsets = ()
#     ordering = ()
    
@admin.register(SMS)
class SMSAdmin(admin.ModelAdmin):
    list_display = ('phone', 'code')
    search_fields = ('phone',)
    read_only = ('code')
    

