from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from PIL import Image
from django.conf import settings
from django.db.models.signals import post_save, pre_save, pre_delete
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from clientManager.models import ClientManagerProfile
from salesManager.models import SalesManagerProfile
from integration.scripts import delete_partner, make_partner
import os
import shutil

#! МЕНЕДЖЕР АККАУНТОВ
#!##########################


class MyAccountManager(BaseUserManager):
    def create_user(self, email, phone, role, password):
        if not email:
            raise ValueError(
                "Для регистрации необходим адрес электронной почты")
        if role != 'Child':
            if not phone:
                raise ValueError(
                    "Для регистрации необходим номер мобильного телефона")
            try:
                phone_account = Account.objects.get(phone=phone)
                raise ValueError("Данный телефон уже зарегистрирован")
            except Account.DoesNotExist:
                phone_account = None

            user = self.model(
                email=self.normalize_email(email),
                phone=phone,
            )
        else:
            user = self.model(
                email=self.normalize_email(email),
            )
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, phone, password):
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
            phone=phone,
            role='Admin'
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

    def create_admin_user(self, admin_user):
        user = self.model(
            email=admin_user['email'],
            phone=admin_user['phone'],
            second_name=admin_user['second_name'],
            name=admin_user['name'],
            last_name=admin_user['last_name'],
            role=admin_user['role']
        )
        user.set_password('7777')
        user.save(using=self._db)
        return user

#! АККАУНТ
#!########


class Account(AbstractBaseUser):
    def account_avatar_upload_path(instance, filename):
        ext = '.' + filename.split('.')[-1]
        return '{0}/avatar{1}'.format(instance.email, ext)
    email = models.EmailField(verbose_name="Email", max_length=60, unique=True)
    phone = models.CharField(verbose_name="Телефон",
                             max_length=20, null=True, blank=True)

    date_joined = models.DateTimeField(
        verbose_name="Дата создания", auto_now_add=True)
    last_login = models.DateTimeField(
        verbose_name="Дата последней активности", auto_now=True)
    is_admin = models.BooleanField(verbose_name="Администратор", default=False)
    is_active = models.BooleanField(verbose_name="Активирован", default=True)
    is_staff = models.BooleanField(verbose_name="Сотрудник", default=False)
    is_superuser = models.BooleanField(
        verbose_name="Супер пользователь", default=False)

    second_name = models.CharField(
        verbose_name="Фамилия", max_length=20, blank=True)
    name = models.CharField(verbose_name="Имя", max_length=20, blank=True)
    last_name = models.CharField(
        verbose_name="Отчество", max_length=20, blank=True)
    ban = models.BooleanField(verbose_name="Бан", default=False)
    ban_cause = models.TextField(
        verbose_name="Причина бана", null=True, blank=True)
    ROLES = (
        ('Parent', 'Родитель'),
        ('Mentor', 'Ментор'),
        ('Child', 'Ребенок'),
        ('Admin', 'Админ'),
        ('Franchisee', 'Франчайз'),
        ('Branch', 'Филиал'),
        ('Agent', 'Агент'),
        ('BuhAdmin', 'Бухгалтер Администратора'),
        ('BuhFranch', 'Бухгалтер Франчайзи'),
        ('HeadSalesManager', 'Руководитель отдела продаж'),
        ('SalesManager', 'Менеджер по продажам'),
        ('ClientManager', 'Клиентский менеджер')

    )

    role = models.CharField(verbose_name="Роль", choices=ROLES, max_length=20)

    avatar = models.ImageField(
        upload_to=account_avatar_upload_path, verbose_name="Аватар", null=True, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone', ]

    objects = MyAccountManager()

    def unicode(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

    class Meta:
        verbose_name = "Аккаунт"
        verbose_name_plural = "Аккаунты"

#! SMS
#!#####


class SMS(models.Model):
    phone = models.CharField(max_length=20, verbose_name="Телефон")
    code = models.CharField(max_length=5, verbose_name="Код")

    class Meta:
        verbose_name = "SMS"
        verbose_name_plural = "SMS"


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def account_post_save(sender, instance=None, created=False, updated=False, **kwargs):
    if instance.ban:
        delete_partner(instance.id)
    else:
        make_partner(instance.id)
    if created:
        Token.objects.create(user=instance)
        clientmanagers = ClientManagerProfile.objects.all()
        salesmanagers = SalesManagerProfile.objects.all()

        if instance.role == 'Franchisee':

            franchisee = Account.objects.get(email=instance.email)

            if len(clientmanagers) > 0:
                chosen_manager = clientmanagers[0]

                for manager in clientmanagers:
                    if len(manager.franchisees.all()) < len(chosen_manager.franchisees.all()):
                        chosen_manager = manager
                chosen_manager.franchisees.add(franchisee)
                chosen_manager.save()

            if len(salesmanagers) > 0:
                chosen_sales_manager = salesmanagers[0]

                for manager in salesmanagers:
                    if len(manager.franchisees.all()) < len(chosen_sales_manager.franchisees.all()):
                        chosen_sales_manager = manager
                chosen_sales_manager.franchisees.add(franchisee)
                chosen_sales_manager.save()

        if instance.role == 'Agent':

            agent = Account.objects.get(email=instance.email)

            if len(clientmanagers) > 0:
                chosen_manager = clientmanagers[0]

                for manager in clientmanagers:
                    if len(manager.agents.all()) < len(chosen_manager.agents.all()):
                        chosen_manager = manager
                chosen_manager.agents.add(agent)
                chosen_manager.save()

            if len(salesmanagers) > 0:
                chosen_sales_manager = salesmanagers[0]

                for manager in salesmanagers:
                    if len(manager.agents.all()) < len(chosen_sales_manager.agents.all()):
                        chosen_sales_manager = manager
                chosen_sales_manager.agents.add(agent)
                chosen_sales_manager.save()

        if instance.role == 'Parent':

            client = Account.objects.get(email=instance.email)

            if len(clientmanagers) > 0:
                chosen_manager = clientmanagers[0]

                for manager in clientmanagers:
                    if len(manager.clients.all()) < len(chosen_manager.clients.all()):
                        chosen_manager = manager
                chosen_manager.clients.add(client)
                chosen_manager.save()

        if instance.role == 'Mentor':

            mentor = Account.objects.get(email=instance.email)

            if len(clientmanagers) > 0:
                chosen_manager = clientmanagers[0]

                for manager in clientmanagers:
                    if len(manager.mentors.all()) < len(chosen_manager.mentors.all()):
                        chosen_manager = manager
                chosen_manager.mentors.add(mentor)
                chosen_manager.save()

    if instance.avatar:
        avatar = instance.avatar
        _MAX_SIZE = 300
        width = avatar.width
        height = avatar.height

        max_size = max(width, height)
        if max_size > _MAX_SIZE:
            image = Image.open(avatar.path)
            image = image.resize(
                (round(width / max_size * _MAX_SIZE),
                 round(height / max_size * _MAX_SIZE)),
                Image.ANTIALIAS
            )
            width = image.width
            height = image.height
            leftcrop = (max(width, height) / 2) - (min(width, height) / 2)
            topcrop = 0
            rightcrop = (max(width, height) / 2) + (min(width, height) / 2)
            bottomcrop = min(width, height)
            image = image.crop((leftcrop, topcrop, rightcrop, bottomcrop))

            image.save(avatar.path)


@receiver(pre_save, sender=settings.AUTH_USER_MODEL)
def delete_old_image(sender, instance=None, **kwargs):
    if instance.avatar:
        try:
            old_avatar = Account.objects.get(pk=instance.pk).avatar
        except Account.DoesNotExist:
            return
        else:
            new_avatar = instance.avatar

            if old_avatar != new_avatar:
                old_avatar.delete(save=False)


@receiver(pre_delete, sender=settings.AUTH_USER_MODEL)
def delete_media_files(sender, instance=None, **kwargs):
    delete_partner(instance.id)
    if instance.avatar:
        avatar = Account.objects.get(pk=instance.pk).avatar
        dirpath = avatar.path.replace(
            "/avatar." + avatar.name.split('.')[-1], "")
        shutil.rmtree(dirpath, ignore_errors=True)
