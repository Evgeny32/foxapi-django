import random
import string

#!Рандомное число
def get_random_number(length):
    numbers = string.digits
    result_num = ''.join(random.choice(numbers) for i in range(length))
    return result_num
#!Рандомная строка
def get_random_string(length):
    letters = string.digits + string.ascii_lowercase + string.ascii_uppercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str