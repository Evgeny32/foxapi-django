from rest_framework import serializers
from .models import Transaction


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = '__all__'


class TransactionPaySerializer(serializers.Serializer):
    child = serializers.IntegerField()
    transaction = serializers.IntegerField()
    order_id = serializers.CharField()
