from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.


class Transaction(models.Model):
    """Транзакция"""
    user = models.ForeignKey("account.Account", verbose_name=_(
        "Пользователь"), on_delete=models.PROTECT)
    name = models.CharField(_("Название"), max_length=256)
    value = models.BigIntegerField('Сумма')
    date = models.DateTimeField(_("Дата"), auto_now_add=True)
    paid = models.BooleanField(_("Оплачено"), default=False)
    order_id = models.CharField(
        _("ID заказа в сбере"), max_length=256, default="")
