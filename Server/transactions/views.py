from django.shortcuts import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework import serializers, status
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Transaction
from child.models import ChildProfile
from .serializers import TransactionSerializer, TransactionPaySerializer
from child.models import ChildProfile


class TransactionsView(APIView):
    permission_classes = [IsAuthenticated]
    """Список транзакций пользователя. Создание новой транзакции"""

    def get(self, request):
        transactions = Transaction.objects.filter(user=request.user.id)
        serializer = TransactionSerializer(transactions, many=True)
        return Response(serializer.data)

    def post(self, request):
        request.data['_mutable'] = True
        request.data['user'] = request.user.id
        serializer = TransactionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TransactionView(APIView):
    permission_classes = [IsAuthenticated]
    """Транзакция по id. Изменение транзакции. Удаление транзакции"""

    def get(self, request, id):
        transaction = get_object_or_404(Transaction, id=id)
        serializer = TransactionSerializer(transaction)
        return Response(serializer.data)

    def patch(self, request, id):
        transaction = get_object_or_404(Transaction, id=id)
        serializer = TransactionSerializer(
            transaction, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(transaction, validated_data=request.data)
            return Response(serializer.data)
        return Response(serializer.errors)

    def delete(self, request, id):
        transaction = get_object_or_404(Transaction, id=id)
        transaction.delete()
        return Response({'msg': "Transaction deleted"})


class TransactionFillView(APIView):
    permission_classes = [IsAuthenticated]

    def patch(self, request, id):
        transaction = get_object_or_404(Transaction, id=id)
        if transaction.paid:
            return Response({'msg': 'Транзакция уже была проведена'}, status=status.HTTP_406_NOT_ACCEPTABLE)
        transaction.paid = True
        transaction.order_id = request.data['order_id']
        child = get_object_or_404(ChildProfile, id=request.data['childId'])
        child.foxies += transaction.value
        child.save()
        transaction.save()
        return Response({'msg': 'Transaction accepted'}, status=status.HTTP_200_OK)


class TransactionsPayView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = TransactionPaySerializer(data=request.data)
        if serializer.is_valid():
            try:
                transaction = Transaction.objects.get(
                    user=request.user.id, paid=False, id=request.data['transaction'])
            except Transaction.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
            try:
                child = ChildProfile.objects.get(id=request.data['child'])
            except ChildProfile.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
            transaction.paid = True
            transaction.order_id = request.data['order_id']
            transaction.save()
            child.foxies = child.foxies + transaction.value
            child.save()
            serializer = TransactionSerializer(transaction)
            return Response({'msg': "Transaction accepted", "data": serializer.data}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
