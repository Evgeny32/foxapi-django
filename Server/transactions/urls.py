from django.urls import path
from .views import TransactionsView, TransactionView,  TransactionFillView, TransactionsPayView

urlpatterns = [
    path('', TransactionsView.as_view()),
    path('pay', TransactionsPayView.as_view()),
    path('<int:id>', TransactionView.as_view()),
    path('fill/<int:id>', TransactionFillView.as_view()),
]
