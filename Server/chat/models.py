from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.


class Room(models.Model):
    """Комната чата"""
    name = models.CharField(_("Название комнаты"), max_length=50)
    creator = models.ForeignKey("account.Account", related_name='room_creator', verbose_name=_(
        "Создатель комнаты"), on_delete=models.CASCADE)
    createdDate = models.DateTimeField(
        _("Дата создания комнаты"),  auto_now_add=True)
    users = models.ManyToManyField(
        "account.Account", related_name='room_users', verbose_name=_("Пользователи комнаты"))


class Message(models.Model):
    """Сообщение чата"""
    text = models.TextField(_("Текст сообщения"))
    sender = models.ForeignKey("account.Account", verbose_name=_(
        "Отправитель сообщения"), related_name='message_sender', on_delete=models.CASCADE)
    room = models.ForeignKey("Room", verbose_name=_(
        "Комната"), on_delete=models.CASCADE)
    readed = models.ManyToManyField(
        "account.Account", related_name='message_readers', verbose_name=_("Кем прочтено"))
    sendedAt = models.DateTimeField(_("Когда отправлено"),  auto_now_add=True)
