from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from .models import Room, Message
from account.serializers import AccountProfileSerializer


class RoomSerializer(ModelSerializer):
    class Meta:
        model = Room
        fields = '__all__'


class MessageListSerializer(ModelSerializer):
    me = serializers.BooleanField(default=False)

    class Meta:
        model = Message
        fields = '__all__'


class MessageSerializer(ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


class RoomWithMessagesSerializer(ModelSerializer):
    messages = MessageListSerializer(many=True)

    class Meta:
        model = Room
        fields = '__all__'
