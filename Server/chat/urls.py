from django.urls import path
from .views import MessagesView, MessageView, RoomsView, RoomView


urlpatterns = [
    path('', RoomsView.as_view(), name="RoomsView"),
    path('<int:roomId>', RoomView.as_view(), name="RoomView"),
    path('<int:roomId>/messages', MessagesView.as_view(), name="MessagesView"),
    path('message/<int:id>', MessageView.as_view(), name="MessageView")
]
