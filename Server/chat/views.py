from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from .models import Room, Message
from .serializers import RoomSerializer, RoomWithMessagesSerializer, MessageSerializer, MessageListSerializer
from account.models import Account
from rest_framework.pagination import PageNumberPagination


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 1000


class RoomsView(APIView):
    permission_classes = [IsAuthenticated]
    """Получение своих комнат. Создание новой"""

    def get(self, request):
        rooms = Room.objects.filter(users__id=request.user.id)
        serializer = RoomSerializer(rooms, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        request.data['_mutable'] = True
        request.data['creator'] = request.user.id
        request.data['users'] = [request.user.id]
        serializer = RoomSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RoomView(APIView):
    permission_classes = [IsAuthenticated]

    """Комната по id с сообщениями. Изменение комнаты. Удаление комнаты"""

    def get(self, request, roomId):
        room = get_object_or_404(Room, id=roomId)
        room.messages = Message.objects.filter(room=roomId)
        for message in room.messages:
            if message.sender == request.user.id:
                message.me = True
        serializer = RoomWithMessagesSerializer(room)
        return Response(serializer.data)

    def patch(self, request, roomId):
        room = get_object_or_404(Room, id=roomId)
        serializer = RoomSerializer(room, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(room, validated_data=request.data)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, roomId):
        room = get_object_or_404(Room, id=roomId)
        room.delete()
        return Response({'msg': 'Room deleted'})


class MessagesView(APIView, StandardResultsSetPagination):
    permission_classes = [IsAuthenticated]
    """Сообщения по id комнаты. Создание нового сообщения"""

    def get(self, request, roomId):
        messages = Message.objects.filter(room=roomId)
        results = self.paginate_queryset(messages, request, view=self)
        serializer = MessageListSerializer(results, many=True)
        return self.get_paginated_response(serializer.data)

    def post(self, request, roomId):
        request.data['_mutable'] = True
        request.data['room'] = roomId
        serializer = MessageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MessageView(APIView):
    permission_classes = [IsAuthenticated]

    """Сообщение по id. Изменение, удаление сообщения"""

    def get(self, request, id):
        message = get_object_or_404(Message, id=id)
        serializer = MessageSerializer(message)
        return Response(serializer.data)

    def patch(self, request, id):
        message = get_object_or_404(Message, id=id)
        serializer = MessageSerializer(
            message, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(message, validated_data=request.data)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        message = get_object_or_404(Message, id=id)
        message.delete()
        return Response({'msg': 'Message deleted'})
