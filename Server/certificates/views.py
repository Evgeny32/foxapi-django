from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND
from .models import Certificate
from .serializers import CertificateSerializer
# Create your views here.


class CertificatesView(APIView):
    """ CRUD запросы на сертификаты """
    permission_classes = [IsAuthenticated]

    def get(self, request):
        certificates = Certificate.objects.all()
        serializer = CertificateSerializer(certificates, many=True)
        return Response({'msg': 'Certificates received', 'data': serializer.data}, status=HTTP_200_OK)

    def post(self, request):
        serializer = CertificateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'Certificate created', 'data': serializer.data}, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class CertificateView(APIView):
    """ CRUD запросы на сертификат """
    permission_classes = [IsAuthenticated]

    def get(self, request, id):
        certificate = get_object_or_404(Certificate, id=id)
        serializer = CertificateSerializer(certificate)
        return Response({'msg': 'Certificate received', 'data': serializer.data}, status=HTTP_200_OK)

    def patch(self, request, id):
        certificate = get_object_or_404(Certificate, id=id)
        serializer = CertificateSerializer(
            certificate, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(certificate, validated_data=request.data)
            return Response({'msg': 'Certificate updated', 'data': serializer.data}, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        certificate = get_object_or_404(Certificate, id=id)
        certificate.delete()
        return Response({'msg': 'Certificate deleted'}, status=HTTP_200_OK)


class CertificatesMyView(APIView):
    """ Сертификаты пользователя """
    permission_classes = [IsAuthenticated]

    def get(self, request):
        certificates = Certificate.objects.filter(user=request.user.id)
        serializer = CertificateSerializer(certificates, many=True)
        return Response({'msg': 'Certificates received', 'data': serializer.data}, status=HTTP_200_OK)

    def post(self, request):
        request.data['_mutable'] = True
        request.data['user'] = request.user.id
        serializer = CertificateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'Certificate created', 'data': serializer.data}, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
