from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.


TYPE_CHOICES = [
    ('certificate', 'Сертификат'),
    ('diplom', 'Диплом')
]


class Certificate(models.Model):
    """Сертификат/Диплом"""
    name = models.CharField(_("Название сертификата"), max_length=256)
    user = models.ForeignKey("account.Account", verbose_name=_(
        "Владелец сертификата"), on_delete=models.CASCADE)
    file = models.FileField(_("Сертификат"), upload_to='certificates')
    year = models.PositiveIntegerField(_("Год обучения"), default=1)
    type = models.CharField(_("Тип сертификата"),
                            max_length=50, choices=TYPE_CHOICES)
    created_at = models.DateTimeField(
        _("Время получения"),  auto_now_add=False)

    class Meta:
        verbose_name = _("Сертификат")
        verbose_name_plural = _("Сертификаты")

    def __str__(self):
        return self.name
