from django.urls import path
from .views import CertificatesView, CertificateView, CertificatesMyView
urlpatterns = [
    path('', CertificatesView.as_view(), name="SerificatesView"),
    path('<int:id>/', CertificateView.as_view(), name="SerificateView"),
    path('my/', CertificatesMyView.as_view(), name="CerificatesMyView"),
]
