from rest_framework import serializers
from account.models import Account
from child.models import ChildProfile
from account.serializers import AccountSerializer
from mentor.models import MentorProfile




class FIOSerializer(serializers.ModelSerializer):  # *
    class Meta:
        model = Account
        fields = ['id', 'name', 'second_name', 'last_name', 'email', 'phone']



class ChildrenListSerializer(serializers.ModelSerializer):

    tale = serializers.CharField()
    child = FIOSerializer()

    class Meta:
        model = ChildProfile
        fields = ['id', 'child', 'age', 'tale', 'rank', 'creation_date']




class MentorListSerializer(serializers.ModelSerializer):
    children = serializers.IntegerField()
    trainings = serializers.IntegerField()
    groups = serializers.IntegerField()
    all = serializers.IntegerField()
    mentor = FIOSerializer()

    class Meta:
        model = MentorProfile
        fields = ['id', 'mentor', 'children', 'all', 'trainings',
                  'groups', 'all', 'date_work', 'date_layoff', 'job_status', 'passport', 'passport_who', 'passport_when', 'adress']



class MentorProfileSerializer(serializers.ModelSerializer):  # * Для отображения
    mentor = AccountSerializer()

    class Meta:
        model = MentorProfile
        fields = '__all__'


class MentorProfileSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = MentorProfile
        fields = '__all__'
