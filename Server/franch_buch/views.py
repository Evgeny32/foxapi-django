from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework import status
from child.models import ChildProfile
from tales.models import Tale
from mentor.models import MentorProfile, ChildGroup
from child.models import ChildProfile
from coach.models import Coaching
from franchisee.models import FranchiseeProfile
from branch.models import BranchProfile
from .serializers import (
    ChildrenListSerializer,
    MentorListSerializer,
    MentorProfileSerializer,
    
)


class ChildrenListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        try:
            franchisee = FranchiseeProfile.objects.get(
                emailBuch=request.user.id)
        except FranchiseeProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        children = []
        branches = BranchProfile.objects.filter(
            franchisee=franchisee.user_id.id)
        for branch in branches:
            mentor_list = MentorProfile.objects.filter(
                branch=branch.user_id.id)
            for mentor in mentor_list:
                child_list = ChildProfile.objects.filter(
                    mentor=mentor.mentor.id)
                children = children + list(child_list)
        # children = ChildProfile.objects.all()
        for child in children:
            tale = Tale.objects.filter(child=child.id).order_by(
                'number').first()  # * Достаем название последней сказки ребенка
            if tale:
                child.tale = tale.name
            else:
                child.tale = 'Сказка не найдена'

        serializer = ChildrenListSerializer(children, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ChildDetailsView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id):

        try:
            child = ChildProfile.objects.get(child=id)
        except:
            if ChildProfile.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)

        tale = Tale.objects.filter(child=child.id).order_by(
            'number').first()  # * Достаем название последней сказки ребенка
        if tale:
            child.tale = tale.name
        else:
            child.tale = 'Сказка не найдена'

        serializer = ChildrenListSerializer(child)
        return Response(serializer.data, status=status.HTTP_200_OK)


class MentorListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):  # * Список
        try:
            franchisee = FranchiseeProfile.objects.get(
                emailBuch=request.user.id)
        except FranchiseeProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        branches = BranchProfile.objects.filter(
            franchisee=franchisee.user_id.id)
        mentors = []
        for branch in branches:
            mentors += MentorProfile.objects.filter(branch=branch.user_id.id)
        # mentors = MentorProfile.objects.all()

        for mentor in mentors:
            groups = ChildGroup.objects.filter(mentor=mentor.mentor.id)
            children = 0
            for group in groups:
                children += len(group.children.all())
            mentor.children = children  # * Колличество детей ментора
            mentor.trainings = len(
                Coaching.objects.filter(mentor=mentor.mentor.id))  # * Колличество тренингов ментора
            mentor.groups = len(groups)  # * Колличество групп ментора
            mentor.all = 0  # * Оборот ментора
        serializer = MentorListSerializer(
            mentors, many=True)
        return Response(serializer.data)


class MentorDetailsView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        try:
            mentor = MentorProfile.objects.get(mentor=id)
        except MentorProfile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = MentorProfileSerializer(mentor)
        return Response(serializer.data)


# Create your views here.
