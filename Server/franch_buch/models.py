from django.db import models
from account.models import Account
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save, pre_delete
from django.conf import settings


#! ПРОФИЛЬ БУХГАЛТЕРА ФРАНЧАЙЗА
#!!!!!!!!!!!!!!!!!!!!!!!

class FranchBuchProfile(models.Model):

    user_id = models.ForeignKey(
        Account, related_name='franch_buch_account', verbose_name="Бухгалтер франчайза", on_delete=models.CASCADE)
    name = models.CharField(verbose_name="Наименование",
                            max_length=30, blank=True)    
    franchisee = models.ForeignKey(
        Account, related_name='franchisee_buch', verbose_name="Франщиза бухгалтера", null=True, blank=True, on_delete=models.CASCADE)