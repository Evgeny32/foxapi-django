from django.urls import path
from .views import (    
    ChildrenListView,     
    MentorListView,   
    ChildDetailsView,    
    MentorDetailsView)


urlpatterns = [    
    path("children/", ChildrenListView.as_view(), name="ChildrenListView"),
    path("child/<int:id>/", ChildDetailsView.as_view(), name="ChildDetailsView"), 
    path("mentors/", MentorListView.as_view(), name="MentorListView"),
    path("mentor/<int:id>/", MentorDetailsView.as_view(), name="MentorDetailsView"),
    
]