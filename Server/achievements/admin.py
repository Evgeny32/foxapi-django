from django.contrib import admin
from .models import Achievement, ReceivedAchievement
# Register your models here.


admin.site.register(Achievement)
admin.site.register(ReceivedAchievement)