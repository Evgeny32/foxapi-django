from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.


class Achievement(models.Model):
    """Достижение"""
    name = models.CharField(_("Название достижения"),
                            max_length=256, unique=True)
    description = models.TextField(_("Описание достижения"))

    def img_upload_path(instance, filename):
        return 'achivements/{0}/{1}'.format(instance.name, filename)
    img = models.ImageField(_("Стикер"), upload_to=img_upload_path)

    class Meta:
        verbose_name = _("Достижение")
        verbose_name_plural = _("Достижения")

    def __str__(self):
        return self.name


class ReceivedAchievement(models.Model):
    """Связка достижение-пользователь. Показывает, какое достижение какой пользователь получил"""
    user = models.ForeignKey("account.Account", verbose_name=_(
        "Пользователь"), on_delete=models.CASCADE)
    achievement = models.ForeignKey("achievements.Achievement", verbose_name=_(
        "Достижение"), on_delete=models.CASCADE)
    # stage = models.PositiveIntegerField(_("Фаза"))

    class Meta:
        verbose_name = _("Полученнное Достижение")
        verbose_name_plural = _("Полученные Достижения")

    def __str__(self):
        return self.achievement.name
