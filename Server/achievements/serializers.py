from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from .models import Achievement, ReceivedAchievement

class AchievementsSerializer(ModelSerializer):
    achieved = serializers.BooleanField(default=False)
    class Meta:
        model = Achievement
        fields = '__all__'

class ReceivedAchievementSerializer(ModelSerializer):
    class Meta:
        model = ReceivedAchievement
        fields = '__all__'