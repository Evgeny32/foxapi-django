from django.conf import settings
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from cloud.views import IsAdminPermission
from .models import Achievement, ReceivedAchievement
from .serializers import AchievementsSerializer, ReceivedAchievementSerializer
import json
# Create your views here.


class InitView(APIView):
    """ Инициализация. Хватает массив из файла achievements.json и создает достижения  """
    permission_classes = [IsAuthenticated, IsAdminPermission]

    def find_or_create(self, body):
        try:
            achievement = Achievement.objects.get(name=body['name'])
            return
        except Achievement.DoesNotExist:
            achievement = Achievement(**body)
            achievement.save()
            return

    def post(self, request):
        path = '/achievements/achievements.json'
        base = str(settings.BASE_DIR)
        try:
            with open(base+path) as f:
                achievements = json.load(f)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
        for achievement in achievements:
            self.find_or_create(achievement)
        return Response({'data': achievements}, status=status.HTTP_200_OK)


class AchievementsView(APIView):
    """ Отображение всех достижений / Создание нового достижения """
    permission_classes = [IsAuthenticated]

    def get(self, request):
        achievements = Achievement.objects.all()
        for achievement in achievements:
            try:
                ReceivedAchievement.objects.get(
                    achievement=achievement.id, user=request.user.id)
                achievement.achieved = True
            except ReceivedAchievement.DoesNotExist:
                achievement.achieved = False
        serializer = AchievementsSerializer(achievements, many=True)
        return Response({'msg': "Achievements received", 'data': serializer.data}, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = AchievementsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'Achievement created', 'data': serializer.data}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AchievementView(APIView):
    """ Получение, отмена получения достижений """
    permission_classes = [IsAuthenticated]

    def post(self, request, id):
        data = {
            'user': request.user.id,
            'achievement': id
        }
        try:
            ReceivedAchievement.objects.get(
                user=data['user'], achievement=data['achievement'])
            return Response({'detail': 'Achievement already achieved'}, status=status.HTTP_409_CONFLICT)
        except ReceivedAchievement.DoesNotExist:
            pass
        serializer = ReceivedAchievementSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'Achievement achieved'}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        data = {
            'user': request.user.id,
            'achievement': id
        }

        try:
            achievement = ReceivedAchievement.objects.get(
                user=data['user'], achievement=data['achievement'])
        except ReceivedAchievement.DoesNotExist:
            return Response({'detail': 'Not found'}, status=status.HTTP_404_NOT_FOUND)
        achievement.delete()
        return Response({'msg': 'Achievement unachieved'}, status=status.HTTP_200_OK)
