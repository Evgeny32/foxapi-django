from django.urls import path
from .views import InitView, AchievementsView, AchievementView

urlpatterns = [
    path('', AchievementsView.as_view(), name="AchievementsView"),
    path('<int:id>/', AchievementView.as_view(), name="AchievementView"),
    path('init/', InitView.as_view(), name="InitView"),
]
