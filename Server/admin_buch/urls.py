from django.urls import path
from .views import (
    ChildrenListView, 
    AgentListView, 
    FranchiseeListView, 
    MentorListView,
    AgentDetailsView,
    FranchiseeDetailsView,
    ChildDetailsView,    
    MentorDetailsView)


urlpatterns = [
    path("children/", ChildrenListView.as_view(), name="ChildrenListView"),
    path("child/<int:id>/", ChildDetailsView.as_view(), name="ChildDetailsView"),    
    path("agents/", AgentListView.as_view(), name="AgentListView"),
    path("agent/<int:id>/", AgentDetailsView.as_view(), name="AgentDetailsView"),
    path("franchisee/", FranchiseeListView.as_view(), name="FranchiseeListView"),
    path("franchisee/<int:id>/", FranchiseeDetailsView.as_view(), name="FranchiseeDetailsView"),
    path("mentors/", MentorListView.as_view(), name="MentorListView"),
    path("mentor/<int:id>/", MentorDetailsView.as_view(), name="MentorDetailsView"),
    
]