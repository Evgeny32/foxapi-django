from django.apps import AppConfig


class AdminBuchConfig(AppConfig):
    name = 'admin_buch'
