export default [
  {
    path: "/mentor",
    name: "mentor",
    component: () => import("@/views/mentor/Mentor.vue"),
    meta: { auth: true, role: "Mentor" },
    children: [
      {
        path: "groups",
        component: () => import("@/views/global/LK.vue"),
        meta: { auth: true, role: "Mentor" },
        children: [
          {
            path: "/",
            name: "mentor-groups",
            component: () => import("@/views/mentor/groups/Groups.vue"),
            meta: { auth: true, role: "Mentor" },
          },
          {
            path: ":group",
            name: "mentor-groups-group",
            component: () => import("@/views/mentor/groups/Group.vue"),
            meta: { auth: true, role: "Mentor" },
          },
          {
            path: ":group/child/:id",
            name: "mentor-groups-child",
            component: () => import("@/views/mentor/groups/Child.vue"),
            meta: { auth: true, role: "Mentor" },
          },
        ],
      },
      {
        path: "dictionary",
        name: "mentor-dictionary",
        component: () => import("@/views/mentor/Dictionary.vue"),
        meta: { auth: true, role: "Mentor" },
      },
      {
        path: "profile",
        name: "mentor-profile",
        component: () => import("@/views/mentor/Profile.vue"),
        meta: { auth: true, role: "Mentor" },
      },
    ],
  },
];
