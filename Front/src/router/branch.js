export default [
  {
    path: "/branch",
    name: "branch",
    component: () => import("@/views/branch/Branch.vue"),
    meta: { auth: true, role: "Branch" },
    children: [
      {
        path: "analytics",
        component: () => import("@/views/branch/analytics/AnalyticsUnion.vue"),
        meta: { auth: true, role: "Branch" },
        children: [
          {
            path: "/",
            name: "branch-analytics",
            component: () => import("@/views/branch/analytics/Analytics.vue"),
            meta: { auth: true, role: "Branch" },
          },
          {
            path: "mentor/:id",
            name: "branch-analytics-mentor",
            component: () => import("@/views/branch/analytics/Mentor.vue"),
            meta: { auth: true, role: "Branch" },
          },
          {
            path: "child/:id",
            name: "branch-analytics-child",
            component: () => import("@/views/branch/analytics/child/Info.vue"),
            meta: { auth: true, role: "Branch" },
          },
          {
            path: "child/:id/tale/:tale",
            name: "branch-analytics-child-progress",
            component: () =>
              import("@/views/branch/analytics/child/Progress.vue"),
            meta: { auth: true, role: "Branch" },
          },
          {
            path: "child/:id/archive",
            name: "branch-analytics-child-archive",
            component: () =>
              import("@/views/branch/analytics/child/Archive.vue"),
            meta: { auth: true, role: "Branch" },
          },
        ],
      },
      {
        path: "data",
        name: "branch-data",
        component: () => import("@/views/branch/data/Data.vue"),
        meta: { auth: true, role: "Branch" },
      },
      {
        path: "support",
        name: "branch-support",
        component: () => import("@/views/agent/Support.vue"),
        meta: { auth: true, role: "Branch" },
      },
    ],
  },
];
