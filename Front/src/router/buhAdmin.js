export default [
  {
    path: "/buh-admin",
    component: () => import("@/views/buhadmin/BuhAdmin.vue"),
    meta: { auth: true, role: "BuhAdmin" },
    children: [
      {
        path: "agent-doc",
        name: "buhadmin-agent-doc",
        component: () =>
          import("@/components/buhfranch/docs/AgentDocViewer.vue"),
        meta: { auth: true, role: "BuhAdmin" },
      },
      {
        path: "product-doc",
        name: "buhadmin-product-doc",
        component: () =>
          import("@/components/buhfranch/docs/ProductDocViewer.vue"),
        meta: { auth: true, role: "BuhAdmin" },
      },
      {
        path: "upd-doc",
        name: "buhadmin-upd-doc",
        component: () => import("@/components/buhfranch/docs/UPDDocViewer.vue"),
        meta: { auth: true, role: "BuhAdmin" },
      },
      {
        path: "bill-doc",
        name: "buhadmin-bill-doc",
        component: () =>
          import("@/components/buhfranch/docs/BillDocViewer.vue"),
        meta: { auth: true, role: "BuhAdmin" },
      },
      {
        path: "act-doc",
        name: "buhadmin-act-doc",
        component: () => import("@/components/buhfranch/docs/ActDocViewer.vue"),
        meta: { auth: true, role: "BuhAdmin" },
      },
      {
        path: "license-doc",
        name: "buhadmin-license-doc",
        component: () =>
          import("@/components/buhfranch/docs/LicenseDocViewer.vue"),
        meta: { auth: true, role: "BuhAdmin" },
      },
      {
        path: "sublicense-doc",
        name: "buhadmin-sublicense-doc",
        component: () =>
          import("@/components/buhfranch/docs/SubLicenseDocViewer.vue"),
        meta: { auth: true, role: "BuhAdmin" },
      },
      {
        path: "seminar-doc",
        name: "buhadmin-seminar-doc",
        component: () =>
          import("@/components/buhfranch/docs/SeminarDocViewer.vue"),
        meta: { auth: true, role: "BuhAdmin" },
      },
      {
        path: "agent",

        component: () => import("@/views/buhadmin/AgentUnion.vue"),
        meta: { auth: true, role: "BuhAdmin" },
        children: [
          {
            path: "/",
            name: "buhadmin-agents-table",
            component: () => import("@/components/buhadmin/Tables/Agents.vue"),
            meta: { auth: true, role: "BuhAdmin" },
          },
          {
            path: "info/:id",
            name: "buhadmin-agent-info",
            component: () =>
              import(
                "@/components/buhadmin/PersonalInfo/PersonalInfoAgent.vue"
              ),
            meta: { auth: true, role: "BuhAdmin" },
          },
          {
            path: "act/:id",
            name: "buhadmin-agent-act",
            component: () =>
              import("@/components/buhadmin/Acts/ActViewerAgent.vue"),
            meta: { auth: true, role: "BuhAdmin" },
          },
          {
            path: "report/:id",
            name: "buhadmin-agent-report",
            component: () =>
              import("@/components/buhadmin/Reports/ReportAgent.vue"),
            meta: { auth: true, role: "BuhAdmin" },
          },
        ],
      },
      {
        path: "franchisee",

        component: () => import("@/views/buhadmin/FranchiseeUnion.vue"),
        meta: { auth: true, role: "BuhAdmin" },
        children: [
          {
            path: "/",
            name: "buhadmin-franchisee-table",
            component: () =>
              import("@/components/buhadmin/Tables/Franchisee.vue"),
            meta: { auth: true, role: "BuhAdmin" },
          },
          {
            path: "info/:id",
            name: "buhadmin-franchisee-info",
            component: () =>
              import(
                "@/components/buhadmin/PersonalInfo/PersonalInfoFranchisee.vue"
              ),
            meta: { auth: true, role: "BuhAdmin" },
          },
          {
            path: "act/:id",
            name: "buhadmin-franchisee-act",
            component: () =>
              import("@/components/buhadmin/Acts/ActViewerFranchisee.vue"),
            meta: { auth: true, role: "BuhAdmin" },
          },
          {
            path: "report/:id",
            name: "buhadmin-franchisee-report",
            component: () =>
              import("@/components/buhadmin/Reports/ReportFranchisee.vue"),
            meta: { auth: true, role: "BuhAdmin" },
          },
        ],
      },
      {
        path: "children",

        component: () => import("@/views/buhadmin/ChildrenUnion.vue"),
        meta: { auth: true, role: "BuhAdmin" },
        children: [
          {
            path: "/",
            name: "buhadmin-children-table",
            component: () =>
              import("@/components/buhadmin/Tables/Children.vue"),
            meta: { auth: true, role: "BuhAdmin" },
          },
          {
            path: "info/:id",
            name: "buhadmin-children-info",
            component: () =>
              import(
                "@/components/buhadmin/PersonalInfo/PersonalInfoChildren.vue"
              ),
            meta: { auth: true, role: "BuhAdmin" },
          },
          {
            path: "act/:id",
            name: "buhadmin-children-act",
            component: () =>
              import("@/components/buhadmin/Acts/ActViewerChild.vue"),
            meta: { auth: true, role: "BuhAdmin" },
          },
          {
            path: "report",
            name: "buhadmin-children-report",
            component: () =>
              import("@/components/buhadmin/Reports/ReportChildren.vue"),
            meta: { auth: true, role: "BuhAdmin" },
          },
        ],
      },
      {
        path: "mentors",

        component: () => import("@/views/buhadmin/MentorsUnion.vue"),
        meta: { auth: true, role: "BuhAdmin" },
        children: [
          {
            path: "/",
            name: "buhadmin-mentors-table",
            component: () => import("@/components/buhadmin/Tables/Mentors.vue"),
            meta: { auth: true, role: "BuhAdmin" },
          },
          {
            path: "info/:id",
            name: "buhadmin-mentor-info",
            component: () =>
              import(
                "@/components/buhadmin/PersonalInfo/PersonalInfoMentor.vue"
              ),
            meta: { auth: true, role: "BuhAdmin" },
          },
          {
            path: "act/:id",
            name: "buhadmin-mentor-act",
            component: () =>
              import("@/components/buhadmin/Acts/ActViewerMentor.vue"),
            meta: { auth: true, role: "BuhAdmin" },
          },
          {
            path: "report",
            name: "buhadmin-mentor-report",
            component: () =>
              import("@/components/buhadmin/Reports/ReportMentor.vue"),
            meta: { auth: true, role: "BuhAdmin" },
          },
        ],
      },
    ],
  },
];
