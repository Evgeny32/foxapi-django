export default [
  {
    path: "/franchisee",
    component: () => import("@/views/franchisee/Franchisee.vue"),
    meta: { auth: true, role: "Franchisee" },
    children: [
      {
        path: "analytics",
        component: () => import("@/views/franchisee/analytics/Analytics.vue"),
        meta: { auth: true, role: "Franchisee" },
        children: [
          {
            path: "/",
            name: "franchisee-analytics",
            component: () =>
              import("@/views/franchisee/analytics/Branches.vue"),
            meta: { auth: true, role: "Franchisee" },

          },
          {
            path: "moreinfo",
            name: "franchisee-analytics-moreinfo",
            component: () =>
              import("@/views/franchisee/analytics/MoreInfo.vue"),
            meta: { auth: true, role: "Franchisee" },
          },
          {
            path: "mentor/:id",
            name: "franchisee-analytics-mentor",
            component: () => import("@/views/branch/analytics/Mentor.vue"),
            meta: { auth: true, role: "Branch" },
          },
          {
            path: "branch/:id",
            name: "franchisee-analytics-branch",
            component: () => import("@/views/franchisee/analytics/Branch.vue"),
            meta: { auth: true, role: "Franchisee" },
          },
          {
            path: "child/:id",
            name: "franchisee-analytics-child",
            component: () => import("@/views/branch/analytics/child/Info.vue"),
            meta: { auth: true, role: "Franchisee" },
          },
          {
            path: "child/:id/tale/:tale",
            name: "franchisee-analytics-child-tale",
            component: () =>
              import("@/views/branch/analytics/child/Progress.vue"),
            meta: { auth: true, role: "Franchisee" },
          },
          {
            path: "child/:id/archive",
            name: "franchisee-analytics-child-archive",
            component: () =>
              import("@/views/branch/analytics/child/Archive.vue"),
            meta: { auth: true, role: "Franchisee" },
          },
          {
            path: ":pack",
            name: 'franchisee-analytics-pack',
            component: () =>
              import("@/views/franchisee/analytics/MoreInfo.vue"),
            meta: { auth: true, role: "Franchisee" },
          },
        ],
      },
      {
        path: "data",
        component: () => import("@/views/franchisee/data/DataUnion.vue"),
        meta: { auth: true, role: "Franchisee" },
        children: [
          {
            path: "/",
            name: "franchisee-data",
            component: () => import("@/views/franchisee/data/Data.vue"),
            meta: { auth: true, role: "Franchisee" },
          },
          {
            path: "change",
            name: "franchisee-data-change",
            component: () => import("@/views/franchisee/data/Change.vue"),
            meta: { auth: true, role: "Franchisee" },
          },

          {
            path: "documents/:id",
            name: "franchisee-data-documents",
            component: () => import("@/views/franchisee/data/Documents.vue"),
            meta: { auth: true, role: "Franchisee" },
          },
        ],
      },
      {
        path: "buy",
        name: "franchisee-buy",
        component: () => import("@/views/franchisee/buy/Buy.vue"),
        meta: { auth: true, role: "Franchisee" },
      },
      {
        path: "support",
        name: "franchise-support",
        component: () => import("@/views/agent/Support.vue"),
        meta: { auth: true, role: "Franchisee" },
      },
    ],
  },
];
