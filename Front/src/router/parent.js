export default [
  {
    path: "/parent",
    component: () => import("@/views/parent/Parent.vue"),
    meta: { auth: true, role: "Parent" },
    children: [
      {
        path: "children",
        component: () => import("@/views/global/LK.vue"),
        meta: { auth: true, role: "Parent" },

        children: [
          {
            path: "/",
            name: "parent-child-children",
            component: () => import("@/views/parent/child/Children.vue"),
            meta: { auth: true, role: "Parent" },
          },

          {
            path: "mychild",
            name: "parent-child-my",
            component: () => import("@/views/parent/child/MyChild.vue"),
            meta: { auth: true, role: "Parent" },
          },
        ],
      },
      {
        path: "momdadclub",
        name: "parent-momdadclub",
        component: () => import("@/views/parent/MomDadClub.vue"),
        meta: { auth: true, role: "Parent" },
      },
      {
        path: "profile",
        name: "parent-profile",
        component: () => import("@/views/parent/Profile.vue"),
        meta: { auth: true, role: "Parent" },
      },
    ],
  },
];