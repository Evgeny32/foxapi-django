export default [
  {
    path: "/head-client-manager/:managerId",
    component: () => import("@/views/clientManager/ClientManagerIndex.vue"),
    meta: { auth: true, role: "ClientManager" },
    children: [
      {
        path: "analytics",
        component: () =>
          import("@/views/clientManager/analytics/Analytics.vue"),
        meta: { auth: true, role: "ClientManager" },
        children: [
          {
            path: "/",
            name: "head-clientmanager-analytics-crm",
            component: () => import("@/views/clientManager/analytics/CRM.vue"),
            meta: { auth: true, role: "ClientManager" },
          },
          {
            path: "franchisee",
            component: () =>
              import("@/views/franchisee/analytics/Analytics.vue"),
            meta: { auth: true, role: "ClientManager" },
            children: [
              {
                path: "/",
                name: "head-clientmanager-analytics-franchisee",
                component: () =>
                  import("@/views/franchisee/analytics/Branches.vue"),
                meta: { auth: true, role: "ClientManager" },
              },
              {
                path: "moreinfo",
                name: "head-clientmanager-analytics-moreinfo",
                component: () =>
                  import(
                    "@/views/clientManager/analytics/franchisee/more-info.vue"
                  ),
                meta: { auth: true, role: "ClientManager" },
              },
              {
                path: "branch/:id",
                name: "head-clientmanager-analytics-branch",
                component: () =>
                  import("@/views/franchisee/analytics/Branch.vue"),
                meta: { auth: true, role: "Admin" },
              },
              {
                path: ":pack",
                name: "head-clientmanager-analytics-pack",
                component: () =>
                  import(
                    "@/views/clientManager/analytics/franchisee/more-info.vue"
                  ),
                meta: { auth: true, role: "ClientManager" },
              },
              {
                path: "franch-personal-data",
                name: "head-clientmanager-analytics-fr-data-mydata",
                component: () => import("@/views/clientManager/data/Data.vue"),
                meta: { auth: true, role: "ClientManager" },
              },
              {
                path: "franch-documents/:id",
                name: "head-clientmanager-analytics-fr-data-documents",
                component: () =>
                  import("@/views/franchisee/data/Documents.vue"),
                meta: { auth: true, role: "Admin" },
              },
            ],
          },
          {
            path: "agent/:id",
            name: "head-clientmanager-agent",
            component: () => import("@/views/agent/Analytics.vue"),
            meta: { auth: true, role: "Admin" },
          },
          {
            path: "reports",
            name: "head-clientmanager-analytics-reports",
            component: () =>
              import("@/views/clientManager/analytics/Reports.vue"),
            meta: { auth: true, role: "ClientManager" },
          },
        ],
      },
      {
        path: "franchisee",
        component: () =>
          import("@/views/clientManager/franchisee/Analytics.vue"),
        meta: { auth: true, role: "ClientManager" },
        children: [
          {
            path: "/",
            name: "head-clientmanager-franchisee-all",
            component: () => import("@/views/clientManager/franchisee/All.vue"),
            meta: { auth: true, role: "ClientManager" },
          },

          {
            path: ":id",
            name: "head-clientmanager-franchisee-info",
            component: () =>
              import("@/views/clientManager/franchisee/Info.vue"),
            meta: { auth: true, role: "ClientManager" },
          },

          {
            path: ":id/franch-data",
            name: "head-clientmanager-analytics-fr-data",
            component: () => import("@/views/clientManager/data/Change.vue"),
            meta: { auth: true, role: "ClientManager" },
          },
        ],
      },
      {
        path: "agents",
        component: () => import("@/views/clientManager/agents/Analytics.vue"),
        meta: { auth: true, role: "ClientManager" },
        children: [
          {
            path: "/",
            name: "head-clientmanager-agents-all",
            component: () => import("@/views/clientManager/agents/All.vue"),
            meta: { auth: true, role: "ClientManager" },
          },

          {
            path: "agents/:id",
            name: "head-clientmanager-agent-info",
            component: () => import("@/views/clientManager/agents/Info.vue"),
            meta: { auth: true, role: "ClientManager" },
          },

          {
            path: "agent-data/:id",
            name: "head-clientmanager-analytics-agent-data",
            component: () => import("@/views/clientManager/agents/Data.vue"),
            meta: { auth: true, role: "ClientManager" },
          },

          {
            path: "agent-anketa/:id",
            name: "head-clientmanager-agent-anketa",
            component: () => import("@/views/agent/Docs.vue"),
            meta: { auth: true, role: "ClientManager" },
          },
        ],
      },
      {
        path: "parents",
        component: () => import("@/views/clientManager/parents/Analytics.vue"),
        meta: { auth: true, role: "ClientManager" },
        children: [
          {
            path: "/",
            name: "head-clientmanager-parents-all",
            component: () => import("@/views/clientManager/parents/All.vue"),
            meta: { auth: true, role: "ClientManager" },
          },

          {
            path: "parents/:id",
            name: "head-clientmanager-parent-info",
            component: () => import("@/views/clientManager/parents/Info.vue"),
            meta: { auth: true, role: "ClientManager" },
          },

          {
            path: "parent-anketa/:id",
            name: "head-clientmanager-analytics-parent-data",
            component: () =>
              import("@/components/clientManager/parents/ParentInfoChange.vue"),
            meta: { auth: true, role: "ClientManager" },
          },
        ],
      },
      {
        path: "mentors",
        component: () => import("@/views/clientManager/mentors/Analytics.vue"),
        meta: { auth: true, role: "ClientManager" },
        children: [
          {
            path: "/",
            name: "head-clientmanager-mentors-all",
            component: () => import("@/views/clientManager/mentors/All.vue"),
            meta: { auth: true, role: "ClientManager" },
          },

          {
            path: "mentor/:id",
            name: "head-clientmanager-mentor-info",
            component: () => import("@/views/clientManager/mentors/Info.vue"),
            meta: { auth: true, role: "ClientManager" },
          },

          {
            path: "mentor-anketa/:id",
            name: "head-clientmanager-analytics-mentor-data",
            component: () =>
              import("@/components/clientManager/mentors/MentorInfoChange.vue"),
            meta: { auth: true, role: "ClientManager" },
          },
        ],
      },
    ],
  },
];
