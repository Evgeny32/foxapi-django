import clientCab from "@/router/clientManCab";
import salesCab from "@/router/salesManCab";

export default [
  {
    path: "/head-sales-manager",
    component: () =>
      import("@/views/headSalesManager/managers/ManagerIndex.vue"),
    meta: { auth: true, role: "HeadSalesManager" },
    children: [
      {
        path: "analytics",
        component: () =>
          import("@/views/headSalesManager/analytics/Analytics.vue"),
        meta: { auth: true, role: "HeadSalesManager" },
        children: [
          {
            path: "/",
            name: "headsalesmanager-analytics-crm",
            component: () =>
              import("@/views/headSalesManager/analytics/CRM.vue"),
            meta: { auth: true, role: "CHeadSalesManager" },
          },
          {
            path: "managers",
            name: "admin-managers",
            component: () =>
              import("@/views/headSalesManager/managers/All.vue"),
            meta: { auth: true, role: "HeadSalesManager" },
          },
          {
            path: "manager/:id",
            name: "admin-managers-info",
            component: () =>
              import("@/views/headSalesManager/managers/Info.vue"),
            meta: { auth: true, role: "HeadSalesManager" },
            children: [],
          },
          {
            path: "reports",
            name: "headSalesmanager-analytics-reports",
            component: () => import("@/views/headSalesManager/analytics/Reports.vue"),
            meta: { auth: true, role: "SalesManager" },
          },
        ],
      },
      {
        path: "franchisee",
        component: () =>
          import("@/views/headSalesManager/franchisee/Analytics.vue"),
        meta: { auth: true, role: "HeadSalesManager" },
        children: [
          {
            path: "/",
            name: "headsalesmanager-franchisee-all",
            component: () =>
              import("@/views/headSalesManager/franchisee/All.vue"),
            meta: { auth: true, role: "HeadSalesManager" },
          },
          {
            path: "franchisee/:id",
            name: "headsalesmanager-franchisee-info",
            component: () =>
              import("@/views/headSalesManager/franchisee/Info.vue"),
            meta: { auth: true, role: "HeadSalesManager" },
          },
        ],
      },
      {
        path: "agents",
        component: () =>
          import("@/views/headSalesManager/agents/Analytics.vue"),
        meta: { auth: true, role: "HeadSalesManager" },
        children: [
          {
            path: "/",
            name: "headsalesmanager-agents-all",
            component: () => import("@/views/headSalesManager/agents/All.vue"),
            meta: { auth: true, role: "HeadSalesManager" },
          },

          {
            path: "agent/:id",
            name: "headsalesmanager-agent-info",
            component: () => import("@/views/headSalesManager/agents/Info.vue"),
            meta: { auth: true, role: "HeadSalesManager" },
          },
        ],
      },

      {
        path: "parents",
        component: () =>
          import("@/views/headSalesManager/parents/Analytics.vue"),
        meta: { auth: true, role: "HeadSalesManager" },
        children: [
          {
            path: "/",
            name: "headsalesmanager-parents-all",
            component: () => import("@/views/headSalesManager/parents/All.vue"),
            meta: { auth: true, role: "HeadSalesManager" },
          },

          {
            path: "agent/:id",
            name: "headsalesmanager-parent-info",
            component: () =>
              import("@/views/headSalesManager/parents/Info.vue"),
            meta: { auth: true, role: "HeadSalesManager" },
          },
        ],
      },

      {
        path: "mentors",
        component: () =>
          import("@/views/headSalesManager/mentors/Analytics.vue"),
        meta: { auth: true, role: "HeadSalesManager" },
        children: [
          {
            path: "/",
            name: "headsalesmanager-mentors-all",
            component: () => import("@/views/headSalesManager/mentors/All.vue"),
            meta: { auth: true, role: "HeadSalesManager" },
          },

          {
            path: "mentor/:id",
            name: "headsalesmanager-mentor-info",
            component: () =>
              import("@/views/headSalesManager/mentors/Info.vue"),
            meta: { auth: true, role: "HeadSalesManager" },
          },
        ],
      },
    ],
  },
  ...clientCab,
  ...salesCab
];
