export default [
  {
    path: "/head-sales-manager/:managerId",
    component: () => import("@/views/salesManager/SalesManagerIndex.vue"),
    meta: { auth: true, role: "SalesManager" },
    children: [
      {
        path: "analytics",
        component: () => import("@/views/salesManager/analytics/Analytics.vue"),
        meta: { auth: true, role: "SalesManager" },
        children: [
          {
            path: "/",
            name: "head-salesmanager-analytics-crm",
            component: () => import("@/views/salesManager/analytics/CRM.vue"),
            meta: { auth: true, role: "SalesManager" },
          },

          {
            path: "franchisee/:id",
            component: () =>
              import("@/views/franchisee/analytics/Analytics.vue"),
            meta: { auth: true, role: "SalesManager" },
            children: [
              {
                path: "/",
                name: "head-salesmanager-analytics-branches",
                component: () =>
                  import("@/views/franchisee/analytics/Branches.vue"),
                meta: { auth: true, role: "SalesManager" },
              },
              {
                path: "moreinfo",
                name: "head-salesmanager-analytics-moreinfo",
                component: () =>
                  import(
                    "@/views/salesManager/analytics/franchisee/more-info.vue"
                  ),
                meta: { auth: true, role: "SalesManager" },
              },
              {
                path: "mentor/:id",
                name: "head-salesmanager-analytics-mentor",
                component: () => import("@/views/branch/analytics/Mentor.vue"),
                meta: { auth: true, role: "SalesManager" },
              },
              {
                path: "branch/:id",
                name: "head-salesmanager-analytics-branch",
                component: () =>
                  import("@/views/franchisee/analytics/Branch.vue"),
                meta: { auth: true, role: "SalesManager" },
              },
              {
                path: "child/:id",
                name: "head-salesmanager-analytics-child",
                component: () =>
                  import("@/views/branch/analytics/child/Info.vue"),
                meta: { auth: true, role: "SalesManager" },
              },
              {
                path: "child/:id/archive",
                name: "head-salesmanager-analytics-child-archive",
                component: () =>
                  import("@/views/branch/analytics/child/Archive.vue"),
                meta: { auth: true, role: "SalesManager" },
              },
              {
                path: ":pack",
                name: "head-salesmanager-analytics-pack",
                component: () =>
                  import(
                    "@/views/salesManager/analytics/franchisee/more-info.vue"
                  ),
                meta: { auth: true, role: "SalesManager" },
              },
              {
                path: "franch-personal-data",
                name: "head-salesmanager-analytics-fr-data-mydata",
                component: () => import("@/views/clientManager/data/Data.vue"),
                meta: { auth: true, role: "SalesManager" },
              },
              {
                path: "franch-documents/:id",
                name: "head-salesmanager-analytics-fr-data-documents",
                component: () =>
                  import("@/views/franchisee/data/Documents.vue"),
                meta: { auth: true, role: "SalesManager" },
              },
            ],
          },

          {
            path: "agent/:id",
            name: "head-salesmanager-agent",
            component: () => import("@/views/agent/Analytics.vue"),
            meta: { auth: true, role: "SalesManager" },
          },

          {
            path: "reports",
            name: "head-salesmanager-analytics-reports",
            component: () =>
              import("@/views/salesManager/analytics/Reports.vue"),
            meta: { auth: true, role: "SalesManager" },
          },
        ],
      },
      {
        path: "franchisee",
        component: () =>
          import("@/views/salesManager/franchisee/Analytics.vue"),
        meta: { auth: true, role: "SalesManager" },
        children: [
          {
            path: "/",
            name: "head-salesmanager-franchisee-all",
            component: () => import("@/views/salesManager/franchisee/All.vue"),
            meta: { auth: true, role: "SalesManager" },
          },

          {
            path: "franchisee/:id",
            name: "head-salesmanager-franchisee-info",
            component: () => import("@/views/salesManager/franchisee/Info.vue"),
            meta: { auth: true, role: "SalesManager" },
          },

          {
            path: "franchisee/:id/franch-data",
            name: "head-salesmanager-analytics-fr-data",
            component: () => import("@/views/clientManager/data/Change.vue"),
            meta: { auth: true, role: "SalesManager" },
          },
        ],
      },
      {
        path: "agents",
        component: () => import("@/views/salesManager/agents/Analytics.vue"),
        meta: { auth: true, role: "SalesManager" },
        children: [
          {
            path: "/",
            name: "head-salesmanager-agents-all",
            component: () => import("@/views/salesManager/agents/All.vue"),
            meta: { auth: true, role: "SalesManager" },
          },

          {
            path: "agents/:id",
            name: "head-salesmanager-agent-info",
            component: () => import("@/views/salesManager/agents/Info.vue"),
            meta: { auth: true, role: "SalesManager" },
          },

          {
            path: "agent-data/:id",
            name: "head-salesmanager-analytics-agent-data",
            component: () => import("@/views/salesManager/agents/Data.vue"),
            meta: { auth: true, role: "SalesManager" },
          },

          {
            path: "agent-anketa/:id",
            name: "head-salesmanager-agent-anketa",
            component: () => import("@/views/agent/Docs.vue"),
            meta: { auth: true, role: "SalesManager" },
          },
        ],
      },
    ],
  },
];
