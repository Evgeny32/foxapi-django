import Vue from "vue";
import VueRouter from "vue-router";
import globalLinks from "@/router/global";
import adminLinks from "@/router/admin";
import franchiseeLinks from "@/router/franchisee";
import branchLinks from "@/router/branch";
import agentLinks from "@/router/agent";
import parentLinks from "@/router/parent";
import mentorLinks from "@/router/mentor";
import childLinks from "@/router/child";
import buhadminLinks from "@/router/buhAdmin";
import buhfranchLinks from "@/router/buhFranch";
import talesLinks from "@/router/tales/tales";
import headSalesLinks from "@/router/headSales";
import salesManLinks from "@/router/salesMan";
import clientManLinks from "@/router/clientMan";
import { store } from "../store";


Vue.use(VueRouter);

const routes = [
  ...globalLinks,
  ...adminLinks,
  ...franchiseeLinks,
  ...branchLinks,
  ...agentLinks,
  ...parentLinks,
  ...mentorLinks,
  ...childLinks,
  ...talesLinks,
  ...buhadminLinks,
  ...buhfranchLinks,
  ...headSalesLinks,
  ...salesManLinks,
  ...clientManLinks,
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,

  routes,
});

const isAuthenticated = () => true;

router.beforeEach((to, from, next) => {
  if (to.matched.some((route) => route.meta?.requiresAuth)) {
    if (isAuthenticated()) {
      next();
    } else {
      next("/login");
    }
  } else {
    next();
  }
});

router.beforeEach((to, _, next) => {
  if (to.meta.auth && !store.getters["AuthStore/online"]) {
    next("/login");
  } else if (
    ["/", "/login", "/childlogin", "/adminlogin"].includes(to.path) &&
    store.getters["AuthStore/online"]
  ) {
    next(`/${store.getters["AuthStore/role"].toLowerCase()}/profile`);
  } else if (to.meta.role && to.meta.role != store.getters["AuthStore/role"]) {
    next(`/${store.getters["AuthStore/role"].toLowerCase()}/profile`);
  } else if (to.meta.tale && to.meta.tale > store.getters["TaleStore/tales"]) {
    if (store.getters["TaleStore/tales"]) {
      next(`/tale${store.getters["TaleStore/tales"]}-1`);
    } else [next(`/${store.getters["AuthStore/role"].toLowerCase()}/profile`)];
  } else {
    next();
  }
});

export default router;
