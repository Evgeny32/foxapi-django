export default [
  {
    path: "/childlogin",
    component: () => import("@/views/child/login/LoginRoot.vue"),
    children: [
      {
        path: "/",
        name: "child-login",
        component: () => import("@/views/child/login/Login.vue"),
      },
      {
        path: "steps",
        name: "child-login-steps",
        component: () => import("@/views/child/login/Steps.vue"),
      },
    ],
  },
  {
    path: "/child",
    component: () => import("@/views/child/Child.vue"),
    meta: { auth: true, role: "Child" },
    children: [
      {
        path: "tales",
        name: "child-tales",
        component: () => import("@/views/child/Tales.vue"),
        meta: { auth: true, role: "Child" },
      },

      {
        path: "dictionary",
        name: "child-dictionary",
        component: () => import("@/views/child/Dictionary.vue"),
        meta: { auth: true, role: "Child" },
      },
      {
        path: "notebook",
        name: "child-notebook",
        component: () => import("@/views/child/Notebook.vue"),
        meta: { auth: true, role: "Child" },
      },
      {
        path: "profile",
        name: "child-profile",
        component: () => import("@/views/child/Profile.vue"),
        meta: { auth: true, role: "Child" },
      },
    ],
  },
];
