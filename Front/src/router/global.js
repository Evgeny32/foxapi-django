export default [
  {
    // Страница регистрации для родителя и ментора
    path: "/",
    name: "global-register",
    component: () => import("@/views/global/Register.vue"),
  },

  {
    path: "/login",
    name: "global-login",
    component: () => import("@/views/global/Login.vue"),
  },
  {
    path: "/franchisee-authentification",
    component: () => import("@/views/global/admin/Login.vue"),
    children: [
      {
        path: "/",
        name: "global-admin-role",
        component: () => import("@/views/global/admin/ChoiseRole.vue"),
      },
      {
        path: "agent",
        name: "global-admin-agent",
        component: () => import("@/views/global/admin/AgentRegister.vue"),
      },
      {
        path: "franchisee",
        name: "global-admin-franchisee",
        component: () => import("@/views/global/admin/FranchiseeRegister.vue"),
      },
      {
        path: "franchisee/:referal",
        name: "global-admin-franchisee-referal",
        component: () => import("@/views/global/admin/FranchiseeRegister.vue"),
      },
    ],
  },
  {
    path: "/referal/:referal",
    name: "global-register-referal",
    component: () => import("@/views/global/Register.vue"),
  },
];
