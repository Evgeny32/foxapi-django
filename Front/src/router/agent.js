export default [
  {
    path: "/agent",
    component: () => import("@/views/agent/Agent.vue"),
    meta: { auth: true, role: "Agent" },
    children: [
      {
        path: "/",
        name: "agent-analytics",
        component: () => import("@/views/agent/Analytics.vue"),
        meta: { auth: true, role: "Agent" },
      },
      {
        path: "data",
        name: "agent-data-union",
        component: () => import("@/views/agent/DataUnion.vue"),
        meta: { auth: true, role: "Agent" },
        children: [
          {
            path: "/",
            name: "agent-data",
            component: () => import("@/views/agent/Data.vue"),
            meta: { auth: true, role: "Agent" },
          },
          {
            path: "docs",
            name: "agent-docs",
            component: () => import("@/views/agent/Docs.vue"),
            meta: { auth: true, role: "Agent" },
          },
        ],
      },

      {
        path: "support",
        name: "agent-support",
        component: () => import("@/views/agent/Support.vue"),
        meta: { auth: true, role: "Agent" },
      },
    ],
  },
];
