import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";
const baseUrl = process.env.VUE_APP_API_URL + "/api/v1/transactions";
Axios.defaults.headers.post["Content-Type"] = "application/json";

class TransactionState {
  error = null;
  // transaction = null;
}

class TransactionGetters extends Getters {}

class TransactionMutations extends Mutations {
  setError(error) {
    this.state.error = error;
  }
  // setTransaction(transaction) {
  //   this.state.transaction = transaction;
  // }
}

class TransactionActions extends Actions {
  async getTransactions() {
    this.mutations.setError(null);
    try {
      let response = await Axios.get(baseUrl, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      });
      return response.data;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  // async postTransaction(data) {
  //   this.mutations.setError(null);
  //   await Axios.post(baseUrl + "/", data, {
  //     headers: {
  //       Authorization: "Token " + store.getters["AuthStore/token"],
  //     },
  //   }).catch((error) => this.mutations.setError(error.response.data));
  // }
  // async getTransaction(id) {
  //   this.mutations.setError(null);
  //   try {
  //     let response = await Axios.get(`${baseUrl}/${id}`, {
  //       headers: {
  //         Authorization: "Token " + store.getters["AuthStore/token"],
  //       },
  //     });
  //     return response.data;
  //   } catch (error) {
  //     this.mutations.setError(error.response.data);
  //   }
  // }
  // async patchTransaction({ id, data }) {
  //   this.mutations.setError(null);
  //   await Axios.post(`${baseUrl}/${id}/`, data, {
  //     headers: {
  //       Authorization: "Token " + store.getters["AuthStore/token"],
  //     },
  //   }).catch((error) => this.mutations.setError(error.response.data));
  // }
  // async deleteTransaction(id) {
  //   this.mutations.setError(null);
  //   await Axios.delete(`${baseUrl}/${id}`, {
  //     headers: {
  //       Authorization: "Token " + store.getters["AuthStore/token"],
  //     },
  //   }).catch((error) => this.mutations.setError(error.response.data));
  // }
}

const transaction = new Module({
  state: TransactionState,
  getters: TransactionGetters,
  mutations: TransactionMutations,
  actions: TransactionActions,
});
export const TransactionMapper = createMapper(transaction);
export default transaction;
