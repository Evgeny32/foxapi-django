import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

// const base_url = "http://localhost:8000/api/v1/tales";
// const media_url = "http://localhost:8000";
const base_url = process.env.VUE_APP_API_URL + "/api/v1/tales";
const media_url = process.env.VUE_APP_API_URL;
Axios.defaults.headers.post["Content-Type"] = "application/json";

class TaleState {
  tales = [];
  error = null;
  tale = {};
  taleTitle = "";
  taleSubTitle = "";
  talePage = "";
}

class TaleGetters extends Getters {
  get tales() {
    return this.state.tales.length;
  }
}

class TaleMutations extends Mutations {
  setTales(tales) {
    this.state.tales = tales;
  }
  clearTales() {
    this.state.tales = [];
  }
  setError(error) {
    this.state.error = error;
  }
  clearError() {
    this.state.error = null;
  }
  setTale(tale) {
    tale.tasks.forEach((task) => {
      task.answers.forEach((answer) => {
        if (answer.answer) {
          answer.answer = media_url + answer.answer;
        }
      });
    });
    this.state.tale = tale;
  }
  clearTale() {
    this.state.tale = {};
  }
  setTaleSubTitle(subTitle) {
    this.state.taleSubTitle = subTitle;
  }
  clearTaleSubTitle() {
    this.state.taleSubTitle = "";
  }
  setTaleTitle(title) {
    this.state.taleTitle = title;
  }
  clearTaleTitle() {
    this.state.taleTitle = "";
  }
  setTalePage(page) {
    this.state.talePage = page;
  }
  clearTalePage() {
    this.state.taleTitle = "";
  }
}

class TaleActions extends Actions {
  async getTales() {
    this.mutations.clearError();
    this.mutations.clearTales();
    await Axios.get(`${base_url}/`, {
      headers: { Authorization: "Token " + store.getters["AuthStore/token"] },
    })
      .then((response) => this.mutations.setTales(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getTale(id) {
    this.mutations.clearError();
    this.mutations.clearTale();
    await Axios.get(`${base_url}/${id}`, {
      headers: { Authorization: "Token " + store.getters["AuthStore/token"] },
    })
      .then((response) => this.mutations.setTale(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async postTale({ taleId, childId }) {
    this.mutations.clearError();
    await Axios.post(`${base_url}/create/${taleId}/${childId}/`, {}, {
      headers: { Authorization: "Token " + store.getters["AuthStore/token"] },
    }).catch((error) => this.mutations.setError(error.response.data));
  }

  async patchTale(data) {
    this.mutations.clearError();
    await Axios.patch(
      `${base_url}/${this.state.tale.id}/`,
      { ...data },
      {
        headers: { Authorization: "Token " + store.getters["AuthStore/token"] },
      }
    )
      .then(() => this.actions.getTale(this.state.tale.id))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchTask({ id, data }) {
    let taleid = this.state.tale.id
    this.mutations.clearError();
    await Axios.patch(
      `${base_url}/task/${id}/`,
      { ...data },
      {
        headers: { Authorization: "Token " + store.getters["AuthStore/token"] },
      }
    )
      .then(() => this.actions.getTale(taleid))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchAnswer({ id, data }) {
    this.mutations.clearError();
    await Axios.patch(
      `${base_url}/answer/${id}/`,
      { ...data },
      {
        headers: { Authorization: "Token " + store.getters["AuthStore/token"] },
      }
    )
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async patchFormAnswer({ id, data }) {
    this.mutations.clearError();
    await Axios.patch(`${base_url}/answer/${id}/`, data, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
        "Content-Type": "multipart/form-data",
      },
    })
      .catch((error) => this.mutations.setError(error.response.data));
  }
}

const tale = new Module({
  state: TaleState,
  getters: TaleGetters,
  mutations: TaleMutations,
  actions: TaleActions,
});
export const TaleMapper = createMapper(tale);
export default tale;
