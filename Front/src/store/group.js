import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";

const base_url = process.env.VUE_APP_API_URL + "/api/v1/mentor";
const media_url = process.env.VUE_APP_API_URL;

Axios.defaults.headers.post["Content-Type"] = "application/json";

class GroupState {
  groups = null;
  error = null;
  group = null;
  mediaUrl = media_url;
}

class GroupMutations extends Mutations {
  setError(error) {
    this.state.error = error;
  }
  clearError() {
    this.state.error = null;
  }

  setGroups(groups) {
    this.state.groups = groups;
  }
  clearGroups() {
    this.state.groups = null;
  }

  setGroup(group) {
    this.state.group = group;
  }
  clearGroup() {
    this.state.group = null;
  }
}

class GroupGetters extends Getters { }

class GroupActions extends Actions {
  async getGroups(token) {
    this.mutations.clearError();
    this.mutations.clearGroups();
    await Axios.get(base_url + "/groups/", {
      headers: {
        Authorization: "Token " + token,
      },
    })
      .then((response) => this.mutations.setGroups(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async postGroups(payload) {
    this.mutations.clearError();
    await Axios.post(
      base_url + "/groups/",
      { ...payload },
      {
        headers: {
          Authorization: "Token " + payload.token,
        },
      }
    )
      .then(() => this.actions.getGroups(payload.token))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async putChildAddGroup(payload) {
    this.mutations.clearError();
    await Axios.put(
      base_url + "/group/add/" + this.state.group["id"] + "/",
      { ...payload },
      {
        headers: {
          Authorization: "Token " + payload.token,
        },
      }
    )
      .then(() => this.actions.getGroups(payload.token))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async putChildDenyGroup(payload) {
    this.mutations.clearError();
    await Axios.put(
      base_url + "/group/deny/" + this.state.group["id"] + "/",
      { ...payload },
      {
        headers: {
          Authorization: "Token " + payload.token,
        },
      }
    )
      .then(() => {
        this.actions.getGroups(payload.token);
        this.mutations.setGroup(this.state.groups.find(this.actions.findGroup));
      })
      .catch((error) => this.mutations.setError(error.response.data));
  }
}

const group = new Module({
  state: GroupState,
  getters: GroupGetters,
  mutations: GroupMutations,
  actions: GroupActions,
});
export const GroupMapper = createMapper(group);
export default group;
