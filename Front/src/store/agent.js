import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

const baseUrl = process.env.VUE_APP_API_URL + "/api/v1/agent/";
const mediaUrl = process.env.VUE_APP_API_URL;
// const mediaUrl = "http://localhost:8000";

// Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
Axios.defaults.headers.post["Content-Type"] = "application/json";
// Axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

class AgentState {
  agentProfile = { user_id: {} };
  error = null;
  packages = []
}

class AgentGetters extends Getters { }

class AgentMutations extends Mutations {
  clearAgentProfile() {
    this.state.agentProfile = { user_id: {} };
  }
  setAgentProfile(profile) {
    profile.passportPDF = mediaUrl + profile.passportPDF;
    profile.snilsPDF = mediaUrl + profile.snilsPDF;
    profile.innPDF = mediaUrl + profile.innPDF;
    profile.resumePDF = mediaUrl + profile.resumePDF;
    this.state.agentProfile = profile;
  }
  setError(error) {
    this.state.error = error;
  }
  clearError() {
    this.state.error = null;
  }
  setPackages(packages) {
    this.state.packages = packages;
  }
  clearPackages() {
    this.state.packages = [];
  }
}

class AgentActions extends Actions {
  async getAgentProfile() {
    this.mutations.clearError();
    this.mutations.clearAgentProfile();
    await Axios.get(baseUrl, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setAgentProfile(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async patchAgentProfile(data) {
    this.mutations.clearError();
    this.mutations.clearAgentProfile();
    await Axios.patch(baseUrl, data, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
        "Content-Type": "multipart/form-data",
      },
    })
      .then((response) => this.mutations.setAgentProfile(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getOtherAgentProfile(id) {
    this.mutations.clearError();
    this.mutations.clearAgentProfile();
    await Axios.get(baseUrl + id)
      .then((response) => this.mutations.setAgentProfile(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getPackages() {
    this.mutations.clearError();
    this.mutations.clearPackages()
    await Axios.get(baseUrl + 'packages', {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      }
    })
      .then(response => this.mutations.setPackages(response.data))
      .catch(() => this.mutations.setError(true));
  }


}

const agent = new Module({
  getters: AgentGetters,
  state: AgentState,
  mutations: AgentMutations,
  actions: AgentActions,
});
export const AgentMapper = createMapper(agent);
export default agent;
