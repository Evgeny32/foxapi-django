import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

const base_url = process.env.VUE_APP_API_URL + "/api/v1/coachings";
// const media_url = process.env.VUE_APP_API_URL;
Axios.defaults.headers.post["Content-Type"] = "application/json";

class CoachState {
  free_coachings = null;
  error = null;
  taken_coachings = null;
  coaching_id = null;
}

class CoachGetters extends Getters {
  get coachers() {
    let coachings = this.state.free_coachings;
    if (coachings) {
      let coachers = [];
      for (let i = 0; i < coachings.length; i++) {
        coachers[i] = {
          id: coachings[i].id,
          second_name: coachings[i].second_name,
          name: coachings[i].name,
          last_name: coachings[i].last_name,
        };
      }
      const unique_coachers = new Set(coachers);
      return [...unique_coachers];
    }
    return null;
  }

  get costs() {
    let coachings = this.state.free_coachings;
    if (coachings) {
      let costs = [];
      for (let i = 0; i < coachings.length; i++) {
        costs[i] = {
          id: coachings[i].id,
          cost: coachings[i].cost,
        };
      }
      const unique_costs = new Set(costs);
      return [...unique_costs];
    }
    return null;
  }
}

class CoachMutations extends Mutations {
  setFreeCoaching(free_coachings) {
    this.state.free_coachings = free_coachings;
  }
  clearFreeCoaching() {
    this.state.free_coachings = null;
  }

  setTakenCoaching(taken_coachings) {
    this.state.taken_coachings = taken_coachings;
  }
  clearTakenCoaching() {
    this.state.taken_coachings = null;
  }

  setError(error) {
    this.state.error = error;
  }
  clearError() {
    this.state.error = null;
  }

  setCoachingId(coaching_id) {
    this.state.coaching_id = coaching_id;
  }
  clearCoachingId() {
    this.state.coaching_id = null;
  }
}

class CoachActions extends Actions {
  async getFreeCoachings() {
    this.mutations.clearError();
    this.mutations.clearFreeCoaching();
    try {
      const response = await Axios.get(base_url + "/free/", {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      });
      if (response.status == 200) {
        this.mutations.setFreeCoaching(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getTakenCoachings() {
    this.mutations.clearError();
    this.mutations.clearTakenCoaching();
    try {
      const response = await Axios.get(base_url + "/taken/", {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      });
      if (response.status == 200) {
        this.mutations.setTakenCoaching(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async takeCoaching() {
    this.mutations.clearError();
    try {
      const response = await Axios.put(
        base_url + "/" + this.state.coaching_id + "/",
        {},
        {
          headers: {
            Authorization: "Token " + store.getters["AuthStore/token"],
          },
        }
      );
      if (response.status == 200) {
        this.actions.getTakenCoachings();
        this.actions.getFreeCoachings();
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
}

const coach = new Module({
  state: CoachState,
  getters: CoachGetters,
  mutations: CoachMutations,
  actions: CoachActions,
});
export const CoachMapper = createMapper(coach);
export default coach;
