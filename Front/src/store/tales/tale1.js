import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
// import { store } from "@/store";

const base_url = process.env.VUE_APP_API_URL + "/api/v1/tales/1";
const media_url = process.env.VUE_APP_API_URL;
// const base_url = "http://localhost:8000/api/v1/tales/1";
// const media_url = "http://localhost:8000";
Axios.defaults.headers.post["Content-Type"] = "application/json";

class Tale1State {
  page = 1;
  error = null;
  tale = null;
}

class Tale1Getters extends Getters { }

class Tale1Mutations extends Mutations {
  changePage(page) {
    this.state.page = page;
  }
  setError(error) {
    this.state.error = error;
  }
  clearError() {
    this.state.error = null;
  }
  set1Tale(tale) {
    for (let i = 1; i <= 5; i++) {
      tale[`communication_club_quest${i}_report`]
        ? (tale[`communication_club_quest${i}_report`] =
          media_url + tale[`communication_club_quest${i}_report`])
        : (tale[`communication_club_quest${i}_report`] = null);

      tale[`business_vocabulary_task2_answer${i}`]
        ? (tale[`business_vocabulary_task2_answer${i}`] = media_url + tale[`business_vocabulary_task2_answer${i}`])
        : (tale[`business_vocabulary_task2_answer${i}`] = null);

      tale[`creative_project_answer${i}`]
        ? (tale[`creative_project_answer${i}`] = media_url + tale[`creative_project_answer${i}`])
        : (tale[`creative_project_answer${i}`] = null);
    }

    this.state.tale = tale;
  }
}

class Tale1Actions extends Actions {
  async turnPage(payload) {
    this.mutations.clearError();
    await Axios.patch(
      `${base_url}/`,
      { pages: payload.tale.pages, child: payload.tale.child },
      {
        headers: { Authorization: "Token " + payload.token },
      }
    )
      .then((response) => this.mutations.set1Tale(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async changeEmoji(payload) {
    this.mutations.clearError();
    await Axios.patch(
      `${base_url}/`,
      { emoji: payload.tale.emoji, child: payload.tale.child },
      {
        headers: { Authorization: "Token " + payload.token },
      }
    )
      .then((response) => this.mutations.set1Tale(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async changeTalePage(payload) {
    this.mutations.clearError();
    await Axios.patch(
      `${base_url}/`,
      { tale_pages: payload.tale.tale_pages, child: payload.tale.child },
      {
        headers: { Authorization: "Token " + payload.token },
      }
    )
      .then((response) => this.mutations.set1Tale(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async acceptTask1(payload) {
    this.mutations.clearError();
    await Axios.patch(
      `${base_url}/`,
      {
        task1_accept: true,
        child: payload.tale.child,
        pages: "11",
        task1_coins: 1,
      },
      {
        headers: {
          Authorization: "Token " + payload.token,
        },
      }
    )
      .then((response) => this.mutations.set1Tale(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async setTime(payload) {
    this.mutations.clearError();
    await Axios.patch(
      `${base_url}/`,
      { ...payload.time, child: payload.tale.child },
      {
        headers: { Authorization: "Token " + payload.token },
      }
    )
      .then((response) => this.mutations.set1Tale(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async postAnswer(payload) {
    this.mutations.clearError();
    await Axios.patch(`${base_url}/`, payload.data, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: "Token " + payload.token,
      },
    })
      .then((response) => this.mutations.set1Tale(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async setReport(payload) {
    this.mutations.clearError();
    await Axios.patch(`${base_url}/`, payload.data, {

      headers: { "Content-Type": "multipart/form-data", Authorization: "Token " + payload.token },
    })
      .then((response) => this.mutations.set1Tale(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async patchTale(payload) {
    this.mutations.clearError();
    await Axios.patch(
      `${base_url}/`,
      { ...payload.data, child: payload.tale.child },
      {
        headers: { Authorization: "Token " + payload.token },
      }
    )
      .then((response) => this.mutations.set1Tale(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
}

const tale1 = new Module({
  state: Tale1State,
  getters: Tale1Getters,
  mutations: Tale1Mutations,
  actions: Tale1Actions,
});
export const Tale1Mapper = createMapper(tale1);
export default tale1;
