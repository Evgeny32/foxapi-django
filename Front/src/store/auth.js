import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";

const baseUrl = process.env.VUE_APP_API_URL + "/api/v1/account";
const media_url = process.env.VUE_APP_API_URL;

Axios.defaults.headers.post["Content-Type"] = "application/json";

class AuthState {
  online = false;
  token = null;
  profile = null;
  error = null;
  avatar = null;
}

class AuthGetters extends Getters {
  get names() {
    return (
      this.state.profile.name,
      this.state.profile.second_name,
      this.state.profile.last_name
    );
  }
  get role() {
    return this.state.profile.role;
  }
  get online() {
    return this.state.online;
  }
  get token() {
    return this.state.token;
  }
}

class AuthMutations extends Mutations {
  clearOnline() {
    this.state.online = false;
  }
  setOnline() {
    this.state.online = true;
  }

  clearProfile() {
    this.state.profile = null;
  }
  setProfile(profile) {
    profile.avatar
      ? (profile.avatar = media_url + profile.avatar)
      : (profile.avatar = null);
    this.state.profile = profile;
  }

  setError(error) {
    this.state.error = error;
  }
  clearError() {
    this.state.error = null;
  }

  setToken(token) {
    this.state.token = token;
  }
  clearToken() {
    this.state.token = null;
  }
  setChildAvatar(payload) {
    this.state.avatar = payload;
  }
}

class AuthActions extends Actions {
  async signIn(payload) {
    this.mutations.clearOnline();
    this.mutations.clearToken();
    this.mutations.clearError();
    await Axios.post(baseUrl + "/login/", { ...payload })
      .then((response) => {
        this.mutations.setToken(response.data.token);
        this.mutations.setOnline();
      })
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getProfile() {
    this.mutations.clearError();
    await Axios.get(
      baseUrl + "/profile/",

      {
        headers: {
          Authorization: "Token " + this.state.token,
        },
      }
    )
      .then((response) => this.mutations.setProfile(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getOtherProfile(id) {
    this.mutations.clearError();
    await Axios.get(baseUrl + "/profile/" + id + "/")
      .then((response) => this.mutations.setProfile(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async signUp(payload) {
    this.mutations.clearError();
    await Axios.post(baseUrl + "/register/", { ...payload })
      .then((response) => this.mutations.setProfile(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async signOut() {
    this.mutations.clearError();
    this.mutations.clearOnline();
    this.mutations.clearProfile();
  }
  async sendSms(data) {
    this.mutations.clearError();
    await Axios.post(baseUrl + "/send_code/", {
      ...data,
    })
      .then(() => {
        this.mutations.clearOnline();
        this.mutations.clearToken();
      })
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async patchProfile(data) {
    this.mutations.clearError();
    this.mutations.clearProfile();
    await Axios.patch(baseUrl + "/profile/", data, {
      headers: {
        Authorization: "Token " + this.state.token,
      },
    })
      .then((response) => this.mutations.setProfile(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async patchOtherProfile({ id, data }) {
    this.mutations.clearError();
    await Axios.patch(
      baseUrl + "/profile/" + id.toString() + "/",
      { ...data },
      {
        headers: {
          Authorization: "Token " + this.state.token,
        },
      }
    ).catch((error) => this.mutations.setError(error.response.data));
  }
  async patchFormDataProfile(data) {
    this.mutations.clearError();
    this.mutations.clearProfile();
    await Axios.patch(baseUrl + "/profile/", data, {
      headers: {
        Authorization: "Token " + this.state.token,
        "Content-Type": "multipart/form-data",
      },
    })
      .then((response) => this.mutations.setProfile(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async changePassword(data) {
    this.mutations.clearError();

    await Axios.patch(
      baseUrl + "/change_password/",
      { ...data },
      {
        headers: {
          Authorization: "Token " + this.state.token,
        },
      }
    ).catch((error) => this.mutations.setError(error.response.data));
  }
}
const auth = new Module({
  state: AuthState,
  getters: AuthGetters,
  mutations: AuthMutations,
  actions: AuthActions,
});
export const AuthMapper = createMapper(auth);
export default auth;
