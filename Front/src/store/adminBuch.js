import {
    Getters,
    Mutations,
    Actions,
    Module,
    createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

const baseUrl = process.env.VUE_APP_API_URL + "/api/v1/admin-buch/";
//   const mediaUrl = process.env.VUE_APP_API_URL;
// const mediaUrl = "http://localhost:8000";

// Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
Axios.defaults.headers.post["Content-Type"] = "application/json";
// Axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

class AdminBuchState {
    agents = [];
    agentDetails = {};
    children = [];
    childDetails = {};
    franchisee = [];
    franchiseeDetails = {};
    mentors = [];
    mentorDetails = {};
    error = null;
}

class AdminBuchGetters extends Getters { }

class AdminBuchMutations extends Mutations {

    setAgents(agent) {
        this.state.agents = agent;
    }

    clearAgents() {
        this.state.agents = [];
    }

    setAgentDetails(agent) {
        this.state.agentDetails = agent;
    }

    clearAgentDetails() {
        this.state.agentDetails = {};
    }

    setChildren(child) {
        this.state.children = child;
    }

    clearChildren() {
        this.state.children = [];
    }

    setChildDetails(child) {
        this.state.childDetails = child;
    }

    clearChildDetails() {
        this.state.childDetails = [];
    }

    setFranchisee(franchisee) {
        this.state.franchisee = franchisee;
    }

    clearFranchisee() {
        this.state.franchisee = [];
    }

    setFranchiseeDetails(franchisee) {
        this.state.franchiseeDetails = franchisee;
    }

    clearFranchiseeDetails() {
        this.state.franchiseeDetails = {};
    }

    setMentors(mentor) {
        this.state.mentors = mentor;
    }

    clearMentors() {
        this.state.mentors = [];
    }

    setMentorDetails(mentor) {
        this.state.mentorDetails = mentor;
    }

    clearMentorDetails() {
        this.state.mentorDetails = {};
    }

    setError(error) {
        this.state.error = error;
    }
    clearError() {
        this.state.error = null;
    }
}

class AdminBuchActions extends Actions {

    async getChildrenList() {
        this.mutations.clearError();
        this.mutations.clearChildren();
        await Axios.get(baseUrl + "children/", {
            headers: {
                Authorization: "Token " + store.getters["AuthStore/token"],
            },
        })
            .then((response) => this.mutations.setChildren(response.data))
            .catch(() => this.mutations.setError(true));
    }


    async getChildDetails(id) {
        // * Получение подробностей об одном менторе
        this.mutations.clearError();
        this.mutations.clearChildDetails();
        await Axios.get(
            baseUrl + "child/" + id.toString(),

            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                },
            }
        )
            .then((response) => this.mutations.setChildDetails(response.data))
            .catch((error) => this.mutations.setError(error.response.data));
    }


    async getAgents() {
        this.mutations.clearError();
        this.mutations.clearAgents();
        await Axios.get(baseUrl + "agents/", {
            headers: {
                Authorization: "Token " + store.getters["AuthStore/token"],
            },
        })
            .then((response) => this.mutations.setAgents(response.data))
            .catch(() => this.mutations.setError(true));
    }


    async getAgentDetails(id) {
        // * Получение подробностей об одном менторе
        this.mutations.clearError();
        this.mutations.clearAgentDetails();
        await Axios.get(
            baseUrl + "agent/" + id.toString(),

            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                },
            }
        )
            .then((response) => this.mutations.setAgentDetails(response.data))
            .catch((error) => this.mutations.setError(error.response.data));
    }


    async getFranchiseeDetails(id) {
        // * Получение подробностей об одном менторе
        this.mutations.clearError();
        this.mutations.clearFranchiseeDetails();
        await Axios.get(
            baseUrl + "franchisee/" + id.toString(),

            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                },
            }
        )
            .then((response) => this.mutations.setFranchiseeDetails(response.data))
            .catch((error) => this.mutations.setError(error.response.data));
    }



    async getFranchisee() {
        this.mutations.clearError();
        this.mutations.clearFranchisee();
        await Axios.get(baseUrl + "franchisee/", {
            headers: {
                Authorization: "Token " + store.getters["AuthStore/token"],
            },
        })
            .then((response) => this.mutations.setFranchisee(response.data))
            .catch(() => this.mutations.setError(true));
    }


    async getMentors() {
        this.mutations.clearError();
        this.mutations.clearMentors();
        await Axios.get(baseUrl + "mentors/", {
            headers: {
                Authorization: "Token " + store.getters["AuthStore/token"],
            },
        })
            .then((response) => this.mutations.setMentors(response.data))
            .catch(() => this.mutations.setError(true));
    }

    async getMentorDetails(id) {
        // * Получение подробностей об одном менторе
        this.mutations.clearError();
        this.mutations.clearMentorDetails();
        await Axios.get(
            baseUrl + "mentor/" + id.toString(),

            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                },
            }
        )
            .then((response) => this.mutations.setMentorDetails(response.data))
            .catch((error) => this.mutations.setError(error.response.data));
    }

}

const adminBuch = new Module({
    getters: AdminBuchGetters,
    state: AdminBuchState,
    mutations: AdminBuchMutations,
    actions: AdminBuchActions,
});
export const AdminBuchMapper = createMapper(adminBuch);
export default adminBuch;
