import { Getters, Mutations, Actions, Module } from "vuex-smart-module";
import AuthStore from "./auth";
import BranchStore from "./branch";
import CoachStore from "./coach";
import ChildStore from "./child";
import GroupStore from "./group";
import DictStore from "./dict";
import TaleStore from "./tale";
import Tale1Store from "./tales/tale1";
import AgentStore from "./agent";
import SupportStore from "./support";
import FranchiseeStore from "./franchisee";
import MentorStore from "./mentor";
import AdminStore from "./admin";
import AdminBuchStore from "./adminBuch";
import FranchBuchStore from "./franchBuch";
import PlayerStore from "./player";
import HeadManagerStore from "./headManager";
import ClientManagerStore from "./clientManager";
import SalesManagerStore from "./salesManager";
import TransStore from "./trans";
import SberStore from "./sber";

const tales = { Tale1Store };

class RootState {}

class RootGetters extends Getters {}

class RootMutations extends Mutations {}

class RootActions extends Actions {}

export default new Module({
  state: RootState,
  getters: RootGetters,
  mutations: RootMutations,
  actions: RootActions,
  modules: {
    AuthStore,
    BranchStore,
    CoachStore,
    ChildStore,
    GroupStore,
    DictStore,
    TaleStore,
    AgentStore,
    SupportStore,
    FranchiseeStore,
    MentorStore,
    AdminStore,
    AdminBuchStore,
    FranchBuchStore,
    PlayerStore,
    HeadManagerStore,
    ClientManagerStore,
    SalesManagerStore,
    TransStore,
    SberStore,

    ...tales,
  },
});
