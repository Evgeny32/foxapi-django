import {
    Getters,
    Mutations,
    Actions,
    Module,
    createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

const baseUrl = process.env.VUE_APP_API_URL + "/api/v1/support/";
//   const mediaUrl = process.env.VUE_APP_API_URL;
// const mediaUrl = "http://localhost:8000";

// Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
Axios.defaults.headers.post["Content-Type"] = "application/json";
// Axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

class SupportState {

    error = null;
    token = null;
    support = [];
    adminSupport = [];
    adminSupportTheme = [];
}

class SupportGetters extends Getters { }

class SupportMutations extends Mutations {

    setError(error) {
        this.state.error = error;
    }
    clearError() {
        this.state.error = null;
    }

    setToken(token) {
        this.state.token = token;
    }

    clearToken() {
        this.state.token = null;
    }
    setSupport(msgs) {
        this.state.support = msgs;
    }

    clearSupport() {
        this.state.support = [];
    }

    setAdminSupport(msgs) {
        this.state.adminSupport = msgs;
    }

    clearAdminSupport() {
        this.state.adminSupport = [];
    }

    setAdminSupportTheme(msgs) {
        this.state.adminSupportTheme = msgs;
    }

    clearAdminSupportTheme() {
        this.state.adminSupportTheme = [];
    }
}

class SupportActions extends Actions {

    async sendSupport(data) {
        this.mutations.clearError();
        await Axios.post(
            baseUrl,
            { ...data },
            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                },
            }
        )
            .then(() => this.actions.getSupport())
            .catch((error) => this.mutations.setError(error.response.data));

    }
    async getSupport() {
        this.mutations.clearError();
        this.mutations.clearSupport();
        await Axios.get(
            baseUrl,

            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                },
            }
        )
            .then((response) => this.mutations.setSupport(response.data))
            .catch((error) => this.mutations.setError(error.response.data));
    }

    async getAdminSupport() {
        this.mutations.clearError();
        this.mutations.clearAdminSupport();
        await Axios.get(
            baseUrl + "admin/",

            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                },
            }
        )
            .then((response) => this.mutations.setAdminSupport(response.data))
            .catch((error) => this.mutations.setError(error.response.data));
    }
    async sendAdminSupport({ id, data }) {
        this.mutations.clearError();
        await Axios.post(
            `${baseUrl}admin/${id}`,
            { ...data },
            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                },
            }
        )
            .then(() => this.actions.getAdminSupportTheme(id))
            .catch((error) => this.mutations.setError(error.response.data));

    }

    async getAdminSupportTheme(id) { // * Получение подробностей об одном филиале

        this.mutations.clearError();
        this.mutations.clearAdminSupportTheme();
        await Axios.get(
            baseUrl + "admin/" + id.toString(),

            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                },
            }
        )
            .then((response) => this.mutations.setAdminSupportTheme(response.data))
            .catch((error) => this.mutations.setError(error.response.data));
    }
}

const support = new Module({
    getters: SupportGetters,
    state: SupportState,
    mutations: SupportMutations,
    actions: SupportActions,
});
export const SupportMapper = createMapper(support);
export default support;
