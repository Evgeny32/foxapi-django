import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";
const baseUrl = process.env.VUE_APP_API_URL + "/api/v1/branch/";
const mediaUrl = process.env.VUE_APP_API_URL;
// const mediaUrl = "http://localhost:8000";

// Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
Axios.defaults.headers.post["Content-Type"] = "application/json";
// Axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";

class BranchState {
  error = null;
  // !ОТДЕЛ ФИЛИАЛА
  branchProfile = {}; // * Профиль филиала
  // !ОТДЕЛ ФИЛИАЛА
  // ! ОТДЕЛ МЕНТОРОВ
  mentors = [];
  mentorDetails = {
    id: 0,
    mentor: {
      id: 0,
      email: "",
      phone: "",
      second_name: "",
      name: "",
      last_name: "",
      role: "",
      avatar: null,
    },
    date_work: "",
    date_layoff: "",
    turnover: 0,
    job_status: "",
    passport: "",
    passport_who: "",
    passport_when: "",
    adress: "",
    passport_file: "",
    inn_file: "",
    snils_file: "",
    branch: 0,
  };
  mentorGroups = [];
  // ! ОТДЕЛ МЕНТОРОВ
  // ! ОТДЕЛ ДЕТЕЙ
  children = [];
  child = {
    id: 0,
    child: {
      id: 0,
      name: "",
      second_name: "",
      last_name: "",
    },
    age: 0,
    parent: {
      id: 0,
      name: "",
      second_name: "",
      last_name: "",
    },
    branch: "",
    mentor: {
      id: 0,
      name: "",
      second_name: "",
      last_name: "",
    },
    group: "",
    tale: "",
    rank: "",
  };
  taleLogs = [];
  tales = [];
  tale = {};
  filter = {
    program: "",
    level: "",
    status: "",
    tale: "",
    age: "",
    format: "",
    date: "",
    search: "",
  };
  // ! ОТДЕЛ ДЕТЕЙ
  supportMsgs = [];

  program = [];
}

class BranchGetters extends Getters {
  get groupCount() {
    if (this.state.mentors && this.state.mentors.length > 1) {
      let groupCount = 0;
      this.state.mentors.forEach((x) => (groupCount += x.groups));
      return groupCount;
    } else if (this.state.mentors && this.state.mentors.length == 1) {
      return this.state.mentors[0].groups;
    } else {
      return 0;
    }
  }
  get mentorCount() {
    return this.state.mentors && this.state.mentors.length
      ? this.state.mentors.length
      : 0;
  }
  get childCount() {
    return this.state.children && this.state.children.length
      ? this.state.children.length
      : 0;
  }
}

class BranchMutations extends Mutations {
  setError(error) {
    this.state.error = error;
  }
  clearError() {
    this.state.error = null;
  }
  // !ОТДЕЛ ФИЛИАЛА
  setBranchProfile(profile) {
    profile.registration_sertificate
      ? (profile.registration_sertificate =
          mediaUrl + profile.registration_sertificate)
      : "";
    profile.accounting_sertificate
      ? (profile.accounting_sertificate =
          mediaUrl + profile.accounting_sertificate)
      : "";
    profile.partner_form
      ? (profile.partner_form = mediaUrl + profile.partner_form)
      : "";
    console.log(profile);
    this.state.branchProfile = profile;
  }

  clearBranchProfile() {
    this.state.branchProfile = {};
  }
  // !ОТДЕЛ ФИЛИАЛА
  // !ОТДЕЛ МЕНТОРОВ
  setMentors(mentors) {
    mentors.forEach((mentor) => {
      mentor.passport_file = mediaUrl + mentor.passport_file;
      mentor.snils_file = mediaUrl + mentor.snils_file;
      mentor.inn_file = mediaUrl + mentor.inn_file;
    });
    this.state.mentors = mentors;
  }

  clearMentors() {
    this.state.mentors = [];
  }

  setMentorDetails(details) {
    Object.entries(details).forEach((i) => {
      if (i[0].indexOf("_file") != -1 && i[1]) {
        details[i[0]] = mediaUrl + i[1];
      }
    });
    this.state.mentorDetails = details;
  }

  clearMentorDetails() {
    this.state.mentorDetails = {
      id: 0,
      mentor: {
        id: 0,
        email: "",
        phone: "",
        second_name: "",
        name: "",
        last_name: "",
        role: "",
        avatar: null,
      },
      date_work: "",
      date_layoff: "",
      turnover: 0,
      job_status: "",
      passport: "",
      passport_who: "",
      passport_when: "",
      adress: "",
      passport_file: "",
      inn_file: "",
      snils_file: "",
      branch: 0,
    };
  }
  setMentorGroups(mentorGroups) {
    this.state.mentorGroups = mentorGroups;
  }

  clearMentorGroups() {
    this.state.mentorGroups = [];
  }
  // !ОТДЕЛ МЕНТОРОВ
  // !ОТДЕЛ ДЕТЕЙ

  setChildren(children) {
    this.state.children = children;
  }

  clearChildren() {
    this.state.children = [];
  }

  setTaleLogs(taleLogs) {
    this.state.taleLogs = taleLogs;
  }

  clearTaleLogs() {
    this.state.taleLogs = [];
  }

  setTales(tales) {
    this.state.tales = tales;
  }

  clearTales() {
    this.state.tales = [];
  }
  setTale(tale) {
    tale.tasks.forEach((task) => {
      task.answers.forEach((answer) => {
        answer.answer ? (answer.answer = mediaUrl + answer.answer) : null;
      });
    });
    this.state.tale = tale;
  }

  clearTale() {
    this.state.tale = {};
  }

  setChild(child) {
    this.state.child = child;
  }

  clearChild() {
    this.state.child = {
      id: 0,
      child: {
        id: 0,
        name: "",
        second_name: "",
        last_name: "",
      },
      age: 0,
      parent: {
        id: 0,
        name: "",
        second_name: "",
        last_name: "",
      },
      branch: "",
      mentor: {
        id: 0,
        name: "",
        second_name: "",
        last_name: "",
      },
      group: "",
      tale: "",
      rank: "",
    };
  }
  // !ОТДЕЛ ДЕТЕЙ

  setSupportMsgs(msgs) {
    this.state.supportMsgs = msgs;
  }

  clearSupportMsgs() {
    this.state.supportMsgs = [];
  }

  setProgram(program) {
    this.state.program = program;
  }

  clearProgram() {
    this.state.program = [];
  }
}

class BranchActions extends Actions {
  // ! ОТДЕЛ ПРОФИЛЯ ФИЛИАЛА
  async getBranchProfile() {
    // * Получение профиля филиала
    this.mutations.clearError();
    this.mutations.clearBranchProfile();
    await Axios.get(baseUrl, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setBranchProfile(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async postBranchProfile(data) {
    // * Создание профиля филиала
    this.mutations.clearError();
    this.mutations.clearBranchProfile();
    await Axios.post(baseUrl, data, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
        "Content-Type": "multipart/form-data",
      },
    })
      .then((response) => this.mutations.setBranchProfile(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchBranchProfile(data) {
    // * Изменение профиля филиала
    this.mutations.clearError();
    this.mutations.clearBranchProfile();
    await Axios.patch(baseUrl, data, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
        "Content-Type": "multipart/form-data",
      },
    })
      .then((response) => this.mutations.setBranchProfile(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  // ! ОТДЕЛ ПРОФИЛЯ ФИЛИАЛА

  // ! ОТДЕЛ МЕНТОРОВ
  async postMentorProfile(data) {
    // * Создание нового ментора
    this.mutations.clearError();
    await Axios.post(baseUrl + "mentor/", data, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
        "Content-Type": "multipart/form-data",
      },
    })
      .then(() => this.actions.getMentors())
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getMentors() {
    // * Получение списка менторов
    this.mutations.clearError();
    this.mutations.clearMentors();
    await Axios.get(baseUrl + "mentor/", {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setMentors(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchMentor({ id, data }) {
    // * Увольнение ментора
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}mentor/${id.toString()}/`,
      { ...data },
      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then(() => this.actions.getMentors(store.getters["AuthStore/token"]))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getMentorDetails(id) {
    // * Получение подробностей об одном менторе
    this.mutations.clearError();
    this.mutations.clearMentorDetails();
    await Axios.get(
      baseUrl + "mentor/" + id.toString(),

      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then((response) => this.mutations.setMentorDetails(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getMentorGroups(id) {
    // * Получение групп ментора
    this.mutations.clearError();
    this.mutations.clearMentorGroups();
    await Axios.get(`${baseUrl}mentor/${id}/group/`, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setMentorGroups(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  // ! ОТДЕЛ МЕНТОРОВ

  //  ! ОТДЕЛ ДЕТЕЙ

  async getChildren() {
    // * Получение списка детей
    this.mutations.clearError();
    this.mutations.clearChildren();
    await Axios.get(
      baseUrl + "child/",

      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then((response) => this.mutations.setChildren(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getChildInfo(id) {
    // * Получение подробнной информации об одном ребенке
    this.mutations.clearError();
    this.mutations.clearChild();
    await Axios.get(
      baseUrl + "child/" + id.toString(),

      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then((response) => this.mutations.setChild(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getChildLevelLogs(id) {
    // * Получение списка ежедневных логов ребенка
    this.mutations.clearError();
    this.mutations.clearTaleLogs();
    await Axios.get(`${baseUrl}child/${id}/tale_log/`, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setTaleLogs(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getChildTales(id) {
    // * Получение списка сказок ребенка
    this.mutations.clearError();
    this.mutations.clearTales();
    await Axios.get(
      `${baseUrl}child/${id}/tales/`,

      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then((response) => this.mutations.setTales(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getChildTale(id) {
    // * Получение списка сказок ребенка
    this.mutations.clearError();
    this.mutations.clearTale();
    await Axios.get(`${baseUrl}child/tales/${id}`, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setTale(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async banChild({ id, data }) {
    // * Бан/разбан ребенка
    this.mutations.clearError();
    await Axios.patch(
      baseUrl + "child/ban/" + id.toString() + "/",
      { ...data },
      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then(() => this.actions.getChildInfo(id))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  //  ! ОТДЕЛ ДЕТЕЙ

  async getPrograms(id) {
    this.mutations.clearError();
    this.mutations.clearProgram();
    await Axios.get(
      baseUrl + "program/" + id.toString(),

      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then((response) => this.mutations.setProgram(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async sendSupportMsg(payload) {
    this.mutations.clearError();
    await Axios.post(
      baseUrl + "support/",
      { ...payload.payload },
      {
        headers: {
          Authorization: "Token " + payload.token,
        },
      }
    )
      .then(() => this.actions.getSupportMsgs(payload.token))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getSupportMsgs(token) {
    this.mutations.clearError();
    this.mutations.clearSupportMsgs();
    await Axios.get(
      baseUrl + "support/",

      {
        headers: {
          Authorization: "Token " + token,
        },
      }
    )
      .then((response) => this.mutations.setSupportMsgs(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
}

const branch = new Module({
  getters: BranchGetters,
  state: BranchState,
  mutations: BranchMutations,
  actions: BranchActions,
});
export const BranchMapper = createMapper(branch);
export default branch;
