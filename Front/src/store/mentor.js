import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";
const baseUrl = process.env.VUE_APP_API_URL + "/api/v1/mentor/";
const mediaUrl = process.env.VUE_APP_API_URL;
// const mediaUrl = "http://localhost:8000";

// Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
Axios.defaults.headers.post["Content-Type"] = "application/json";
// Axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

class MentorState {
  // !ГРУППЫ
  groups = [];
  group = {};
  child = {};
  tales = [];
  tale = {};
  // !ГРУППЫ
  // !ТЕРМИНЫ
  terms = [];
  age = "1";
  // !ТЕРМИНЫ
  error = null;
}

class MentorGetters extends Getters {
  get requestTerms() {
    return this.state.terms.filter(
      (term) => !term.is_active && !term.deny_reason
    );
  }
  get requestTerms1() {
    return this.state.terms.filter(
      (term) =>
        !term.is_active && !term.deny_reason && term.age_category == "5-7"
    );
  }
  get requestTerms2() {
    return this.state.terms.filter(
      (term) =>
        !term.is_active && !term.deny_reason && term.age_category == "8-12"
    );
  }
  get requestTerms3() {
    return this.state.terms.filter(
      (term) =>
        !term.is_active && !term.deny_reason && term.age_category == "13-16"
    );
  }

  get activeTerms() {
    return this.state.terms.filter((term) => term.is_active);
  }
  get activeTerms1() {
    return this.state.terms.filter(
      (term) => term.is_active && term.age_category == "5-7"
    );
  }
  get activeTerms2() {
    return this.state.terms.filter(
      (term) => term.is_active && term.age_category == "8-12"
    );
  }
  get activeTerms3() {
    return this.state.terms.filter(
      (term) => term.is_active && term.age_category == "13-16"
    );
  }
}

class MentorMutations extends Mutations {
  // !ГРУППЫ
  setGroups(groups) {
    this.state.groups = groups;
  }
  clearGroups() {
    this.state.groups = [];
  }
  setGroup(group) {
    group.children.forEach((child) => {
      child.child.avatar
        ? (child.child.avatar = mediaUrl + child.child.avatar)
        : null;
    });
    group.requests.forEach((child) => {
      child.child.avatar
        ? (child.child.avatar = mediaUrl + child.child.avatar)
        : null;
    });
    this.state.group = group;
  }
  clearGroup() {
    this.state.group = {};
  }
  setChild(child) {
    child.child.avatar
      ? (child.child.avatar = mediaUrl + child.child.avatar)
      : null;

    this.state.child = child;
  }
  clearChild() {
    this.state.child = {};
  }
  setTales(tales) {
    this.state.tales = tales;
  }
  clearTales() {
    this.state.tales = [];
  }
  setTale(tale) {
    tale.tasks.forEach((task) => {
      task.answers.forEach((answer) => {
        answer.answer ? (answer.answer = mediaUrl + answer.answer) : null;
      });
    });
    this.state.tale = tale;
  }

  clearTale() {
    this.state.tale = {};
  }
  // !ГРУППЫ
  // !ТЕРМИНЫ
  setTerms(terms) {
    this.state.terms = terms;
  }
  clearTerms() {
    this.state.terms = [];
  }
  setAge(age) {
    this.state.age = age;
  }
  // !ТЕРМИНЫ
  setError(error) {
    this.state.error = error;
  }
  clearError() {
    this.state.error = null;
  }
}

class MentorActions extends Actions {
  async getGroups() {
    this.mutations.clearError();
    this.mutations.clearGroups();
    await Axios.get(baseUrl + "group/", {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setGroups(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getGroup(id) {
    this.mutations.clearError();
    this.mutations.clearGroup();
    await Axios.get(baseUrl + "group/" + id, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setGroup(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async postGroup( data) {
    this.mutations.clearError();
    this.mutations.clearGroups();
    await Axios.post(
      baseUrl + "group/",
      { ...data },
      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then(() => this.actions.getGroups())
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async patchGroup({ id, data }) {
    this.mutations.clearError();
    this.mutations.clearGroup();
    await Axios.patch(
      baseUrl + "group/" + id,
      { ...data },
      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then(() => this.actions.getGroup(id))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getChild(id) {
    this.mutations.clearError();
    this.mutations.clearChild();
    await Axios.get(baseUrl + "child/" + id, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setChild(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getTales(id) {
    this.mutations.clearError();
    this.mutations.clearTales();
    await Axios.get(baseUrl + "child/" + id + "/tales/", {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setTales(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getTale(id) {
    // * Получение списка сказок ребенка
    this.mutations.clearError();
    this.mutations.clearTale();
    await Axios.get(`${baseUrl}tale/${id}`, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setTale(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async patchTale(data) {
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}tale/${this.state.tale.id}/`,
      { ...data },
      {
        headers: { Authorization: "Token " + store.getters["AuthStore/token"] },
      }
    )
      .then(() => this.actions.getTale(this.state.tale.id))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchTask({ id, data }) {
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}task/${id}/`,
      { ...data },
      {
        headers: { Authorization: "Token " + store.getters["AuthStore/token"] },
      }
    )
      .then(() => this.actions.getTale(this.state.tale.id))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchAnswer({ id, data }) {
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}answer/${id}/`,
      { ...data },
      {
        headers: { Authorization: "Token " + store.getters["AuthStore/token"] },
      }
    )
      .then(() => this.actions.getTale(this.state.tale.id))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getTerms() {
    this.mutations.clearError();
    this.mutations.clearTerms();
    await Axios.get(baseUrl + "term/", {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setTerms(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async postTerm(data) {
    this.mutations.clearError();
    await Axios.post(
      `${baseUrl}term/`,
      { ...data },
      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then(() => this.actions.getTerms())
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async patchTerm({ id, data }) {
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}term/${id}/`,
      { ...data },
      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then(() => this.actions.getTerms())
      .catch((error) => this.mutations.setError(error.response.data));
  }
}

const mentor = new Module({
  getters: MentorGetters,
  state: MentorState,
  mutations: MentorMutations,
  actions: MentorActions,
});
export const MentorMapper = createMapper(mentor);
export default mentor;
