import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import { store } from "@/store";
import axios from "axios";

const baseUrl = process.env.VUE_APP_API_URL + "/api/v1/clientmanager/";
// const mediaUrl = process.env.VUE_APP_API_URL;
axios.defaults.headers.post["Content-Type"] = "application/json";

class ClientManagerState {
  clientManagerId= null;
  franchisees = [];
  report = [];
  packages = [];
  franchisee = {};
  agents = [];
  agent = {};
  parents = [];
  parent = {};
  mentors = [];
  mentor = {};
  error = null;
  token = null;
}

class ClientManagerGetters extends Getters {}

class ClientManagerMutations extends Mutations {
  setClientManagerId(id){
    this.state.clientManagerId = id;
  }

  setFranchisees(franchisees) {
    this.state.franchisees = franchisees;
  }

  clearFranchisees() {
    this.state.franchisees = [];
  }

  setReport(report) {
    this.state.report = report;
  }

  clearReport() {
    this.state.report = [];
  }

  setPackages(packages) {
    this.state.packages = packages
  }

  clearPackages() {
    this.state.packages = []
  }

  setFranchisee(franchisee) {
    this.state.franchisee = franchisee;
  }

  clearFranchisee() {
    this.state.franchisee = {};
  }

  setAgents(agents) {
    this.state.agents = agents;
  }

  clearAgents() {
    this.state.agents = [];
  }

  setAgent(agent) {
    this.state.agent = agent;
  }

  clearAgent() {
    this.state.agent = {};
  }

  setParents(parents) {
    this.state.parents = parents;
  }

  clearParents() {
    this.state.parents = [];
  }

  setParent(parent) {
    this.state.parent = parent;
  }

  clearParent() {
    this.state.parent = {};
  }

  setMentors(mentors) {
    this.state.mentors = mentors;
  }

  clearMentors() {
    this.state.mentors = [];
  }

  setMentor(mentor) {
    this.state.mentor = mentor;
  }

  clearMentor() {
    this.state.mentor = {};
  }

  setError(error) {
    this.state.error = error;
  }

  clearError() {
    this.state.error = null;
  }

  setToken(token) {
    this.state.token = token;
  }

  clearToken() {
    this.state.token = null;
  }
}

class ClientManagerActions extends Actions {
  //FRANCHISEE

  async getFranchiseeList(id) {
    this.mutations.clearError();
    this.mutations.clearFranchisees();
    await axios
      .get(baseUrl + "franchisees/" + id.toString(), {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setFranchisees(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getClientManagerReport(id) {
    this.mutations.clearError();
    this.mutations.clearReport();
    await axios
      .get(baseUrl + "franchisees/all/" + id.toString(), {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setReport(response.data))
      .catch(() => this.mutations.setError(true));
  }


  async getFranchiseePackages(id) {
    this.mutations.clearError();
    this.mutations.clearPackages();
    await axios
      .get(baseUrl + "franchisees/packages/" + id.toString(), {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setPackages(response.data))
      .catch(() => this.mutations.setError(true));
  }


  async getFranchiseeListById(id) {
    this.mutations.clearError();
    this.mutations.clearFranchisees();
    await axios
      .get(baseUrl + id + "/franchisees/", {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setFranchisees(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getClientFranchiseeDetails(id) {
    // * Получение подробнной информации об одном ребенке
    this.mutations.clearError();
    this.mutations.clearFranchisee();
    await axios
      .get(
        baseUrl + "franchisee/" + id.toString(),

        {
          headers: {
            Authorization: "Token " + store.getters["AuthStore/token"],
          },
        }
      )
      .then((response) => this.mutations.setFranchisee(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async putFranchiseeProfile({ id, data }) {
    this.mutations.clearError();
    await axios
      .put(baseUrl + "franchisee/" + id.toString() + "/", data, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .catch((error) => this.mutations.setError(error.response.data));
  }

  //FRANCHISEE

  //AGENTS

  async getAgentsList(id) {
    this.mutations.clearError();
    this.mutations.clearAgents();
    await axios
      .get(baseUrl + "agents/" + id.toString(), {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setAgents(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getAgentsListById(id) {
    this.mutations.clearError();
    this.mutations.clearAgents();
    await axios
      .get(baseUrl + id + "/agents/", {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setAgents(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getAgentDetails(id) {
    // * Получение подробнной информации об одном ребенке
    this.mutations.clearError();
    this.mutations.clearAgent();
    await axios
      .get(
        baseUrl + "agent/" + id.toString(),

        {
          headers: {
            Authorization: "Token " + store.getters["AuthStore/token"],
          },
        }
      )
      .then((response) => this.mutations.setAgent(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchAgentProfile({ id, data }) {
    this.mutations.clearError();
    await axios
      .patch(`${baseUrl}agent/${id.toString()}/`, data, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
          "Content-Type": "multipart/form-data",
        },
      })
      .then((response) => this.mutations.setAgent(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  //AGENTS

  //PARENTS

  async getParentsList(id) {
    this.mutations.clearError();
    this.mutations.clearParents();
    await axios
      .get(baseUrl + "parents/" + id.toString(), {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setParents(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getParentsListById(id) {
    this.mutations.clearError();
    this.mutations.clearParents();
    await axios
      .get(baseUrl + id + "/parents/", {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setParents(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getParentDetails(id) {
    // * Получение подробнной информации об одном ребенке
    this.mutations.clearError();
    this.mutations.clearParent();
    await axios
      .get(
        baseUrl + "parent/" + id.toString(),

        {
          headers: {
            Authorization: "Token " + store.getters["AuthStore/token"],
          },
        }
      )
      .then((response) => this.mutations.setParent(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  //PARENTS

  //MENTORS

  async getMentorsList(id) {
    this.mutations.clearError();
    this.mutations.clearMentors();
    await axios
      .get(baseUrl + "mentors/" + id.toString(), {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setMentors(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getMentorsListById(id) {
    this.mutations.clearError();
    this.mutations.clearMentors();
    await axios
      .get(baseUrl + id + "/mentors/", {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setMentors(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getMentorProfile(id) {
    // * Получение подробнной информации об одном ребенке
    this.mutations.clearError();
    this.mutations.clearMentor();
    await axios
      .get(
        baseUrl + "mentor/" + id.toString(),

        {
          headers: {
            Authorization: "Token " + store.getters["AuthStore/token"],
          },
        }
      )
      .then((response) => this.mutations.setMentor(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchMentorProfile({ id, data }) {
    this.mutations.clearError();
    await axios
      .patch(`${baseUrl}mentor/${id.toString()}/`, data, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setMentor(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  //MENTORS
}

const clientManager = new Module({
  getters: ClientManagerGetters,
  state: ClientManagerState,
  mutations: ClientManagerMutations,
  actions: ClientManagerActions,
});
export const ClientManagerMapper = createMapper(clientManager);
export default clientManager;
