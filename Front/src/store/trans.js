import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";

class TransState {
  error = null;
  transaction = null;
  childFillId = null;
  postTransactionData = null;
}

class TransGetters extends Getters {}

class TransMutations extends Mutations {
  setError(error) {
    this.state.error = error;
  }
  setTransaction(trans) {
    this.state.transaction = trans;
  }
  setChildFillId(id) {
    this.state.childFillId = id;
  }
}

class TransActions extends Actions {}

const trans = new Module({
  getters: TransGetters,
  state: TransState,
  mutations: TransMutations,
  actions: TransActions,
});
export const TransMapper = createMapper(trans);
export default trans;
