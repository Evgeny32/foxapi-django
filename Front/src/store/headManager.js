import {
    Getters,
    Mutations,
    Actions,
    Module,
    createMapper,
} from "vuex-smart-module";
import { store } from "@/store";
import axios from "axios";

const baseUrl = process.env.VUE_APP_API_URL + "/api/v1/headsalesmanager/";
// const mediaUrl = process.env.VUE_APP_API_URL;
axios.defaults.headers.post["Content-Type"] = "application/json";

class HeadManagerState {
  
    managers = [];
    selectManagers = [];
    manager = {};
    franchisees = [];
    franchisee = {};
    packages = [];
    agents = [];
    agent = {};
    parents = [];
    parent = {};
    mentors = [];
    mentor = {};
    error = null;
    token = null;
}

class HeadManagerGetters extends Getters {
   
}

class HeadManagerMutations extends Mutations {
  
    setManagers(managers) {
        this.state.managers = managers;
    }

    clearManagers() {
        this.state.managers = [];
    } 

    setSelectManagers(managers) {
        this.state.selectManagers = managers;
    }

    clearSelectManagers() {
        this.state.selectManagers = [];
    } 

    setManager(manager) {
        this.state.manager = manager;
    }

    clearManager() {
        this.state.manager = {};
    } 

    setFranchisees(franchisees) {
        this.state.franchisees = franchisees;
    }

    clearFranchisees() {
        this.state.franchisees = [];
    } 

    setPackages(packages) {
        this.state.packages = packages;
    }

    clearPackages() {
        this.state.packages = [];
    } 


    setFranchisee(franchisee) {
        this.state.franchisee = franchisee;
    }

    clearFranchisee() {
        this.state.franchisee = {};
    } 

    setAgents(agents) {
        this.state.agents = agents;
    }

    clearAgents() {
        this.state.agents = [];
    } 

    setAgent(agent) {
        this.state.agent= agent;
    }

    clearAgent() {
        this.state.agent = {};
    } 

    setParents(parents) {
        this.state.parents = parents;
    }

    clearParents() {
        this.state.parents = [];
    } 

    setParent(parent) {
        this.state.parent = parent;
    }

    clearParent() {
        this.state.parent = {};
    } 

    setMentors(mentors) {
        this.state.mentors = mentors;
    }

    clearMentors() {
        this.state.mentors = [];
    } 

    setMentor(mentor) {
        this.state.mentor = mentor;
    }

    clearMentor() {
        this.state.mentor = {};
    }  

    setError(error) {
        this.state.error = error;
    }

    clearError() {
        this.state.error = null;
    }

    setToken(token) {
        this.state.token = token;
    }

    clearToken() {
        this.state.token = null;
    }

}

class HeadManagerActions extends Actions {
   
// MANAGERS

    async getManagers() {
        this.mutations.clearError()
        this.mutations.clearManagers()
        await axios.get(`${baseUrl}`,
            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                }
            }).then(response => this.mutations.setManagers(response.data))
            .catch(error => this.mutations.setError(error.response.data))
    }

    async postManager(manager) {
        this.mutations.clearError()
        await axios.post(`${baseUrl}`, manager,
            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                }
            }).then(() => this.actions.getManagers())
            .catch(error => this.mutations.setError(error.response.data))
    }

    async getManager(id) {
        this.mutations.clearError()
        this.mutations.clearManager()
        await axios.get(`${baseUrl}manager/` + id.toString(),
            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                }
            }).then(response => this.mutations.setManager(response.data))
            .catch(error => this.mutations.setError(error.response.data))
    }


    async getSelectionManagers() {
        this.mutations.clearError()
        this.mutations.clearSelectManagers()
        await axios.get(`${baseUrl}`,
            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                }
            }).then(response => this.mutations.setSelectManagers(response.data))
            .catch(error => this.mutations.setError(error.response.data))
    }


    async SelectManager(data) {
        this.mutations.clearError()
        await axios.patch(`${baseUrl}selectmanager/`, data,
            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                }
            }).then(() => this.actions.getSelectionManagers())
            .catch(error => this.mutations.setError(error.response.data))
    }
  

//MANAGERS

//FRANCHISEE

async getFranchiseeList() {
    this.mutations.clearError();
    this.mutations.clearFranchisees();
    await axios.get(baseUrl + "franchisees/", {
        headers: {
            Authorization: "Token " + store.getters["AuthStore/token"],
        },
    })
        .then((response) => this.mutations.setFranchisees(response.data))
        .catch(() => this.mutations.setError(true));
}

async getFranchiseeDetails(id) {
    // * Получение подробнной информации об одном ребенке
    this.mutations.clearError();
    this.mutations.clearFranchisee();
    await axios.get(
        baseUrl + "franchisee/" + id.toString(),

        {
            headers: {
                Authorization: "Token " + store.getters["AuthStore/token"],
            },
        }
    )
        .then((response) => this.mutations.setFranchisee(response.data))
        .catch((error) => this.mutations.setError(error.response.data));
}

async getPackages() {
    // * Получение списка франшиз
    this.mutations.clearError();
    this.mutations.clearPackages();
    await axios
      .get(`${baseUrl}packages/`, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setPackages(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

//FRANCHISEE

//AGENTS

async getAgentList() {
    this.mutations.clearError();
    this.mutations.clearAgents();
    await axios.get(baseUrl + "agents/", {
        headers: {
            Authorization: "Token " + store.getters["AuthStore/token"],
        },
    })
        .then((response) => this.mutations.setAgents(response.data))
        .catch(() => this.mutations.setError(true));
}

async getAgentDetails(id) {
    // * Получение подробнной информации об одном ребенке
    this.mutations.clearError();
    this.mutations.clearAgent();
    await axios.get(
        baseUrl + "agent/" + id.toString(),

        {
            headers: {
                Authorization: "Token " + store.getters["AuthStore/token"],
            },
        }
    )
        .then((response) => this.mutations.setAgent(response.data))
        .catch((error) => this.mutations.setError(error.response.data));
}

//AGENTS

//PARENTS

async getParentsList() {
    this.mutations.clearError();
    this.mutations.clearParents();
    await axios.get(baseUrl + "parents/", {
        headers: {
            Authorization: "Token " + store.getters["AuthStore/token"],
        },
    })
        .then((response) => this.mutations.setParents(response.data))
        .catch(() => this.mutations.setError(true));
}

async getParentDetails(id) {
    // * Получение подробнной информации об одном ребенке
    this.mutations.clearError();
    this.mutations.clearParent();
    await axios.get(
        baseUrl + "parent/" + id.toString(),

        {
            headers: {
                Authorization: "Token " + store.getters["AuthStore/token"],
            },
        }
    )
        .then((response) => this.mutations.setParent(response.data))
        .catch((error) => this.mutations.setError(error.response.data));
}

//PARENTS

//MENTORS

async getMentorList() {
    this.mutations.clearError();
    this.mutations.clearAgents();
    await axios.get(baseUrl + "mentors/", {
        headers: {
            Authorization: "Token " + store.getters["AuthStore/token"],
        },
    })
        .then((response) => this.mutations.setMentors(response.data))
        .catch(() => this.mutations.setError(true));
}

async getMentorDetails(id) {
    // * Получение подробнной информации об одном ребенке
    this.mutations.clearError();
    this.mutations.clearMentor();
    await axios.get(
        baseUrl + "mentor/" + id.toString(),

        {
            headers: {
                Authorization: "Token " + store.getters["AuthStore/token"],
            },
        }
    )
        .then((response) => this.mutations.setMentor(response.data))
        .catch((error) => this.mutations.setError(error.response.data));
}

//MENTORS

    

}

const headManager = new Module({
    getters: HeadManagerGetters,
    state: HeadManagerState,
    mutations: HeadManagerMutations,
    actions: HeadManagerActions,
});
export const HeadManagerMapper = createMapper(headManager);
export default headManager;
