import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";

const base_url = process.env.VUE_APP_API_URL + "/api/v1/mentor/terms";
// const media_url = process.env.VUE_APP_API_URL
Axios.defaults.headers.post["Content-Type"] = "application/json";

class DictState {
  ageCategories = null;
  ageCategory = null;
  terms = null;
  notModTerms = null;
  term = null;
}

class DictMutations extends Mutations {
  setError(error) {
    this.state.error = error;
  }
  clearError() {
    this.state.error = null;
  }

  setAgeCategories(payload) {
    this.state.ageCategories = payload;
  }
  clearAgeCategories() {
    this.state.ageCategories = null;
  }

  setAgeCategory(category) {
    this.state.ageCategory = category;
  }
  clearAgeCategory() {
    this.state.ageCategory = null;
  }

  setTerms(terms) {
    this.state.terms = terms;
  }
  clearTerms() {
    this.state.terms = null;
  }

  setNotModTerms(notModTerms) {
    this.state.notModTerms = notModTerms;
  }
  clearNotModTerms() {
    this.state.notModTerms = null;
  }

  setTerm(term) {
    this.state.term = term;
  }
  clearTerm() {
    this.state.term = null;
  }

  setTermReason(reason) {
    this.state.term["deny_reason"] = reason;
  }
  clearTermReason() {
    this.state.term["deny_reason"] = null;
  }
}

class DictGetters extends Getters { }

class DictActions extends Actions {
  async getTermsAgeCategory(token) {
    this.mutations.clearError();
    this.mutations.clearAgeCategories();
    try {
      const response = await Axios.get(base_url + "/age/", {
        headers: {
          Authorization: "Token " + token,
        },
      });
      if (response.status == 200) {
        this.mutations.setAgeCategories(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getTermsByAge(token) {
    this.mutations.clearError();
    this.mutations.clearTerms();
    try {
      const response = await Axios.get(
        base_url + "/" + this.state.ageCategory + "/",
        {
          headers: {
            Authorization: "Token " + token,
          },
        }
      );
      if (response.status == 200) {
        this.mutations.setTerms(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getNotModTermsByAge(token) {
    this.mutations.clearError();
    this.mutations.clearNotModTerms();
    try {
      const response = await Axios.get(
        base_url + "/" + this.state.ageCategory + "/moderate/",
        {
          headers: {
            Authorization: "Token " + token,
          },
        }
      );
      if (response.status == 200) {
        this.mutations.setNotModTerms(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async putTermDeny(payload) {
    this.mutations.clearError();
    try {
      const response = await Axios.put(
        base_url + "/deny/" + this.state.term["id"] + "/",
        { ...payload },
        {
          headers: {
            Authorization: "Token " + payload.token,
          },
        }
      );
      if (response.status == 200) {
        this.actions.getNotModTermsByAge(payload.token);
        this.actions.getTermsByAge(payload.token);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async putTermAccept(payload) {
    this.mutations.clearError();
    try {
      const response = await Axios.put(
        base_url + "/accept/" + this.state.term["id"] + "/",
        { ...payload },
        {
          headers: {
            Authorization: "Token " + payload.token,
          },
        }
      );
      if (response.status == 200) {
        this.actions.getNotModTermsByAge(payload.token);
        this.actions.getTermsByAge(payload.token);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async postTerm(payload) {
    this.mutations.clearError();
    try {
      const response = await Axios.post(
        base_url + "/",
        { ...payload },
        {
          headers: {
            Authorization: "Token " + payload.token,
          },
        }
      );
      if (response.status == 201) {
        this.actions.getTermsAgeCategory(payload.token);
        this.actions.getTermsByAge(payload.token);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
}

const dict = new Module({
  state: DictState,
  getters: DictGetters,
  mutations: DictMutations,
  actions: DictActions,
});
export const DictMapper = createMapper(dict);
export default dict;
