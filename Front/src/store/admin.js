import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";
import axios from "axios";

const baseUrl = process.env.VUE_APP_API_URL + "/api/v1/administration/";
const mediaUrl = process.env.VUE_APP_API_URL;
Axios.defaults.headers.post["Content-Type"] = "application/json";

class AdminState {
  // * CRM
  packages = [];
  package = {};
  agentCount = 0;
  franchiseeCount = 0;
  childCount = 0;
  packageCount = 0;
  // *CRM
  // *CRM Agent
  agentProfile = { user_id: {} };
  // *CRM Agent
  // * Администрация
  adminUsers = [];
  calcData = {
    kagent_LK_cost: 0,
    kagent_franch_online_procent: 0,
    kagent_franch_offline_procent: 0,
    kagent_procent: 0,
    mentor_LK_online: 0,
    mentor_LK_offline: 0,
    mentor_coach_procent: 0,
  };
  // * Администрация

  adminProfile = {};
  children = [];
  branches = [];
  child = {};
  agents = [];
  franchisee = [];
  franchiseeDetails = {};

  error = null;
  token = null;
}

class AdminGetters extends Getters {
  get allChildCount() {
    return this.state.branches.length > 1
      ? this.state.branches.map((x) => x.children).reduce((x, y) => x + y)
      : this.state.branches.length
      ? this.state.branches[0].children
      : 0;
  }
  get allGroupCount() {
    return this.state.branches.length > 1
      ? this.state.branches.map((x) => x.groups).reduce((x, y) => x + y)
      : this.state.branches.length
      ? this.state.branches[0].groups
      : 0;
  }
  get allMentorCount() {
    return this.state.branches.length > 1
      ? this.state.branches.map((x) => x.mentors).reduce((x, y) => x + y)
      : this.state.branches.length
      ? this.state.branches[0].children
      : 0;
  }
  get allBranchCount() {
    return this.state.branches.length ? this.state.branches.length : 0;
  }

  get lastPackage() {
    return this.state.packages.length
      ? this.state.packages[this.state.packages.length - 1]
      : null;
  }
}

class AdminMutations extends Mutations {
  // * CRM

  setPackages(packages) {
    packages = packages.map((pack) => {
      if (pack.franchisee) {
        Object.entries(pack.franchisee).forEach((item) => {
          if (item && item[0].indexOf("PDF") > 0 && item[1])
            pack.franchisee[item[0]] = mediaUrl + item[1];
        });
      }
      if (pack.agent) {
        Object.entries(pack.agent).forEach((item) => {
          if (item && item[0].indexOf("PDF") > 0 && item[1])
            pack.agent[item[0]] = mediaUrl + item[1];
        });
      }
      return pack;
    });
    this.state.packages = packages;
  }

  clearPackages() {
    this.state.packages = [];
  }

  setPackage(pack) {
    this.state.package = pack;
  }

  clearPackage() {
    this.state.package = {};
  }
  setAgentCount(count) {
    this.state.agentCount = count;
  }
  clearAgentCount() {
    this.state.agentCount = 0;
  }
  setFranchiseeCount(count) {
    this.state.franchiseeCount = count;
  }
  clearFranchiseeCount() {
    this.state.franchiseeCount = 0;
  }
  setChildCount(count) {
    this.state.childCount = count;
  }
  clearChildCount() {
    this.state.childCount = 0;
  }
  setPackageCount(count) {
    this.state.packageCount = count;
  }
  clearPackageCount() {
    this.state.packageCount = 0;
  }
  // * CRM
  // *CRM Agent
  clearAgentProfile() {
    this.state.agentProfile = { user_id: {} };
  }
  setAgentProfile(prof) {
    this.state.agentProfile = prof;
  }
  // * CRM Agent
  // * Администрация
  setAdminUsers(users) {
    this.state.adminUsers = users;
  }
  clearAdminUsers() {
    this.state.adminUsers = [];
  }
  setCalcData(data) {
    this.state.calcData = data;
  }
  clearCalcData() {
    this.state.calcData = {
      kagent_LK_cost: 0,
      kagent_franch_online_procent: 0,
      kagent_franch_offline_procent: 0,
      kagent_procent: 0,
      mentor_LK_online: 0,
      mentor_LK_offline: 0,
      mentor_coach_procent: 0,
    };
  }
  // * Администрация
  setAdminProfile(profile) {
    this.state.adminProfile = profile;
  }

  clearAdminProfile() {
    this.state.adminProfile = {};
  }
  setBranches(branches) {
    this.state.branches = branches;
  }

  clearBranches() {
    this.state.branches = [];
  }

  setChildren(children) {
    this.state.children = children;
  }

  clearChildren() {
    this.state.children = [];
  }

  setChild(child) {
    this.state.child = child;
  }

  clearChild() {
    this.state.child = [];
  }

  setAgents(agents) {
    this.state.agents = agents;
  }

  clearAgents() {
    this.state.agents = [];
  }

  setFranchisee(franchisee) {
    this.state.franchisee = franchisee;
  }

  clearFranchisee() {
    this.state.franchisee = [];
  }

  setFranchiseeDetails(details) {
    this.state.franchiseeDetails = details;
  }

  clearFranchiseeDetails() {
    this.state.franchiseeDetails = {};
  }

  setError(error) {
    this.state.error = error;
  }

  clearError() {
    this.state.error = null;
  }

  setToken(token) {
    this.state.token = token;
  }

  clearToken() {
    this.state.token = null;
  }
}

class AdminActions extends Actions {
  // * CRM
  async getAgentFranchiseeCount() {
    this.mutations.clearError();
    this.mutations.clearAgentCount();
    this.mutations.clearFranchiseeCount();
    await Axios.get(`${baseUrl}all/`, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => {
        this.mutations.setAgentCount(response.data["agents"]);
        this.mutations.setFranchiseeCount(response.data["franchisee"]);
      })
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getChildPackageCount() {
    this.mutations.clearError();
    this.mutations.clearChildCount();
    this.mutations.clearPackageCount();
    await Axios.get(`${baseUrl}child-package/`, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => {
        this.mutations.setChildCount(response.data["children"]);
        this.mutations.setPackageCount(response.data["packages"]);
      })
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async blockFranchiseePackage({ id, data }) {
    // * Блокирование франшизы
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}package/${id.toString()}/block/`,
      { ...data },
      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    ).catch((error) => this.mutations.setError(error.response.data));
  }
  async getPackages() {
    // * Получение списка франшиз
    this.mutations.clearError();
    this.mutations.clearPackages();
    await Axios.get(`${baseUrl}packages/`, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setPackages(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getOtherPackages(id) {
    // * Получение списка франшиз
    this.mutations.clearError();
    this.mutations.clearPackages();
    await Axios.get(`${baseUrl}packages/franchisee/${id}`, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setPackages(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async patchPackage({ id, data }) {
    // * Изменение франшизы
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}packages/${id.toString()}/`,
      { ...data },
      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then(() => this.actions.getPackages())
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchChildPackage({ id, data }) {
    this.mutations.clearError();
    await Axios.patch(`${baseUrl}childpackages/${id.toString()}/`, data, {
      headers: {
        // Authorization: "Token " + store.getters["AuthStore/token"],
        "Content-Type": "multipart/form-data; charset=utf-8",
      },
    })
      .then(() => this.actions.getPackages())
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getPackage(id) {
    // * Получение подробностей об одной франшизе
    this.mutations.clearError();
    this.mutations.clearPackage();
    await Axios.get(
      `${baseUrl}package/${id.toString()}`,

      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then((response) => this.mutations.setPackage(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  // * CRM
  // *CRM Franchisee
  async getFranchiseeDetails(id) {
    // * Получение подробностей об одном филиале

    this.mutations.clearError();
    this.mutations.clearFranchiseeDetails();
    await Axios.get(
      `${baseUrl}franchisee/${id.toString()}`,

      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then((response) => this.mutations.setFranchiseeDetails(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async putFranchiseeDetails({ id, data }) {
    this.mutations.clearError();
    await Axios.patch(`${baseUrl}franchisee/${id.toString()}/`, data, {
      headers: {
        // Authorization: "Token " + store.getters["AuthStore/token"],
        "Content-Type": "multipart/form-data",
      },
    })
      .then((response) => this.mutations.setFranchiseeDetails(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  // *CRM Franchisee
  // * CRM Agent
  async getAgentProfile(id) {
    // * Получение подробностей об одном филиале

    this.mutations.clearError();
    this.mutations.clearAgentProfile();
    await Axios.get(`${baseUrl}agent/${id.toString()}`, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setAgentProfile(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async patchAgentProfile({ id, data }) {
    this.mutations.clearError();
    await Axios.patch(`${baseUrl}agent/${id.toString()}/`, data, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
        "Content-Type": "multipart/form-data",
      },
    })
      .then((response) => this.mutations.setAgentProfile(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  // * CRM Agent
  // * Администрация
  async getAdminUsers() {
    this.mutations.clearError();
    this.mutations.clearAdminUsers();
    await axios
      .get(`${baseUrl}user/`, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setAdminUsers(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async postUser(user) {
    this.mutations.clearError();
    await axios
      .post(`${baseUrl}user/`, user, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then(() => this.actions.getAdminUsers())
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getCalcData() {
    this.mutations.clearError();
    this.mutations.clearCalcData();
    await axios
      .get(`${baseUrl}calc/`, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setCalcData(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async patchCalcData(data) {
    this.mutations.clearError();
    this.mutations.clearCalcData();
    await axios
      .patch(`${baseUrl}calc/`, data, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setCalcData(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  // * Администрация

  async getAgents() {
    // * Получение списка филиалов по токену
    this.mutations.clearError();
    this.mutations.clearAgents();
    await Axios.get(baseUrl + "agents/", {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setAgents(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getFranchisee() {
    // * Получение списка филиалов по токену
    this.mutations.clearError();
    this.mutations.clearFranchisee();
    await Axios.get(baseUrl + "franchisee/", {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setFranchisee(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  // * Филиалы
  async getBranches() {
    // * Получение списка филиалов по токену
    this.mutations.clearError();
    this.mutations.clearBranches();
    await Axios.get(baseUrl + "branches/", {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setBranches(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getOtherBranches(id) {
    // * Получение списка филиалов по токену
    this.mutations.clearError();
    this.mutations.clearBranches();
    await Axios.get(baseUrl + "branches/" + id, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setBranches(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async postBranch(data) {
    // * Создание филиала
    this.mutations.clearError();
    await Axios.post(baseUrl + "branch/", data, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
        "Content-Type": "multipart/form-data",
      },
    })
      .then(() => this.actions.getBranches())
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getBranch(id) {
    // * Получение подробностей об одном филиале

    this.mutations.clearError();
    this.mutations.clearBranch();
    await Axios.get(
      baseUrl + "branch/" + id.toString(),

      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then((response) => this.mutations.setBranch(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchBranch({ id, data }) {
    // * Изменение филиала
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}branch/${id.toString()}/`,
      { ...data },
      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then(() => this.actions.getBranches())
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async banBranch({ id, data }) {
    // * Бан филиала
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}branch/${id.toString()}/ban/`,
      { ...data },
      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then(() => this.actions.getBranches())
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async makeMainBranch({ id, data }) {
    // * Бан филиала
    this.mutations.clearError();
    await Axios.patch(`${baseUrl}branch/main/${id.toString()}/`, data, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then(() => this.actions.getBranches())
      .catch((error) => this.mutations.setError(error.response.data));
  }

  // * Филиалы
  async getAdminProfile() {
    this.mutations.clearError();
    this.mutations.clearAdminProfile();
    await Axios.get(baseUrl, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setAdminProfile(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async postAdminProfile(data) {
    this.mutations.clearError();
    this.mutations.clearAdminProfile();
    await Axios.post(baseUrl, data, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
        "Content-Type": "multipart/form-data",
      },
    })
      .then((response) => this.mutations.setAdminProfile(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchAdminProfile(data) {
    this.mutations.clearError();
    this.mutations.clearAdminProfile();
    await Axios.patch(baseUrl, data, {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
        "Content-Type": "multipart/form-data",
      },
    })
      .then((response) => this.mutations.setAdminProfile(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getChildrenList() {
    this.mutations.clearError();
    this.mutations.clearChildren();
    await Axios.get(baseUrl + "children/", {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setChildren(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getChildDetails(id) {
    // * Получение подробнной информации об одном ребенке
    this.mutations.clearError();
    this.mutations.clearChild();
    await Axios.get(
      baseUrl + "child/" + id.toString(),

      {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      }
    )
      .then((response) => this.mutations.setChild(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  // async getChildLevelLogs(id) {
  //     // * Получение списка ежедневных логов ребенка
  //     this.mutations.clearError();
  //     this.mutations.clearTaleLogs();
  //     await Axios.get(`${baseUrl}child/${id}/tale_log/`, {
  //       headers: {
  //         Authorization: "Token " + store.getters["AuthStore/token"],
  //       },
  //     })
  //       .then((response) => this.mutations.setTaleLogs(response.data))
  //       .catch((error) => this.mutations.setError(error.response.data));
  //   }

  //   async getChildTales(id) {
  //     // * Получение списка сказок ребенка
  //     this.mutations.clearError();
  //     this.mutations.clearTales();
  //     await Axios.get(
  //       `${baseUrl}child/${id}/tales/`,

  //       {
  //         headers: {
  //           Authorization: "Token " + store.getters["AuthStore/token"],
  //         },
  //       }
  //     )
  //       .then((response) => this.mutations.setTales(response.data))
  //       .catch((error) => this.mutations.setError(error.response.data));
  //   }
  //   async getChildTale(id) {
  //     // * Получение списка сказок ребенка
  //     this.mutations.clearError();
  //     this.mutations.clearTale();
  //     await Axios.get(`${baseUrl}child/tales/${id}`, {
  //       headers: {
  //         Authorization: "Token " + store.getters["AuthStore/token"],
  //       },
  //     })
  //       .then((response) => this.mutations.setTale(response.data))
  //       .catch((error) => this.mutations.setError(error.response.data));
  //   }

  //   async banChild({ id, ban }) {
  //     // * Бан/разбан ребенка
  //     this.mutations.clearError();
  //     await Axios.patch(
  //       baseUrl + "child/ban/" + id.toString() + "/",
  //       { ban: ban },
  //       {
  //         headers: {
  //           Authorization: "Token " + store.getters["AuthStore/token"],
  //         },
  //       }
  //     )
  //       .then(() => this.actions.getChildInfo(id))
  //       .catch((error) => this.mutations.setError(error.response.data));
  //   }
}

const admin = new Module({
  getters: AdminGetters,
  state: AdminState,
  mutations: AdminMutations,
  actions: AdminActions,
});
export const AdminMapper = createMapper(admin);
export default admin;
