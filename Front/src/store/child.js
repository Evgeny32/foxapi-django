import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from '@/store';

const base_url = process.env.VUE_APP_API_URL + "/api/v1/child";
const mediaUrl = process.env.VUE_APP_API_URL;

Axios.defaults.headers.post["Content-Type"] = "application/json";

class ChildState {
  children = null;
  profile = null;
  error = null;
  kid = null;
  mediaUrl = process.env.VUE_APP_API_URL;
  terms = null;
  avatar = null;
  term = null;
  mentors = null;
  groups = null;
  mentor = null;
  group = null;
}

class ChildGetters extends Getters { }

class ChildMutations extends Mutations {
  clearError() {
    this.state.error = null;
  }
  setError(error) {
    this.state.error = error;
  }

  clearChildren() {
    this.state.children = null;
  }
  setChildren(payload) {
    this.state.children = payload;
  }

  clearChild() {
    this.state.kid = null;
  }
  setChild(kid) {
    this.state.kid = kid;
  }

  clearChildName() {
    this.state.kid.child["name"] = null;
    this.state.kid.child["second_name"] = null;
    this.state.kid.child["last_name"] = null;
    this.state.kid["slogan"] = null;
  }
  setChildName(kid) {
    this.state.kid.child["name"] = kid["name"];
    this.state.kid.child["second_name"] = kid["second_name"];
    this.state.kid.child["last_name"] = kid["last_name"];
    this.state.kid["slogan"] = kid["slogan"];
  }

  clearTerms() {
    this.state.terms = null;
  }
  setTerms(terms) {
    this.state.terms = terms;
  }

  clearAvatar() {
    this.state.avatar = null;
  }
  setAvatar(avatar) {
    this.state.avatar = avatar;
  }

  clearProfile() {
    this.state.profile = null;
  }
  setProfile(profile) {
    profile.child.avatar = profile.child.avatar ? mediaUrl + profile.child.avatar : ''
    this.state.profile = profile;
  }

  clearSlogan() {
    this.state.profile.slogan = null;
  }
  setSlogan(slogan) {
    this.state.profile.slogan = slogan;
  }

  clearTerm() {
    this.state.term = null;
  }
  setTerm(term) {
    this.state.term = term;
  }

  setMentors(mentors) {
    this.state.mentors = mentors;
  }
  clearMentors() {
    this.state.mentors = null;
  }

  setMentor(mentor) {
    this.state.mentor = mentor;
  }
  clearMentor() {
    this.state.mentor = null;
  }

  setGroups(groups) {
    this.state.groups = groups;
  }
  clearGroups() {
    this.state.groups = null;
  }

  setGroup(group) {
    this.state.group = group;
  }
  clearGroup() {
    this.state.group = null;
  }
}

class ChildActions extends Actions {
  async postChild(payload) {
    this.mutations.clearError();
    try {
      const response = await Axios.post(
        base_url + "/register/",
        { ...payload },
        {
          headers: {
            Authorization: "Token " + payload.token,
          },
        }
      );
      if (response.status == 201) {
        this.actions.getChildren(payload.token);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getChildren(token) {
    this.mutations.clearError();
    this.mutations.clearChildren();
    try {
      const response = await Axios.get(base_url, {
        headers: {
          Authorization: "Token " + token,
        },
      });
      if (response.status == 200) {
        this.mutations.setChildren(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getChildProfile(token) {
    this.mutations.clearError();
    this.mutations.clearProfile();
    try {
      const response = await Axios.get(base_url + "/profile/", {
        headers: {
          Authorization: "Token " + token,
        },
      });
      if (response.status == 200) {
        this.mutations.setProfile(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async changeChildName(payload) {
    this.mutations.clearError();
    this.mutations.clearChildName();
    try {
      const response = await Axios.put(
        base_url + "/name/",
        { ...payload },
        {
          headers: {
            Authorization: "Token " + payload.token,
          },
        }
      );
      if (response.status == 200) {
        await this.actions.getChildren(payload.token);
        await this.mutations.setChildName(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getChildTerms(payload) {
    this.mutations.clearError();
    this.mutations.clearTerms();
    try {
      const response = await Axios.post(
        base_url + "/terms/",
        { ...payload },
        {
          headers: {
            Authorization: "Token " + payload.token,
          },
        }
      );
      if (response.status == 200) {
        this.mutations.setTerms(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async makeTerm(payload) {
    this.mutations.clearError();
    try {
      const response = await Axios.post(
        base_url + "/term/",
        { ...payload },
        {
          headers: {
            Authorization: "Token " + payload.token,
          },
        }
      );
      if (response.status == 200) {
        this.actions.getDictionary(payload.token);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async setChildNickAndSlogan(payload) {
    this.mutations.clearError();
    try {
      await Axios.put(
        base_url + "/nickname/",
        { ...payload },
        {
          headers: {
            Authorization: "Token " + payload.token,
          },
        }
      );
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getNotebook(payload) {
    this.mutations.clearError();
    this.mutations.clearTerms();
    try {
      const response = await Axios.post(
        base_url + "/notebook/",
        { ...payload },
        {
          headers: {
            Authorization: "Token " + payload.token,
          },
        }
      );
      if (response.status == 200) {
        this.mutations.setTerms(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getDictionary(payload) {
    this.mutations.clearError();
    this.mutations.clearTerms();
    try {
      const response = await Axios.post(
        base_url + "/dictionary/",
        { ...payload },
        {
          headers: {
            Authorization: "Token " + payload.token,
          },
        }
      );
      if (response.status == 200) {
        this.mutations.setTerms(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async takeTerm(payload) {
    this.mutations.clearError();
    try {
      await Axios.put(
        base_url + "/term/take/" + this.state.term.id + "/",
        { ...payload },
        {
          headers: {
            Authorization: "Token " + payload.token,
          },
        }
      );
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getMentors(token) {
    this.mutations.clearError();
    this.mutations.clearMentors();
    try {
      const response = await Axios.get(base_url + "/mentor/", {
        headers: {
          Authorization: "Token " + token,
        },
      });
      if (response.status == 200) {
        this.mutations.setMentors(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }

  async getMentor(id) {
    this.mutations.clearError();
    this.mutations.clearMentor();
    try {
      const response = await Axios.get(base_url + "/mentor/" + id, {
        headers: {
          Authorization: "Token " + store.getters['AuthStore/token'],
        },
      });
      if (response.status == 200) {
        this.mutations.setMentor(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getGroups(payload) {
    this.mutations.clearError();
    this.mutations.clearGroups();
    try {
      const response = await Axios.post(
        base_url + "/group/",
        { ...payload },
        {
          headers: {
            Authorization: "Token " + payload.token,
          },
        }
      );
      if (response.status == 200) {
        this.mutations.setGroups(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getGroup(id) {
    this.mutations.clearError();
    this.mutations.clearGroup();
    try {
      const response = await Axios.get(
        base_url + "/group/" + id,
        {
          headers: {
            Authorization: "Token " + store.getters['AuthStore/token'],
          },
        }
      );
      if (response.status == 200) {
        this.mutations.setGroup(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
}

const child = new Module({
  state: ChildState,
  getters: ChildGetters,
  mutations: ChildMutations,
  actions: ChildActions,
});
export const ChildMapper = createMapper(child);
export default child;
