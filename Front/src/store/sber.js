import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
// import Axios from "axios";
class SberState {
  error = null;
  title = "Пополнение баланса";
  value = 0;
  type = "Fill";
}

class SberGetters extends Getters {}

class SberMutations extends Mutations {
  setError(error) {
    this.state.error = error;
  }
  setValue(value) {
    this.state.value = value;
  }
  setTitle(title) {
    this.state.title = title;
  }
  setType(type) {
    this.state.type = type;
  }
}

class SberActions extends Actions {}

const sber = new Module({
  getters: SberGetters,
  state: SberState,
  mutations: SberMutations,
  actions: SberActions,
});
export const SberMapper = createMapper(sber);
export default sber;
