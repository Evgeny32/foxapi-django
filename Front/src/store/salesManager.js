import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import { store } from "@/store";
import axios from "axios";

const baseUrl = process.env.VUE_APP_API_URL + "/api/v1/salesmanager/";
const mediaUrl = process.env.VUE_APP_API_URL;
axios.defaults.headers.post["Content-Type"] = "application/json";

class SalesManagerState {
  salesManagerId = null;
  manager = {};
  franchisees = [];
  franchisee = {};
  agents = [];
  agent = {};
  packages = [];
  package = {};
  agentCount = 0;
  franchiseeCount = 0;
  error = null;
  token = null;
}

class SalesManagerGetters extends Getters {}

class SalesManagerMutations extends Mutations {
  setSalesManagerId(id) {
    this.state.salesManagerId = id;
  }
  setPackages(packages) {
    packages = packages.map((pack) => {
      if (pack.franchisee) {
        Object.entries(pack.franchisee).forEach((item) => {
          if (item && item[0].indexOf("PDF") > 0 && item[1])
            pack.franchisee[item[0]] = mediaUrl + item[1];
        });
      }
      if (pack.agent) {
        Object.entries(pack.agent).forEach((item) => {
          if (item && item[0].indexOf("PDF") > 0 && item[1])
            pack.agent[item[0]] = mediaUrl + item[1];
        });
      }
      return pack;
    });
    this.state.packages = packages;
  }

  clearPackages() {
    this.state.packages = [];
  }

  clearManagerProfile() {
    this.state.manager = [];
  }

  setManagerProfile(manager) {
    this.state.manager = manager;
  }

  setPackage(pack) {
    this.state.package = pack;
  }

  clearPackage() {
    this.state.package = {};
  }
  setAgentCount(count) {
    this.state.agentCount = count;
  }
  clearAgentCount() {
    this.state.agentCount = 0;
  }
  setFranchiseeCount(count) {
    this.state.franchiseeCount = count;
  }
  clearFranchiseeCount() {
    this.state.franchiseeCount = 0;
  }

  setFranchisees(franchisees) {
    this.state.franchisees = franchisees;
  }

  clearFranchisees() {
    this.state.franchisees = [];
  }

  setFranchisee(franchisee) {
    this.state.franchisee = franchisee;
  }

  clearFranchisee() {
    this.state.franchisee = {};
  }

  setAgents(agents) {
    this.state.agents = agents;
  }

  clearAgents() {
    this.state.agents = [];
  }

  setAgent(agent) {
    this.state.agent = agent;
  }

  clearAgent() {
    this.state.agent = {};
  }

  setError(error) {
    this.state.error = error;
  }

  clearError() {
    this.state.error = null;
  }

  setToken(token) {
    this.state.token = token;
  }

  clearToken() {
    this.state.token = null;
  }
}

class SalesManagerActions extends Actions {
  //CRM

  async getPackages(id) {
    // * Получение списка франшиз
    this.mutations.clearError();
    this.mutations.clearPackages();
    await axios
      .get(`${baseUrl}packages/${id.toString()}/`, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setPackages(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getPackagesById(id) {
    // * Получение списка франшиз
    this.mutations.clearError();
    this.mutations.clearPackages();
    await axios
      .get(`${baseUrl}${id}/packages/`, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setPackages(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchPackage({ id, data }) {
    // * Изменение франшизы
    this.mutations.clearError();
    await axios
      .patch(
        `${baseUrl}package/${id.toString()}/`,
        { ...data },
        {
          headers: {
            Authorization: "Token " + store.getters["AuthStore/token"],
          },
        }
      )
      .then(() => this.actions.getPackages())
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getAgentFranchiseeCount() {
    this.mutations.clearError();
    this.mutations.clearAgentCount();
    this.mutations.clearFranchiseeCount();
    await axios
      .get(`${baseUrl}all/`, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => {
        this.mutations.setAgentCount(response.data["agents"]);
        this.mutations.setFranchiseeCount(response.data["franchisee"]);
      })
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getAgentFranchiseeCountById(id) {
    this.mutations.clearError();
    this.mutations.clearAgentCount();
    this.mutations.clearFranchiseeCount();
    await axios
      .get(`${baseUrl}${id}/all/`, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => {
        this.mutations.setAgentCount(response.data["agents"]);
        this.mutations.setFranchiseeCount(response.data["franchisee"]);
      })
      .catch((error) => this.mutations.setError(error.response.data));
  }

  //CRM

  //FRANCHISEE

  async getFranchiseeList(id) {
    this.mutations.clearError();
    this.mutations.clearFranchisees();
    await axios
      .get(baseUrl + `franchisees/${id.toString()}/`, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setFranchisees(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getFranchiseeListById(id) {
    this.mutations.clearError();
    this.mutations.clearFranchisees();
    await axios
      .get(baseUrl + id + "/franchisees/", {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setFranchisees(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getFranchiseeDetails(id) {
    // * Получение подробнной информации об одном ребенке
    this.mutations.clearError();
    this.mutations.clearFranchisee();
    await axios
      .get(
        baseUrl + "franchisee/" + id.toString(),

        {
          headers: {
            Authorization: "Token " + store.getters["AuthStore/token"],
          },
        }
      )
      .then((response) => this.mutations.setFranchisee(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async putFranchiseeProfile(id, data) {
    this.mutations.clearError();
    await axios
      .put(baseUrl + "franchisee/" + id.toString(), data, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .catch((error) => this.mutations.setError(error.response.data));
  }

  //FRANCHISEE

  //AGENTS

  async getAgentsList(id) {
    this.mutations.clearError();
    this.mutations.clearAgents();
    await axios
      .get(baseUrl + `agents/${id.toString()}/`, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setAgents(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getAgentsListById(id) {
    this.mutations.clearError();
    this.mutations.clearAgents();
    await axios
      .get(baseUrl + id + "/agents/", {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
        },
      })
      .then((response) => this.mutations.setAgents(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getAgentDetails(id) {
    // * Получение подробнной информации об одном ребенке
    this.mutations.clearError();
    this.mutations.clearAgent();
    await axios
      .get(
        baseUrl + "agent/" + id.toString(),

        {
          headers: {
            Authorization: "Token " + store.getters["AuthStore/token"],
          },
        }
      )
      .then((response) => this.mutations.setAgent(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchAgentProfile({ id, data }) {
    this.mutations.clearError();
    await axios
      .patch(`${baseUrl}agent/${id.toString()}/`, data, {
        headers: {
          Authorization: "Token " + store.getters["AuthStore/token"],
          "Content-Type": "multipart/form-data",
        },
      })
      .then((response) => this.mutations.setAgent(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  //AGENTS

  async getOtherManagerProfile(id) {
    this.mutations.clearError();
    this.mutations.clearManagerProfile();
    await axios
      .get(baseUrl + id)
      .then((response) => this.mutations.setManagerProfile(response.data))
      .catch(() => this.mutations.setError(true));
  }
}

const salesManager = new Module({
  getters: SalesManagerGetters,
  state: SalesManagerState,
  mutations: SalesManagerMutations,
  actions: SalesManagerActions,
});
export const SalesManagerMapper = createMapper(salesManager);
export default salesManager;
