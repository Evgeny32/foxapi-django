import moment from 'moment';
moment.locale('ru');

export default {
    methods: {
        getPackageStatus(number) {
            switch (number) {
                case 0:
                    return "Новая";
                case 1:
                    return "Повторная";
                case 2:
                    return "В работе";
                case 3:
                    return "Документы отправлены";
                case 4:
                    return "Отклонена";
                case 5:
                    return "Счет оплачен";
                case 6:
                    return "Заблокирована";
                default:
                    return "Неизвестная заявка";
            }
        },
        getPackageType(number) {
            switch (number) {
                case 0:
                    return "Детский сад";
                case 1:
                    return "Начальная школа";
                case 2:
                    return "Школа";
                default:
                    return "Неизвестный тип франшизы";
            }
        },
        getPackageAge(number) {
            switch (number) {
                case 0:
                    return "5-9";
                case 1:
                    return "9-12";
                case 2:
                    return "13-16";
                default:
                    return "Возраст неизвестен";
            }
        },
        getPackageFormat(number) {
            switch (number) {
                case 0:
                    return "Offline";
                case 1:
                    return "Online";
                default:
                    return "Формат неизвестен";
            }
        },
        formatPackages(packages) {
            let items = packages;
            items =  items.reverse().map((item, i) => {
                if (item.franchisee) {
                    return {
                        number: i + 1,
                        id: item.id,
                        purchaserId: item.franchisee.user_id.id,
                        agentname: item.franchisee.name,
                        status: item.status,
                        type_agent: "Франчайзи",
                        region: item.franchisee.urAdress,
                        age: this.getPackageAge(item.name),
                        package: this.getPackageType(item.name),
                        date: moment(item.date),
                        rotate: false,
                        inn: item.franchisee.inn,
                        organisation: item.franchisee.name,
                        account: item.franchisee.RS,
                        bank: item.franchisee.bank,
                        bankBIK: item.franchisee.BIK,
                        corrAccount: item.franchisee.CS,
    
                        phone: item.franchisee.user_id.phone,
                        email: item.franchisee.user_id.email,
                        adress: item.franchisee.adress,
                        documents: Object.entries(item.franchisee)
                            .filter((i) => i[0].indexOf("PDF") > 0 && i[1])
                            .map((i) => i[1]),
                    };
                }
                if (item.agent) {
                    return {
                        number: i + 1,
                        id: item.id,
                        purchaserId: item.agent.user_id.id,
                        agentname: `${item.agent.user_id.second_name} ${item.agent.user_id.name} ${item.agent.user_id.last_name}`,
                        status: item.status,
                        type_agent: "Агент",
                        region: item.agent.adress,
                        age: this.getPackageAge(item.name),
                        package: "-",
                        date: moment(item.date),
                        rotate: false,
                        inn: item.agent.inn,
                        organisation: "Не организация",
                        account: "Нет расчетного счета",
                        bank: "Нет банка",
                        bankBIK: "Нет БИК",
                        corrAccount: "Нет КС",
    
                        phone: item.agent.user_id.phone,
                        email: item.agent.user_id.email,
                        adress: item.agent.adress,
                        documents: Object.entries(item.agent)
                            .filter((i) => i[0].indexOf("PDF") > 0 && i[1])
                            .map((i) => i[1]),
                    };
                }
                return {
                    number: i + 1,
                    id: item.id,
                    purchaserId: "",
                    agentname: "",
                    status: "",
                    type_agent: "",
                    region: "",
                    age: "",
                    package: "",
                    date: "",
                    rotate: false,
                    inn: "",
                    organisation: "",
                    account: "",
                    bank: "",
                    bankBIK: "",
                    corrAccount: "",
    
                    phone: "",
                    email: "",
                    adress: "",
                };
            });
            return items
        }
    },
    
}