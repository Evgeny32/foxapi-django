import Vue from "vue";
import App from "@/App.vue";
import Vuelidate from "vuelidate";

//  Скачанные компоненты для быстрой верстки и красоты
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import VueTheMask from "vue-the-mask";
import "@/assets/fonts/Montserrat/stylesheet.css";
import vueScrollBehavior from "vue-scroll-behavior";
import VueTeleport from "@desislavsd/vue-teleport";
import toastMixin from "@/scripts/toastMixin";
import VueApexCharts from "vue-apexcharts";

// СТОР
import { store } from "@/store";
// РОУТЕР
import router from "@/router";
import ui from "@/components/ui";
//  Используемые компоненты
var VueTouch = require("vue-touch");

Vue.use(VueTouch, { name: "v-touch" });
Vue.use(VueTheMask);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Vuelidate);
Vue.use(VueTeleport);
Vue.use(vueScrollBehavior, {
  router: router, // The router instance
  ignore: [/\/.*/],
  delay: 0, // Delay by a number of milliseconds
});
Vue.component("apexchart", VueApexCharts);

Vue.config.productionTip = false;
Vue.mixin(toastMixin);
ui.forEach((i) => {
  Vue.component(i.name, i);
});
//  Инициализация всего для работы!
new Vue({
  router,
  store: store,
  render: (h) => h(App),
}).$mount("#app");
