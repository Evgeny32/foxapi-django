import axios from "axios";
import { store } from "@/store";
import { TransMapper } from "@/store/trans";

export default {
  computed: {
    ...TransMapper.mapState(["childFillId"]),
  },
  methods: {
    ...TransMapper.mapMutations(["setTransaction"]),
    // @transaction - {user, name, value} - Данные транзакции для записи в системе
    // @childId - ID ребенка, которому начисляются фоксики
    async createFillTransaction(transaction) {
      let createdTransaction = null;
      try {
        let response = await axios.post(
          `${process.env.VUE_APP_API_URL}/api/v1/transactions/`,
          transaction,
          {
            headers: {
              Authorization: "Token " + store.getters["AuthStore/token"],
            },
          }
        );
        createdTransaction = response.data;
        this.setTransaciont(createdTransaction);
      } catch (error) {
        return this.errorToast(error.response.data);
      }
      try {
        let response = await axios.post(
          `http://localhost:8080/payment/rest/register.do?amount=${createdTransaction.value *
            100}&currency=643&language=ru&orderNumber=${
            createdTransaction.id
          }&returnUrl=http://localhost:8080/parent/children/mychild?fillTransaction=${
            createdTransaction.id
          }&password=T5024213977&userName=T5024213977-api`
        );
        let formUrl = response.data.formUrl;
        window.location.href = formUrl;
      } catch (error) {
        return this.errorToast(error.response.data);
      }
    },
  },
  async mounted() {
    if (this.$route.query.fillTransaction) {
      try {
        await axios.patch(
          `${process.env.VUE_APP_API_URL}/api/v1/transactions/fill/${this.$route.query.fillTransaction}/`,
          {
            paid: true,
            order_id: this.$route.query.orderId,
            child: this.childFillId,
          },
          {
            headers: {
              Authorization: "Token " + store.getters["AuthStore/token"],
            },
          }
        );
        this.toast(
          "Успешно",
          "Платежные средства успешно внесены. Счет пополнен.",
          "success"
        );
      } catch (error) {
        this.errorToast(error.response.data);
      }
    }
  },
};
